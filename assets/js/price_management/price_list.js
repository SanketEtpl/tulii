function deletePriceList($this) // delete record of price list
{
  if($($this).attr("data-row-id")){
    var key=$($this).attr("data-row-id");
             
   var temp = "delPrice('"+key+"');"; 
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $(".inner_body").html('<b>Are you sure you want to delete this price?</b>');
    $(".inner_footer").html('<button type="button" onclick="'+temp+'"  class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');   

     
  } else {     
    $("#LoadingDiv").css({"display":"none"}); // loader
    toastr.error("Something went wrong!!!.");    
  }
}   


function delPrice(key)
{  if(key){
      
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"price_management/Price_list/deletePriceList",
          beforeSend: function() {
             //$('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
          data: {"key": key},
        }).success(function (json) {
          if(json.status == "success"){           
            toastr.success(json.msg);           
           }else{
            toastr.error(json.msg);             
          }
      });  
     
  } else {     
    toastr.error("Something went wrong!!!.");    
  }
  $('.commanPopup').modal('hide');
  $("#table").dataTable().fnDraw();
} 

function editPriceData($this) // edit price list data
{
  $('#priceListForm')[0].reset(); //reset of price list value 
  $(".price_per_hour").remove();
  $(".categories").remove();
  $(".price_per_km").remove();
  $(".subCategory").remove();
   
  $( "h3" ).html( "<h3 class='modal-title'><b>Edit price</b></h3>" );
  if($($this).attr("data-row-id")){ // get price list id     
   // $("#LoadingDiv").css({"display":"block"});
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"price_management/Price_list/viewPriceInfo",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        if(json.status == "success"){            
          $("#user_key").val($($this).attr("data-row-id"));
          $("#categories").val(json.priceData.c_id);
          //$("#subCategory").val(json.priceData.sc_id).attr('selected','selected');
          $("#subCategory").html(json.subCategory);
          $("#categories").prop("disabled", true);
          $("#subCategory").prop("disabled", true);
          if(json.priceData.c_id == 1 )
          {
            $('.subCategoryHide').css({"display":"block"});
          }
          else 
          {
            $('.subCategoryHide').css({"display":"none"});
          }

          if(json.priceData.c_id == 1 || json.priceData.c_id == 3 || json.priceData.c_id == 4)
          {
            $("#pricePerHour").css({"display":"block"});  
            $("#pricePerKm").css({"display":"none"}); 
            $("#price_per_hour").val(json.priceData.rp_cost);    
          } else {
            $("#pricePerHour").css({"display":"none"});  
            $("#pricePerKm").css({"display":"block"}); 
            $("#price_per_km").val(json.priceData.rp_cost);   
          }
          $('#priceListModal').modal('show', {backdrop: 'static'});        
        }
        else{
            toastr.error(json.msg);
        }        
         $("#LoadingDiv").css({"display":"none"}); 
      });
   
  } else {
    $("#user_key").val('');
    $('#priceListModal').modal('show', {backdrop: 'static'});
  }
}

$(document).ready(function(){
  $('#categories').on('change',function(){
      var categoryID = $(this).val();         
        if(categoryID){
            $.ajax({
                type:'POST',
                url:baseURL+"price_management/Price_list/get_category",
                data:{"categoryID":categoryID},
                dataType:'json',
                success:function(json){                                                     
                    if(json.status == "true") {
                      $('.subCategoryHide').css({"display":"block"});
                      $('#subCategory').html(json.subCategory);                                           
                    } else {
                      $('.subCategoryHide').css({"display":"none"});
                      $('#subCategory').html(json.subCategory); 
                    }                                         
                }
            }); 
        }else{            
            $('#subCategory').html('<option value="">-- Select category --</option>'); 
        }
    });
 $('#priceListForm')[0].reset();
  $('#btnPriceList').click(function(){ // save record of price list  
    var categories = $("#categories").val();
    var price_per_hour = $("#price_per_hour").val();
    var price_per_km = $("#price_per_km").val();
    var subCategory = $("#subCategory").val();
    var user_key = $("#user_key").val();
    var subCategory = $("#subCategory").val();
    var flag = 0;
    if(categories == 1 || categories == 3) {
        if(price_per_hour == ''){
        $(".price_per_hour").remove();  
        $("#price_per_hour").parent().append("<div class='price_per_hour' style='color:red;'>Price per hour field is required.</div>");
        flag = 1;
      } else if(price_per_hour == 0 ) {
        $(".price_per_hour").remove();  
        $("#price_per_hour").parent().append("<div class='price_per_hour' style='color:red;'>Price should be greater than zero.</div>");
        flag = 1;
      } else if(price_per_hour.length >= 4){
        $(".price_per_hour").remove();  
        $("#price_per_hour").parent().append("<div class='price_per_hour' style='color:red;'>Price has been exceeded.</div>");
        flag = 1;
      } else {
        $(".price_per_hour").remove();        
      }
    }
    
    if(categories == 2) {
      if(price_per_km == ''){
        $(".price_per_km").remove();  
        $("#price_per_km").parent().append("<div class='price_per_km' style='color:red;'>Price per km field is required.</div>");
        flag = 1;
      } else if(price_per_km == 0 ) {
        $(".price_per_km").remove();  
        $("#price_per_km").parent().append("<div class='price_per_km' style='color:red;'>Price should be greater than zero.</div>");
        flag = 1;
      } else if(price_per_km.length >= 4 ) {
        $(".price_per_km").remove();  
        $("#price_per_km").parent().append("<div class='price_per_km' style='color:red;'>Price has been exceeded.</div>");
        flag = 1;
      } else {
        $(".price_per_km").remove();        
      }
    }  

    if(categories == ''){
      $(".categories").remove();  
      $("#categories").parent().append("<div class='categories' style='color:red;'>Please select category.</div>");
      flag = 1;
    }else{
      $(".categories").remove();          
    }
    if(categories == 1 || categories == 4) {
    if(subCategory == ''){
      $(".subCategory").remove();  
      $("#subCategory").parent().append("<div class='subCategory' style='color:red;'>Please select sub category.</div>");
      flag = 1;
    }else{
      $(".subCategory").remove();          
    }
  }
    
    if(flag == 1)
      return false;    
    else
    {      
      //$("#LoadingDiv").css({"display":"block"});
      $.ajax({
        type:"POST",
        url:baseURL+"price_management/Price_list/addPrice",            
        data:{"editData":user_key,"categories":categories,"subCategory":subCategory,"price_per_hour":price_per_hour,"price_per_km":price_per_km},
        dataType:"json",
        async:false,
        success:function(response){                     
          if(response.status == "success")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#priceListModal").modal('hide');
            $("#table").dataTable().fnDraw();
            toastr.success(response.message);                                      
          }
          else if(response.status == "warning")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#priceListModal").modal('show');
            toastr.warning(response.message); 
          }
          else
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#priceListModal").modal('show');
            toastr.error(response.message);                                       
          }
        }
      });    
    }   
  });  

  $('#addPopUp').on("click",function(){
    $("#categories").prop("disabled", false);
    $("#subCategory").prop("disabled", false);
    $( "h3" ).html( "<h3 class='modal-title'><b>Add price</b></h3>" );
    $(".price_per_hour").remove();
    $(".categories").remove();
    $(".price_per_km").remove();
    $(".subCategory").remove();
    $("#user_key").val("");
    $("#priceListForm")[0].reset();
    $('.subCategoryHide').css({"display":"none"});
    $("#priceListModal").modal('show');    
  });

  $("#categories").on("change",function(){
    if($(this).val() == 2) {
      $("#pricePerHour").css({"display":"none"});  
      $("#pricePerKm").css({"display":"block"});  
    } else {
      $("#pricePerHour").css({"display":"block"});  
      $("#pricePerKm").css({"display":"none"});  
    }
  });
});