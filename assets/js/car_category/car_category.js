function deleteCatList($this) // delete record of price list
{
  if($($this).attr("data-row-id")){
    var key=$($this).attr("data-row-id");
             
   var temp = "delCat('"+key+"');"; 
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $(".inner_body").html('<b>Are you sure you want to delete this category?</b>');
    $(".inner_footer").html('<button type="button" onclick="'+temp+'"  class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');   

     
  } else {     
    $("#LoadingDiv").css({"display":"none"}); // loader
    toastr.error("Something went wrong!!!.");    
  }
}   


function delCat(key)
{  if(key){
      
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"car_management/Car/deleteCatList",
          beforeSend: function() {
             //$('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
          data: {"key": key},
        }).success(function (json) {
          if(json.status == "success"){           
            toastr.success(json.msg);           
           }else{
            toastr.error(json.msg);             
          }
      });  
     
  } else {     
    toastr.error("Something went wrong!!!.");    
  }
  $('.commanPopup').modal('hide');
  $("#table").dataTable().fnDraw();
} 

function editCatData($this) // edit price list data
{
  $('#CategoryForm')[0].reset(); //reset of price list value 
  $(".category").remove();
  $(".price_per_km").remove();
  //$(".car_capacity").remove();
   
  $( "h3" ).html( "<h3 class='modal-title'><b>Edit car category</b></h3>" );
  if($($this).attr("data-row-id")){ // get price list id 
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"car_management/Car/viewCatInfo",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        if(json.status == "success"){            
          $("#user_key").val($($this).attr("data-row-id"));
          $("#car_category").val(json.catData.cat_name);
          //$("#subCategory").val(json.priceData.sc_id).attr('selected','selected');
          $("#car_category_rate").val(json.catData.cat_rate);
          //$("#car_capacity").val(json.catData.capacity);
          // if(json.priceData.c_id == 1 || json.priceData.c_id == 3)
          // {
          //   $("#pricePerHour").css({"display":"block"});  
          //   $("#pricePerKm").css({"display":"none"}); 
          //   $("#price_per_hour").val(json.priceData.rp_cost);    
          // } else {
          //   $("#pricePerHour").css({"display":"none"});  
          //   $("#pricePerKm").css({"display":"block"}); 
          //   $("#price_per_km").val(json.priceData.rp_cost);   
          // }
          $('#categoryModal').modal('show', {backdrop: 'static'});        
        }
        else{
            toastr.error(json.msg);
        }        
         $("#LoadingDiv").css({"display":"none"}); 
      });
   
  } else {
    $("#user_key").val('');
    $('#categoryModal').modal('show', {backdrop: 'static'});
  }
}

$(document).ready(function(){
  $('#categories').on('change',function(){
      var categoryID = $(this).val();         
        if(categoryID){
            $.ajax({
                type:'POST',
                url:baseURL+"price_management/Price_list/get_category",
                data:{"categoryID":categoryID},
                dataType:'json',
                success:function(json){                                                     
                    $('#subCategory').html(json.subCategory);                                         
                }
            }); 
        }else{            
            $('#subCategory').html('<option value="">Select category</option>'); 
        }
    });
 $('#CategoryForm')[0].reset();
  $('#btnCategoryList').click(function(){ // save record of price list  
    var categories = $("#car_category").val();
    var rate = $("#car_category_rate").val();
    //var car_capacity = $("#car_capacity").val();
    var user_key = $("#user_key").val();
    var flag = 0;
    // if(categories == 1 || categories == 3) {
        if(categories == ''){
        $(".category").remove();  
        $("#car_category").parent().append("<div class='category' style='color:red;'>Category field is required.</div>");
        flag = 1;
      }

      if(rate == ''){
        $(".price_per_km").remove();  
        $("#car_category_rate").parent().append("<div class='price_per_km' style='color:red;'>Rate per km field is required.</div>");
        flag = 1;
      } else if(rate == 0 ) {
        $(".price_per_km").remove();  
        $("#car_category_rate").parent().append("<div class='price_per_km' style='color:red;'>Rate should be greater than zero.</div>");
        flag = 1;
      } else if(rate.length > 4 ) {
        $(".price_per_km").remove();  
        $("#car_category_rate").parent().append("<div class='price_per_km' style='color:red;'>Rate has been exceeded.</div>");
        flag = 1;
      } else {
        $(".price_per_km").remove();        
      }
    // }  

    /*if(car_capacity == ''){
        $(".car_capacity").remove();  
        $("#car_capacity").parent().append("<div class='car_capacity' style='color:red;'>Capacity field is required.</div>");
        flag = 1;
      } else if(car_capacity == 0 ) {
        $(".car_capacity").remove();  
        $("#car_category_rate").parent().append("<div class='car_capacity' style='color:red;'>Capacity should be greater than zero.</div>");
        flag = 1;
      } else if(car_capacity.length > 4 ) {
        $(".car_capacity").remove();  
        $("#car_capacity").parent().append("<div class='car_capacity' style='color:red;'>Capacity has been exceeded.</div>");
        flag = 1;
      } else {
        $(".car_capacity").remove();        
      }*/
    
    if(flag == 1)
      return false;    
    else
    {      
      //$("#LoadingDiv").css({"display":"block"});
      $.ajax({
        type:"POST",
        url:baseURL+"car_management/Car/addCarCategory",            
        data:{"editData":user_key,"categories":categories,"rate":rate},
        dataType:"json",
        async:false,
        success:function(response){                     
          if(response.status == "success")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#categoryModal").modal('hide');
            $("#table").dataTable().fnDraw();
            toastr.success(response.message);                                      
          }
          else if(response.status == "warning")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#categoryModal").modal('show');
            toastr.warning(response.message); 
          }
          else
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#categoryModal").modal('show');
            toastr.error(response.message);                                       
          }
        }
      });    
    }   
  });  

  $('#addCategory').on("click",function(){
    // $("#categories").prop("disabled", false);
    // $("#subCategory").prop("disabled", false);
    $( "h3" ).html( "<h3 class='modal-title'><b>Add car category</b></h3>" );
    // $(".price_per_hour").remove();
    // $(".categories").remove();
    // $(".price_per_km").remove();
    // $(".subCategory").remove();
    // $("#user_key").val("");
    $("#CategoryForm")[0].reset();
    $("#categoryModal").modal('show');    
  });

  // $("#categories").on("change",function(){
  //   if($(this).val() == 2) {
  //     $("#pricePerHour").css({"display":"none"});  
  //     $("#pricePerKm").css({"display":"block"});  
  //   } else {
  //     $("#pricePerHour").css({"display":"block"});  
  //     $("#pricePerKm").css({"display":"none"});  
  //   }
  // });


  $('#car_capacity').keypress(function(e) {
            var a = [46];
            var k = e.which;
            console.log( k );
                a.push(8);
                a.push(0);

            for (i = 48; i < 58; i++)
                a.push(i);

            if (!($.inArray(k,a)>=0))
                e.preventDefault();
        });

   $('#car_category_rate').keypress(function(e) {
            var a = [46];
            var k = e.which;
            console.log( k );
                a.push(8);
                a.push(0);

            for (i = 48; i < 58; i++)
                a.push(i);

            if (!($.inArray(k,a)>=0))
                e.preventDefault();
        });

});