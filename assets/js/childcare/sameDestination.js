function loadSameDestinationList()
{   
  var table = $('#care-grid').DataTable();
  $('#care-grid').empty();
  table.destroy();
  $('#care-grid').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order. 
    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": baseURL+"care_driver_management/Same_destination/sameDestinationList",
      beforeSend: function() {
        var search = $("input[type=search]").val();
        if(search=="")
         // $('#LoadingDiv').show();     
        $("input[type=search]").on("keyup",function() {
          if($("#clear").length == 0) {
             if($(this).val() != "") {
              $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
            } 
          }
          if($(this).val() == "")  
          $("#clear").remove();      
        });
        $("input[type=search]").keydown(function(event) {
          k = event.which;
          if (k === 32 && !this.value.length)
            event.preventDefault();
        });     
      },
      complete: function(){
      $('#LoadingDiv').hide();
    },
   // data : { category : category },
      "type": "POST"
    },
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
      "targets": [0,5,6], //first column / numbering column
      "orderable": false, //set not orderable
    },
    ],
  }); 
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
      if($(this).val() != ""){
        $("#care-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  }); 
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  var service_type = $('#service_type').val();
  loadSameDestinationList();
}

jQuery(document).ready(function(){  
  jQuery('#LoadingDiv').show();
  jQuery(document).on("click", ".viewSameDestination", function(){
    var userId = $(this).data("userid"),
    hitURL = baseURL + "care_driver_management/Same_destination/viewSameDestinationDetails";
    $.ajax({
      type : "POST",
      async: false,
      dataType : "json",
      url : hitURL,
      beforeSend: function() {
        //$('#LoadingDiv').show();
      },
      complete: function() {
        $('#LoadingDiv').hide();
      },
      data : { userId : userId }
    }).success(function (json) {
        //console.log(json);
      $("#ModalLabel").html('Same destination details<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $(".inner_body").html(json.data);
      $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
      $('.commanPopup').modal('show');  
    });      
  }); 

  jQuery(document).on("click", ".deleteSameDestination", function(){
    var userid = $(this).data("userid");
    var category = $(this).data("category");
    var temp = "delSameDestination('"+userid+"','"+ category +"');"; 
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure you want to delete this Care?</b>');
    $(".inner_footer").html('<button type="button" onclick="'+temp+'"  class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');
  });

  jQuery(document).on("click", ".viewrideAndCarePic", function(){
    //$('#LoadingDiv').show();
    //setTimeout(function(){  $('#LoadingDiv').hide(); }, 1000);
    var baseurl = $(this).data("baseurl");
    var picname = $(this).data("picname");
    var error_msg = $(this).data("error");
    var headerMsg = $(this).data("headermsg");
    var imgPath = $(this).data("imgpath");
    if(picname) {
      var array = picname.split("|");
      // var cnt =1 ;
      var temp= '';
      $("#modalHeader").html(headerMsg+'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $("#modalBody").html('<div id="image-gallery"><div class="image-container"></div><img src="'+imgPath+'assets/images/left.svg" class="prev"/><img src="'+imgPath+'assets/images/right.svg"  class="next"/><div class="footer-info"><span class="current"></span>/<span class="total"></span></div></div> ');
      $("#modalFooter").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
      $('.photoGallery').modal('show');      
      a = new Array();
      for (var i in array) {
        var http = new XMLHttpRequest();
        http.open('HEAD', baseurl+array[i], false);
        http.send();
        if(http.status != 404) {
          a[i]=baseurl+array[i]; 
        } else {
          a[i]=imgPath+'assets/images/not_found.png';   
        }     
      }

      var images = Array();
      for(var j=0;j<a.length;j++) {
        images.push({small:a[j],big:a[j]}); 
      }         
      var curImageIdx = 1,
      total = images.length;
      var wrapper = $('#image-gallery'),
      curSpan = wrapper.find('.current');
      var viewer = ImageViewer(wrapper.find('.image-container')); 
      //display total count
      wrapper.find('.total').html(total);
 
      function showImage() {
        var imgObj = images[curImageIdx - 1];
        viewer.load(imgObj.small, imgObj.big);
        curSpan.html(curImageIdx);
      }
 
      wrapper.find('.next').click(function(){
        curImageIdx++;
        if(curImageIdx > total) curImageIdx = 1;
        showImage();
      });
 
      wrapper.find('.prev').click(function(){
        curImageIdx--;
        if(curImageIdx < 0) curImageIdx = total;
        showImage();
      });
 
      //initially show image
      showImage();  
    } else {
      toastr.error(error_msg); 
    }
  });
});

function sameDestiantion_popup(userId,user_status,active_id,inActive_id)
{ 
  var temp = "sameDestiantionStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
  $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
  $(".inner_body").html('<b>Are you sure you want to change status?</b>');
  $(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
  $('.commanPopup').modal('show');
}

function sameDestiantionStatus(userId,user_status,active_id,inActive_id)
{
  $('.commanPopup').modal('hide');
  jQuery.ajax({
    type : "POST",
    dataType : "json",
    url : hitURL = baseURL + "care_driver_management/Same_destination/sameDestiantionStatus",
    beforeSend: function() {
      // $('#LoadingDiv').show();
      },
      complete: function(){
      //$('#LoadingDiv').hide();
      },
      data : { userId : userId, user_status: user_status} 
    }).done(function(data){
        //console.log(data);
    if(user_status == 1) { 
      var str_success="Same destination has been active successfully.";
      var str_error="Same destination activation failed.";

      $("#"+active_id).removeClass("btn-active-disable");
      $("#"+active_id).addClass("btn-active-enable");

      $("#"+inActive_id).removeClass("btn-inactive-enable");
      $("#"+inActive_id).addClass("btn-inactive-disable");

      $("#"+active_id).prop('disabled', true);
      $("#"+inActive_id).prop('disabled', false);
    } else {
      $("#"+active_id).removeClass("btn-active-enable");
      $("#"+active_id).addClass("btn-active-disable");

      $("#"+inActive_id).removeClass("btn-inactive-disable ");
      $("#"+inActive_id).addClass("btn-inactive-enable");

      var str_success="Same destination has been inactive successfully.";
      var str_error="Same destination inactivation failed.";

      $("#"+active_id).prop('disabled', false);
      $("#"+inActive_id).prop('disabled', true);
    }
    if(data.status == true) {
      toastr.success(str_success);
    } else if(data.status == false) { 
      toastr.error(str_error); 
    } else { 
      alert("Access denied..!");
    }
  }); 
}


function delSameDestination(userid,category)
{
  $('#LoadingDiv').show();
  if(userid) {
    $('#LoadingDiv').show();
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"care_driver_management/Same_destination/deleteSameDestination",
      beforeSend: function() {             
      },
      complete: function(){
        $('#LoadingDiv').hide();
      },
      data: {"key":userid},
    }).success(function (json) {
    if(json.status == "success") {
      $(this).parents("tr:first").remove();
      toastr.success(json.msg);
      $("#table").dataTable().fnDraw();      
    } else {
      toastr.error(json.msg);
    }
  });
  $('#LoadingDiv').hide();
  } else {     
    toastr.error("Something went wrong!!!.");    
  }
  $('.commanPopup').modal('hide');
  loadSameDestinationList();
} 