function checkEmail()
{
  $(".email1").remove();
  var user_key = $("#user_key").val();
  var result = 0;
  if(user_key !="")
    return result;
  else
  {
    var email = $("#email").val();  
    $.ajax({
      type:"POST",
      url:baseURL+"childcare_provider/Childcare/check_email_availability",
      data:{'email':email,'user_key':user_key},
      dataType:"json",
      async:false,          
      success:function(response){        
        if(response.status == true)
        {
          $(".email1").remove(); 
          $("#email").parent().append("<div class='email1' style='color:red;'>"+ response.message +"</div>");
          $(".email2").remove();
          result = 1;
        }
        else
        {       
          $(".email1").remove();       
        }     
      }
    });  
  }
  return result;
}

function userStatus(userId,user_status,active_id,inActive_id)
{  
  if(confirm('Are you sure you want to change status?'))
  {
    $.ajax({
      type : "POST",
      dataType : "json",
      url : baseURL + "childcare_provider/Childcare/childcareStatusChange",
      data : { userId : userId, user_status: user_status} 
      }).success(function(data){   
      if(user_status == 1) { 
        
        var str_success="Childcare provider has been actived successfully";
        var str_error="Childcare provider has been activation failed";

        $(active_id).removeClass("btn-active-disable");
        $(active_id).addClass("btn-active-enable");

        $(inActive_id).removeClass("btn-inactive-enable");
        $(inActive_id).addClass("btn-inactive-disable");

        $(active_id).prop('disabled', true);
        $(inActive_id).prop('disabled', false);
        } else {
          $(active_id).removeClass("btn-active-enable");
          $(active_id).addClass("btn-active-disable");

          $(inActive_id).removeClass("btn-inactive-disable ");
          $(inActive_id).addClass("btn-inactive-enable");

          var str_success="Childcare provider has been inactived successfully";
          var str_error="Childcare provider has been inactivation failed";

          $(active_id).prop('disabled', false);
          $(inActive_id).prop('disabled', true);
        }
      if(data.status = true) {
        toastr.success(str_success);
       }
      else if(data.status = false) { 
          toastr.error(str_error);
        }
      else {
          toastr.error("Access denied..!");
        }

    });  
  }
}

function viewChildcare($this)
{
  $("#LoadingDiv").css({"display":"block"});                
  if($($this).attr("data-row-id")){            
    $.ajax({
          type: "POST",
          dataType: "json",
         url: baseURL+"childcare_provider/Childcare/viewUserInfo",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        console.log(json);
          if(json.status == "success"){
          $("#LoadingDiv").css({"display":"none"});                
          $("#child_name").text(json.childData.user_name);
          $("#child_email").text(json.childData.user_email); 
          $("#user_phone_number").text(json.childData.user_phone_number);
          if(json.childData.user_gender == "female")
            $("#user_gender").text("Female");
          else if(json.childData.user_gender == "male")
            $("#user_gender").text("Male"); 
          else
            $("#user_gender").text("-"); 
          $("#user_address").text(json.childData.user_address);
          $('#viewChildcareModal').modal('show', {backdrop: 'static'});
        }
         else{
          toastr.error(json.msg);
          $("#LoadingDiv").css({"display":"none"});
         }
      });
  }else{
    toastr.error("Something went wrong!!!.");         
  }
}

function deleteChildcare($this)
{
  $("#LoadingDiv").css({"display":"block"});
  if($($this).attr("data-row-id")){
    if(confirm('Are you sure you want to delete?'))
    {          
      $.ajax({
            type: "POST",
            dataType: "json",
           url: baseURL+"childcare_provider/Childcare/deleteChildcare",
            data: {"key":$($this).attr("data-row-id")},
        }).success(function (json) {
            if(json.status == "success"){
              $("#LoadingDiv").css({"display":"none"});
              $($this).parents("tr:first").remove();
              toastr.success(json.msg);
              $("#table").dataTable().fnDraw();
              
             }else{
                toastr.error(json.msg);
                $("#LoadingDiv").css({"display":"none"});
              }
        });
     } 
  } else {     
    toastr.error("Something went wrong!!!.");    
  }
}   

function editChildData($this)
{
  $('#childcareForm')[0].reset();
   $(".name").remove();
    $(".email2").remove();
    $(".email1").remove();
    $(".phone").remove();
    $(".userRole").remove();
    $(".address").remove();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"childcare_provider/Childcare/viewUserInfo",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#user_key").val($($this).attr("data-row-id"));
            $("#fname").val(json.childData.user_name);
            $("#email").val(json.childData.user_email); 
            $("#phone").val(json.childData.user_phone_number);
            if(json.childData.user_gender == "female")
              $("#genderFemale").prop("checked",true);
            else if(json.childData.user_gender == "male")
              $("#genderMale").prop("checked",true); 
            else {
              $("#genderFemale").prop("checked",false);
              $("#genderMale").prop("checked",false); 
            }              
            $("#address").text(json.childData.user_address);
            $('#childcareModal').modal('show', {backdrop: 'static'});        
          }
          else{
             // toastr.error(json.msg,"Error:");
          }
      });
  } else {
    $("#user_key").val('');
    $('#childcareModal').modal('show', {backdrop: 'static'});
  }
}

$(document).ready(function(){
 $('#childcareForm')[0].reset();
  $('#btnChildcare').click(function(){   

    var userName = $("#fname").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var editData = $("#user_key").val();
    var address= $("#address").val();
    var gender = $("input[name=gender]:checked","#childcareForm").val();
    var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,50}$/i;
    var flag = 0;
    if(email == ''){      
      flag = 1;
      $(".email2").remove(); 
      $(".email1").remove(); 
      $("#email").parent().append("<div class='email2' style='color:red;'>Email field is required</div>");
    }else{
      if( !emailPattern.test(email)){
        $(".email2").remove(); 
        $(".email1").remove(); 
        $("#email").parent().append("<div class='email2' style='color:red;'>Incorrect email id. Please try again</div>");
       flag = 1;
      }else{        
        $(".email1").remove(); 
        $(".email2").remove(); 
      }
    }

    if(phone == ''){     
      flag = 1;
      $(".phone").remove();  
      $("#phone").parent().append("<div class='phone' style='color:red;'>Contact number field is required.</div>");
    }
    else
    {
      if(phone.length < 8)
      {
        $(".phone").remove();  
        $("#phone").parent().append("<div class='phone' style='color:red;'>Contact number field must contain minimum 8 digits .</div>");
       flag = 1;
      }
      else
      { 
        $(".phone").remove();  
      }   
    }

    if(userName == ''){
      $(".name").remove();  
      $("#fname").parent().append("<div class='name' style='color:red;'>Full name field is required.</div>");
      flag = 1;
    }else{
      $(".name").remove();        
    }

    if(address == ''){
      $(".address").remove();  
      $("#address").parent().append("<div class='address' style='color:red;'>Address field is required.</div>");
      flag = 1;
    }else{
      $(".address").remove();          
    }     
  
    var ckk = checkEmail();
    if(flag == 1 || ckk == 1)
      return false;    
    else
    {
      $("#LoadingDiv").css({"display":"block"});
      $.ajax({
        type:"POST",
        url:baseURL+"childcare_provider/Childcare/addUser",            
        data:{"editData":editData,"address":address,"phone":phone,"email":email,"userName":userName,"gender":gender},
        dataType:"json",
        async:false,
        success:function(response){                     
          if(response.status == "success")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#childcareModal").modal('hide');
            $("#table").dataTable().fnDraw();
            toastr.success(response.message);                                      
          }
          else
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#childcareModal").modal('hide');
            toastr.error(response.message);                                       
          }
        }
      }); 
    }   
  });  

  $('#addPopUp').on("click",function(){
    $(".name").remove();
    $(".email2").remove();
    $(".email1").remove();
    $(".phone").remove();
    $(".address").remove();
    $("#childcareForm")[0].reset();
    $("#childcareModal").modal('show');
  });
});