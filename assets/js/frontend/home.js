$(document).ready(function(){      
    $(".email").remove();  
    $(".fname").remove();  
    $(".subject").remove();  
    $(".message").remove();  
    $(".mobileno").remove();
    $("#btnContact").on("click",function(){
      var email = $("#email").val();
      var fname = $("#fname").val();
      var subject = $("#subject").val();
      var message = $("#message").val();
      var mobileno = $("#mobileno").val();
      var flag = 0;        
       if(email == ''){
        $(".email").remove();  
        $("#email").parent().append("<span class='email' style='color:red;'>Email field is required.</span>");
        flag = 1;
      }else{
        $(".email").remove();        
      }

      if(fname == ''){
        $(".fname").remove();  
        $("#fname").parent().append("<span class='fname' style='color:red;'>Name field is required.</span>");
        flag = 1;
      } else {
        $(".fname").remove();        
      }

      if(mobileno == ''){
        $(".mobileno").remove();  
        $("#mobileno").parent().append("<span class='mobileno' style='color:red;'>Mobile no field is required.</span>");
        flag = 1;
      } else {
        $(".mobileno").remove();        
      }

      if(subject == ''){
        $(".subject").remove();  
        $("#subject").parent().append("<span class='subject' style='color:red;'>Subject field is required.</span>");
        flag = 1;
      } else {
        $(".subject").remove();        
      }

      if(message == ''){
        $(".message").remove();  
        $("#message").parent().append("<span class='message' style='color:red;'>message field is required.</span>");
        flag = 1;
      } else {
        $(".message").remove();        
      }

      if(flag == 1)
        return false;    
      else
      {
        $.ajax({
          type:"POST",
          url:baseURL+"frontend/Home_controller/contactUs",            
          data:{"fname":fname,"email":email,"subject":subject,"message":message,"mobileno":mobileno},
          dataType:"json",
          async:false,
          success:function(response){  
            if(response.status == true){
              $('#respMessage').html('<div class="alert alert-success alert-dismissable" style="color:green">'+response.message+'</div>');
              setInterval(function(){ $('#respMessage').html(''); $("#contactForm")[0].reset(); }, 3000);                        
            }
            else
            {
              $('#respMessage').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+response.message+'</div>');
              setInterval(function(){ $('#respMessage').html(''); }, 3000);                  
            }
          }
        }); 
      }  
    });

});
