function checkEmail()
{
  $(".email1").remove();
  var user_key = $("#user_key").val();
  var result = 0;
  if(user_key !="")
    return result;
  else
  {
    var email = $("#email").val();  
    $.ajax({
      type:"POST",
      url:baseURL+"checkemailAvailability",
      data:{'email':email,'user_key':user_key},
      dataType:"json",
      async:false,          
      success:function(response){        
        if(response.status == true)
        {
          $(".email1").remove(); 
          $("#email").parent().append("<div class='email1' style='color:red;'>"+ response.message +"</div>");
          $(".email2").remove();
          result = 1;
        }
        else
        {       
          $(".email1").remove();       
        }     
      }
    });  
  }
  return result;
}
function parentProfileStatus_popup(userId,user_status,active_id,inActive_id)
{
 
var temp = "parentProfileStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
$("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
$(".inner_body").html('<b>Are you sure want to change status ?</b>');
$(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
$('.commanPopup').modal('show');
}
function parentProfileStatus(userId,user_status,active_id,inActive_id)
{  
  $('.commanPopup').modal('hide');
      $.ajax({
      type : "POST",
      dataType : "json",
      url : baseURL + "parent_management/ParentdetailsController/parentChangeStatus",
      data : { userId : userId, user_status: user_status} 
      }).done(function(data){
      console.log(data);
      if(user_status == 1) { 
        var str_success="Parent profile has been actived successfully";
        var str_error="Parent profile has been activation failed";

        $("#"+active_id).removeClass("btn-active-disable");
        $("#"+active_id).addClass("btn-active-enable");

        $("#"+inActive_id).removeClass("btn-inactive-enable");
        $("#"+inActive_id).addClass("btn-inactive-disable");

        $("#"+active_id).prop('disabled', true);
        $("#"+inActive_id).prop('disabled', false);
        } else {
          $("#"+active_id).removeClass("btn-active-enable");
          $("#"+active_id).addClass("btn-active-disable");

          $("#"+inActive_id).removeClass("btn-inactive-disable ");
          $("#"+inActive_id).addClass("btn-inactive-enable");

          var str_success="Parent profile has been inactived successfully";
          var str_error="Parent profile has been inactivation failed";

          $("#"+active_id).prop('disabled', false);
          $("#"+inActive_id).prop('disabled', true);
        }
      if(data.status = true) {
        toastr.success(str_success);
      }
      else if(data.status = false) {
        toastr.error(str_error);        
       }
      else {
        toastr.error("Access denied..!");
        }
    }); 
 }
function deleteParentData($this)
{
  if($($this).attr("data-row-id")){
    var kay= "'"+ $($this).attr("data-row-id") +"'";
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure to delete this parent ?</b>');
    $(".inner_footer").html('<button type="button" onclick="delParent('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');       
    
  } else {
 toastr.error("Parent user something get wrong.");
 }
}  
function delParent(key){
 
 $.ajax({
          type: "POST",
          dataType: "json",
          beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
            url: baseURL+"parent_management/ParentdetailsController/deleteParentInfo",
          data: {"key":key},
        }).success(function (json) {
          if(json.status == "success"){
            toastr.success(json.msg); // success message
            }else{
            toastr.error(json.msg); // error message
          }
      });
    $('.commanPopup').modal('hide');
    parentList();
} 
function parentList()
{
var table = $('#table').DataTable();
            $('#table').empty();
            table.destroy();

       $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"parent_management/ParentdetailsController/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
              $("#LoadingDiv").css({"display":"block"}); 
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
          },
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,5,6 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
}   

function viewParentData($this)
{
  if($($this).attr("data-row-id")){            
    $.ajax({
          type: "POST",
          dataType: "json",
         url: baseURL+"parent_management/ParentdetailsController/viewParentInfo",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      	console.log(json);
          if(json.status == "success"){
          $("#parent_name").text(json.parentData.user_name);
         	$("#parent_email").text(json.parentData.user_email); 
         	$("#user_phone_number").text(json.parentData.user_phone_number);
         	if(json.parentData.user_gender == "female")
         		$("#user_gender").text("Female");
         	else if(json.parentData.user_gender == "male")
         		$("#user_gender").text("Male"); 
         	else
         		$("#user_gender").text("-"); 
         	$("#user_address").text(json.parentData.user_address);
         	$("#user_birth_date").text(json.parentData.user_birth_date); 
         	
          var content="";
          var count=0;
          $.each(json.kidsData, function(i,kidsData){
            var kidGender="";
            if(kidsData.kid_gender == "female")
              kidGender ="Female";
            else if(kidsData.kid_gender == "male")
              kidGender ="Male";
            else
              kidGender ="-";
            content += '<tr><td>' + (count+1) + '</td>';
            content += '<td>' + kidsData.kid_name + '</td>';
            content += '<td>' + kidGender + '</td>';
            content += '<td>'+kidsData.kid_age+'</td>';
            content += '<tr/>';
            count++;            
          });          
          $("#kidsList tbody").html(content);
          $("#user_no_of_kids").text(count);
          $('#parentUserModal').modal('show', {backdrop: 'static'});

         } else {
           toastr.error(json.msg); 
         }

      });
  }else{
    toastr.error("Parent user something get wrong."); 	     	
  }
} 

function editParentData($this)
{
   $(".name1").remove();
    $(".email1").remove();
    $(".phone1").remove();
    $(".noOfKis").remove();
    $(".address1").remove();
    $(".email2").remove(); 
  $('#parentProfileForm')[0].reset();
  $('#dynamic_field tbody').html("");
  $("#parent_id").val();
  $("h3").html('<h3 class="modal-title"><b>Edit parent</b></h3>');
  if($($this).attr("data-row-id")){
    $("#LoadingDiv").css({"display":"block"});
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"parent_management/ParentdetailsController/viewParentInfo",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){     
          $("#LoadingDiv").css({"display":"none"});       
            $("#user_key").val($($this).attr("data-row-id"));
            $("#name").val(json.parentData.user_name);            
            $("#email").val(json.parentData.user_email);
            $("#email").prop('disabled', true);
            $("#phone").val(json.parentData.user_phone_number);    
            $("#noOfKis").val(json.parentData.user_no_of_kids);    
            $("#address").val(json.parentData.user_address);   
            $("#parent_id").val(json.parentData.user_id); 
            if(json.parentData.user_gender == "female")
            $("#genderFemale").prop("checked",true);        
            else if(json.parentData.user_gender == "male")
            $("#genderMale").prop("checked",true);
            else
            { $("#genderFemale").prop("checked",false);        
            $("#genderMale").prop("checked",false);  }
             var content ="";
             i=0;
             //alert("iudshf"+json.kidsData);
             if(json.kidsData != "")
             {
              $.each(json.kidsData, function(i,kidsData){
                //alert(kidsData);
                var kdm=kdf="";
                var birthDate="";
                if(kidsData.kid_gender == "male")
                  kdm ="selected";
                if(kidsData.kid_gender == "female")
                  kdf ="selected";
                //alert(kidsData.);
                if(i == 0)
                {                
                  $('#dynamic_field tbody').append('<tr id="row'+i+'" data-row-id="row'+i+'"><td><input type="text" name="kidName[]" placeholder="Enter your Name"  class="form-control name_list name" id="kidName0" value="'+kidsData.kid_name+'" maxlength="50" /></td><td><select class="form-control kidgender" id="kidGender0" name="kidGender[]"><option value="" class="selecttxt" disable selected hidden></option><option value="male" '+kdm+'>Male</option><option value="female" '+kdf+'>Female</option></select></td><td><input type="text" name="kidDOB[]" placeholder="Date of birth" class="form-control dateOfBirth" id="kidDOB0" value="'+kidsData.kid_birthdate+'" readOnly/></td>  <td><input type="text" name="kidAge[]" placeholder="Age" class="form-control age" id="kidAge0" value="'+kidsData.kid_age+'" maxlength="2"/></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
                }
                else{
                 $('#dynamic_field tbody').append('<tr id="row'+i+'" data-row-id="row'+i+'"><td><input type="text" name="kidName[]" placeholder="Enter your Name"  class="form-control name_list name" id="kidName'+i+'" value="'+kidsData.kid_name+'" maxlength="50" /></td><td><select class="form-control kidgender" id="kidGender'+i+'" name="kidGender[]"><option value="" class="selecttxt" disable selected hidden></option><option value="male" '+kdm+'>Male</option><option value="female" '+kdf+'>Female</option></select></td><td><input type="text" name="kidDOB[]" placeholder="Date of birth" class="form-control dateOfBirth" id="kidDOB'+i+'" value="'+kidsData.kid_birthdate+'" readOnly/></td>  <td><input type="text" name="kidAge[]" id="kidAge'+i+'" placeholder="Age" class="form-control age" value="'+kidsData.kid_age+'" maxlength="2"/></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');      
                }               
                
                 $(".name").keydown(function(event) {
                  k = event.which;
                  if ((k >= 65 && k <= 90) || k == 8 || k == 222 || k == 189 || k == 173 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
                    if ($(this).val().length == 100) {
                      if (k == 8) {
                        return true;
                      } else {
                        event.preventDefault();
                        return false;
                      }
                    }
                  } else {
                    event.preventDefault();
                    return false;
                  }
                  if (k === 32 && !this.value.length)
                      event.preventDefault();
                });

                  $(".age").keydown(function (e) {        
                  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
                      (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
                      (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                           return;
                  }        
                  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                      e.preventDefault();
                  }
              }); 
                $(".dateOfBirth").datepicker({           
                  changeMonth:true,
                  changeYear:true,
                  maxDate:"-2d",                          
                });
                i++;    
            });   
          }
          else
          {
            $('#dynamic_field tbody').append('<tr id="row0" data-row-id="row0"><td><input type="text" name="kidName[]" placeholder="Enter your Name"  class="form-control name_list name" id="kidName0" maxlength="50" /></td><td><select class="form-control kidgender" id="kidGender0" name="kidGender[]"><option value="" class="selecttxt" disable selected hidden></option><option value="male">Male</option><option value="female">Female</option></select></td><td><input type="text" name="kidDOB[]" placeholder="Date of birth" class="form-control dateOfBirth" id="kidDOB0" readOnly/></td>  <td><input type="text" name="kidAge[]" placeholder="Age" class="form-control age" id="kidAge0" maxlength="2"/></td><td><button type="button" name="remove" id="0" class="btn btn-danger btn_remove">X</button></td></tr>');  
            $(".name").keydown(function(event) {
                  k = event.which;
                  if ((k >= 65 && k <= 90) || k == 8 || k == 222 || k == 189 || k == 173 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
                    if ($(this).val().length == 100) {
                      if (k == 8) {
                        return true;
                      } else {
                        event.preventDefault();
                        return false;
                      }
                    }
                  } else {
                    event.preventDefault();
                    return false;
                  }
                  if (k === 32 && !this.value.length)
                      event.preventDefault();
                });

                  $(".age").keydown(function (e) {        
                  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
                      (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
                      (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                           return;
                  }        
                  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                      e.preventDefault();
                  }
              }); 
                $(".dateOfBirth").datepicker({           
                  changeMonth:true,
                  changeYear:true,
                  maxDate:"-2d",                          
                });
                i++;                
          }
            $('#parentProfileModal').modal('show', {backdrop: 'static'});        
          }
          else{
            $("#LoadingDiv").css({"display":"none"});
             // toastr.error(json.msg,"Error:");
          }
      });
  } else {
    $("#user_key").val('');
    $('#parentProfileModal').modal('show', {backdrop: 'static'});
  }
}

function addrow(){  
  var count=0;
  $("#dynamic_field tbody tr").each(function (i) {
    count++;
  });
  var i = count;  
  //alert(i);return false;
  $('#dynamic_field').append('<tr id="row'+i+'" data-row-id="row'+i+'"><td><input type="text" name="kidName[]" placeholder="Enter your Name"  class="form-control name_list name" id="kidName'+i+'" maxlength="50" /></td><td><select class="form-control kidgender" name="kidGender[]" id="kidGender'+i+'"><option value="" class="selecttxt" disable selected hidden></option><option value="male">Male</option><option value="female">Female</option></select></td><td><input type="text" name="kidDOB[]" placeholder="Date of birth" class="form-control dateOfBirth" id="kidDOB'+i+'" readOnly/></td>  <td><input type="text" name="kidAge[]" placeholder="Age" id="kidAge'+i+'" class="form-control age" maxlength="2"/></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
  i++;
  $('#count_id').val(i);  
  $(".dateOfBirth").datepicker({           
    changeMonth:true,
    changeYear:true,
    maxDate:"-2d",                          
  });

  $(".name").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 222 || k == 189 || k == 173 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

  $(".age").keydown(function (e) {        
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
          (e.keyCode >= 35 && e.keyCode <= 40)) {                 
               return;
    }        
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  }); 
}

$(document).ready(function(){
  $(".name1").remove();
  $(".email1").remove();
  $(".phone1").remove();
  $(".noOfKis").remove();
  $(".address1").remove();  

  /*$("#searchText").on("keyup",function(){
    var value = this.value.toLowerCase().trim();
    $("table tbody tr").each(function (index) {
      //if (!index) return;
      $(this).find("td").each(function () {
        //alert($(this).text());
        var id = $(this).text().toLowerCase().trim();
        var not_found = (id.indexOf(value) == -1);
        $(this).closest('tr').toggle(!not_found);
        return not_found;
      });
    });
  });*/

  $(".name").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 222 || k == 189 || k == 173 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
      event.preventDefault();
  });

  $(".age").keydown(function (e) {        
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
        (e.keyCode >= 35 && e.keyCode <= 40)) {                 
      return;
    }        
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  });  
  $(document).on('click', '.btn_remove', function(){  
    var button_id = $(this).attr("id");   
    $('#row'+button_id+'').remove();  

    $("#dynamic_field tbody tr").each(function (i) {
      if($("#kidName"+i).val()=="")
      {
        $(".kidName"+i).remove();  
        $("#kidName"+i).parent().append("<span class='kidName"+i+"' style='color:red;'>Name field is required.</span>");
          flag = 1;
      }
      else
      {
         $(".kidName"+i).remove();  
      }

      if($("#kidGender"+i).val()=="")
      {
        $(".kidGender"+i).remove();  
        $("#kidGender"+i).parent().append("<span class='kidGender"+i+"' style='color:red;'>Gender field is required.</span>");
        flag = 1;
      }
      else
      {
        $(".kidGender"+i).remove();  
      }

      if($("#kidDOB"+i).val()=="")
      {
        $(".kidDOB"+i).remove();  
        $("#kidDOB"+i).parent().append("<span class='kidDOB"+i+"' style='color:red;'>Date of birth field is required.</span>");
          flag = 1;
      }
      else
      {
        $(".kidDOB"+i).remove();  
      }

      if($("#kidAge"+i).val()=="")
      {
        $(".kidAge"+i).remove();  
        $("#kidAge"+i).parent().append("<span class='kidAge"+i+"' style='color:red;'>Age field is required.</span>");
        flag = 1;
      }
      else
      {
         $(".kidAge"+i).remove();  
      }
        i++;     
    });

 $(".name").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 222 || k == 189 || k == 173 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

  $(".age").keydown(function (e) {        
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
          (e.keyCode >= 35 && e.keyCode <= 40)) {                 
        return;
      }        
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    }); 
  });   
  
  $('#btnParentProfileSave').click(function(){            
    var parentName = $("#name").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var address = $("#address").val();
    var noOfKis= $("#noOfKis").val();
    var editData = $("#user_key").val();
    var parent_id= $("#parent_id").val();

    var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,50}$/i;
    var emailResetFlag = false;
    var parentGenderFlag =false;
    var phoneFlag =false;
    var addressFlag =false;
    var nameFlag =false;
    var name= $("#name").val();
    var phone= $("#phone").val();
    var noOfKis= $("#noOfKis").val();
    var email = $("#email").val();
    var address = $("#address").val();   
    var kidname =$("#kidName0").val();
    var gender =$("#kidGender0").val();
    var dob =$("#kidDOB0").val();
    var age =$("#kidAge0").val(); 
    var flag = 0;

    if(email == ''){      
      flag = 1;
      $(".email2").remove(); 
      $(".email1").remove(); 
      $("#email").parent().append("<div class='email2' style='color:red;'>Email field is required</div>");
    }else{
      if( !emailPattern.test(email)){
        $(".email2").remove(); 
        $(".email1").remove(); 
        $("#email").parent().append("<div class='email2' style='color:red;'>Incorrect email id. Please try again</div>");
       flag = 1;
      }else{        
        $(".email1").remove(); 
        $(".email2").remove(); 
      }
    }
    if(phone == ''){     
      flag = 1;
      $(".phone1").remove();  
      $("#parentContact").parent().append("<div class='phone1' style='color:red;'>Phone number field is required.</div>");
    }
    else
    {
      if(phone.length < 8)
      {
        $(".phone1").remove();  
        $("#parentContact").parent().append("<div class='phone1' style='color:red;'>Phone number field must contain minimum 8 digits .</div>");
       flag = 1;
      }
      else
      { 
        $(".phone1").remove();  
      }   
    }

    if(name == ''){
      $(".name1").remove();  
      $("#parentName").parent().append("<div class='name1' style='color:red;'>Name field is required.</div>");
      flag = 1;
    }else{
      $(".name1").remove();        
    }

    if(noOfKis == ''){
      $(".noOfKis").remove();  
      $("#noOfKis").parent().append("<div class='noOfKis' style='color:red;'>No. of kids field is required.</div>");
      flag = 1;
    }else{
      $(".noOfKis").remove();          
    }

    if(address == ''){
      $(".address1").remove();  
      $("#perentAddress").parent().append("<div class='address1' style='color:red;'>Address field is required.</div>");
      flag = 1;
    }else{
      $(".address1").remove();            
    }

    $("#dynamic_field tbody tr").each(function (i) {
      if($("#kidName"+i).val()=="")
      {
        $(".kidName"+i).remove();  
        $("#kidName"+i).parent().append("<span class='kidName"+i+"' style='color:red;'>Name field is required.</span>");
          flag = 1;
      }
      else
      {
         $(".kidName"+i).remove();  
      }

      if($("#kidGender"+i).val()=="")
      {
        $(".kidGender"+i).remove();  
        $("#kidGender"+i).parent().append("<span class='kidGender"+i+"' style='color:red;'>Gender field is required.</span>");
        flag = 1;
      }
      else
      {
        $(".kidGender"+i).remove();  
      }

      if($("#kidDOB"+i).val()=="")
      {
        $(".kidDOB"+i).remove();  
        $("#kidDOB"+i).parent().append("<span class='kidDOB"+i+"' style='color:red;'>Date of birth field is required.</span>");
          flag = 1;
      }
      else
      {
        $(".kidDOB"+i).remove();  
      }

      if($("#kidAge"+i).val()=="")
      {
        $(".kidAge"+i).remove();  
        $("#kidAge"+i).parent().append("<span class='kidAge"+i+"' style='color:red;'>Age field is required.</span>");
        flag = 1;
      }
      else
      {
         $(".kidAge"+i).remove();  
      }
        i++;     
    });
    var parentGender = $('input[name=gender]:checked', '#parentProfileForm').val();
    var po_items_array = $("#dynamic_field tbody tr").map(function () {
            return {                
                age : $(this).find('.age').val(),
                name : $(this).find('.name').val(),
                dateOfBirth : $(this).find('.dateOfBirth').val(),
                kidgender : $(this).find('.kidgender').val(),
            };
        }).get();
    var ckk = checkEmail();
    if(flag == 1 || ckk == 1)
      return false;    
    else
    {
      $("#LoadingDiv").css({"display":"block"});
      $.ajax({
        type:"POST",
        url:baseURL+"parent_management/ParentdetailsController/addParentData",            
        data:{'parent_id':parent_id,'editData':editData,'kidsData':po_items_array,"parentName":parentName,"email":email,"phone":phone,"address":address,"parentGender":parentGender,"user_no_of_kids":noOfKis},
        dataType:"json",
        async:false,
        success:function(response){                     
          if(response.status == "success")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#parentProfileModal").modal('hide');
            toastr.success(response.message);
            $("#table").dataTable().fnDraw();            
          }
          else
          {
            $("#parentProfileModal").modal('hide');
            $("#LoadingDiv").css({"display":"none"});
            toastr.error(response.message);                                     
          }
        }
      }); 
    }   
  });  
  $('#addPopUp').on("click",function(){
    $("#user_key").val("");
    $("#parent_id").val("");
    $(".name1").remove();
    $(".email1").remove();
    $(".phone1").remove();
    $(".noOfKis").remove();
    $(".address1").remove();
    $(".email2").remove(); 
    $("h3").html('<h3 class="modal-title"><b>Add parent</b></h3>');
    $("#email").prop('disabled', false);
    var html='<tr data-row-id="row0" id="row0"><td><input type="text" name="kidName[]" placeholder="Name" id="kidName0" class="form-control name_list name" /></td><td><select class="form-control kidgender" id="kidGender0" name="kidGender[]"><option value="" class="selecttxt" disable selected hidden></option><option value="male">Male</option><option value="female">Female</option></select></td><td><input type="text" name="kidDOB[]" id="kidDOB0" placeholder="Date of birth" class="form-control dateOfBirth" readOnly/></td><td><input type="text" name="kidAge[]" id="kidAge0" placeholder="Age" class="form-control age" maxlength="2"/></td><td><button type="button" name="remove" id="0" class="btn btn-danger btn_remove">X</button></td></tr>';
    $("#dynamic_field tbody").html(html);   
    $("#parentProfileForm")[0].reset();
    $("#parentProfileModal").modal('show');
    $(".dateOfBirth").datepicker({           
      changeMonth:true,
      changeYear:true,
      maxDate:"-2d",                          
    });
  
  $(".name").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 222 || k == 189 || k == 173 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

  $(".age").keydown(function (e) {        
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
          (e.keyCode >= 35 && e.keyCode <= 40)) {                 
        return;
      }        
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    }); 
  
  });
});

// add for datepicker select year & month issue.
var enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
$confModal.on('hidden', function() {
 $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
});
$confModal.modal({ backdrop : false });