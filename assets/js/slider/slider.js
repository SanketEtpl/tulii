var flag = 1;
function sliderList(){

var table = $('#table').DataTable();
            $('#table').empty();
            table.destroy();

       $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"slider_management/Slider/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
                
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              }); 
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"});
            $("#table").css({"opacity":"1"});

          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,2,3,4], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    });  
}
function deleteBanner($this) // delete record of price list
{
  if($($this).attr("data-row-id")){
    var kay= "'"+ $($this).attr("data-row-id") +"'";
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure you want to delete this slider?</b>');
    $(".inner_footer").html('<button type="button" onclick="delBanner('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');


  } else {     
        toastr.error("Something went wrong!!!.");    
  }
} 
function delBanner(key){
 
 $.ajax({
          type: "POST",
          dataType: "json",
          beforeSend: function() {
           //  $('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
          url: baseURL+"slider_management/Slider/deleteBanner",
          data: {"key":key},
        }).success(function (json) {
          if(json.status == "success"){
            $(this).parents("tr:first").remove();
            $("#table").dataTable().fnDraw();
            toastr.success(json.msg); // success message
            }else{
            toastr.error(json.msg); // error message
          }
      });
    $('.commanPopup').modal('hide');    
}
function sliderStatus_popup(userId,user_status,active_id,inActive_id)
{
 
var temp = "sliderStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
$("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
$(".inner_body").html('<b>Are you sure you want to change status?</b>');
$(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
$('.commanPopup').modal('show');
}
function sliderStatus(userId,user_status,active_id,inActive_id)
{ 
$('.commanPopup').modal('hide'); 
  
    $.ajax({
      type : "POST",
      dataType : "json",
      url : baseURL + "slider_management/Slider/sliderStatus",
      data : { userId : userId, user_status: user_status} 
      }).done(function(data){
      console.log(data);
      if(user_status == 1) { 
        var str_success="Slider image has been activated successfully.";
        var str_error="Slider image has been activation failed.";

        $("#"+active_id).removeClass("btn-active-disable");
        $("#"+active_id).addClass("btn-active-enable");

        $("#"+inActive_id).removeClass("btn-inactive-enable");
        $("#"+inActive_id).addClass("btn-inactive-disable");

        $("#"+active_id).prop('disabled', true);
        $("#"+inActive_id).prop('disabled', false);
        } else {
          $("#"+active_id).removeClass("btn-active-enable");
          $("#"+active_id).addClass("btn-active-disable");

          $("#"+inActive_id).removeClass("btn-inactive-disable ");
          $("#"+inActive_id).addClass("btn-inactive-enable");

          var str_success="Slider image has been inactivated successfully.";
          var str_error="Slider image has been inactivation failed.";

          $("#"+active_id).prop('disabled', false);
          $("#"+inActive_id).prop('disabled', true);
        }
      if(data.status = true) {
        toastr.success(str_success);
      }
      else if(data.status = false) {
        toastr.error(str_error);        
       }
      else {
        toastr.error("Access denied..!");
        }
    }); 
 
}

/*function Upload()
{
  var width = 0;
  var height=0;
  var fileUpload = document.getElementById("slider_img");
  var reader = new FileReader();
  if (fileUpload.files[0] && fileUpload.files[0].type.match('image.*')) {
    reader.readAsDataURL(fileUpload.files[0]);
  }
  //reader.readAsDataURL(fileUpload.files[0]);
  reader.onload = function (e) {
  var image = new Image();
  image.src = e.target.result;           
  image.onload = function () {
      var height = this.height;
      var width = this.width;      
      document.getElementById('SliderWHImage').width = width;
      document.getElementById('SliderWHImage').height = height;
    };
  }
}*/

function editBanner($this)
{
  $(".slider_img").remove();
  $(".slider_name").remove();
  $('#sliderForm')[0].reset();  
  $("#showFilename").html("No file chosen"); 
  $( "h3" ).html( "<h3 class='modal-title'><b>Edit slider</b></h3>" );
  $("#parent_id").val();
  if($($this).attr("data-row-id")){
    //$("#LoadingDiv").css({"display":"block"});
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"slider_management/Slider/viewBanner",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){     
          $("#LoadingDiv").css({"display":"none"});       
            $("#user_key").val($($this).attr("data-row-id"));
            $("#slider_name").val(json.bannerData.title);                        
            $('#sliderModal').modal('show', {backdrop: 'static'});        
          }
          else{           
              toastr.error(json.msg,"Error:");
          }
      });
  } else {
    $("#user_key").val('');
    $('#sliderModal').modal('show', {backdrop: 'static'});
  }
}

$(document).ready(function(){ 
  var imgSize = 0;
  $('#slider_img').bind('change', function() {
    imgSize = this.files[0].size;    
  });
  //$("#showFilename").html("No file chosen");    
  $('#addPopUp').on("click",function(){    
    $("#showFilename").html("No file chosen"); 
    $( "h3" ).html( "<h3 class='modal-title'><b>Add slider</b></h3>" );
    $("#user_key").val("");
    $(".slider_name").remove();
    $(".slider_img").remove();
    $("#sliderForm")[0].reset();
    $("#sliderModal").modal('show');   
  });
  $('input[type="file"]').change(function(e){
    var width = 0;
    var height=0;
    var fileUpload = document.getElementById("slider_img");
    var reader = new FileReader();
    if (fileUpload.files[0] && fileUpload.files[0].type.match('image.*')) {
      reader.readAsDataURL(fileUpload.files[0]);
    }
    //reader.readAsDataURL(fileUpload.files[0]);
    reader.onload = function (e) {
    var image = new Image();
    image.src = e.target.result;           
    image.onload = function () {
        var height = this.height;
        var width = this.width;      
        document.getElementById('SliderWHImage').width = width;
        document.getElementById('SliderWHImage').height = height;
      };
    }
  });
  
  $('#btnSlider').click(function(){   
  //alert("helljhsdf");         
    var slider_name = $("#slider_name").val();
    var slider_img = $("#slider_img").val();
    var w =0;
    var h =0;
    var w = $("#SliderWHImage").attr('width');
    var h = $('#SliderWHImage').attr('height');
    var ext = $('#slider_img').val().split('.').pop().toLowerCase();   
    var flag = 0;
    var size = 2097152;
    if(slider_name == ''){
      $(".slider_name").remove();  
      $("#slider_name").parent().append("<div class='slider_name' style='color:red;'>Slider name field is required.</div>");
      flag = 1;
    }else{
      $(".slider_name").remove();        
    }
/*    if(w < 1800 || h< 600){
      alert("low image");
    }else {
      alert("correct");
    }*/
    //alert("width "+w+ " height "+h);return false;

    if(slider_img == ''){
      $(".slider_img").remove();  
      $("#slider_img").parent().append("<div class='slider_img' style='color:red;'>Slider image field is required.</div>");
      flag = 1;
    } else if(typeof w === "undefined" || typeof h === "undefined"){
     $(".slider_img").remove();  
      $("#slider_img").parent().append("<div class='slider_img' style='color:red;'>Slider image width and height undefined.</div>");
      flag = 1;
    } else if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
      $(".slider_img").remove();  
      $("#slider_img").parent().append("<div class='slider_img' style='color:red;'>Uploaded file is not a valid image.</div>");
      flag = 1;
    } else if(imgSize >= size){
     $(".slider_img").remove();  
      $("#slider_img").parent().append("<div class='slider_img' style='color:red;'>Slider image should be less than 2MB.</div>");
      flag = 1;
    } else if(w < 1800 || h< 600){
       $(".slider_img").remove();  
        $("#slider_img").parent().append("<div class='slider_img' style='color:red;'>Slider image should be greater than 1800*600.</div>");
        flag = 1;
    } else {
      $(".slider_img").remove();          
    }
    
    if(flag == 1)
      return false;    
    else
    {
     // $("#LoadingDiv").css({"display":"block"});
      var formData = new FormData($('#sliderForm')[0]); 
      $.ajax({
        type:"POST",
        url:baseURL+"slider_management/Slider/addBanner",            
        dataType:"json",
        data:formData,            
        processData: false,
        contentType: false,
        success:function(response){                     
          if(response.status == "success")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#slider_name").val("");
            $("#slider_img").val("");
            $("#showFilename").html("No file chosen");
            toastr.success(response.message);
            $("#table").dataTable().fnDraw();
            $("#user_key").val('');
            document.getElementById('SliderWHImage').width = 0;
            document.getElementById('SliderWHImage').height = 0;
            $("#sliderModal").modal('hide');
          }
          else
          {
            $("#LoadingDiv").css({"display":"none"});
            toastr.error(response.message); // success message
            $("#table").dataTable().fnDraw();
          }
        }
      }); 
    }   
  });    
});
