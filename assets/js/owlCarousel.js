$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        dots: true,
        loop:true,
        margin:10,
        nav:true,
        /* slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,*/
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
});