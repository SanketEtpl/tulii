function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  groupList();
}

function groupList(){
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [],

    // Load data for the table's content from an Ajax source
      "ajax": {
        "url": baseURL+"group_management/Group/ajax_list",
        beforeSend: function() {
           // $("#LoadingDiv").css({"display":"block"});
          var search = $("input[type=search]").val();
          if(search=="")
           $("input[type=search]").on("keyup",function(event) {

            if($("#clear").length == 0) {
               if($(this).val() != ""){
                $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
              } 
            }
            if($(this).val() == "")  
            $("#clear").remove();      
          }); 
          $("input[type=search]").keydown(function(event) {
            k = event.which;
            if (k === 32 && !this.value.length)
                event.preventDefault();
          });
         },
        complete: function(){
        $("#LoadingDiv").css({"display":"none"}); 
        $("#table").css({"opacity":"1"});
      },
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 0,5,6,7 ], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  }); 
}

function viewGroup($this)
{
  if($($this).attr("data-row-id")){
  $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"group_management/Group/viewGroupMembers",
      beforeSend: function() {
      },
      complete: function(){      
    },
      data: {"key":$($this).attr("data-row-id")},
  }).success(function (json) {
    if(json.status == "success"){   
      $("#group_name").text(json.grpData[0].group_name);            
      $("#description").text(json.grpData[0].description);
      $("#no_of_peoples").text(json.grpData[0].no_of_people);      
      $("#group_last_date").text(json.grpData[0].group_limit_date);      
      var groupMember = "";
      $.each(json.grpData, function( index, value ) {        
        groupMember +=  "<tr><td>"+(index+1)+"</td><td>"+value.pass_fullname+"</td><td>"+value.pass_dob+"</td><td>"+value.pass_gender+"</td><td>"+value.pass_address+"</td></tr>";
        if(value.pass_fullname == null && value.pass_address == null && value.pass_gender == null && value.pass_dob == null)
          groupMember =  "<tr><td> No group member are available</td></tr>";
      });
      $("#groupList tbody").html(groupMember);
      $('#groupListingModal').modal('show', {backdrop: 'static'});
    } else {
      toastr.error(json.msg,"Error:");
    }
    });
  } else {
    toastr.error("Something went wrong!!!.");    
  }
}


$(document).ready(function(){
  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
      event.preventDefault();
  });

  
  
});