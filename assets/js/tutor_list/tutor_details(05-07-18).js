function deleteTutor($this)
{
  	if($($this).attr("data-row-id")){
	    var kay= "'"+ $($this).attr("data-row-id") +"'";
	    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	    $(".inner_body").html('<b>Are you sure you want to delete this tutor?</b>');
	    $(".inner_footer").html('<button type="button" onclick="deltutor('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
	    $('.commanPopup').modal('show');       
    } else {
 		toastr.error("Something went wrong!!!.");           
 	}
}  

function deltutor(key){ 
 	$.ajax({
        type: "POST",
        dataType: "json",
        beforeSend: function() {
    	    //$('#LoadingDiv').show();
        },
        complete: function(){
        	$('#LoadingDiv').hide();
        },
         	url: baseURL+"tutor_list/Tutor_list/deleteTutor",
          	data: {"key":key},
        }).success(function (json) {
          	if(json.status == "success") {
	            toastr.success(json.msg); // success message
	            $("#table").dataTable().fnDraw();
            } else {
            	toastr.error(json.msg); // error message
          	}
     	});
    $('.commanPopup').modal('hide');    
}

$('#addTutorSubCatgoryPopUp').on("click",function(){
	$(".tutor_sub_category_name").remove();
	$(".tutorCategory").remove();
    $("#tutorSubCatForm")[0].reset(); 
    $("#addtutorsubcatModal").modal('show');    
  });

function btnAddTutorSubCat(){
    var tutor_sub_category_name = $("#tutor_sub_category_name").val();
    var tutorCategory = $("#tutorCategory").val();
    var flag = 0;
    if(tutorCategory == ''){
	    $(".tutorCategory").remove();  
    	$("#tutorCategory").parent().append("<div class='tutorCategory' style='color:red;'>Tutor category field is not empty.</div>");
      	flag = 1;
    } else {
      	$(".tutorCategory").remove();          
    }     

    if(tutor_sub_category_name == ''){
	    $(".tutor_sub_category_name").remove();  
    	$("#tutor_sub_category_name").parent().append("<div class='tutor_sub_category_name' style='color:red;'>Tutor sub-category field is not empty.</div>");
      	flag = 1;
    } else {
      	$(".tutor_sub_category_name").remove();          
    }     
     if(flag == 1)
      	return false;    
    else
    {            	
      	$.ajax({
	        type:"POST",
	        url:baseURL+"tutor_list/Tutor_list/addTutorSubCategory",            
	        data:{category_id:tutorCategory,tutor_sub_category_name:tutor_sub_category_name},
	        dataType:"json",
	         beforeSend: function() {
           // $('#LoadingDiv').show();
            },
            complete: function(){
 		    $('#LoadingDiv').hide();
			    },
	        async:false,
	        success:function(response){                     
	          	if(response.status == "success")
	          	{		           
		           $('#addtutorsubcatModal').modal('hide');
		           toastr.success(response.message);                                      
	         	} else if(response.status == "warning") {			            
		            $("#addtutorsubcatModal").modal('show');
		            toastr.warning(response.message); 
			    } else {
		            toastr.error(response.message);                                       
	          	}
	        }
      	});    
    }   
}

$("#category").on("change",function(){
	$.ajax({
        type:"POST",
        url:baseURL+"tutor_list/Tutor_list/showTutorSubcategory",            
         dataType:"json",
         beforeSend: function() {
        //$('#LoadingDiv').show();
        },
        complete: function(){
		    $('#LoadingDiv').hide();
		    },
		data:{"category_id":$("#category").val()},    
        async:false,
        success:function(response){                     
          	if(response.status == "success")
          	{
          		//$("#hideSubCategory").css({"display":"block"});
	         	$("#sub_category").html(response.data); 	                                          
         	}
         	else 
         	{
         		$("#sub_category").html(response.data);
         		//$("#hideSubCategory").css({"display":"none"});	         	 
         	}
        }
  	});
    $("#sub_category").multiselect('destroy');
    $("#sub_category").multiselect();   
});


function btnAddTutor() 
{
    var tutorName = $("#tutorName").val();
    var category = $("#category").val();
    var sub_category = $("#sub_category").val();

    var user_key = $("#user_key").val();
    if(user_key != "") {
    	var selected = $("#sub_category option:selected");
	  	var sub_category = "";
	    selected.each(function () {
	        if(sub_category == '') {
	            sub_category +=  $(this).val(); 
	        } else {
	            sub_category += ','+ $(this).val(); 
	        }
	    });	
    }
    var flag = 0;
    //alert(tutorName); return false;
    if(tutorName == ''){
	    $(".tutorName").remove();  
    	$("#tutorName").parent().append("<div class='tutorName' style='color:red;'>Please select tutor name.</div>");
      	flag = 1;
    } else {
      	$(".tutorName").remove();          
    } 
    if(category == ''){
	    $(".category").remove();  
    	$("#category").parent().append("<div class='category' style='color:red;'>Please select category name.</div>");
      	flag = 1;
    } else {
      	$(".category").remove();          
    } 
    if(flag == 1)
      	return false;    
    else
    {            	
      	$.ajax({
	        type:"POST",
	        url:baseURL+"tutor_list/Tutor_list/addTutor",            
	        data:{tutorName:tutorName,category:category,sub_category:sub_category,user_key:user_key},
	        dataType:"json",
	         beforeSend: function() {
           // $('#LoadingDiv').show();
            },
            complete: function(){
 		    $('#LoadingDiv').hide();
			    },
	        async:false,
	        success:function(response){                     
	          	if(response.status == "success")
	          	{		           
		           $('#addTutorModal').modal('hide');
		           toastr.success(response.message);  
		           $("#table").dataTable().fnDraw();                                      
	         	}else if(response.status == "warning") {			            
		            $("#addTutorModal").modal('show');
		            toastr.warning(response.message); 
			    } else {
		            toastr.error(response.message);                                       
	          	}
	        }
      	});   
      	$("#user_key").val(''); 
    }   
}
$(document).ready(function() {
	$('#addPopUp').on("click",function(){
    $(".tutorName").remove();
    $(".category").remove();
    $(".sub_category").remove();
    $("#tutorName").prop('disabled', false);
    $("#tutorForm")[0].reset();


    $.ajax({
        type:"POST",
        url:baseURL+"tutor_list/Tutor_list/showTutorSubcategory",            
         dataType:"json",
         beforeSend: function() {
        //$('#LoadingDiv').show();
        },
        complete: function(){
		    $('#LoadingDiv').hide();
		    },
		data:{"category_id":$("#category").val()},    
        async:false,
        success:function(response){                     
          	if(response.status == "success")
          	{
          		$("#sub_category").html(response.data); 	                                          
         	}
         	else 
         	{
         		$("#sub_category").html(response.data);         		
         	}
        }
  	});
    $("#sub_category").multiselect('destroy');
    $("#sub_category").multiselect();       
    $("#addTutorModal").modal('show');    
  });

$('#addTutorCatgoryPopUp').on("click",function(){
    $(".tutor_category_name").remove();
    $("#tutorcatForm")[0].reset(); 
    $("#tutorcategoryModal").modal('show');    
  });
});


function btnAddTutorCat(){
    var tutor_category_name = $("#tutor_category_name").val();
    var flag = 0;
    if(tutor_category_name == ''){
	    $(".tutor_category_name").remove();  
    	$("#tutor_category_name").parent().append("<div class='tutor_category_name' style='color:red;'>Tutor category field is not empty.</div>");
      	flag = 1;
    } else {
      	$(".tutor_category_name").remove();          
    }     
     if(flag == 1)
      	return false;    
    else
    {            	
      	$.ajax({
	        type:"POST",
	        url:baseURL+"tutor_list/Tutor_list/addTutorCategory",            
	        data:{category_name:tutor_category_name},
	        dataType:"json",
	         beforeSend: function() {
           // $('#LoadingDiv').show();
            },
            complete: function(){
 		    $('#LoadingDiv').hide();
			    },
	        async:false,
	        success:function(response){                     
	          	if(response.status == "success")
	          	{		           
		           $('#tutorcategoryModal').modal('hide');
		           toastr.success(response.message);                                      
	         	} else if(response.status == "warning") {			            
		            $("#tutorcategoryModal").modal('show');
		            toastr.warning(response.message); 
			    } else {
		            toastr.error(response.message);                                       
	          	}
	        }
      	});    
    }   
}

function tutorStatus_popup(userId,user_status,active_id,inActive_id)
{
	var temp = "tutorStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
	$("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$(".inner_body").html('<b>Are you sure you want to change status?</b>');
	$(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
	$('.commanPopup').modal('show');
}

function tutorStatus(tId,t_status,active_id,inActive_id)
{ 
 	$('.commanPopup').modal('hide');
    $.ajax({
	    type : "POST",
	    dataType : "json",
	    url : baseURL + "tutor_list/Tutor_list/tutorStatusChange",
	    data : { tId : tId, t_status: t_status} 
    }).success(function(data){       	
      if(t_status == 1) { 
        
        var str_success="Tutor has been activated successfully.";
        var str_error="Tutor has been activation failed.";

        $("#"+active_id).removeClass("btn-active-disable");
        $("#"+active_id).addClass("btn-active-enable");

        $("#"+inActive_id).removeClass("btn-inactive-enable");
        $("#"+inActive_id).addClass("btn-inactive-disable");

        $("#"+active_id).prop('disabled', true);
        $("#"+inActive_id).prop('disabled', false);
        } else {
          $("#"+active_id).removeClass("btn-active-enable");
          $("#"+active_id).addClass("btn-active-disable");

          $("#"+inActive_id).removeClass("btn-inactive-disable ");
          $("#"+inActive_id).addClass("btn-inactive-enable");

          var str_success="Tutor has been inactivated successfully.";
          var str_error="Tutor has been inactivation failed.";

          $("#"+active_id).prop('disabled', false);
          $("#"+inActive_id).prop('disabled', true);
        }
      	if(data.status = true) {
        	toastr.success(str_success);
        }
        else if(data.status = false) { 
          	toastr.error(str_error);
        } else {
          toastr.error("Access denied..!");
        }
    });   
 }

function editTutor($this) // edit tutor list data
{
	$('#tutorForm')[0].reset(); //reset of tutor list value 
	$(".tutorName").remove();
	$(".category").remove();
	$("#tutorName").prop('disabled', true);
	
	$( ".changeHeader").html( "<h3 class='modal-title'><b>Edit Tutor</b></h3>" );
	if($($this).attr("data-row-id")){ // get tutor list id 
		
		$.ajax({
		        type:"POST",
		        url:baseURL+"tutor_list/Tutor_list/showTutorSubcategory",            
		         dataType:"json",
		         data:{"key":$($this).attr("data-row-id")},
		         beforeSend: function() {
                
                },
                complete: function(){
     		    
  			    },
		        async:false,
		        success:function(response){                     
		          	if(response.status == "success")
		          	{
			         $("#sub_category").html(response.data); 
			                                          
		         	} 
		        }
	      	});

		$("#sub_category").multiselect('destroy');

	    $.ajax({
	        type: "POST",
	        dataType: "json",
	        url: baseURL+"tutor_list/Tutor_list/viewTutor",
	        data: {"key":$($this).attr("data-row-id")},
	    }).success(function (json) {
	    	console.log(json);
	        if(json.status == "success"){            
	        	$("#user_key").val($($this).attr("data-row-id"));
	        	$("#tutorName").val(json.tutorData.user_id);
	        	$("#category").val(json.tutorData.category_id);
	          	var str = json.tutorData.subcategory_id;
		        var str_array = str.split(',');
	          	$("#sub_category").multiselect('select',str_array);
	        } else {
	            toastr.error(json.msg);
	        }
	    });   
      
  	} else {
    	$("#user_key").val('');    
 	} 	 
 	$('#addTutorModal').modal('show');
}

