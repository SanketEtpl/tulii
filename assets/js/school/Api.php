<?php 
/*
Author: Ram K
Page: APi
Description: Created Api for App.
*/

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("data_model");
        $this->load->model("login_model");
        $this->load->model("common_model");
        $this->load->library('Common');
        $this->load->library("email"); 
        $this->load->helper("email_template"); 
        $this->load->helper("cias"); 
        $this->load->library('S3');
        $this->load->library("fileupload");
        $this->load->library('form_validation');
        date_default_timezone_set("Asia/Kolkata");
        header('Access-Control-Allow-Origin: *');
        ini_set('display_errors', '0');
        //error_reporting(E_ALL);        
    }    

    /**
     * Title       : sign up  
     * Description : user sign up
     *
     * @param full_name
     * @param user_email
     * @param contact_no
     * @param user_type
     * @param password
     * @param user_device_id
     * @param user_device_type
     * @param user type.
     * @return Array 
     */

    public function signUp_post ()
    {
        // post data
        $postData = $this->post();
        $today    = date('Y-m-d');
        $ageDiff  = date_diff(date_create($postData['user_dob']), date_create($today));
        $userAge  = $ageDiff->format('%y');
        
        // input validation
        if ( $postData['full_name'] == '' ) {
            $this->response(array("status" => false, "message" => 'Full name field is required.'), 200); exit;
        }
        if ( $postData['user_email'] == '' ) { 
            $this->response(array("status" => false, "message" => 'Email field is required.'), 200); exit; 
        } else if ( is_email_exist(trim($postData['user_email'])) ) {
            $this->response(array("status" => false, "message" => 'Email address already exists.'), 200); exit;
        }
        if ( $postData['contact_no'] == '' ) { 
            $this->response(array("status" => false, "message" => 'Contact no field is required.'), 200); exit; 
        }
        if ( $postData['user_type'] == '' ) { 
            $this->response(array("status" => false, "message" => 'User type field is required.'), 200); exit; 
        }    
        if ( $postData['password'] == '') { 
            $this->response(array("status" => false, "message" => 'Password field is required.'), 200); exit;
        }
        if ( $postData['user_device_id'] == '' ) { 
            $this->response(array("status" => false, "message" => 'Device id field is required.'), 200); exit; 
        }   
        if ( $postData['user_device_type'] == '') { 
            $this->response(array("status" => false, "message" => 'Device type field is required.'), 200); exit;
        }
        if ( $postData['user_dob'] == '') { 
            $this->response(array("status" => false, "message" => 'Date of birth field is required.'), 200); exit;
        } else if ( $userAge < 18 ) {
            $this->response(array("status" => false, "message" => 'You are below 18 years so you can\'t register.'), 200); exit;
        }   

        $status = "0";
        if ( $postData['user_type'] == ROLE_PARENTS ) {
            $user_type = 'Parent';
            $status    = "1" ;
        }
        if ( $postData['user_type'] == ROLE_CARE_DRIVER ) {
            $user_type = 'Care driver';            
        }
        if ( $postData['user_type'] == ROLE_TUTOR ) {
            $user_type = 'Tutor';            
        }
        if ( $postData['user_type'] == SCHOOL ) {
            $user_type = 'School';            
        }
        if ( $postData['user_type'] == ORGANIZATION ) {
            $user_type = 'Organization';            
        }
        
        // insert user record
        $userdata = array(
            'user_email'          => $postData['user_email'],
            'user_password'       => getHashedPassword(trim($postData['password'])),
            'user_name'           => $postData['full_name'],
            'user_phone_number'   => $postData['contact_no'],
            'user_birth_date'     => $postData['user_dob'],
            'user_status'         => $status,
            'user_device_id'      => $postData['user_device_id']   ? $postData['user_device_id']   : '',
            'user_device_type'    => $postData['user_device_type'] ? $postData['user_device_type'] : '',
            'roleId'              => $postData['user_type'],            
            'new_record_status' => "1",
            'notification_status' => "1"
            );

        try {     
            $this->db->db_debug = false;
            $user  = $this->db->insert(TB_USERS, $userdata);
            //echo $this->db->last_query(); exit();
            $error = $this->db->error(); //exit();
            if ( !empty($error['message']) ) {
                $this->response(array('status' => false, 'message' => $error['message']),300); 
                return false;    
            }
            $user_id = $this->db->insert_id();
        } catch ( Exception $ex ) {                  
            $this->response(array('status' => false, 'message' => $ex->getCode()),300); exit;
        } 

        // check role 
        if ( $user ) {
            // Send email & notification
            $userName = $postData['full_name'];
            $message  = "You have registered successfully as a ".$user_type; 
            $deviceId = $postData['user_device_id'];
            $myData   = array("message"=>"Hi $userName,<br/> Tulii admin will review your profile and active your account within 24hr.");
            $this->sendPushNotification($deviceId,$message,$myData,$user_type);

            $send = sendEmail($postData['user_email'],$userName,$message,"Tulii registration");
            $this->response(array('status' => true, 'message' => 'User has been created sucessfully', 'user_id' => $user_id));
        } else {
            $this->response(array("status" => false, "message" => "Something went wrong. !!"));
        }  
    }

    /* ========================== Register api is not use =================================== */ 

    /**
     * title: register  
     * @param email, mobile no, user type.
     * @return : json
     */
    public function register_post()
    {       
        $postData = $this->post();
        if($postData !='')
        {
            if($postData['user_email'] != '' && $postData['mobile_no'] != '' && $postData['user_type'] != '')
            {
                $isExist = $this->data_model->checkEmailExists($postData['user_email']);

                if(count($isExist) !=0)
                {
                    $this->response(array("status"=>false,"message"=> 'Email id already exists'), 300);exit;
                }else{

                    $isMobileExist = $this->common_model->select("user_phone_number",TB_USERS,array('user_phone_number'=>$postData['mobile_no']));
                    if(count($isMobileExist) != 0)
                    {
                        $this->response(array("status"=>false,"message"=> 'Mobile no already exists'), 300);exit;
                    } else {
                        if($postData['user_type'] == 2) 
                        {
                            $userdata = array(
                                'user_email' => $this->post('user_email'),
                                'user_password' => getHashedPassword(trim($this->post('password'))),
                                'user_name' => $this->post('user_name'),
                                'user_phone_number' => $this->post('mobile_no'),
                                'user_status' => '0',
                                'user_gender' => $this->post('gender') ? $this->post('gender') : '',
                                'user_address' => $this->post('address') ? $this->post('address') : '',
                                'user_no_of_kids' => $this->post('user_no_of_kids') ? $this->post('user_no_of_kids') : '',
                                'user_device_id' => $this->post('user_device_id') ? $this->post('user_device_id') : '',
                                'user_device_token' => $this->post('user_device_token') ? $this->post('user_device_token') : '',
                                'user_device_type' => $this->post('user_device_type') ? $this->post('user_device_type') : '',
                                'roleId' => 2,
                                );
                            $user = $this->db->insert(TB_USERS, $userdata);
                            $user_id = $this->db->insert_id();
                            if ($user_id) {
                                if ($this->post('user_no_of_kids') > 0 && $this->post('user_kids')) {
                                    foreach ($this->post('user_kids') as $kid) {
                                        $kids['kid_name'] = isset($kid['name']) ? $kid['name'] : '';
                                        $kids['kid_gender'] = isset($kid['gender']) ? $kid['gender'] : '';
                                        $kids['kid_birthdate'] = isset($kid['birthdate']) ? $kid['birthdate'] : '';
                                        $kids['kid_age'] = isset($kid['age']) ? $kid['age'] : '';
                                        $kids['kid_status'] = 1;
                                        $kids['user_id'] = $user_id;
                                        $kids_all[] = $kids;
                                    }
                                    $this->db->insert_batch(TB_KIDS, $kids_all);
                                }
                                $userName = $this->post('user_name');
                                $userEmail = $this->post('user_email');
                                $deviceId = $this->post('user_device_id');
                                $message  = "You have registered successfully as parent"; 
                                $myData   = array("message"=>"Hi $userName,<br/> Tulii admin will review your profile and active your account within 24hr.");
                                $this->sendPushNotification($deviceId,$message,$myData);
                                sendEmail($userEmail,$userName);
                                $this->response(array('status' => true, 'message' => 'User has been created sucessfully', 'user_id' => $user_id),200);exit;
                            } else {
                                $this->response(array("status" => false, "message" => "Something went wrong. !!"),300);exit;
                            }
                        }
                        else
                        {
                            $upload_car_pics = $licenseCopy= $certification =$car_pics="";
                            $scan_copy_of_license = "";
                            $upload_certification = "";
                            $temp = "";
                            $fileImages = $_FILES;
                            $allFiles = array();
                            if(!empty($fileImages)) {
                                $j =1;                          
                                foreach ($fileImages as $key => $value) {
                                    $cpt = count($_FILES[$key]['name']);
                                    if($j == 1)
                                        $temp = 'car_img';
                                    else if($j == 2)
                                        $temp = 'license_img';
                                    else
                                        $temp = 'certification_img';
                                    $files_name = array();
                                    for($i=0; $i<$cpt; $i++){
                                        $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
                                        $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
                                        $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
                                        $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
                                        $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
                                        $files_name[$i]['name'] = $_FILES[$temp]['name'];
                                        $files_name[$i]['type'] = $_FILES[$temp]['type'];
                                        $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                                        $files_name[$i]['error'] = $_FILES[$temp]['error'];
                                        $files_name[$i]['size'] = $_FILES[$temp]['size'];
                                    }
                                    $allFiles[$temp]=$files_name;
                                    $j++;                            
                                }
                                foreach ($allFiles as $key => $value) {
                                    $i = 0;                                
                                    for($j=0;$j < count($value); $j++) { 
                                        if (strcmp($key,'car_img') == 0 ) {                                   
                                            $upload_car_pics = multiple_image_upload($value[$j],'uploads/driver/carPics/'); 
                                            $car_pics .= "|".$upload_car_pics; 
                                        } else if (strcmp($key,'certification_img') == 0 ) {
                                            $upload_certification = multiple_image_upload($value[$j],'uploads/driver/certification/'); 
                                            $certification .= "|".$upload_certification; 
                                        } else if(strcmp($key,'license_img') == 0 ){
                                            $scan_copy_of_license = multiple_image_upload($value[$j],'uploads/driver/licenseCopy/'); 
                                            $licenseCopy .= "|".$scan_copy_of_license;                                         
                                        } else {}         
                                        $i++;                                                     
                                    }
                                }
                            }     
                            
                            $userdata = array(
                                'user_email' => $this->post('user_email'),
                                'user_password' => getHashedPassword(trim($this->post('password'))),
                                'user_name' => $this->post('user_name'),
                                'user_phone_number' => $this->post('mobile_no'),
                                'user_status' => '0',
                                'user_gender' => $this->post('gender') ? $this->post('gender') : '',
                                'user_address' => $this->post('address') ? $this->post('address') : '',
                                'roleId' => 3,
                                'user_birth_date' => $this->post('dob') ? $this->post('dob') : '',
                                'user_age' => $this->post('age') ? $this->post('age') : '', 
                                'is_your_own_car' => $this->post('own_car') ? $this->post('own_car') : '',
                                'driving_license_type' => $this->post('driver_license') ? $this->post('driver_license') : '', 
                                'license_no' => $this->post('license_no') ? $this->post('license_no') : '',
                                'valid_from_date' => $this->post('valid_from') ? $this->post('valid_from') : '',
                                'valid_until_date' => $this->post('valid_until') ? $this->post('valid_until') : '',
                                'car_model' => $this->post('car_model') ? $this->post('car_model') : '',
                                'car_register_no' => $this->post('car_reg_no') ? $this->post('car_reg_no') : '',
                                'school' => $this->post('school_name') ? $this->post('school_name') : '',
                                'degree' => $this->post('degree') ? $this->post('degree') : '',
                                'specialization' => $this->post('specialization') ? $this->post('specialization') : '',
                                'education_from' => $this->post('from_date') ? $this->post('from_date') : '',
                                'education_to' => $this->post('to_date') ? $this->post('to_date') : '',
                                'certification_name' => $this->post('certification_name') ? $this->post('certification_name') : '',
                                'user_device_id' => $this->post('user_device_id') ? $this->post('user_device_id') : '',
                                'user_device_token' => $this->post('user_device_token') ? $this->post('user_device_token') : '',
                                'user_device_type' => $this->post('user_device_type') ? $this->post('user_device_type') : '',
                                'license_pic'=>trim($licenseCopy,"|"),
                                'upload_certificate'=>trim($certification,"|"),
                                'car_pic'=>trim($car_pics,"|")
                                );                            
                            $user = $this->db->insert(TB_USERS, $userdata);
                            $user_id = $this->db->insert_id();
                            if ($user_id) {

                                $userName = $this->post('user_name');
                                $userEmail = $this->post('user_email');
                                $message  = "You have registered successfully as service provider"; 
                                $deviceId = $this->post('user_device_id');
                                $myData   = array("message"=>"Hi $userName,<br/> Tulii admin will review your profile and active your account within 24hr.");
                                $this->sendPushNotification($deviceId,$message,$myData);
                                $send = sendEmail($userEmail,$userName);
                               // print_r($send);die();
                                $this->response(array('status' => true, 'message' => 'Driver register has been created sucessfully', 'user_id' => $user_id),200);exit;
                            } else {
                                $this->response(array("status" => false, "message" => "Something went wrong. !!"),300);exit;
                            }
                        }
                    }
                }
            } else {
                $this->response(array("status"=>false,"message"=> 'Please add user_type, email and mobile no.'), 300);exit;
            }

        } else {
            $this->response(array("status"=>false,"message"=> 'Please fill data.'), 300);exit;
        }        
    }

    /**
     * Title : driver_update
     * Description : update driver details
     * 
     * @param Integer user_id
     * @return Array 
     */

    public function driver_update_post ()
    {
        // Input data
        $postData = $_POST;  

        // Checked user is authenticate or not 
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        }

        if ( $postData['user_id'] != "" && $postData['user_id'] > 0 ) {
            $user_id = $this->common_model->select("user_email, user_password, user_name, user_phone_number, user_gender, user_address, user_birth_date, user_age, is_your_own_car, driving_license_type, license_no, valid_from_date, valid_until_date, car_model, car_register_no, school, degree, specialization, education_from, education_to, certification_name, license_pic, upload_certificate, car_pic", TB_USERS, array('user_id' => $postData['user_id']));
            
            if(empty($user_id)) {
                $this->response(array("status"=>false,"message"=> 'User id does not exists'), 300);exit;
            } else {    
                $upload_car_pics      = "";
                $licenseCopy          = "";
                $certification        = "";
                $car_pics             = "";
                $scan_copy_of_license = "";
                $upload_certification = "";
                $temp                 = "";
                $fileImages           = $_FILES;
                $allFiles             = array();
                
                if ( !empty($fileImages) ) {
                    $j =1;            

                    foreach ($fileImages as $key => $value) {
                        $cpt = count($_FILES[$key]['name']);
                        if ( $j == 1 )
                            $temp = 'car_img';
                        else if ( $j == 2 )
                            $temp = 'license_img';
                        else
                            $temp = 'certification_img';
                        $files_name = array();

                        for ( $i=0; $i < $cpt; $i++ ) {
                            $_FILES[$temp]['name']      = $_FILES[$key]['name'][$i];
                            $_FILES[$temp]['type']      = $_FILES[$key]['type'][$i];
                            $_FILES[$temp]['tmp_name']  = $_FILES[$key]['tmp_name'][$i];
                            $_FILES[$temp]['error']     = $_FILES[$key]['error'][$i];
                            $_FILES[$temp]['size']      = $_FILES[$key]['size'][$i];    
                            $files_name[$i]['name']     = $_FILES[$temp]['name'];
                            $files_name[$i]['type']     = $_FILES[$temp]['type'];
                            $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                            $files_name[$i]['error']    = $_FILES[$temp]['error'];
                            $files_name[$i]['size']     = $_FILES[$temp]['size'];
                        }
                        $allFiles[$temp]=$files_name;
                        $j++;                            
                    }

                    foreach ( $allFiles as $key => $value ) {
                        $i = 0;      

                        for ( $j=0; $j < count($value); $j++ ) { 

                            if ( strcmp($key,'car_img') == 0 ) {                                   
                                $upload_car_pics = multiple_image_upload($value[$j],'uploads/driver/carPics/'); 
                                $car_pics .= "|".$upload_car_pics; 
                            } else if ( strcmp($key,'certification_img') == 0 ) {
                                $upload_certification = multiple_image_upload($value[$j],'uploads/driver/certification/'); 
                                $certification .= "|".$upload_certification; 
                            } else if(strcmp($key,'license_img') == 0 ){
                                $scan_copy_of_license = multiple_image_upload($value[$j],'uploads/driver/licenseCopy/'); 
                                $licenseCopy .= "|".$scan_copy_of_license;                                         
                            } else {}         
                            $i++;                                                     
                        }
                    }    
                } else {
                    $car_pics = $user_id[0]['car_pic'] ? $user_id[0]['car_pic'] :'';
                    $certification = $user_id[0]['upload_certificate'] ? $user_id[0]['upload_certificate'] :'';
                    $licenseCopy = $user_id[0]['license_pic'] ? $user_id[0]['license_pic'] :'';
                }
                
                // update driver details      
                $userdata = array(                    
                    'user_name'            => $this->post('user_name'),                    
                    'user_gender'          => $this->post('gender'),
                    'user_address'         => $this->post('address'),
                    'user_birth_date'      => $this->post('dob'),
                    'user_age'             => $this->post('age'), 
                    'is_your_own_car'      => $this->post('own_car'),
                    'driving_license_type' => $this->post('driver_license'), 
                    'license_no'           => $this->post('license_no'),
                    'valid_from_date'      => $this->post('valid_from'),
                    'valid_until_date'     => $this->post('valid_until'),
                    'car_model'            => $this->post('car_model'),
                    'car_register_no'      => $this->post('car_reg_no'),
                    'school'               => $this->post('school_name'),
                    'degree'               => $this->post('degree'),
                    'specialization'       => $this->post('specialization'),
                    'education_from'       => $this->post('from_date'),
                    'education_to'         => $this->post('to_date'),
                    'certification_name'   => $this->post('certification_name'),
                    'user_device_id'       => $this->post('user_device_id') ? $this->post('user_device_id') : '',
                    'user_device_token'    => $this->post('user_device_token') ? $this->post('user_device_token') : '',
                    'user_device_type'     => $this->post('user_device_type') ? $this->post('user_device_type') : '',
                    'license_pic'          => trim($licenseCopy,"|"),
                    'upload_certificate'   => trim($certification,"|"),
                    'car_pic'              => trim($car_pics,"|")
                    );
                $user = $this->db->update(TB_USERS, $userdata, array('user_id' => $this->post('user_id')));

                if ( $user ) {
                    $this->response(array("status" => true, "message" => 'Driver has been updated successfully'), 200); exit;
                } else {
                    $this->response(array("status" => false,"message" => 'Something went wrong'), 300); exit;
                }
            }
        } else {
            $this->response(array("status" => false, "message" => 'Please enter the user id.'), 300); exit;
        }
    }

    /** 
     * Title       : Login 
     * Description : User login credential
     *
     * @param  String user_email 
     * @param  String password
     * @return Array 
     */

    public function login_post ()
    {        
        // Post data
        $postData = $_POST;        
        // Checked user are authenticate
        $userArr = $this->login_model->login($postData["user_email"], $postData['password']);
        if ( count($userArr) > 0 ) {            

            // Checked user is active or not
            if ( $userArr[0]['user_status'] == '1' ) {

                // Checked user type              
                $user_token = md5(uniqid(rand(), true));   
                $cond       = array("user_email" => $postData["user_email"]);                    
                $userData   = $this->data_model->getAllUsers($cond);
                $this->session->set_userdata("userLoggedin", $userData[0]);
                $cond  = array("roleId" => $userData[0]['roleId']);
                $table = TB_ROLES;
                $roles = $this->data_model->select("roleId,role",$table,$cond);
                $userArr[0] += ['type' => $roles[0]['role']];

                if($roles[0]['roleId'] == 3)
                {
                    $cond  = array("category_id" => $userData[0]['user_category_id']);
                    $table = TB_CATEGORIES;
                    $usercategory = $this->data_model->select("category_id,category",$table,$cond);

                    if($usercategory[0]['category_id'] == 4 || $usercategory[0]['category_id'] == 1)
                    {
                        $cond = array("category_id" => $userData[0]['user_category_id']);
                        $table = TB_SUBCATEGORIES;
                        $usersubcategory = $this->data_model->select("sub_category_id,name",$table,$cond);

                        if ( isset($usercategory[0]['category']) && $usercategory[0]['category'] != '' ) {
                            $userArr[0] += ['category' => $usercategory[0]['category']];
                        } else {
                            $userArr[0] += ['category' => ''];
                        }
                        if ( isset($usersubcategory[0]['name']) && $usersubcategory[0]['name']!='' ) {
                            $userArr[0] +=['sub_category' => $usersubcategory[0]['name']];
                        } else {
                            $userArr[0] +=['sub_category' => ''];
                        }
                    }
                }                   
                
                $date = date("Y-m-d H:i:s");
                $user_token = md5(uniqid(rand(), true)); 
                $this->common_model->update(TB_USERS,array("user_id"=>$userArr[0]['user_id']),array('user_device_token'=>$user_token,'updated_at'=>$date));
                
                    // Checked device id                    
                if ( isset($postData["device_id"]) && $postData["device_id"] != "" ) {

                   $ssss= explode(",", $userArr[0]['user_device_id']);
                   if ( in_array($postData['device_id'], $ssss) ) {
                       $sql = $userArr[0]['user_device_id'];
                   } else {
                      $sql = $userArr[0]['user_device_id'].','.$postData["device_id"];
                  }                   	
                  $device_id = $userArr[0]['user_device_id'];
                  $this->common_model->update(TB_USERS, array("user_id" => $userArr[0]['user_id']), array('user_device_id' => $sql));
              }
              array_splice($userArr[0], 7, 1);
              $this->response(array("status" => true, "user_token" => $user_token, "message" => "You have successfully login.", "user" => $userArr[0]), 200); exit;
              
          } else {                
            $this->response(array("status" => false, "message" => "Your account is inactive."), 300); exit;
        }            
    } else {
        $this->response(array("status" => false, "message" => "Email or password is incorrect."), 300); exit;
    }
}

    /**
     * Title       : forgot_password
     * Description : Forgot user password 
     * 
     * @param      : String email
     * @return     : Array  
     */

    function forgot_password_post ()
    {
        // Post data
        $postData = $_POST;
        
        // Checked email address empty or not      
        if ( trim($postData["user_email"]) == "" ) {
            $this->response(array("status" => false, "message" => "Please enter email."), 300); exit;
        } else {            
            $email    = $postData['user_email'];
            $userdata = $this->common_model->select("user_id,user_name,user_status", TB_USERS, array("user_email" => trim($email)));
            
            if ( !count($userdata) ) {                
                $this->response(array("status" => false, "message" => "Please enter correct email address, this email address does not exist."), 300); exit;
            } else {

                if ($userdata[0]['user_status'] == '0' || $userdata[0]['user_status'] == "") {
                    $this->response(array("status" => false, "message" => "Your account is inactive, You need to contact to admin."), 300); exit;
                }
                $userId     =$userdata[0]['user_id'];
                $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';

                // Generate random value
                for ( $i = 0; $i < 8; $i++ ) {
                    $randstring .= $characters[rand(0, strlen($characters))];
                }
                
                // Updated password & password send to email                
                $update = $this->common_model->update(TB_USERS,array("user_id" => $userdata[0]['user_id']), array('user_password' => getHashedPassword($randstring)));
                if ($update) {
                    $hostname           = $this->config->item('hostname');
                    $usrname            = $userdata[0]['user_name'];
                    $config['mailtype'] ='html';
                    $config['charset']  ='iso-8859-1';
                    $this->email->initialize($config);
                    $from               = EMAIL_FROM; 
                    $this->messageBody  = email_header();
                    $this->messageBody  .= "Hello ".$usrname." 
                    <br/><br/>You recently requested to reset your password for your tulii account. Please refer this password.  
                    <br/>Password: <b>" . $randstring ."</b>
                    ";
                    
                    $this->messageBody  .= email_footer();
                    $this->email->from($from, $from);
                    $this->email->to("$email");
                    $this->email->subject('Your new Password');
                    $this->email->message($this->messageBody); 
                    $this->email->send();
                    $user_token = md5(uniqid(rand(), true));
                    $data       = array("user_token" => $user_token,"user_id" => $userId);
                    $this->response(array("status" => true, "result" => $data, "message" => "Your password has been sent to your email address. please check your email address."), 200); exit;
                } else {
                    $this->response(array("status" => false, "user_token" => $user_token, "message" => "Something went wrong !!!"), 300); exit;
                }    
            }
        }
    }

    /**
     * Title       : Change password
     * Description : Change user forgot password
     * 
     * @param  Integer userid
     * @param  String token 
     * @param  String new password
     * @return Array
     */

    function change_password_post () 
    {
        // Post data
        $postData = $_POST;

        // Checked user exist or not
        if ( $this->user_exists($postData["user_id"]) == 0 ) {
            $this->response(array("status" => false, "message" => 'User id is not exists'), 300); exit;
        }

        $cond_check  = array('user_id' => $postData['user_id']);
        $currentUser = $this->login_model->validUser(TB_USERS,'user_password,user_name', $cond_check);

        if( !empty($currentUser) ) {
            if ( verifyHashedPassword(trim($postData['old_pwd']), $currentUser[0]['user_password']) ) {
                if( count($currentUser) == 0 ) {
                    $this->response(array("status" => false, "message" => 'You have entered incorrect old password.'), 300 ); exit;
                }            
                
                // Updated password
                $cond       = array("user_id" => $postData['user_id']);
                $updateData = array("user_password" => getHashedPassword($postData['new_pwd']));
                $result     = $this->common_model->update(TB_USERS, $cond, $updateData);

                // Result 
                if ( $result ) {
                    $this->response(array("status" => true, "message" => 'Password has been updated successfully.'), 200); exit;
                } else {
                    $this->response(array("status" => false, "message" => 'Error occured during updating Password.'), 300); exit;
                } 
            } else {
                $this->response(array("status" => false, "message" => 'Old password and new password should not be same.'), 300); exit;
            }
        }        
    }

    /**     
     * Title        : getProfile
     * Description  : user details
     * 
     * @param  Integer user_id
     * @return Array
     */

    function getProfile_post () 
    {
        // Post data
        $postData = $_POST;

        // Checked user authenticate
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }

        // Checked user exist or not
        if ( $this->user_exists($postData["user_id"]) == 0 ) {
            $this->response(array("status" => false,"message" => 'User is not exists.'), 300); exit;
        }

        // User details
        if ( $postData["user_id"] != '' ) {
            $cond                       = array("user_id" => $postData["user_id"]);
            $userData                   = $this->common_model->select("user_pic,roleId,user_id,user_name,user_gender,user_device_id,user_address,user_age,user_birth_date,user_no_of_kids,user_email,user_type,user_phone_number,is_your_own_car,driving_license_type,license_no,valid_from_date,valid_until_date,car_model,car_register_no,car_seating_capacity,car_manufacturing_year,car_age,school,degree,specialization,education_from,education_to,certification_name,license_pic,car_pic,upload_certificate",TB_USERS,$cond);
            $link                       = base_url().USER_PROFILE_IMG.$userData[0]['user_pic'];
            $data['user_id']            = $userData[0]['user_id']           ? $userData[0]['user_id']           : 'N/A';
            $data['role_id']            = $userData[0]['roleId']            ? $userData[0]['roleId']            : 'N/A';
            $data['user_name']          = $userData[0]['user_name']         ? $userData[0]['user_name']         : 'N/A';
            $data['user_email']         = $userData[0]['user_email']        ? $userData[0]['user_email']        : 'N/A';
            $data['user_type']          = $userData[0]['user_type']         ? $userData[0]['user_type']         : 'N/A';
            $data['user_phone_number']  = $userData[0]['user_phone_number'] ? $userData[0]['user_phone_number'] : 'N/A';            
            $data['user_birth_date']    = $userData[0]['user_birth_date']   ? $userData[0]['user_birth_date']   : 'N/A';
            $data['user_age']           = $userData[0]['user_age']          ? $userData[0]['user_age']          : 'N/A';
            $data['user_gender']        = $userData[0]['user_gender']       ? $userData[0]['user_gender']       : 'N/A';
            $data['user_address']       = $userData[0]['user_address']      ? $userData[0]['user_address']      : 'N/A';
            $data['user_device_id']     = $userData[0]['user_device_id']    ? $userData[0]['user_device_id']    : 'N/A';
            $data['user_pic']           = $link;

            // Parent details
            if ( $userData[0]['roleId'] == ROLE_PARENTS ) {
                $data['user_no_of_kids'] = $userData[0]['user_no_of_kids'] ? $userData[0]['user_no_of_kids'] : '';
                $kidData                 = $this->common_model->select("kid_id,kid_name,kid_gender,kid_birthdate, YEAR( CURDATE( ) ) - YEAR( kid_birthdate ) AS kid_age",TB_KIDS,array("user_id" => $postData["user_id"]));
                
                // Children detailsd
                if (count($kidData)>0) {
                    
                    foreach ( $kidData as $key => $value ) {
                        $kidInfo[] = array(
                            'kid_id'        => $value['kid_id'],
                            'kid_name'      => $value['kid_name'],
                            'kid_gender'    => $value['kid_gender'], 
                            'kid_birthdate' => $value['kid_birthdate'],                 
                            'kid_age'       => $value['kid_age']                               
                            ); 
                    }
                    $data['kidsDetails'] = $kidInfo;
                } else {
                    $data['kidsDetails'] = array();
                }
            } elseif( $userData[0]['roleId'] == ROLE_CARE_DRIVER ) { 
                // Driver details
                $data["is_your_own_car"]        = $userData[0]['is_your_own_car']        ? $userData[0]['is_your_own_car']        : 'N/A';
                $data["driving_license_type"]   = $userData[0]['driving_license_type']   ? $userData[0]['driving_license_type']   : 'N/A';
                $data["license_no"]             = $userData[0]['license_no']             ? $userData[0]['license_no']             : 'N/A';
                $data["valid_from_date"]        = $userData[0]['valid_from_date']        ? $userData[0]['valid_from_date']        : 'N/A';
                $data["valid_until_date"]       = $userData[0]['valid_until_date']       ? $userData[0]['valid_until_date']       : 'N/A';
                $data["car_model"]              = $userData[0]['car_model']              ? $userData[0]['car_model']              : 'N/A';
                $data["car_register_no"]        = $userData[0]['car_register_no']        ? $userData[0]['car_register_no']        : 'N/A';
                $data["car_seating_capacity"]   = $userData[0]['car_seating_capacity']   ? $userData[0]['car_seating_capacity']   : 'N/A';
                $data["car_manufacturing_year"] = $userData[0]['car_manufacturing_year'] ? $userData[0]['car_manufacturing_year'] : 'N/A';
                $data["car_age"]                = $userData[0]['car_age']                ? $userData[0]['car_age']                : 'N/A';
                $data["school"]                 = $userData[0]['school']                 ? $userData[0]['school']                 : 'N/A';
                $data["degree"]                 = $userData[0]['degree']                 ? $userData[0]['degree']                 : 'N/A';
                $data["specialization"]         = $userData[0]['specialization']         ? $userData[0]['specialization']         : 'N/A';
                $data["education_from"]         = $userData[0]['education_from']         ? $userData[0]['education_from']         : 'N/A';
                $data["education_to"]           = $userData[0]['education_to']           ? $userData[0]['education_to']           : 'N/A';
                $data["certification_name"]     = $userData[0]['certification_name']     ? $userData[0]['certification_name']     : 'N/A';

                // License images
                $licenseArr = explode("|", $userData[0]['license_pic']);                
                if ( !empty($licenseArr[0]) ) {
                    foreach ($licenseArr as $lkey => $lvalue) {
                        $license[$lkey]['license_images'] = base_url().'uploads/driver/licenseCopy/'.$lvalue;
                    }
                } else {
                    $license = array();
                }

                // Car images
                $carArr = explode("|", $userData[0]['car_pic']);
                if ( !empty($carArr[0]) ) {
                    foreach ($carArr as $ckey => $carValue) {
                        $carpics[$ckey]['car_images'] = base_url().'uploads/driver/carPics/'.$carValue;
                    }
                } else {
                    $carpics = array();
                }

                // Certificate images
                $certificateArr = explode("|", $userData[0]['upload_certificate']);
                if( !empty($certificateArr[0]) ) {
                    foreach ( $certificateArr as $cetkey => $certifValue ) {
                        $certification[$cetkey]['certification_images'] = base_url().'uploads/driver/certification/'.$certifValue;
                    }
                } else {
                    $certification = array();
                }
                
                $data["license_pics"]     = $license; 
                $data["car_pics"]         = $carpics;                                               
                $data["certificate_pics"] = $certification;
            } 

            // Result of user detail
            $this->response(array("status" => true, "user_details" => $data), 200); exit;
        }
    }

    /**
     * Title        : save_profilePic
     * Description  : Upload user profile picture
     *
     * @param  Integer user_id
     * @return Array
     */

    public function save_profilePic_post ()
    { 
        // Post data
        $postData = $_POST;

        // Checked user authenticate
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }

        // Checked user exist or not
        if ( $this->user_exists($postData["user_id"]) == 0 ) {
            $this->response(array("status" => false,"message" => 'User is not exists.'), 300); exit;
        }
        
        $user_id = $postData["user_id"];

        // Upload user picture
        if ( isset($_FILES['user_pic']['name']) ) {   

            if ( isset($_FILES['user_pic']) && !empty($_FILES['user_pic']) ) {
                $result = upload_image($_FILES['user_pic'], './' . USER_PROFILE_IMG);
            }        

            if ( isset($result['status']) == "error" ) {
                $this->response(array("status" => false, "message" => strip_tags($result['message'])), 300); exit;               
            } else {
                $profile_data['user_pic'] = $result['filename'];
            }            
        } else {
            $this->response(array("status" => false,"message" => 'User picture do not empty.'), 300); exit;
        }

        // Update user profile picture
        if ( !empty($postData) ) {    

            $where = array("user_id" => $user_id);            
            $update_profile = $this->common_model->update(TB_USERS, $where, $profile_data);

            if ( $update_profile ) {
                $link = base_url().USER_PROFILE_IMG.$profile_data['user_pic'];                
                $arr_result[] = array(                         
                    'message'   => 'User profile pic uploaded successfully.',
                    'url'       => $link,
                    'user_id'   => $user_id
                    );
                // Result of user profile
                $this->response(array("status" => true,'result' => $arr_result), 200); exit;                
            } else {
                $this->response(array("status" => false, "message" => 'Something went wrong !!!'), 300); exit;
            }            
        } else {
            $this->response(array("status" => false, "message"=> 'User data can not empty!!!'), 300); exit;
        }        
    }

    /**
     * Title        : user_exists
     * Description  : Check if user exists
     *   
     * @param  Integer user_id
     * @return Integer   
     */

    public function user_exists ( $user_id )
    { 
        // Check user available or not
        $cond        = array("user_id" => $user_id, "user_status" => "1");
        $exist_users = $this->data_model->getAllUsers($cond); 

        // If user available return 1 otherwise 0
        if ( count($exist_users) > 0 ) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Title        : services_list
     * Description  : List of category & sub-category
     *   
     * @return Array
     */

    function services_list_get ()
    {
        // Checked user is authenticate
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }

        // Get category & sub-category data
        $resultCategory     = $this->common_model->select("category_id as id, category as category_name", TB_CATEGORIES, array("status" => 1));
        $resultSubCategory  = $this->common_model->select("sub_category_id as id, name as sub_category_name", TB_SUBCATEGORIES, array("status" => 1));
        $data               = array("category" => $resultCategory,"sub_categories" => $resultSubCategory);

        // Result llst of category & sub-category
        $this->response(array('status' => true, 'result' => $data), 200); exit;
    }

    /**
     * Edit user
     * Added by Rajendra pawar
     */
    public function user_edit_post()
    {
        $data = $_POST;
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }
        if(isset($data['user_id']) && $data['user_id'] > 0) {        
            $result=$this->common_model->select_join("user_id,role",TB_USERS,array("user_id"=>trim($data['user_id'])),array(),array(),array(TB_ROLES=>'tbl_users.roleId = tbl_roles.roleId'),null);
            if(!count($result)) {
                $this->response(array('status' => false, 'message' => 'Invalid user'), 300);exit;
            } else {                   
                //check email 
                $emailExist = $this->common_model->select_join("user_email",TB_USERS,array("user_email"=>trim($this->post('user_email')),"user_id !="=> $this->post('user_id')),array(),array(),array(),null); 
                if ($emailExist) {
                    $this->response(array("status" => false, "message" => "Duplicate email!!!"),300);exit;
                }           

                $userdata = array(
                    'user_name' => $this->post('user_name'),
                    'user_gender' => $this->post('gender'),
                    'user_address' => $this->post('address'),
                    'user_email' => $this->post('user_email'),                
                    'user_phone_number' => $this->post('mobile_no')
                    );

                try {
                 $this->common_model->update(TB_USERS,array("user_id"=>$data['user_id']),$userdata);
             } catch (Exception $ex) {
                $this->response(array('status' => false, 'message' => $ex->getCode()),300);exit;
            }
            
            $this->response(array('status' => true, 'message' => 'User updated successfully.'), 200);exit;
        }
    } else {
        $this->response(array('status' => false, 'message' => 'Provide user_id'), 300);exit;
    }
}

    /**
     * Title: kid_edit
     * Description : update kids details
     * 
     * @param String kid_name
     * @param String kid_gender
     * @param Integer user_id
     * @param Date dob
     * @return Array 
     */

    public function kid_edit_post ()
    {
        // Input data
        $data = $_POST;
        // Checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        }
        
        // Input validation 
        if ( $this->post('kid_name') == "" ) {
            $this->response(array("status" => "error", "message" => 'Child name field is required.'), 200); exit; 
        }    
        if ( $this->post('kid_gender') == "" ) {
            $this->response(array("status" => "error", "message" => 'Gender field is required.'), 200); exit; 
        }    
        if ( $this->post('user_id') == "" ) { 
            $this->response(array("status" => "error", "message" => 'Parent id field is required.'), 200); exit; 
        }    
        if ( $this->post('dob') == "" ) { 
            $this->response(array("status" => "error", "message" => 'Child date of birth field is required.'), 200); exit; 
        }    
        
        $dateOfBirth = $this->post('dob');
        $today       = date("Y-m-d");
        $diff        = date_diff(date_create($dateOfBirth), date_create($today));
        $age         = $diff->format('%y');
        if ( 5 > $age || 15 < $age ) { 
            $this->response(array("status" => false, "message" => 'Child age should be 5 to 15 years allowed.'), 200); exit; 
        }    
        
        // Update kids details
        $kidData = array(
            'kid_name'     => $this->post('kid_name'),
            'kid_gender'   => $this->post('kid_gender'),
            'kid_age'      => $age." Year",
            'user_id'      => $this->post('user_id'),   
            'kid_birthdate'=> $this->post('dob'),     
            'kid_status'   => 1,
            );

        if ( isset($data['user_id']) && $data['user_id'] > 0 ) {            
            $result = $this->common_model->select_join("user_id, role", TB_USERS, array("user_id" => trim($data['user_id'])), array(), array(), array(TB_ROLES => 'tbl_users.roleId = tbl_roles.roleId'), null);
            
            if ( !count($result) ) {
                $this->response(array('status' => false, 'message' => 'Invalid user'), 300); exit;
            }
            
            if ( isset($data['kid_id']) && $data['kid_id'] > 0 ) {
                try {
                 $this->common_model->update(TB_KIDS, array("kid_id" => $data['kid_id']), $kidData);
                 $this->response(array('status' => true, 'message' => 'Kid updated successfully.'), 200); exit;
             } catch (Exception $ex) {
                $this->response(array('status' => false, 'message' => $ex->getCode()),300); exit;
            }
        } else {
            try {
                $this->common_model->insert(TB_KIDS, $kidData);
                $this->response(array('status' => true, 'message' => 'Kid saved successfully.'), 200); exit;
            } catch (Exception $ex) {
                $this->response(array('status' => false, 'message' => $ex->getCode()),300); exit;
            }
        }
    } else {
     $this->response(array('status' => false, 'message' => 'Provide user_id'), 300); exit; 
 }
}

    /**
     * Title : validate_booking
     * Description : Booking a ride validation
     *
     * @param type $post
     * @return Array
     */

    function validate_booking ( $post ) 
    {        
        $bookingdata = [];
        $duration    = array(DAILY,WEEKLY,MONTHLY);
        $today       = date('Y-m-d');
        if ( $today > trim($post['start_date']) ) {
            $this->response(array('status' => false, 'message' => 'Your can book today onwards.'), 300); exit;
        } 
        if ( trim($post['start_date']) == "" ) {
            $this->response(array('status' => false, 'message' => 'Please enter duration daily or weekly or monthly'), 300); exit;
        }
        if ( trim($post['duration']) == "" ) {
            $this->response(array('status' => false, 'message' => 'Please enter duration daily or weekly or monthly'), 300); exit;
        }
        if ( !in_array(trim($post['duration']),$duration) ) {
            $this->response(array('status' => false, 'message' => $post['duration'].' Invalid duration'), 300); exit;
        }
        if ( trim($post['booking_service_type']) == MY_PREFERENCE && (!isset($post['booking_my_preference_service_users']) || !is_array($post['booking_my_preference_service_users'])) ) {
            $this->response(array('status' => false, 'message' => 'You must provide an booking_my_preference_service_users in array format.'), 300); exit;
        }
        if ( trim($post['booking_service_type']) == MY_MATCHES && (isset($post['booking_service_user']) && $post['booking_service_user'] < 1) ) {
            $this->response(array('status' => false, 'message' => 'You must provide an booking_service_user.'), 300); exit;
        }
        if ( $post['duration'] == DAILY && strtotime($post['date']) < strtotime(date('Y-m-d')) ) {
            $this->response(array('status' => false, 'message' => 'Please enter date greater than today and date format is Y-m-d(2018-01-31)'), 300); exit;
        }
        if ( ($post['duration'] == WEEKLY || $post['duration'] == MONTHLY) && ((strtotime($post['start_date']) < strtotime(date('Y-m-d'))) || (strtotime($post['start_date']) > strtotime($post['end_date'])) || (strtotime($post['end_date']) < strtotime($post['start_date']))) ) {
            $this->response(array('status' => false, 'message' => 'Please enter valid start and end date and date format is Y-m-d(2018-01-31)'), 300);exit;
        }
        $user = $this->common_model->select("user_status, user_type, roleId", TB_USERS,array('user_id' => $post['user_id']));
        if ( !$user ) {
            $this->response(array('status' => false, 'message' => 'Invalid user'), 300); exit;
        }

        $service_provider = $this->common_model->select("user_id",TB_USERS,array('user_id' => $this->post('service_provider_user_id'), 'roleId' => ROLE_SERVICE_PROVIDER, 'user_status' => '1'));
        
        if ( !$service_provider ) {
            $this->response(array('status' => false, 'message' => 'Invalid service provider, please enter valid service_provider_user_id.'), 300); exit;
        }

        if ( $user[0]['user_status'] != 1 ) {
            $this->response(array('status' => false, 'message' => 'User not active'), 300); exit;
        }
        if ( $user[0]['roleId'] != USER_TYPE_PARENT ) {
            $this->response(array('status' => false, 'message' => 'User type not parent'), 300); exit;
        }

        $catdata = $this->common->validate_category($post);
        if ( trim($post['duration']) == DAILY ) {
            $bookingdata['booking_date']       = date('Y-m-d', strtotime($post['date']));
            $bookingdata['booking_total_days'] = 1;
        } else {
            $bookingdata['booking_start_date'] = date('Y-m-d', strtotime($post['start_date']));
            $bookingdata['booking_end_date']   = date('Y-m-d', strtotime($post['end_date']));
            $start_date                        = $post['start_date'];
            $datetime1                         = new DateTime($post['start_date']);
            $datetime2                         = new DateTime($post['end_date']);
            $interval                          = date_diff($datetime1, $datetime2);
            
            $bookingdata['booking_total_days'] = $interval->days;
            if ($post['duration'] == 'weekly') {
                $bookingdata['booking_total_weeks'] = ceil(($interval->days) / 7);
            }
            if ($post['duration'] == 'monthly') {
                $bookingdata['booking_total_moths'] = ($interval->format('%m')) + 1;
            }
        }
        
        $bookingdata['booking_pick_up_location']  = $post['pick_up_location'];
        $bookingdata['pick_up_location_lat']      = $post['pick_up_location_lat'];
        $bookingdata['pick_up_location_lng']      = $post['pick_up_location_lng'];
        $bookingdata['booking_drop_off_location'] = $post['drop_off_location'];
        $bookingdata['drop_off_location_lat']     = $post['drop_off_location_lat'];
        $bookingdata['drop_off_location_lng']     = $post['drop_off_location_lng'];
        $bookingdata['booking_pick_up_time']      = $post['pick_up_time'];
        $bookingdata['booking_drop_off_time']     = $post['drop_off_time'];
        $bookingdata['booking_category_id']       = $catdata['category_id'];
        $bookingdata['booking_sub_category_id']   = isset($catdata['sub_category_id']) ? $catdata['sub_category_id'] : '';
        // $bookingdata['booking_secret_code']    = $post['secret_code'];
        $bookingdata['booking_secret_code']       = '';
        $bookingdata['booking_duration']          = $post['duration'];
        $bookingdata['user_id']                   = $post['user_id'];
        $booking_service_type                     = json_decode(SERVICE_TYPES, true);
        $bookingdata['booking_service_type']      = $post['booking_service_type'] && in_array($post['booking_service_type'], $booking_service_type) ? $post['booking_service_type'] : PLACE_REQUEST;
        $bookingdata['booking_no_off_kids']       = count($post['child_id']);
        
        return $bookingdata;
    }

     /**
     * Title : booking
     * Description : user book a ride
     *
     * @param Array user_kids['name']
     * @param Array user_kids['gender']
     * @param Array user_kids['age']
     * @param Date start_date
     * @param Date end_date
     * @param String pick_up_location
     * @param String drop_off_location
     * @param Time pick_up_time
     * @param Time drop_off_time
     * @param Integer category
     * @param Integer sub_category
     * @param String secret_code
     * @param String duration
     * @param String booking_service_type
     * @param Integer user_id
     * @param Integer booking_service_user
     * @param Integer booking_my_preference_service_users
     * @param Integer service_provider_user_id
     * @param Date date
     * @param Double pick_up_location_lat
     * @param Double pick_up_location_lng
     * @param Double drop_off_location_lat
     * @param Double drop_off_location_lng
     * @return Array
     */

     function booking_post () 
     {
        // Input data
        $data = $this->post();  

        // checked user is authenticate
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        }      

        // booking input field validation
        $bookingdata = $this->validate_booking($data);
        
        if ( $bookingdata ) {

            // Insert ride booking details
            $booking = $this->db->insert(TB_BOOKINGS, $bookingdata);
            $booking_id = $this->db->insert_id();

            if ( $booking_id ) {                
                $booking_service_user = '';
                if ( $this->post('booking_service_type') == MY_PREFERENCE ) {
                    $booking_service_user = $this->post('booking_my_preference_service_users');
                }
                if ( $this->post('booking_service_type') == MY_MATCHES ) {
                    $booking_service_user = $this->post('booking_service_user');
                }

                // kids info details insert
                if ( $this->post('child_id') ) {

                    $user_kids = $this->post('child_id');
                    $kids_all  = array(
                        'kids_id'    => isset($user_kids) ? $user_kids : '',
                        'booking_id' => $booking_id
                        );               
                    $this->db->insert(TB_KIDS_BOOKING, $kids_all);
                }

                $respone['booking_status']['success']    = 'Booking has been saved successfully';
                $respone['booking_status']['booking_id'] = $booking_id;               
                
                // ride request insert
                $service_request = array(
                    "service_booking_id"       => $booking_id,
                    "service_parent_user_id"   => $this->post('user_id'),
                    "service_provider_user_id" => $this->post('service_provider_user_id'),
                    "service_request_status"   => PENDING
                    );
                $result = $this->db->insert(TB_SERVICE_REQUESTS, $service_request);

                if (!$result) {
                    $respone['message'] = 'Request not send to driver, please try again!!!';
                } else {
                    $userData  = $this->common_model->select('user_id,user_name',TB_USERS,array('user_id' => $this->post('user_id')));
                    $userName  = isset($userData[0]['user_name'])?$userData[0]['user_name']:"Someone";
                    $message   = $userName." has booked a ride"; 
                    $service_provider_device_id = $this->common_model->select("user_device_id, roleId", TB_USERS, array("user_id" => $this->post('service_provider_user_id')));
                    $device_id = isset($service_provider_device_id[0]['user_device_id'])?$service_provider_device_id[0]['user_device_id']:"";
                    $myData    = array("message" => "Booking the ride please check it.");
                    $this->sendPushNotification($device_id, $message, $myData, $service_provider_device_id[0]['roleId']);

                    $checkAlreadyMyPref = $this->common_model->select("mp_id, trip_count", TB_MY_PREFERENCE, array("parent_id" => $this->post('user_id'), "driver_id" => $this->post('service_provider_user_id')));
                    if ( count($checkAlreadyMyPref) > 0 ) {
                        $my_preference = array(
                            "trip_count" => $checkAlreadyMyPref[0]['trip_count']+1,
                            "updated_at" => date('Y-m-d H:i:s')
                            );
                        $result = $this->common_model->update(TB_MY_PREFERENCE,array("parent_id"=>$this->post('user_id'),"driver_id"=>$this->post('service_provider_user_id')),$my_preference);
                    } else {
                        $my_preference = array(
                            "parent_id"  => $this->post('user_id'),
                            "driver_id"  => $this->post('service_provider_user_id'),
                            "trip_count" => 1,
                            "created_at" => date('Y-m-d H:i:s')
                            );
                        $result = $this->db->insert(TB_MY_PREFERENCE, $my_preference);
                    }                    
                    $respone['message'] = 'Request has been sent successfully';
                } 
                $this->response(array('status' => true, 'message' => $respone), 200); exit;
            }
        }
        $this->response(array('status' => false, 'message' => 'Some error has been occurred!!!'), 300); exit;
    }

    /**
     * Title : my_bookings
     * Description : booking list show
     *
     * @param  Integer user_id
     * @return Array 
     */

    function my_bookings_post() 
    {
        // Checked user is authenticate or not
        if(is_authorized()) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        }   

        // Input data
        $user_id = $_POST['user_id'];
        
        if ( $user_id > 0 ) {
            $post    = $this->post();       
            $catdata = $this->common->validate_category($this->post());
            $cond    = ['bookings.booking_category_id' => $catdata['category_id']];
            if ( isset($catdata['sub_category_id']) && $catdata['sub_category_id'] > 0 ) {
                $cond['bookings.booking_sub_category_id'] = $catdata['sub_category_id'];
            }
            $user = $this->common_model->select("user_status, user_type, roleId", TB_USERS, array('user_id' => $post['user_id']));
            
            if ( count($user) > 0 ) {
                if ( $user[0]['roleId'] == USER_TYPE_SERVICE_PROVIDER ) {
                    $like       = array();
                    $conds      = array(TB_BOOKINGS.".user_id" => $user_id,"booking_status" => $this->post('booking_status'));
                    $select     = TB_BOOKINGS . ".*,".TB_BOOKINGS.".user_id, kid_id, kid_name, kid_gender, kid_birthdate,".TB_CATEGORIES.".category,".TB_SUBCATEGORIES.".name,kid_age,kid_status";
                    $table_name = TB_BOOKINGS;
                    $join = array(
                        TB_CATEGORIES    => TB_CATEGORIES . '.category_id=' . TB_BOOKINGS . '.booking_category_id',
                        TB_SUBCATEGORIES => TB_SUBCATEGORIES . '.sub_category_id=' . TB_BOOKINGS . '.booking_sub_category_id',
                        TB_KIDS          => TB_KIDS . '.user_id=' . TB_BOOKINGS . '.user_id',
                        TB_KIDS_BOOKING  => TB_KIDS_BOOKING . '.booking_id=' . TB_BOOKINGS . '.booking_id'
                        );
                    $default_order_column = "created_at";
                    $service_requests     = $this->common_model->getRowsPerPage($select, $table_name, $conds, $like, array(), '', $join);
                    
                    // Show list of booking details
                    foreach ( $service_requests as $key => $service_request ) {
                        $data[$key]['booking_id']                = $service_request['booking_id'];
                        $data[$key]['booking_duration']          = $service_request['booking_duration'];
                        $data[$key]['booking_date']              = $service_request['booking_date'];
                        $data[$key]['booking_total_days']        = $service_request['booking_total_days'];
                        $data[$key]['booking_pick_up_location']  = $service_request['booking_pick_up_location'];
                        $data[$key]['pick_up_location_lat']      = $service_request['pick_up_location_lat'];
                        $data[$key]['pick_up_location_lng']      = $service_request['pick_up_location_lng'];
                        $data[$key]['booking_drop_off_location'] = $service_request['booking_drop_off_location'];
                        $data[$key]['drop_off_location_lat']     = $service_request['drop_off_location_lat'];
                        $data[$key]['drop_off_location_lng']     = $service_request['drop_off_location_lng'];
                        $data[$key]['booking_pick_up_time']      = $service_request['booking_pick_up_time'];
                        $data[$key]['booking_drop_off_time']     = $service_request['booking_drop_off_time'];
                        $data[$key]['booking_category_id']       = $service_request['booking_category_id'];
                        $data[$key]['booking_sub_category_id']   = $service_request['booking_sub_category_id'];
                        $data[$key]['booking_no_off_kids']       = $service_request['booking_no_off_kids'];
                        $data[$key]['booking_price']             = $service_request['booking_price'];
                        $data[$key]['booking_total_weeks']       = $service_request['booking_total_weeks'];
                        $data[$key]['booking_total_moths']       = $service_request['booking_total_moths'];
                        $data[$key]['booking_status']            = $service_request['booking_status'];
                        $data[$key]['user_id']                   = $service_request['user_id'];
                        $data[$key]['booking_service_type']      = $service_request['booking_service_type'];
                        $data[$key]['created_at']                = $service_request['created_at'];
                        $data[$key]['category']                  = $service_request['category'];
                        $data[$key]['sub_category']              = $service_request['name'];
                        $data[$key]['kid_id']                    = $service_request['kid_id'];
                        $data[$key]['kid_name']                  = $service_request['kid_name'];
                        $data[$key]['kid_gender']                = $service_request['kid_gender'];
                        $data[$key]['kid_birthdate']             = $service_request['kid_birthdate'];
                        $data[$key]['kid_age']                   = $service_request['kid_age'];
                    }
                } else {
                    $cond['user_id']                 = $user_id;
                    $cond['bookings.booking_status'] = $this->post('booking_status');
                    
                    $like       = array();
                    $conds      = array(TB_BOOKINGS.".user_id" => $user_id, "booking_status" => $this->post('booking_status'));
                    $select     = TB_BOOKINGS . ".*,".TB_BOOKINGS.".user_id,kid_id,kid_name,kid_gender,kid_birthdate,kid_age,kid_status";
                    $table_name = TB_BOOKINGS;
                    $join = array(
                        TB_KIDS         => TB_KIDS . '.user_id=' . TB_BOOKINGS . '.user_id',
                        TB_KIDS_BOOKING => TB_KIDS_BOOKING . '.booking_id=' . TB_BOOKINGS . '.booking_id'
                        );
                    
                    // Fetch records
                    $default_order_column = "created_at";
                    $service_requests = $this->common_model->getRowsPerPage($select,$table_name,$conds,$like,array(),'',$join);
                    
                    // show list of data 
                    $data = array();
                    foreach ($service_requests as $key => $service_request) {
                        $data[$key]['booking_id']                = $service_request['booking_id'];
                        $data[$key]['booking_duration']          = $service_request['booking_duration'];
                        $data[$key]['booking_date']              = $service_request['booking_date'];
                        $data[$key]['booking_total_days']        = $service_request['booking_total_days'];
                        $data[$key]['booking_pick_up_location']  = $service_request['booking_pick_up_location'];
                        $data[$key]['pick_up_location_lat']      = $service_request['pick_up_location_lat'];
                        $data[$key]['pick_up_location_lng']      = $service_request['pick_up_location_lng'];
                        $data[$key]['booking_drop_off_location'] = $service_request['booking_drop_off_location'];
                        $data[$key]['drop_off_location_lat']     = $service_request['drop_off_location_lat'];
                        $data[$key]['drop_off_location_lng']     = $service_request['drop_off_location_lng'];
                        $data[$key]['booking_pick_up_time']      = $service_request['booking_pick_up_time'];
                        $data[$key]['booking_drop_off_time']     = $service_request['booking_drop_off_time'];
                        $data[$key]['booking_category_id']       = $service_request['booking_category_id'];
                        $data[$key]['booking_sub_category_id']   = $service_request['booking_sub_category_id'];
                        $data[$key]['booking_no_off_kids']       = $service_request['booking_no_off_kids'];
                        $data[$key]['booking_price']             = $service_request['booking_price'];
                        $data[$key]['booking_total_weeks']       = $service_request['booking_total_weeks'];
                        $data[$key]['booking_total_moths']       = $service_request['booking_total_moths'];
                        $data[$key]['booking_status']            = $service_request['booking_status'];
                        $data[$key]['user_id']                   = $service_request['user_id'];
                        $data[$key]['booking_service_type']      = $service_request['booking_service_type'];
                        $data[$key]['created_at']                = $service_request['created_at'];
                        $data[$key]['kid_id']                    = $service_request['kid_id'];
                        $data[$key]['kid_name']                  = $service_request['kid_name'];
                        $data[$key]['kid_gender']                = $service_request['kid_gender'];
                        $data[$key]['kid_birthdate']             = $service_request['kid_birthdate'];
                        $data[$key]['kid_age']                   = $service_request['kid_age'];
                    }
                }

                $result['booking_details'] = $data;
                $this->response(array('status' => true, 'result' => $result),200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Invalid user_id'), 300); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please provide user_id'), 300); exit;
        }
    }


    /**
     * accept_or_cancel_get
     * @param type $service_request_id
     * @param type $status
     */
   /* function accept_or_cancel_post() {
        $service_request_id = $_POST['service_request_id'];
        if ($service_request_id > 0) {
            // Check service request is exist or not 
            
            $service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id));
            
            if (!$service_requests) {
                $this->response(array('status' => false, 'message' => 'Service request not available'), 300);exit;
            }

            //start service request
            if ($_POST['status'] == 'ACCEPTED') {
                //Check that booking already started by anouther one 

                $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id,'service_request_status'=>'IN_PROCESS'));

                //Check another service not started in that duration
                //hold for the moment
                $validbooking = 1;
                if ($validbooking) {
                    if (count($is_service_requests) < 1) {
                        //Start that request
                        $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id,'service_request_status'=>'IN_PROCESS'));
                        $updateData = array('service_request_status' => 'IN_PROCESS');
                        $start_service =  $this->common_model->update(TB_SERVICE_REQUESTS,array("service_request_id"=>$service_request_id,"service_request_status"=>'PENDING'),$updateData);

                        $updateData = array('booking_status' => 'IN_PROCESS');
                        $start_booking =  $this->common_model->update(TB_BOOKINGS,array("booking_id"=>$service_requests[0]['service_booking_id'],"booking_status"=>'PENDING'),$updateData);
                        
                        if ($start_service && $start_booking) {
                            $this->response(array('status' => true, 'message' => 'Service accepted successfully'));exit;
                        } else {
                            $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);exit;
                        }
                    } else {
                        $this->response(array('status' => false, 'message' => 'Service already accepted'), 300);exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'You can\'n able to start this request, because already another booking is in process in that duration'), 300);exit;
                }
            }

            //cancel service request
            if ($_POST['status'] == 'CANCELLED') {

                $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_booking_id'=>$service_requests[0]['service_booking_id'],'service_request_status'=>'CANCELLED'));

                if (count($is_service_requests) < 1) {
                    //Cancel that request                
                    $cancelservice = Servicerequest::where(['service_requests.service_request_id' => $service_request_id])->update(['service_requests.service_request_status' => $_POST['status']]);

                    $updateData = array('service_request_status' => 'CANCELLED');
                    $cancelservice =  $this->common_model->update(TB_SERVICE_REQUESTS,array("service_request_id"=>$service_request_id),$updateData);


                    if ($cancelservice) {
                        $this->response(array('status' => true, 'message' => 'Service has been cancelled successfully'));exit;
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'Service has been already cancelled'), 300);exit;
                }
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please provide service_request_id'), 300);exit;
        }
    } */

    
    /*
     * Title : booking_status
     * Description : booking list of status
     * 
     * @param Integer service_request_id
     * @param String status
     * @return Array 
     */
    function booking_status_post () 
    {
        // checked user is authenticate or not 
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        $service_request_id = $_POST['service_request_id'];

        if ( $service_request_id > 0 ) {
            // Check service request is exist or not             
            $service_requests = $this->common_model->select("*", TB_SERVICE_REQUESTS, array('service_request_id' => $service_request_id));
            
            if ( !$service_requests ) {
                $this->response(array('status' => false, 'message' => 'Service request not available'), 300); exit;
            }

            //Accept service request
            if ( $_POST['status'] == ACCEPTED ) {
                //Check that booking already started by anouther one 
                $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id,'service_request_status'=>IN_PROCESS));

                //Check another service not started in that duration
                //hold for the moment
                $validbooking = 1;
                
                if ( $validbooking ) {
                    
                    // update records
                    if ( count($is_service_requests) < 1 ) {
                        //Start that request                        
                        $accept_service =  $this->common_model->update(TB_SERVICE_REQUESTS, array("service_request_id" => $service_request_id, "service_request_status" => PENDING), array('service_request_status' => IN_PROCESS));
                        $accept_booking =  $this->common_model->update(TB_BOOKINGS, array("booking_id" => $service_request_id, "booking_status" => PENDING), array('booking_status' => IN_PROCESS));
                        
                        if ( $accept_service && $accept_booking ) {
                            $this->response(array('status' => true, 'message' => 'Service has been accepted successfully'),200); exit;
                        } else {
                            $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300); exit;
                        }
                    } else {
                        $this->response(array('status' => false, 'message' => 'Service already accepted'), 300); exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'You can\'n able to start this request, because already another booking is in process in that duration'), 300); exit;
                }
            }

            //cancel service request
            if ( $_POST['status'] == CANCELLED ) {
                $is_service_requests = $this->common_model->select("*", TB_SERVICE_REQUESTS, array('service_booking_id' => $service_request_id, 'service_request_status' => CANCELLED));
                
                // update records                
                if ( count($is_service_requests) < 1 ) {                    
                    $cancel_service =  $this->common_model->update(TB_SERVICE_REQUESTS, array("service_request_id" => $service_request_id), array('service_request_status' => CANCELLED));
                    $cancel_booking =  $this->common_model->update(TB_BOOKINGS, array('booking_id' => $service_request_id), array('booking_status' => CANCELLED));
                    
                    if ( $cancel_service & $cancel_booking ) {
                        $this->response(array('status' => true, 'message' => 'Service has been cancelled successfully'),200); exit;
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300); exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'Service already cancelled'), 300); exit;
                }
            }

            // Start service request
            if ( $_POST['status'] == START ) {
                $is_service_requests = $this->common_model->select("*", TB_SERVICE_REQUESTS, array('service_request_id' => $service_request_id, 'service_request_status' => START));
                
                // update records
                if ( count($is_service_requests) > 0 ) {
                    $this->response(array('status' => false, 'message' => 'Service already started.'), 300); exit;
                } else {                                  
                    $start_service = $this->common_model->update(TB_SERVICE_REQUESTS, array('service_request_id' => $service_request_id), array('service_request_status' => START));           
                    if ( $start_service ) {
                        $this->response(array('status' => true, 'message' => 'Service has been started successfully'), 200); exit;
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300); exit;
                    }
                }
            }

            // End service request
            if ( $_POST['status'] == END ) {
                $is_service_requests = $this->common_model->select("*", TB_SERVICE_REQUESTS, array('service_request_id' => $service_request_id, 'service_request_status' => END));
                
                // update records
                if ( count($is_service_requests) > 0 ) {
                    $this->response(array('status' => false, 'message' => 'Service already end.'), 300); exit;
                } else{                                  
                    $end_service = $this->common_model->update(TB_SERVICE_REQUESTS, array('service_request_id' => $service_request_id), array('service_request_status' => END));  
                    $end_booking = $this->common_model->update(TB_BOOKINGS, array('booking_id' => $service_request_id), array('booking_status' => COMPLETED));        
                    if ( $end_service && $end_booking ) {
                      $this->response(array('status' => true, 'message' => 'Service has been ended successfully'),200); exit;
                  } else {
                    $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300); exit;
                }
            }
        }
    } else {
        $this->response(array('status' => false, 'message' => 'Please provide service_request_id'), 300); exit;
    }
}

    /**
     * Title : slider_list 
     * Descriptin : show the list of slider images
     *
     * @return Array
     */
    function slider_list_get ()
    {
        // checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        $sliderList = $this->common_model->select("id, title, banner_img", TB_BANNER, array("banner_status" => 1));
        $data = array();

        // list of slider 
        if ( count($sliderList) > 0 ) {

            foreach ( $sliderList as $key => $slider ) {
                $data[$key]['id']         = $slider['id'];
                $data[$key]['title']      = $slider['title'];
                $data[$key]['banner_img'] = base_url().$slider['banner_img'];                    
            }
            $this->response(array('status' => true, 'slider_list' => $data),200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'no slider images are available '), 300); exit;
        }
    }

    /**
     * Title : start_or_end_service
     * Description : booking service start or end
     *
     * @param Integer service_request_id
     * @param Integer status
     * @return Array
     */
    function start_or_end_service_post () 
    {
        // Checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // Input data
        $postData = $_POST;
        if ( $postData['service_request_id'] > 0 ) {
            // Check service request is exist or not 
            $service_requests = $this->common_model->select("*", TB_SERVICE_REQUESTS, array('service_request_id' => $postData['service_request_id'], 'service_request_status' => IN_PROCESS));
            if ( !$service_requests ) {
                $this->response(array('status' => false, 'message' => 'Service request not available'), 300); exit;
            }

            //Start service request
            if ( $postData['status'] == START || $postData['status'] == END ) {
                $checkstatus = AVAILABLE;
                $statuname   = 'ended';

                if ($postData['status'] == START) {
                    $checkstatus = NOT_AVAILABLE;
                    $statuname   = 'started';
                }

                //Check that sp already started service
                $is_service_requests = $this->common_model->select("*",TB_USERS,array('user_id' => $service_requests[0]['service_provider_user_id'], 'is_user_available' => $checkstatus));
                if ( count($is_service_requests) < 1 ) {
                    //Update that service provider to not available
                    $update = $this->common_model->update(TB_USERS, array('user_id' => $service_requests[0]['service_provider_user_id']), array('is_user_available' => $checkstatus));
                    
                    if ( $update ) {
                        
                        if ( $postData['status'] == END ) {
                            //if end service complete that booking
                            $this->common_model->update(TB_SERVICE_REQUESTS, array('service_request_id' => $postData['service_request_id']), array('service_request_status' => COMPLETED));
                            $this->common_model->update(TB_BOOKINGS, array('booking_id' => $service_requests[0]['service_booking_id']), array('booking_status' => COMPLETED));
                        }
                        $this->response(array('status' => true, 'message' => 'Service ' . $statuname . ' successfully'),200); exit;
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300); exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'Service ' . $statuname . ' already'), 300); exit;
                }
            } else {
                $this->response(array('status' => false, 'message' => 'Invalid status'), 300); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please provide service_request_id'), 300); exit;
        }
    }

    /**
     * Title : price_list
     * Description : price list according to category & sub-category
     * 
     * @return Array 
     */
    function price_list_get () 
    {
        // Checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // Get the all price details
        $cond      = array();
        $jointype  = array(TB_CATEGORIES => "LEFT", TB_SUBCATEGORIES => "LEFT");
        $join      = array(TB_CATEGORIES => TB_CATEGORIES.".category_id = ".TB_RIDE_PRICE.".c_id", TB_SUBCATEGORIES => TB_SUBCATEGORIES.".sub_category_id = ".TB_RIDE_PRICE.".sc_id");
        $priceList = $this->Common_model->selectJoin("rp_id, name, category, rp_cost, rp_per_km, rp_per_hour", TB_RIDE_PRICE, $cond, array(), $join, $jointype);
        $data      = array();
        
        if ( count($priceList) > 0 ) {
            $keyCat = $keyTut = 0;
            
            foreach ( $priceList as $key => $price ) {

                // Ride price
                if( $price['category'] == "Ride" &&  $price['name'] == "Single Rides" ) {                   
                    $ride[$key]['id']           = $price['rp_id'];
                    $ride[$key]['category']     = $price['category'];
                    $ride[$key]['sub_category'] = $price['name'];
                    $ride[$key]['cost']         = '$'.$price['rp_cost'];
                    $ride[$key]['hour']         = $price['rp_per_hour'];
                } else if($price['category'] == "Carpooling" && ( $price['name'] == "Multiple Rides With Same Destination" || $price['name'] == "Multiple Rides With Different Destination" ) ) {
                    $carpooling[$keyCat]['id']           = $price['rp_id'];
                    $carpooling[$keyCat]['category']     = $price['category'];
                    $carpooling[$keyCat]['sub_category'] = $price['name'];
                    $carpooling[$keyCat]['cost']         = '$'.$price['rp_cost'];
                    $carpooling[$keyCat]['km']           = $price['rp_per_km'];
                    $keyCat++; 
                } else if($price['category'] == "Care") {
                    $care[$keyTut]['id']           = $price['rp_id'];
                    $care[$keyTut]['category']     = $price['category'];
                    $care[$keyTut]['sub_category'] = $price['name'];
                    $care[$keyTut]['cost']         = '$'.$price['rp_cost'];
                    $care[$keyTut]['hour']         = $price['rp_per_hour'];
                    $keyTut++;
                } else if($price['category'] == "Ride & care") {
                    $rideAndCare[$keyTut]['id']           = $price['rp_id'];
                    $rideAndCare[$keyTut]['category']     = $price['category'];
                    $rideAndCare[$keyTut]['sub_category'] = $price['name'];
                    $rideAndCare[$keyTut]['cost']         = '$'.$price['rp_cost'];
                    $rideAndCare[$keyTut]['hour']         = $price['rp_per_hour'];
                    $keyTut++;
                } else {

                }                
            }

            // Assign array value
            $data['rides'] = $ride;
            $data['carpooling'] = $carpooling;
            $data['care'] = $care; 
            $data['ride and care'] = $rideAndCare; 

            $this->response(array('status' => true, 'price_list' => $data),200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'No data are available '), 300); exit;
        }      
    }

    /**
     * Title : read_notification
     * Description : List of read notification 
     *
     * @param Integer userId
     * @param Integer notifId
     * @return Array 
     */

    function read_notification_post () 
    {
        // Checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // Input data
        $postData = $_POST;

        // Get list of data
        if ( $postData['userId'] != "" && $postData['notifId'] != "" ) {
            $checkNotif = $this->common_model->select("n_id", TB_NOTIFICATION, array('user_id' => $postData['userId'], 'n_id' => $postData['notifId']));
            
            if ( count($checkNotif) > 0 ) {
               $updateNotif = $this->common_model->update(TB_NOTIFICATION, array('user_id' => $postData['userId'], 'n_id' => $postData['notifId']), array('n_read' => "0"));
               
               if($updateNotif) {
                $this->response(array('status' => true, 'message' => 'The notification has been read successfully .'), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'You have already read notification.'), 300); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'You have provided wrong parameter list.'), 300); exit;
        }           
    } else {
        $this->response(array('status' => false, 'message' => 'All fields are required. '), 300); exit;
    }        
}

    /**
     * Title       : notification_list
     * Description : Notification list
     *
     * @param  Integer userId
     * @param  Integer notifId
     * @return Array
     */

    function notification_listing_post () 
    {
        // Checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // post data
        $postData = $_POST;

        if ( $postData['userId'] != "" && $postData['notifId'] != "" ) {
            // Get specific record 
            $checkNotif = $this->common_model->select("n_content, user_id, n_id", TB_NOTIFICATION, array('user_id' => $postData['userId'], 'n_id' => $postData['notifId'], 'n_read' => "1"));
            
            // result
            if ( $checkNotif ) {
                $this->response(array('status' => true, 'message' => 'Send admin notification.', 'notification_list' => $checkNotif), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'You have provided wrong parameter list.'), 300); exit;
            }                 
        } else {
            // Get all notification list
            $checkNotif = $this->common_model->select("n_content,user_id,n_id",TB_NOTIFICATION,array('n_read' => "1"));
            
            // result
            if ( count($checkNotif) > 0 ) {
                $this->response(array('status' => true, 'message' => 'Send admin notification.','notification_list' => $checkNotif), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'You have provided wrong parameter list.'), 300); exit;
            }            
        }     
    }

    /**
     * Title : faq_list
     * Description : FAQ list
     * 
     * @return Array
     */
    function faq_list_get() 
    {
        // Checked user is authenticate or not
        if( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        } 
        
        // Get all record of FAQ list
        $cond     = array(TB_CATEGORIES.'.status' => 1, TB_SUBCATEGORIES.'.status' => 1);
        $jointype = array(TB_CATEGORIES => "LEFT",TB_SUBCATEGORIES => "LEFT");
        $join     = array(TB_CATEGORIES => TB_CATEGORIES.".category_id = ".TB_FAQ.".category_id", TB_SUBCATEGORIES => TB_SUBCATEGORIES.".sub_category_id = ".TB_FAQ.".subcategory_id");
        $faqList  = $this->Common_model->selectJoin("id, name, category, faq_description, faq_title", TB_FAQ, $cond, array(), $join, $jointype);
        $data = array();           
        
        if ( count($faqList) > 0 ) {
            foreach ($faqList as $key => $faq) {
                $data[$key]['id']              = $faq['id'];
                $data[$key]['categoryName']    = $faq['category'];
                $data[$key]['subCategoryName'] = $faq['name'];
                $data[$key]['description']     = $faq['faq_description'];
                $data[$key]['title']           = $faq['faq_title'];
            }
            // result
            $this->response(array('status' => true, 'message' => 'FAQ list.','faq_list' => $data), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Faq data not available.'), 300); exit;
        }       
    }

    /**
     * Title : tutoring_list
     * Description : tutoring list details
     * 
     * @return Array
     */

    function tutoring_list_get () 
    {
        // Checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // Get all data
        $cond    = array();       
        $join    = array(TB_TEACHERS => TB_TEACHERS.".id = ".TB_SUBJECT_MASTER.".tutor_id");
        $subList = $this->Common_model->select_join(TB_TEACHERS.".id, sub_id, teacher_name", TB_SUBJECT_MASTER, $cond, array(), array(), $join);
        $data    = array();

        if ( count($subList) > 0 ) {

            foreach ( $subList as $key => $sub ) {
                $subArray = explode(',', $sub['sub_id']);
                $subjects = $this->Common_model->select_where_in("id,sub_name",TB_SUBJECTS,array(),$subArray);
                $data[$sub['teacher_name']]=$subjects;
            }
            $this->response(array('status' => true, 'teacher_list' => $data),200);exit;
        } else {
            $this->response(array('status' => false, 'message' => 'No data are available '), 300);exit;
        }      
    }

    /**
     * Title : service_list
     * Description : service list
     * 
     * @param Integer category_id
     * @param Integer sub_category_id
     * @param Integer status
     * @return Array
     */

    public function getServiceList_post () 
    {
        // checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // input data
        $postData = $_POST;

        if ( ($postData['category_id'] > 0) && ($this->input->post('status') != "") && ($postData['sub_category_id'] > 0) ) {
            // Check category request is exist or not 
            $category_requests = $this->common_model->select("category_id",TB_CATEGORIES,array('category_id' => $postData['category_id'], 'status' => '1'));
            
            if ( !$category_requests ) {
                $this->response(array('status' => false, 'message' => 'Category request not available'), 300); exit;
            } else {

                // Check sub category request is exist or not 
                $sub_category_requests = $this->common_model->select("sub_category_id", TB_SUBCATEGORIES, array('sub_category_id' => $postData['sub_category_id'], 'status' => '1'));
                if ( !$sub_category_requests ) {
                    $this->response(array('status' => false, 'message' => 'Sub category request not available'), 300); exit;
                } else {

                    if( $postData['status'] != "" ) {
                        // Check status request is exist or not 
                        $status_requests = $this->common_model->select("booking_id", TB_BOOKINGS, array('booking_status' => $postData['status']));
                        if ( !$status_requests ) {
                            $this->response(array('status' => false, 'message' => 'Status request not available'), 300); exit;
                        } else {

                            $result = $this->common_model->selectJoin(TB_USERS.".user_id, booking_id, user_name, user_pic", TB_USERS, array("booking_status" => trim($postData['status']), "booking_category_id" => trim($postData['category_id']), "booking_sub_category_id" => trim($postData['sub_category_id'])), array(), array(TB_BOOKINGS => 'tbl_users.user_id = tbl_bookings.user_id'));
                            if( count($result) > 0 ) {
                                $this->response(array('status' => true, 'service_list' => $result), 200); exit;
                            } else {
                                $this->response(array('status' => false, 'message' => 'Data not found'), 300); exit;
                            }
                        }
                    } else {
                        $this->response(array('status' => false, 'message' => 'Status is required'), 300); exit;
                    }
                }
            }            
        } else {
         $this->response(array('status' => false, 'message' => 'Please provide category id, sub category id & status'), 300); exit;
     }        
 }

    /**
     * booking_service_view
     * @param category_id
     * @param sub_category_id
     * @param status
     * @param user_id
     * @param booking_id
     */
  /*  public function booking_service_view_post() {
        $postData = $_POST;
        if ($postData['category_id'] != "" && $postData['booking_id'] != "" && $postData['status'] != "" && $postData['sub_category_id'] != "" && $postData['user_id'] != "")
        {
            $result = $this->common_model->selectJoin(TB_USERS.".user_id,user_name,user_phone_number,booking_pick_up_location,booking_drop_off_location,booking_pick_up_time,booking_drop_off_time,booking_duration,booking_id",TB_USERS,array("booking_status"=>trim($postData['status']),"booking_category_id"=>trim($postData['category_id']),"booking_sub_category_id"=>trim($postData['sub_category_id']),TB_USERS.".user_id"=>trim($postData['user_id']),"booking_id"=>$postData['booking_id']),array(),array(TB_BOOKINGS=>'tbl_users.user_id = tbl_bookings.user_id'));
            if(count($result) > 0)
            {
                $user_id = $result[0]['user_id'] ? $result[0]['user_id']:'';
                $parent_name = $result[0]['user_name'] ? $result[0]['user_name']:'';
                $parent_no = $result[0]['user_phone_number'] ? $result[0]['user_phone_number']:'';
                $pick_up_point = $result[0]['booking_pick_up_location'] ? $result[0]['booking_pick_up_location']:'';
                $drop_off_point = $result[0]['booking_drop_off_location'] ? $result[0]['booking_drop_off_location']:'';
                $pick_up_time = $result[0]['booking_pick_up_time'] ? $result[0]['booking_pick_up_time']:'';
                $drop_off_time = $result[0]['booking_drop_off_time'] ? $result[0]['booking_drop_off_time']:'';
                $service_type = $result[0]['booking_duration'] ? $result[0]['booking_duration']:'';
                $serviceResult = $this->common_model->select("booking_date,booking_start_date,booking_end_date,booking_total_days,booking_secret_code,booking_price,booking_total_weeks,booking_total_moths",TB_BOOKINGS,array("booking_duration"=>$service_type,"booking_id"=>$postData['booking_id']));
                if(count($serviceResult) > 0)
                {
                    if($service_type == "daily")
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['days'] = $value['booking_total_days']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['price'] = $value['booking_price'];                             
                        }                        
                    }
                    else if($service_type == "weekly")
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['end_date'] = $value['booking_end_date'];
                            $serviceList[$key]['weekly'] = $value['booking_total_weeks']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['price'] = $value['booking_price'];                             
                        }

                    }
                    else
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['end_date'] = $value['booking_end_date']; 
                            $serviceList[$key]['monthly'] = $value['booking_total_moths']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['price'] = $value['booking_price'];                             
                        }

                    }
                }
                else
                {
                    $serviceList = array(); 
                }                
                
                $kidResult = $this->common_model->select("kid_id,kid_gender,kid_age,kid_name",TB_KIDS,array("user_id"=>$postData['user_id']));
                if(count($kidResult) > 0)
                {
                    foreach ($kidResult as $key => $value) {
                        $kidsList[$key]['id'] = $value['kid_id'];
                        $kidsList[$key]['name'] = $value['kid_name'];
                        $kidsList[$key]['gender'] = $value['kid_gender'];
                        $kidsList[$key]['age'] = $value['kid_age'];                    
                    }
                }
                else 
                {
                    $kidsList = array();
                }
                $this->response(array('status' => true,'user_id'=>$user_id,'pick_up_point'=>$pick_up_point,'drop_off_point'=>$drop_off_point,'pick_up_time'=>$pick_up_time,'drop_off_time'=>$drop_off_time,'service_type'=>$service_type,'parent_no'=>$parent_no,'parent_name'=>$parent_name,'service_view'=>$serviceList,'kids_list'=>$kidsList), 200);exit;
            }
            else
            {
                $this->response(array('status' => false, 'message' => 'Data not found'), 300);exit;
            }
        }
        else
        {
            $this->response(array('status' => false, 'message' => 'Please enter all parameters'), 300);exit;
        }
    } */


    /**
     * Title : getServiceDetail     
     * Description : service details
     * 
     * @param Integer user_id
     * @param Integer booking_id
     * @return Array
     */

    public function getServiceDetail_post () 
    {
        // Input data
        $postData = $_POST;

        // Checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // Checke input not empty
        if ( $postData['booking_id'] != ""   && $postData['user_id'] != "" ) {

            $result = $this->common_model->selectJoin(TB_USERS.".user_id, user_name, user_phone_number, booking_pick_up_location, booking_drop_off_location, booking_pick_up_time, booking_drop_off_time, booking_duration, booking_id", TB_USERS, array(TB_USERS.".user_id" => trim($postData['user_id']), "booking_id" => $postData['booking_id']), array(), array(TB_BOOKINGS => 'tbl_users.user_id = tbl_bookings.user_id'));
            if ( count($result) > 0 ) {
                $user_id        = $result[0]['user_id']                   ? $result[0]['user_id']                   :'';
                $parent_name    = $result[0]['user_name']                 ? $result[0]['user_name']                 :'';
                $parent_no      = $result[0]['user_phone_number']         ? $result[0]['user_phone_number']         :'';
                $pick_up_point  = $result[0]['booking_pick_up_location']  ? $result[0]['booking_pick_up_location']  :'';
                $drop_off_point = $result[0]['booking_drop_off_location'] ? $result[0]['booking_drop_off_location'] :'';
                $pick_up_time   = $result[0]['booking_pick_up_time']      ? $result[0]['booking_pick_up_time']      :'';
                $drop_off_time  = $result[0]['booking_drop_off_time']     ? $result[0]['booking_drop_off_time']     :'';
                $service_type   = $result[0]['booking_duration']          ? $result[0]['booking_duration']          :'';
                $serviceResult  = $this->common_model->select("booking_date, booking_start_date, booking_end_date, booking_total_days, booking_secret_code, booking_price, booking_total_weeks, booking_total_moths", TB_BOOKINGS, array("booking_duration" => $service_type, "booking_id" => $postData['booking_id']));
                $duration       = 0;

                if ( $pick_up_time !='' && $drop_off_time !='' ) {
                    $total      = strtotime($drop_off_time) - strtotime($pick_up_time);
                    $hours      = floor($total / 60 / 60);
                    $minutes    = round(($total - ($hours * 60 * 60)) / 60);
                    $duration   = $hours.'.'.$minutes;                    
                }

                if(count($serviceResult) > 0) {

                    if($service_type == "daily") {

                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date']  = $value['booking_start_date']; 
                            $serviceList[$key]['days']        = $value['booking_total_days'];                             
                            $serviceList[$key]['secret_code'] = ''; 
                            $serviceList[$key]['price']       = $value['booking_price'];                             
                        }                        
                    } else if( $service_type == "weekly" ) {

                        foreach ( $serviceResult as $key => $value ) {
                            $serviceList[$key]['start_date']  = $value['booking_start_date']; 
                            $serviceList[$key]['end_date']    = $value['booking_end_date'];
                            $serviceList[$key]['weekly']      = $value['booking_total_weeks']; 
                            $serviceList[$key]['secret_code'] = ''; 
                            $serviceList[$key]['price']       = $value['booking_price'];                             
                        }
                    } else {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date']  = $value['booking_start_date']; 
                            $serviceList[$key]['end_date']    = $value['booking_end_date']; 
                            $serviceList[$key]['monthly']     = $value['booking_total_moths']; 
                            $serviceList[$key]['secret_code'] = '';
                            $serviceList[$key]['price']       = $value['booking_price'];                             
                        }
                    }
                } else {
                    $serviceList = array(); 
                }     

                $subQuery='';           
                $kidIdResult =  $this->common_model->select("kids_id",TB_KIDS_BOOKING,array("booking_id"=>$postData['booking_id'])); 
                
                if ( count($kidIdResult) > 0 ) {

                    foreach ($kidIdResult as $key => $value) {
                        $subQuery = $value['kids_id'];    
                    }
                }

                $kidResult = $this->common_model->select_where_in_with_no_quote("kid_id, kid_gender, kid_age, kid_name", TB_KIDS, "kid_id", $subQuery);
                if(count($kidResult) > 0) {

                    foreach ( $kidResult as $key => $value ) {
                        $kidsList[$key]['id']     = $value['kid_id'];
                        $kidsList[$key]['name']   = $value['kid_name'];
                        $kidsList[$key]['gender'] = $value['kid_gender'];
                        $kidsList[$key]['age']    = $value['kid_age'];                    
                    }
                } else {
                    $kidsList = array();
                }
                $this->response(array('status' => true, 'user_id' => $user_id, 'pick_up_point' => $pick_up_point, 'drop_off_point' => $drop_off_point, 'pick_up_time' => $pick_up_time, 'drop_off_time' => $drop_off_time, 'trip_duration' => $duration, 'service_type' => $service_type, 'parent_no' => $parent_no, 'parent_name' => $parent_name, 'service_view' => $serviceList, 'kids_list' => $kidsList), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Data not found'), 300); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please enter all parameters'), 300); exit;
        }
    }

    /**
     * Title : serviceAction
     * Description : service details 
     *
     * @param Integer booking_id
     * @param Integer user_id
     * @return Array
     */

    public function serviceAction_post () 
    {
        // Input data
        $postData = $_POST;

        // checked user is authenticate or not 
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        if ( $postData['booking_id'] != ""   && $postData['user_id'] != "") {

            $result = $this->common_model->selectJoin(TB_USERS.".user_id, user_name, user_phone_number, booking_pick_up_location, booking_drop_off_location, booking_pick_up_time, booking_drop_off_time, booking_duration, booking_id", TB_USERS, array(TB_USERS.".user_id" => trim($postData['user_id']), "booking_id" => $postData['booking_id']), array(), array(TB_BOOKINGS => 'tbl_users.user_id = tbl_bookings.user_id'));
            if ( count($result) > 0 ) {
                $user_id       = $result[0]['user_id']               ? $result[0]['user_id']               :'';
                $pick_up_time  = $result[0]['booking_pick_up_time']  ? $result[0]['booking_pick_up_time']  :'';
                $drop_off_time = $result[0]['booking_drop_off_time'] ? $result[0]['booking_drop_off_time'] :'';
                $service_type  = $result[0]['booking_duration']      ? $result[0]['booking_duration']      :'';
                $serviceResult = $this->common_model->select("booking_date, booking_start_date, booking_end_date, booking_total_days, booking_secret_code, booking_price, booking_total_weeks, booking_total_moths", TB_BOOKINGS, array("booking_duration" => $service_type,"booking_id" => $postData['booking_id']));
                
                if ( count($serviceResult) > 0 ) {

                    if ( $service_type == "daily" ) {

                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date']  = $value['booking_start_date']; 
                            $serviceList[$key]['total_days']  = $value['booking_total_days']; 
                            $serviceList[$key]['secret_code'] = ''; 
                            $serviceList[$key]['total_price'] = $value['booking_price'];                             
                        }                        
                    } else if( $service_type == "weekly" ) {

                        foreach ( $serviceResult as $key => $value ) {
                            $serviceList[$key]['start_date']  = $value['booking_start_date']; 
                            $serviceList[$key]['end_date']    = $value['booking_end_date'];
                            $serviceList[$key]['total_weeks'] = $value['booking_total_weeks']; 
                            $serviceList[$key]['secret_code'] = '';
                            $serviceList[$key]['total_price'] = $value['booking_price'];                             
                        }
                    } else {

                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date']   = $value['booking_start_date']; 
                            $serviceList[$key]['end_date']     = $value['booking_end_date']; 
                            $serviceList[$key]['total_months'] = $value['booking_total_moths']; 
                            $serviceList[$key]['secret_code']  = '';
                            $serviceList[$key]['total_price']  = $value['booking_price'];                             
                        }
                    }
                } else {
                    $serviceList = array(); 
                }           

                $this->response(array('status' => true, 'user_id' => $user_id, 'booking_id' => $postData['booking_id'], 'service_type' => $service_type, 'service_view' => $serviceList), 200); exit;
            } else { 
                $this->response(array('status' => false, 'message' => 'Data not found'), 300); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please enter all parameters'), 300); exit;
        }    
    }

    /**
     * Title : current_lat_and_lng
     * Description : update current latitude and longitude
     *
     * @param Integer booking_id
     * @param Integer current_lat
     * @param Integer current_lng
     * @return Array
     */

    function current_lat_and_lng_post () 
    {
        // Input data
        $postData = $_POST;

        // checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        if ( $postData['booking_id'] != "" && $postData['current_location_lat'] != "" && $postData['current_location_lng'] != "" ) {     
            $result = $this->db->update(TB_BOOKINGS, array('current_location_lat' => $postData['current_location_lat'], 'current_location_lng' => $postData['current_location_lng']), array('booking_id' => $postData['booking_id']));   
            
            if ( !$result ) {
                $this->response(array('status' => false, 'message' => 'something went wrong ,please try again!!!'), 300); exit;
            } else {
                $this->response(array('status' => true, 'booking_id' => $postData['booking_id'], 'Current latitude' => $postData['current_location_lat'], 'Current longitude' => $postData['current_location_lng'], 'message' => 'Current latitude and longitude has been updated successfully'), 200); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please enter booking id , current latitude and longitude.'), 300); exit;
        }
    } 
    
    /**
     * Title : sendPushNotification
     * Description : send the push notification
     */

    public function sendPushNotification( $device_id, $message, $data, $user_type)
    {     
        $content = array(
            "en" => $message
            );

        if($user_type == ROLE_PARENTS || $user_type == SCHOOL || $user_type == ORGANIZATION)
        {
            $app_id = ONSIGNALKEYPARENT;
        }else{
            $app_id = ONSIGNALKEYSP;
        }

        $fields = array(
            'app_id'             => $app_id,
            'include_player_ids' =>(array)$device_id,
            'data'               => $data,
            'contents'           => $content
            );
        
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
           'Authorization: Basic OGViZTZkMjgtMjRlYy00YWQ0LWIzMWYtNmE2ZWI5MDhmZTUw'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);  
       // print_r($response);exit;            
        curl_close($ch);
    }

    /**
     * Title :driver_location
     * Description : update of driver current location get lat & long
     * 
     * @param  Integer user_id
     * @param  Double loc_lat
     * @param  Double loc_lng
     * @return Array
     */

    public function driver_location_post()
    {
        // post data
        $postData = $this->post();
        // checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // Checked input is not empty
        if ( $postData['user_id'] != "" && $postData['loc_lat'] != "" && $postData['loc_lng'] != "" ) {

            if ( $postData['user_id'] > 0 ) {

                // Checked user is present or not
                $driverIdChecked = $this->common_model->select("user_id", TB_USERS, array("user_id" => $postData['user_id'], "roleId" => ROLE_CARE_DRIVER));
                
                if ( count($driverIdChecked) > 0 ) {

                    // Get user data                               
                    $getDriverId = $this->common_model->select("user_id", TB_DRIVER_LOCATION, array("user_id" => $postData['user_id']));
                    
                    if ( count($getDriverId) > 0 ) {
                        $userdata = array("loc_lat" => $postData['loc_lat'], "loc_long" => $postData['loc_lng'], "created_at" => date("Y-m-d H:i:s"));
                        $user     = $this->db->update(TB_DRIVER_LOCATION, $userdata, array('user_id' => $this->post('user_id')));
                        if ( $user ) {
                            $this->response(array("status" => true, "message" => 'Care driver location has been updated successfully'), 200); exit;
                        } else {
                            $this->response(array("status" => false, "message" => 'Something went wrong'), 300); exit;
                        }
                    } else {
                        $userdata = array("user_id" => $postData['user_id'], "loc_lat" => $postData['loc_lat'], "loc_long" => $postData['loc_lng'], "created_at" => date("Y-m-d H:i:s"));
                        $user     = $this->db->insert(TB_DRIVER_LOCATION, $userdata);
                        if ( $user ) {
                            $this->response(array("status" => true, "message" => 'Care driver location has been inserted successfully'), 200); exit;
                        } else {
                            $this->response(array("status" => false, "message" => 'Something went wrong'), 300); exit;
                        }
                    }
                } else {
                    $this->response(array("status" => false, "message" => 'User id is not driver or exists.'), 300); exit;
                } 
            } else {
                $this->response(array("status" => false, "message" => 'Provide proper user id'), 300); exit;
            }
        } else {
            $this->response(array("status" => false, "message" => 'Please provide userid, lattitude & loggitude.'), 300); exit;
        }
    }


    /**
     * Title :parent_current_location
     * Description : add & update of parent current lat & long location
     * 
     * @param  Integer user_id
     * @param  Double current_lat
     * @param  Double loc_lng
     * @return Array
     */

    public function parent_current_location_post()
    {
        // post data
        $postData = $this->post();
        // checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // Checked input is not empty
        if ( $postData['user_id'] != "" && $postData['current_lat'] != "" && $postData['current_lng'] != "" ) {

            if ( $postData['user_id'] > 0 ) {

                // Checked user is present or not
                $driverIdChecked = $this->common_model->select("user_id", TB_USERS, array("user_id" => $postData['user_id'], "roleId" => ROLE_PARENTS));
                
                if ( count($driverIdChecked) > 0 ) {

                    // Get user data                               
                    $getDriverId = $this->common_model->select("user_id", TB_PARENT_LOCATION, array("user_id" => $postData['user_id']));

                    
                    if ( count($getDriverId) > 0 ) {
                        $userdata = array("current_lat" => $postData['current_lat'], "current_lng" => $postData['current_lng'], "updated_at" => date("Y-m-d"));
                        $user     = $this->db->update(TB_PARENT_LOCATION, $userdata, array('user_id' => $this->post('user_id')));
                        if ( $user ) {
                            $this->response(array("status" => true, "message" => 'Parent location has been updated successfully'), 200); exit;
                        } else {
                            $this->response(array("status" => false, "message" => 'Something went wrong'), 300); exit;
                        }
                    } else {
                        $userdata = array("user_id" => $postData['user_id'], "current_lat" => $postData['current_lat'], "current_lng" => $postData['current_lng'], "created_at" => date("Y-m-d H:i:s"), "updated_at" => date("Y-m-d"));
                        $user     = $this->db->insert(TB_PARENT_LOCATION, $userdata);
                        if ( $user ) {
                            $this->response(array("status" => true, "message" => 'Parent location has been inserted successfully'), 200); exit;
                        } else {
                            $this->response(array("status" => false, "message" => 'Something went wrong'), 300); exit;
                        }
                    }
                } else {
                    $this->response(array("status" => false, "message" => 'User id is not parent or exists.'), 300); exit;
                } 
            } else {
                $this->response(array("status" => false, "message" => 'Provide proper user id'), 300); exit;
            }
        } else {
            $this->response(array("status" => false, "message" => 'Please provide userid, lattitude & loggitude.'), 300); exit;
        }
    }

    /**
     * Title       : get_parent_current_loc
     * Description : get parent current location
     * 
     * @param Integer user_id
     * @return Array
     */
    public function get_parent_current_loc_post()
    {
        // post data
        $postData = $this->post();
        // checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

         // Validation
        if ( trim($postData['user_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'User id field is required.'), 200); exit; 
        } 

        $getparentData = $this->common_model->select("id,user_id,current_lat,current_lng,updated_at", 
            TB_PARENT_LOCATION, array("user_id" => $postData['user_id'],"updated_at" => date("Y-m-d")));
        $parentList = array();
        $i = 0;
        if(count($getparentData) > 0) {
            foreach ($getparentData as $key => $value) {
                $parentList[$i]['id'] = $value['id'];
                $parentList[$i]['user_id'] = $value['user_id'];
                $parentList[$i]['current_lat'] = $value['current_lat'];
                $parentList[$i]['current_lng'] = $value['current_lng'];
                $parentList[$i]['updated_at'] = $value['updated_at'];
                $i++;
            }
            $this->response(array("status" => true, "result" => $parentList), 200); exit;
        } else {
            $this->response(array("status" => false, "message" => 'No records found.'), 300); exit;
        }
    }
    
    
    /**
     * Title       : video_streaming
     * Description : Ride Video streaming
     * 
     * @param Integer booking_id
     * @param Integer created_by
     * @param File video_stream
     * @return Array
     */

    public function video_streaming_post ()
    {       
        // post data
        $postData = $this->post();


        // checked user is authenticate
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        }*/
        
        // Validation
        $streaming_file = "";
        if (  trim($postData['booking_id']) == "" || trim($postData['created_by']) == "" ||  $_FILES['video_stream']['name'] != "" && isset($_FILES['video_stream']) && !empty($_FILES['video_stream'])) {

            $resultUpload = upload_video_on_amazon($_FILES['video_stream']);
            if ( $resultUpload['success'] == 1 ) {
                $video_data      = array("booking_id" => $postData['booking_id'], "file_path" => $resultUpload['videoname'],"isDeleted" => 0);
                $video_streaming = $this->db->insert(TB_VIDEO_STREAM, $video_data);
                
                
                // Get all data trip & parent details
                $cond    = array("booking_id" => trim($postData['booking_id']));       
                $join    = array(TB_USERS => TB_USERS.".user_id = ".TB_BOOKINGS.".user_id");
                $resultUserDetails = $this->Common_model->select_join(TB_BOOKINGS.".booking_id,booking_start_date,booking_pick_up_location, booking_drop_off_location,booking_pick_up_time,booking_drop_off_time,booking_status, user_email, user_name", TB_BOOKINGS, $cond, array(), array(), $join);
                $email = !empty($resultUserDetails[0]['user_email'])?$resultUserDetails[0]['user_email']:"";
                $name = !empty($resultUserDetails[0]['user_name'])?$resultUserDetails[0]['user_name']:"";
                $subject = "Tulii trip details on ".date("Y-m-d H:i:s");
                $message = '<tr> 
                <td style="font-size:16px;word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                    <p>Hello '.$name.',</p>
                    <p>
                        <h4>Tulii trip details are following.<h4><br/>
                            <b>Trip date : '.$resultUserDetails[0]['booking_start_date'].'</b><br/>
                            <b>Pick up location : '.$resultUserDetails[0]['booking_pick_up_location'].'</b><br/>
                            <b>Drop off location : '.$resultUserDetails[0]['booking_drop_off_location'].'</b><br/>
                            <b>Pick up time : '.$resultUserDetails[0]['booking_pick_up_time'].'</b><br/>
                            <b>Drop up time : '.$resultUserDetails[0]['booking_drop_off_time'].'</b><br/>
                            <b>Status : '.$resultUserDetails[0]['booking_status'].'</b><br/>
                            <b>Video link  : '.$resultUpload['fullpath'].'</b><br/>
                            <b>Download video link  : <a href="'.$resultUpload['fullpath'].'" download="tulii_trip_on_'.date("Y_m_d_H_i_s").'">Download</a></b><br/>
                            
                            Thanks and Regards.<br/>            
                            Tulii Admin         
                        </p>                    
                    </td>
                </tr>
                ';  
                $send = sendEmailTemplate($email, $message, $subject);
                
                //result
                if ( $video_streaming ) {
                    $this->response(array("status" => true, "message" => "Video has been uploaded successfully."), 200); exit;     
                } else {
                    $this->response(array("status" => false, "message" => "Some error has been occured, please try again!!!"), 300); exit; 
                }               
            } else {
                $this->response(array("status" => false, "message" => $resultUpload['msg']), 300); exit;                                                
            }
        } else {
            $this->response(array("status" => false, "message" => 'Please fill data.'), 300); exit;
        }        
    } 

    /**
     * Title       : video_trash
     * Description : trash Video streaming of rides
     * 
     * @return Array
     */

    public function video_trash_get ()
    {
        // Get 15 days ago date
        $daysAgo = date('Y-m-d', strtotime('-15 days', strtotime(date('Y-m-d'))));
        $resultVideosData = $this->Common_model->select("id,file_path",TB_VIDEO_STREAM,array('created_at <= ' => $daysAgo)); 
        
        // trash videos
        foreach ($resultVideosData as $key => $value) {
            if ( file_exists("./video/".$value['file_path']) ) {
                unlink("./video/".$value['file_path']);
            }            
        }

        // Delete records
        $resultDeleteVideos = $this->Common_model->delete(TB_VIDEO_STREAM,array('created_at <= ' => $daysAgo)); 
        if( $resultDeleteVideos ) {
            $this->response(array("status" => true, "message" => 'Video file are deleted successfully.'), 200); exit; 
        } else {
            $this->response(array("status" => false, "message" => 'Video file are not deleted successfully.'), 300); exit; 
        }
    }

    /**
     * Title :find_driver_loc 
     * Description : find the current location of the driver within 5 miles 
     *
     * @param Double lattitude
     * @param Double loggitude     
     * @return Array
     */

    public function find_driver_loc_post () 
    {
        // Input data
        $postData = $this->post();

        // checked user is authenticate or not 
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        if ( $postData['lattitude'] != "" && $postData['loggitude'] != "" ) {            
            $cond         = array(TB_DRIVER_LOCATION.'.created_at' => date("Y-m-d"),TB_USERS.'.roleId' => 3);
            $jointype     = array(TB_DRIVER_LOCATION => "LEFT");
            $join         = array(TB_DRIVER_LOCATION => TB_DRIVER_LOCATION.".user_id = ".TB_USERS.".user_id");
            $allDriverLoc = $this->common_model->selectJoin(TB_DRIVER_LOCATION.".user_id, loc_long, loc_lat, user_email, user_name, user_phone_number, user_gender, user_address, user_birth_date, user_age, is_your_own_car, user_category_id, user_sub_category_id, driving_license_type, license_no, valid_from_date, valid_until_date, car_model, car_register_no, car_seating_capacity, car_manufacturing_year, car_age, car_pic, license_pic, school, degree, specialization, education_from, education_to, certification_name, upload_certificate, user_pic", TB_USERS, $cond, array(), $join, $jointype);

            if ( count($allDriverLoc) > 0 ) {
                $data = array();
                $i    = 0; 

                foreach ( $allDriverLoc as $key => $value ) {
                    $carpics  = $license = $certification = array();
                    $milesLoc = round($this->distance($postData['lattitude'], $postData['loggitude'], $value['loc_lat'], $value['loc_long'], "M"));           
                    
                    if ( $milesLoc <= 5 && $milesLoc >= 0 ) {                       
                        $data[$i]["driver_id"]              = $value['user_id'];
                        $data[$i]["email"]                  = $value['user_email'];
                        $data[$i]["name"]                   = $value['user_name'];
                        $data[$i]["mobile_no"]              = $value['user_phone_number'];                        
                        $data[$i]["gender"]                 = $value['user_gender'];
                        $data[$i]["address"]                = $value['user_address'];
                        $data[$i]["dob"]                    = $value['user_birth_date'];
                        $data[$i]["age"]                    = $value['user_age'];
                        $data[$i]["is_your_own_car"]        = $value['is_your_own_car'];
                        $data[$i]["driving_license_type"]   = $value['driving_license_type'];
                        $data[$i]["license_no"]             = $value['license_no'];
                        $data[$i]["valid_from_date"]        = $value['valid_from_date'];
                        $data[$i]["valid_until_date"]       = $value['valid_until_date'];
                        $data[$i]["car_model"]              = $value['car_model'];
                        $data[$i]["car_register_no"]        = $value['car_register_no'];
                        $data[$i]["car_seating_capacity"]   = $value['car_seating_capacity'];
                        $data[$i]["car_manufacturing_year"] = $value['car_manufacturing_year'];
                        $data[$i]["car_age"]                = $value['car_age'];
                        $data[$i]["school"]                 = $value['school'];
                        $data[$i]["degree"]                 = $value['degree'];
                        $data[$i]["specialization"]         = $value['specialization'];
                        $data[$i]["education_from"]         = $value['education_from'];
                        $data[$i]["education_to"]           = $value['education_to'];
                        $data[$i]["certification_name"]     = $value['certification_name'];

                        $licenseArr = explode("|", $value['license_pic']);
                        
                        if ( !empty($licenseArr[0]) ) {

                            foreach ( $licenseArr as $lkey => $lvalue ) {
                                $license[$lkey]['license_images'] = base_url().'uploads/driver/licenseCopy/'.$lvalue;
                            }
                        }

                        $carArr = explode("|", $value['car_pic']);
                        if ( !empty($carArr[0]) ) {

                            foreach ( $carArr as $ckey => $carValue ) {
                                $carpics[$ckey]['car_images'] = base_url().'uploads/driver/carPics/'.$carValue;
                            }
                        }

                        $certificateArr = explode("|", $value['upload_certificate']);
                        if ( !empty($certificateArr[0]) ) {

                            foreach ( $certificateArr as $cetkey => $certifValue ) {
                                $certification[$cetkey]['certification_images'] = base_url().'uploads/driver/certification/'.$certifValue;
                            }
                        }
                        
                        $data[$i]["license_pics"]     = $license; 
                        $data[$i]["car_pics"]         = $carpics;                                               
                        $data[$i]["certificate_pics"] = $certification;
                        $data[$i]["user_pics"]        = $value['user_pic'] ? base_url().'uploads/driver/driverImg/'.$value['user_pic']: "";                       
                        $i++;
                    } 
                }   
                
                if ( empty($data) ) {
                    $this->response(array("status" => false, "message" => 'No drivers found within 5 miles.'), 300); exit;        
                } else {
                    $this->response(array("status" => true, "drivers_list" => $data), 200); exit;        
                }                
            } else {
                $this->response(array("status" => false, "message" => 'Today no drivers available.'), 300); exit;    
            }
        } else {
            $this->response(array("status" => false,"message" => 'Please enter lattitude & loggitude.'), 300); exit;
        }
    }

    /**
     * Title : find_all_drivers_loc
     * Description : find the current location of the driver within 25 miles 
     *
     * @param : attitude
     * @param : loggitude     
     * @return Array
     */

    public function find_all_drivers_loc_post () 
    {
        // Input data
        $postData = $this->post();
        // Checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        } 

        // Validation
        if ( trim($postData['lattitude']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Lattitude field is required.'), 200); exit; 
        }    
        if ( trim($postData['loggitude']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Loggitude field is required.'), 200); exit; 
        } 

        // Get all driver details           
        $cond         = array(TB_DRIVER_LOCATION.'.created_at' => date("Y-m-d"), TB_USERS.'.roleId' => 3);
        $jointype     = array(TB_DRIVER_LOCATION => "LEFT");
        $join         = array(TB_DRIVER_LOCATION => TB_DRIVER_LOCATION.".user_id = ".TB_USERS.".user_id");
        $allDriverLoc = $this->common_model->selectJoin(TB_DRIVER_LOCATION.".user_id, loc_long, loc_lat, user_name, user_phone_number, user_pic", TB_USERS, $cond, array(), $join, $jointype);
        
        // create json formate
        if(count($allDriverLoc) > 0) {
            $driverListArr = array();
            $i = 0;

            foreach ($allDriverLoc as $key => $value) {
                $milesLoc      = round($this->distance($postData['lattitude'], $postData['loggitude'], $value['loc_lat'], $value['loc_long'], "M"));           
                
                if ( $milesLoc <= 25 && $milesLoc >= 0 ) {                       
                    $driverListArr[$i]["driver_id"] = $value['user_id'];
                    $driverListArr[$i]["name"] = $value['user_name'];
                    $driverListArr[$i]["mobile_no"] = $value['user_phone_number']; 
                    $driverListArr[$i]["lattitude"] = $value['loc_lat']; 
                    $driverListArr[$i]["loggitude"] = $value['loc_long']; 
                    $driverListArr[$i]["driver_range"] = $milesLoc." Miles";                                           
                    $driverListArr[$i]["user_pics"] = $value['user_pic'] ? base_url().'uploads/driver/driverImg/'.$value['user_pic']: base_url().'uploads/blank-profile-picture.png';                       
                    $i++;
                }
            }   
            // result
            if ( empty($driverListArr) ) {
                $this->response(array("status" => false, "message" => 'No drivers found within 5 miles.'), 300); exit;        
            } else {
                $this->response(array("status" => true, "drivers_list" => $driverListArr), 200); exit;        
            }                
        } else {
            $this->response(array("status" => false, "message" => 'Today no drivers available.'), 300); exit;    
        }        
    }

    /**
     * Title : distance
     * Description :calculate distance miles , kilometers and Nautical miles
     *
     * @return Integer 
     */

    function distance($lat1, $lon1, $lat2, $lon2, $unit) 
    {
        $theta = $lon1 - $lon2;
        $dist  = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist  = acos($dist);
        $dist  = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit  = strtoupper($unit);

        if ( $unit == "K" ) {
            return ($miles * 1.609344);
        } else if ( $unit == "N" ) {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    /**
     * Title : driver_details
     * Description: get specifi driver details 
     *
     * @param Integer user_id
     * @return Array
     */

    public function driver_details_post () 
    {
        // Input data
        $postData = $this->post();

        // Checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        if ( $postData['user_id'] != "" ) {
            $allDrivers = $this->common_model->select("*", TB_USERS, array("user_id" => $postData['user_id']));
            
            if ( count($allDrivers) > 0 ) { 

                $status = $allDrivers ? $allDrivers[0]['roleId']:"";
                if ( $status == ROLE_SERVICE_PROVIDER && $status != "" ) {
                    $driverRatingArr = $driverArr = $perInfo = $driverDetail = $education = array(); 
                    $resultDriverRating = $this->common_model->select_join("user_name, rating_id, rating, rating_comment, user_pic", TB_RATING, array("tbl_rating.user_id" => trim($postData['user_id']) ,"tbl_rating.isDeleted" => "0"), array(), array(), array(TB_USERS => 'tbl_users.user_id = tbl_rating.ratedByUserId'),null);
                    
                    if ( count($resultDriverRating) > 0 ) {
                        $j = 0; 
                        foreach ($resultDriverRating as $key => $value) {
                            $driverRatingArr[$j]['rating_id']      = $value['rating_id'];    
                            $driverRatingArr[$j]['user_name']      = $value['user_name'];
                            $driverRatingArr[$j]['rating']         = $value['rating'];
                            $driverRatingArr[$j]['user_picture']   = !empty($value['user_pic']) ? base_url().USER_PROFILE_IMG.$value['user_pic'] : base_url()."uploads/blank-profile-picture.png";
                            $driverRatingArr[$j]['rating_comment'] = $value['rating_comment'];     
                            $j++;
                        }
                    } 

                    foreach ( $allDrivers as $key => $value ) {
                        $driverArr[$key]["user_pics"]               = $value['user_pic'] ? base_url().'uploads/driver/driverImg/'.$value['user_pic']: "";
                        $driverArr[$key]["category_id"]             = $value["user_category_id"];
                        $driverArr[$key]["sub_category_id"]         = $value["user_sub_category_id"];                        
                        $driverArr[$key]["car_seating_capacity"]    = $value['car_seating_capacity'];
                        $driverArr[$key]["car_manufacturing_year"]  = $value['car_manufacturing_year'];
                        $driverArr[$key]["car_age"]                 = $value['car_age'];
                        $driverArr[$key]["total_experience"]        = $value['total_experience'];
                        $driverArr[$key]["car_number_plate"]        = $value['car_number_plate'];
                        $perInfo[$key]["driver_id"]                 = $value['user_id'];
                        $perInfo[$key]["email"]                     = $value['user_email'];
                        $perInfo[$key]["name"]                      = $value['user_name'];
                        $perInfo[$key]["mobile_no"]                 = $value['user_phone_number'];                        
                        $perInfo[$key]["gender"]                    = $value['user_gender'];
                        $perInfo[$key]["address"]                   = $value['user_address'];                                
                        $perInfo[$key]["dob"]                       = $value['user_birth_date'];                                
                        $perInfo[$key]["age"]                       = $value['user_age'];                      
                        $driverDetail[$key]["is_your_own_car"]      = $value['is_your_own_car'];
                        $driverDetail[$key]["driving_license_type"] = $value['driving_license_type'];
                        $driverDetail[$key]["license_no"]           = $value['license_no'];
                        $driverDetail[$key]["valid_from_date"]      = $value['valid_from_date'];
                        $driverDetail[$key]["valid_until_date"]     = $value['valid_until_date'];
                        $driverDetail[$key]["car_model"]            = $value['car_model'];
                        $driverDetail[$key]["car_register_no"]      = $value['car_register_no'];

                        $licenseArr = explode("|", $value['license_pic']);
                        
                        if ( !empty($licenseArr[0]) ) {

                            foreach ( $licenseArr as $lkey => $lvalue ) {
                                $license[$lkey]['license_images'] = base_url().'uploads/driver/licenseCopy/'.$lvalue;
                            }
                        } else {
                            $license = array(); 
                        }

                        $carArr = explode("|", $value['car_pic']);
                        if ( !empty($carArr[0]) ) {
                         
                            foreach ( $carArr as $ckey => $carValue ) {
                                $carpics[$ckey]['car_images'] = base_url().'uploads/driver/carPics/'.$carValue;
                            }
                        } else {
                            $carpics = array();
                        }
                        
                        $education[$key]["school"]             = $value['school'];
                        $education[$key]["degree"]             = $value['degree'];
                        $education[$key]["specialization"]     = $value['specialization'];
                        $education[$key]["education_from"]     = $value['education_from'];
                        $education[$key]["education_to"]       = $value['education_to'];
                        $education[$key]["certification_name"] = $value['certification_name'];
                        $certificateArr = explode("|", $value['upload_certificate']);

                        if ( !empty($certificateArr[0]) ) {
                            
                            foreach ( $certificateArr as $cetkey => $certifValue ) {
                                $certification[$cetkey]['certification_images'] = base_url().'uploads/driver/certification/'.$certifValue;
                            }
                        } else {
                            $certification = array();
                        }

                        $driverDetail[$key]["license_pics"]  = $license;
                        $driverDetail[$key]["car_pics"]      = $carpics;    
                        $education[$key]["certificate_pics"] = $certification;
                        $driverArr[$key]["personalInfo"]     = $perInfo;       
                        $driverArr[$key]["driverDetails"]    = $driverDetail;
                        $driverArr[$key]["education"]        = $education;
                        $driverArr[$key]["rating"]           = $driverRatingArr;   
                        
                    }
                    $this->response(array("status" => true, "driver_list" => $driverArr), 200); exit;
                } else {
                    $this->response(array("status" => false ,"message" => 'User is not driver or blank.'), 300); exit;
                }
            } else {
                $this->response(array("status" => false,"message" => 'No drivers has been exists.'), 300); exit;
            }
            
        } else {
            $this->response(array("status" => false,"message" => 'Please enter the user id'), 300); exit;
        }
    }
    
     /**
     * Title : video list
     * Description : video list
     *  
     * @param  Integer user_id
     * @return Array
     */

     public function video_list_post()
     {   
        // Checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // checked inputs are not emtpy
        if ( $this->post('user_id') != '' ) {

            // Get video data
            $cond      = array(TB_USERS.'.isDeleted' => 0, TB_USERS.'.user_status' => '1', TB_BOOKINGS.'.isDeleted' => 0, TB_VIDEO_STREAM.'.isDeleted' => 0, TB_USERS.'.user_id' => $this->post('user_id'));
            $jointype  = array(TB_USERS => "LEFT", TB_BOOKINGS => "LEFT"); 
            $join      = array(TB_BOOKINGS => TB_BOOKINGS.".booking_id = ".TB_VIDEO_STREAM.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_BOOKINGS.".user_id");       
            $videoList = $this->Common_model->selectJoin("user_name, user_phone_number, user_gender, user_address, file_path", TB_VIDEO_STREAM, $cond, array(), $join, $jointype);
            
            $arrayList = array();
            $i = 0;
            // result
            if ( count($videoList) > 0 ) {

                foreach ($videoList as $key => $value) {
                    $arrayList[$i]['user_name']         = $value['user_name'];
                    $arrayList[$i]['user_phone_number'] = $value['user_phone_number']; 
                    $arrayList[$i]['user_gender']       =  $value['user_gender'];
                    $arrayList[$i]['user_address']      = $value['user_address'];
                    $arrayList[$i]['file_path']         = !empty($value['file_path']) ? base_url()."video/".$value['file_path']:"N/A";

                    $i++;
                }
                $this->response(array("status" => true, "result" => $arrayList), 200); exit;
            } else {
                $this->response(array("status" => false, "message" => 'Video not found.'), 400); exit;
            } 
        } else {
            $this->response(array("status" => false,"message" => 'Please provide user id.'), 400); exit; 
        }
    }  
    
    // public function license_pics_post()
    // {       
    //     $postData = $this->post();      
    //     if(is_authorized()) {
    //         $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
    //     }  
    //     if($this->post('user_id') !='') {               
    //         $user_id = $this->common_model->select("*",TB_USERS,array("user_id"=>$postData['user_id']));
    //         if(count($user_id) > 0) {
    //             if(isset($_FILES['license_pics']['tmp_name']) =='') {
    //                $this->response(array("status"=>false,"message"=> 'Please upload license copy pic.'), 300);exit;  
    //             } else {
    //                 $licenseCopy= "";
    //                 $scan_copy_of_license = "";                   
    //                 $temp = "";
    //                 $fileImages = $_FILES;
    //                 $allFiles = array();

    //                 if(!empty($fileImages)) {
    //                     foreach ($fileImages as $key => $value) {
    //                         $cpt = count($_FILES[$key]['name']);
    
    //                         $temp = 'license_img';
    
    //                         $files_name = array();
    //                         for($i=0; $i<$cpt; $i++) {
    //                             $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
    //                             $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
    //                             $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
    //                             $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
    //                             $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
    //                             $files_name[$i]['name'] = $_FILES[$temp]['name'];
    //                             $files_name[$i]['type'] = $_FILES[$temp]['type'];
    //                             $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
    //                             $files_name[$i]['error'] = $_FILES[$temp]['error'];
    //                             $files_name[$i]['size'] = $_FILES[$temp]['size'];
    //                         }
    //                         $allFiles[$temp]=$files_name;                                                         
    //                     }

    //                     foreach ($allFiles as $key => $value) {
    //                         $i = 0;                                
    //                         for($j=0;$j < count($value); $j++) {                                     
    //                             if(strcmp($key,'license_img') == 0 ) {
    //                                 $scan_copy_of_license = multiple_image_upload($value[$j],'uploads/driver/licenseCopy/'); 
    //                                 if(is_array($scan_copy_of_license)) {
    //                                     $this->response($scan_copy_of_license,300);exit;
    //                                 } else {
    //                                     $licenseCopy .= "|".$scan_copy_of_license;    
    //                                 }
    //                             } 
    //                             $i++;                                                     
    //                         }
    //                     }

    //                     $userdata = array(
    //                         'license_pic'=>trim($licenseCopy,"|")                         
    //                     );                            
    //                     $user = $this->db->update(TB_USERS, $userdata,array("user_id"=>$postData['user_id']));
    //                     if ($user) {
    //                         $this->response(array('status' => true, 'message' => 'license copy pic has been uploaded sucessfully.'),200);exit;
    //                     } else {
    //                         $this->response(array("status" => false, "message" => "Something went wrong. !!"),300);exit;
    //                     }
    //                } else {
    //                 $this->response(array("status"=>false,"message"=> 'Please upload license copy pic.'), 300);exit;  
    //                }
    //             }    
    //         } else {
    //             $this->response(array("status"=>false,"message"=> 'User id not exist.'), 300);exit;
    //        }
    //     } else {
    //         $this->response(array("status"=>false,"message"=> 'Please provide user id.'), 300);exit;
    //     }        
    // }

    // public function car_pics_post()
    // {       
    //     $postData = $this->post();     
    //     if(is_authorized()) {
    //         $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
    //     }   
    //     if($this->post('user_id') !='') {               
    //         $user_id = $this->common_model->select("*",TB_USERS,array("user_id"=>$postData['user_id']));
    //         if(count($user_id) > 0) {
    //             if(isset($_FILES['car_pics']['tmp_name']) =='') {
    //                 $this->response(array("status"=>false,"message"=> 'Please upload car pic.'), 300);exit;  
    //             } else {
    //                 $upload_car_pics  = $car_pics="";                           
    //                 $temp = "";
    //                 $fileImages = $_FILES;
    //                 $allFiles = array();
    //                 if(!empty($fileImages)) {
    //                     foreach ($fileImages as $key => $value) {
    //                         $cpt = count($_FILES[$key]['name']);                                   
    //                         $temp = 'car_img';                                   
    //                         $files_name = array();
    //                         for($i=0; $i<$cpt; $i++){
    //                             $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
    //                             $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
    //                             $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
    //                             $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
    //                             $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
    //                             $files_name[$i]['name'] = $_FILES[$temp]['name'];
    //                             $files_name[$i]['type'] = $_FILES[$temp]['type'];
    //                             $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
    //                             $files_name[$i]['error'] = $_FILES[$temp]['error'];
    //                             $files_name[$i]['size'] = $_FILES[$temp]['size'];
    //                         }
    //                         $allFiles[$temp]=$files_name;                                                             
    //                     }

    //                     foreach ($allFiles as $key => $value) {
    //                         $i = 0;                                
    //                         for($j=0;$j < count($value); $j++) {                                         
    //                               if (strcmp($key,'car_img') == 0 ) {                                   
    //                                 $upload_car_pics = multiple_image_upload($value[$j],'uploads/driver/carPics/'); 
    //                                 if(is_array($upload_car_pics)) {
    //                                     $this->response($upload_car_pics,300);exit;
    //                                 } else{
    //                                     $car_pics .= "|".$upload_car_pics;    
    //                                 }
    //                             } 
    //                             $i++;                                                     
    //                         }
    //                     }                                
    
    //                     $userdata = array(
    //                         'car_pic'=>trim($car_pics,"|")                         
    //                     );                            
    //                     $user = $this->db->update(TB_USERS, $userdata,array("user_id"=>$postData['user_id']));
    //                     if ($user) {
    //                         $this->response(array('status' => true, 'message' => 'car pic has been uploaded sucessfully.'),200);exit;
    //                     } else {
    //                         $this->response(array("status" => false, "message" => "Something went wrong. !!"),300);exit;
    //                     }
    //                 } else {
    //                 $this->response(array("status"=>false,"message"=> 'Please upload car pic.'), 300);exit;  
    //                }
    //             }                    
    //         } else {
    //             $this->response(array("status"=>false,"message"=> 'User id not exist.'), 300);exit;
    //        }               

    //     }else{
    //         $this->response(array("status"=>false,"message"=> 'Please provide user id.'), 300);exit;
    //     }        
    // }

    // public function cover_pics_post()
    // {       
    //     $postData = $this->post();    
    //     if(is_authorized()) {
    //         $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
    //     }    
    //     if($this->post('user_id') !='') {               
    //         $user_id = $this->common_model->select("user_id",TB_USERS,array("user_id"=>$postData['user_id']));
    //         if(count($user_id) > 0) {
    //             if(isset($_FILES['cover_pics']['tmp_name']) =='') {
    //                 $this->response(array("status"=>false,"message"=> 'Please upload cover pic.'), 300);exit;  
    //             } else {
    //                 $upload_car_pics  = $car_pics="";                           
    //                 $temp = "";
    //                 $fileImages = $_FILES;
    //                 $allFiles = array();
    //                 if(!empty($fileImages)) {
    //                     foreach ($fileImages as $key => $value) {
    //                         $cpt = count($_FILES[$key]['name']);                                   
    //                         $temp = 'cover_img';                                   
    //                         $files_name = array();
    //                         for($i=0; $i<$cpt; $i++){
    //                             $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
    //                             $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
    //                             $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
    //                             $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
    //                             $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
    //                             $files_name[$i]['name'] = $_FILES[$temp]['name'];
    //                             $files_name[$i]['type'] = $_FILES[$temp]['type'];
    //                             $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
    //                             $files_name[$i]['error'] = $_FILES[$temp]['error'];
    //                             $files_name[$i]['size'] = $_FILES[$temp]['size'];
    //                         }
    //                         $allFiles[$temp]=$files_name;                                                             
    //                     }

    //                     foreach ($allFiles as $key => $value) {
    //                         $i = 0;                                
    //                         for($j=0;$j < count($value); $j++) {
    //                             if (strcmp($key,'cover_img') == 0 ) {                                   
    //                                 $upload_car_pics = multiple_image_upload($value[$j],'uploads/driver/coverPics/'); 
    //                                 if(is_array($upload_car_pics)) {
    //                                     $this->response($upload_car_pics,300);exit;
    //                                 } else{
    //                                     $car_pics .= "|".$upload_car_pics;    
    //                                 }
    //                             } 
    //                             $i++;                                                     
    //                         }
    //                     }                                
    
    //                     $userdata = array(
    //                         'cover_pics'=>trim($car_pics,"|")                         
    //                     );                            
    //                     $user = $this->db->update(TB_USERS, $userdata,array("user_id"=>$postData['user_id']));
    //                     if ($user) {
    //                         $this->response(array('status' => true, 'message' => 'Cover pics has been uploaded sucessfully.'),200);exit;
    //                     } else {
    //                         $this->response(array("status" => false, "message" => "Something went wrong. !!"),300);exit;
    //                     }
    //                 } else {
    //                     $this->response(array("status"=>false,"message"=> 'Please upload cover pics.'), 300);exit;  
    //                 }
    //             }    
    //         } else {
    //             $this->response(array("status"=>false,"message"=> 'User id not exist.'), 300);exit;
    //        }
    //     } else {
    //         $this->response(array("status"=>false,"message"=> 'Please provide user id.'), 300);exit;
    //     }        
    // }


    // public function certificate_pics_post()
    // {       
    //     $postData = $this->post();
    //     if(is_authorized()) {
    //         $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
    //     }         
    //     if($this->post('user_id') !='') {               
    //         $user_id = $this->common_model->select("*",TB_USERS,array("user_id"=>$postData['user_id']));
    //         if(count($user_id) > 0){
    //             if(isset($_FILES['certificate_pics']['tmp_name']) =='') {
    //                  $this->response(array("status"=>false,"message"=> 'Please upload certificate pic.'), 300);exit;  
    //             } else { 
    //                 $upload_certification = $certification ="";                           
    //                 $temp = "";
    //                 $fileImages = $_FILES;
    //                 $allFiles = array();
    //                 if(!empty($fileImages)) {
    //                     foreach ($fileImages as $key => $value) {
    //                         $cpt = count($_FILES[$key]['name']);
    //                        $temp = 'certification_img';
    //                         $files_name = array();
    //                         for($i=0; $i<$cpt; $i++){
    //                             $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
    //                             $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
    //                             $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
    //                             $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
    //                             $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
    //                             $files_name[$i]['name'] = $_FILES[$temp]['name'];
    //                             $files_name[$i]['type'] = $_FILES[$temp]['type'];
    //                             $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
    //                             $files_name[$i]['error'] = $_FILES[$temp]['error'];
    //                             $files_name[$i]['size'] = $_FILES[$temp]['size'];
    //                         }
    //                         $allFiles[$temp]=$files_name;                                                             
    //                     }

    //                     foreach ($allFiles as $key => $value) {
    //                         $i = 0;                                
    //                         for($j=0;$j < count($value); $j++) { 
    //                             if (strcmp($key,'certification_img') == 0 ) {
    //                                 $upload_certification = multiple_image_upload($value[$j],'uploads/driver/certification/'); 
    //                                 if(is_array($upload_certification)) {
    //                                     $this->response($upload_certification,300);exit;
    //                                 } else {
    //                                     $certification .= "|".$upload_certification;    
    //                                 }
    //                             } 
    //                             $i++;                                                     
    //                         }
    //                     }                                
    
    //                     $userdata = array(
    //                         'upload_certificate'=>trim($certification,"|")                         
    //                     );                            
    //                     $user = $this->db->update(TB_USERS, $userdata,array("user_id"=>$postData['user_id']));
    //                     if ($user) {
    //                         $this->response(array('status' => true, 'message' => 'certificate pic has been uploaded sucessfully.'),200);exit;
    //                     } else {
    //                         $this->response(array("status" => false, "message" => "Something went wrong. !!"),300);exit;
    //                     }
    //                } else {
    //                     $this->response(array("status"=>false,"message"=> 'Please upload certificate pic.'), 300);exit;  
    //                } 
    //             }    
    //         } else {
    //             $this->response(array("status"=>false,"message"=> 'User id not exist.'), 300);exit;
    //         } 
    //     } else {
    //         $this->response(array("status"=>false,"message"=> 'Please provide user id.'), 300);exit;
    //     }        
    // }

    /**
     * Title       : upload_pictures
     * Description : Upload multiple pictures
     * 
     * @param  Integer user_id
     * @param  String certificate_pics
     * @param  String license_pics
     * @param  String car_pics
     * @param  String cover_pics     
     * @return Array
     */

    public function upload_pictures_post ()
    {       
        // Post data
        $postData = $this->post();      
        
        // Checked user is authenticate
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        }  
        
        // Checked user not empty
        if ( $this->post('user_id') != "" ) {               
            $user_id = $this->common_model->select("*", TB_USERS, array("user_id" => $postData['user_id']));
            
            // Checked user is exist or not
            if ( count($user_id) > 0 ) {

                $flag = 0;
                $filename = "";
                $filePath = "";
                if ( isset($_FILES['certificate_pics']['tmp_name']) != "" ) {
                    $filename = "certificate";
                    $clmnName = "upload_certificate";
                    $filePath = 'uploads/driver/certification/';
                    $flag     = 1;
                }   
                if ( isset($_FILES['license_pics']['tmp_name']) != "" ) {
                    $filename = 'license';
                    $clmnName = "license_pic";
                    $filePath = 'uploads/driver/licenseCopy/';
                    $flag     = 1;
                } 
                if ( isset($_FILES['car_pics']['tmp_name']) != "" ) {
                    $filename = 'car';
                    $clmnName = "car_pic";
                    $filePath = 'uploads/driver/carPics/';
                    $flag     = 1;
                } 
                if ( isset($_FILES['cover_pics']['tmp_name']) != "" ) {                   
                    $filename = 'cover';
                    $clmnName = "cover_pics";
                    $filePath = 'uploads/driver/coverPics/';
                    $flag     = 1;
                } 
                if ( $flag == 0 ) {
                    $this->response(array("status" => false,"message" => "Please upload images."), 300); exit;  
                }

                // Upload pictures
                if ( $flag == 1 ) 
                {
                    $uploadFileName       = "";
                    $dynamicUploadedFile  = "";                   
                    $temp                 = "";
                    $fileImages           = $_FILES;
                    $allFiles             = array();

                    if ( !empty($fileImages) ) {

                        foreach ( $fileImages as $key => $value ) {
                            $files_name   = array();
                            $countAllFile = count($_FILES[$key]['name']);                           
                            $temp         = $filename;                           
                            
                            for ( $i=0; $i < $countAllFile; $i++ ) {
                                $_FILES[$temp]['name']      = $_FILES[$key]['name'][$i];
                                $_FILES[$temp]['type']      = $_FILES[$key]['type'][$i];
                                $_FILES[$temp]['tmp_name']  = $_FILES[$key]['tmp_name'][$i];
                                $_FILES[$temp]['error']     = $_FILES[$key]['error'][$i];
                                $_FILES[$temp]['size']      = $_FILES[$key]['size'][$i];    
                                $files_name[$i]['name']     = $_FILES[$temp]['name'];
                                $files_name[$i]['type']     = $_FILES[$temp]['type'];
                                $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                                $files_name[$i]['error']    = $_FILES[$temp]['error'];
                                $files_name[$i]['size']     = $_FILES[$temp]['size'];
                            }
                            $allFiles[$temp]=$files_name;                                                         
                        }

                        // uploaded multiple images
                        foreach ( $allFiles as $key => $value ) {
                            
                            for ( $i=0, $j=0; $j < count($value); $j++ ) {                                     
                                if ( strcmp($key,$filename) == 0 ) {
                                    $dynamicUploadedFile = multiple_image_upload( $value[$j], $filePath ); 
                                    if ( is_array($dynamicUploadedFile) ) {
                                        $this->response($dynamicUploadedFile, 300); exit;
                                    } else {
                                        $uploadFileName .= "|".$dynamicUploadedFile;    
                                    }
                                } 
                                $i++;                                                     
                            }
                        }

                        // update table
                        $userdata = array(
                            $clmnName =>trim($uploadFileName,"|")                         
                            );                            
                        $user = $this->db->update(TB_USERS, $userdata, array("user_id" => $postData['user_id']));
                        
                        // result                       
                        if ($user) {
                            $this->response(array('status' => true, 'message' => "${filename} picture has been uploaded sucessfully."),200); exit;
                        } else {
                            $this->response(array("status" => false, "message" => "Something went wrong. !!"), 300); exit;
                        }
                    } else {
                        $this->response(array("status" => false, "message" => "Please upload ${filename} pic."), 300); exit;  
                    }
                }    
            } else {
                $this->response(array("status" => false, "message" => 'User id not exist.'), 300); exit;
            }
        } else {
            $this->response(array("status" => false, "message" => 'Please provide user id.'), 300); exit;
        }        
    }   

    /**
     * demo_post
     * @param lat,lng
     * @return  json
     */
    function lat_lng_post() 
    {
        $params = (array) json_decode(file_get_contents('php://input'), TRUE);
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }  
        if(empty($params['location']['coords']['latitude']) || empty($params['location']['coords']['longitude'])) {
         $this->response(array("status"=>fasle, "message"=> "Please provide lattitude & longitude."), 400);exit; 
     }
     
     $arrData = array("current_lat"=>$params['location']['coords']['latitude'],"current_lng"=>$params['location']['coords']['longitude']);
     $resultData = $this->Common_model->insert(TB_DEMO, $arrData);
     if($resultData) {
        $this->response(array("status"=>true,"message"=> '1 record has been inserted successfully'), 200);exit;
    } else {
        $this->response(array("status"=>false,"message"=> 'Something went wrong'), 300);exit;
    }        
}

    /**
     * latest_lat_lng
     */
    public function latestLatLng_get()
    {     
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }     
        $latestLatLng = $this->db->order_by('id',"desc")
        ->limit(1)
        ->get(TB_DEMO)
        ->row();      
        $data = array();

        if(count($latestLatLng) > 0) {
            $latLng['id']=$latestLatLng->id;
            $latLng['lattitude']=$latestLatLng->current_lat;
            $latLng['longitude']=$latestLatLng->current_lng;
            $data[] = $latLng;
            $this->response(array("status"=>true,"result"=>$data), 200);exit;
        } else {
            $this->response(array("status"=>false,"message"=> 'No lattitude & longitude are exists.'), 400);exit;
        }
    }  

    /**
    Ashwini Changes
     * Driver rating
     *
     * @param driver_id
     * @param user_id
     * @param rating
     * @param comment
     * @param desc
     * @return array
     **/  
    public function rating_post()
    {
        $postData = $this->post();
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }  
        if($postData['driver_id'] == "")
        { 
            $this->response(array("status"=>"error","message"=> 'Driver id field is required.'), 200);exit;
        }
        if($postData['user_id'] == "")
        { 
            $this->response(array("status"=>"error","message"=> 'User id field is required.'), 200);exit;
        }    
        if($postData['rating'] == "")
        { 
            $this->response(array("status"=>"error","message"=> 'Rating field is required.'), 200);exit;
        }    
        if($postData['comment'] == "")
        { 
            $this->response(array("status"=>"error","message"=> 'Comment field is required.'), 200);exit;
        }

        if(!empty($postData['rating']) && $postData['rating'] <= 3)
        {
            if(empty($postData['description']))
            {
                $this->response(array("status"=>"error","message"=> 'Description field is required.'), 200);exit;
            }else
            {
                $desc = $postData['description'];
            }
        }else
        {
            $desc = '';
        }
        $arrData = array(
            "user_id"=>$postData['driver_id'],
            "rating"=>$postData['rating'],
            "rating_comment"=>$postData['comment'],
            "rating_desc"=>$desc,
            "ratedByUserId"=>$postData['user_id'],
            "isDeleted"=>0);
        $resultData = $this->Common_model->insert(TB_RATING, $arrData);
        if($resultData) {
            //--- Block driver if rating <= 3 for 2 times ----
            $cond = array("user_id"=>$postData['driver_id'],"rating <= "=>3);
            $checkDriver = $this->common_model->select("COUNT(*) as rate_cnt",TB_RATING,$cond);
            if($checkDriver[0]['rate_cnt'] >= 2)
            {
                $this->common_model->update(TB_USERS,array("user_id"=>$postData['driver_id']),array("is_block"=>"1"));
            }

            $this->response(array("status"=>true,"message"=> 'Rating has been inserted successfully'), 200);exit;
        } else {
            $this->response(array("status"=>false,"message"=> 'Something went wrong'), 300);exit;
        }
    }

    
    /**
     * Title : rating_list
     * Description : show the list of driver rating 
     *
     * @return Array     
     */ 

    public function rating_list_get ()
    {
        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }  
        
        $ratingList = array();
        $i = 0;
        $join     = array(TB_USERS => TB_USERS.".user_id=".TB_RATING.".user_id");
        $resultOfRatingData = $this->Common_model->select_join(TB_RATING.".user_id, ROUND(AVG(rating),1) as rating,user_pic,user_name as driver_name",TB_RATING,array(TB_RATING.".isDeleted" => "0"),array(),array(),$join,"tbl_rating.user_id");
        
        if ( count($resultOfRatingData) > 0 ) {

            foreach ($resultOfRatingData as $key => $value) {
                $ratingList[$i]['user_id']     = $value['user_id'];
                $ratingList[$i]['driver_name'] = $value['driver_name']; 
                $ratingList[$i]['user_pic']    =  !empty($value['user_pic']) ? base_url()."uploads/driver/driverImg/".$value['user_pic'] : base_url()."uploads/blank-profile-picture.png";
                $ratingList[$i]['rating']      = $value['rating'];
                $i++;
            }
            $this->response(array("status" => true, "result" => $ratingList), 200); exit;
            
        } else {
            $this->response(array("status" => false, "message" => 'No driver ratings found.'), 300); exit;
        }       
    }   

    /**
     * get driver location
     */
    public function get_driver_location_post()
    {
        $postData = $this->post();
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }  
        if($postData['driver_id'] == "")
            { $this->response(array("status"=>"error","message"=> 'driver id field is required.'), 200);exit; }    
        $getDriverLatLng = $this->common_model->select("user_id,loc_lat,loc_long ",TB_DRIVER_LOCATION,array("user_id"=>$postData["driver_id"]));
        if($getDriverLatLng) {
            $this->response(array("status"=>true,"result"=> $getDriverLatLng), 200);exit;
        } else {
            $this->response(array("status"=>false,"message"=> 'Something went wrong'), 300);exit;
        }        
    }

    /**
     * Title : driver_edit_profile
     * Description : edit driver profile
     *
     * @param Integer user_id
     * @param String user_email
     * @return Array
     */

    public function driver_edit_profile_post()
    {
        $data = $_POST;
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }  
        if (isset($data['user_id']) && $data['user_id'] > 0) {        
            $result=$this->common_model->select_join("user_id,role",TB_USERS,array("user_id"=>trim($data['user_id'])),array(),array(),array(TB_ROLES=>'tbl_users.roleId = tbl_roles.roleId'),null);            
            if(!count($result)) {
                $this->response(array('status' => false, 'message' => 'Invalid user'), 300);exit;
            } else {                   
                //check email 
                $emailExist = $this->common_model->select_join("user_email",TB_USERS,array("user_email"=>trim($this->post('user_email')),"user_id !="=> $this->post('user_id')),array(),array(),array(),null); 

                if ($emailExist) {
                    $this->response(array("status" => false, "message" => "Duplicate email!!!"),300);exit;
                }           
                $dateOfBirth =$this->post('dob');
                $today = date("Y-m-d");
                $diff = date_diff(date_create($dateOfBirth), date_create($today));
                $age = $diff->format('%y');

                //----- Ashwini's code here --------
                if($result[0]['role'] == "Tutor")
                {
                    if(empty($this->post('tutor_status')))
                    {
                        $this->response(array('status' => "error", 'message' => 'Tutor status is required.'), 200);exit;
                    }else
                    {
                        $tutor_status = $this->post('tutor_status');
                    }
                }else
                {
                    $tutor_status = '';
                }


                $userdata = array(
                    'user_name' => $this->post('user_name'),
                    'user_gender' => $this->post('gender'),
                    'user_address' => $this->post('address'),
                    'user_add_lat_lng' => $this->post('user_add_lat_lng'),
                    'tutor_status' => $tutor_status,
                    // 'user_email' => $this->post('user_email'),                
                    'user_phone_number' => $this->post('contact_no'),
                    'user_birth_date' => $dateOfBirth, 
                    'tutoring_subject' => $this->post('subject'),
                    'user_age' => $age
                    );
                try {
                 $this->common_model->update(TB_USERS,array("user_id"=>$data['user_id']),$userdata);
             } catch (Exception $ex) {
                $this->response(array('status' => false, 'message' => $ex->getCode()),300);exit;
            }                
            $this->response(array('status' => true, 'message' => 'User updated successfully.'), 200);exit;
        }
    } else {
        $this->response(array('status' => false, 'message' => 'Provide user_id'), 300);exit;
    }
}


    /**
     * Title       : logout
     * Description : User logout
     *   
     * @param Integer user_id
     * @param Integer device_id
     * @return Array
     */

    function logout_post ()
    {
        // Input data
    	$postData = $_POST;

        // Checked user is authenticate or not
        if( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }  

        if ( isset($postData['user_id']) && $postData['user_id'] > 0 ) {

        	if ( isset($postData["device_id"]) && $postData["device_id"] != "" ) {
        		$cond     = array("user_id"=>$postData['user_id']);
        		$userData = $this->data_model->getAllUsers($cond);
        		$ssss     = explode(",", $userData[0]['user_device_id']);

               if ( in_array($postData['device_id'], $ssss) ) {
                 unset($ssss[array_search($postData['device_id'],$ssss)]);
                 $test = implode(',', $ssss);
                 $this->common_model->update(TB_USERS, array("user_id" => $postData['user_id']), array('user_device_id' => $test));
             }
             $this->response(array('status' => true, 'message' => 'You have logout successfully.'), 200); exit;        		
         } else {
           $this->response(array('status' => false, 'message' => 'Provide device_id'), 300); exit;
       }
   } else {
       $this->response(array('status' => false, 'message' => 'Provide user_id'), 300); exit;
   }
}

    /**
     * Title       : resolution_issues
     * Description : Resolution center of service provider issues
     *   
     * @return Array
     */

    public function resolution_issues_get () 
    {
        // Checked user is authenticate or not
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }

        // Get Issues list 
        $resultResolutionIssues = $this->Common_model->select("id, parent_name, parent_number, service_provider, issue, issue_date, CASE WHEN issue_status = 1 THEN 'Issue is pending' ELSE 'Issue is resolved' END as issue_status", TB_RESOLUTION_CENTER, array('isDelete' => 1));
        if ( count($resultResolutionIssues) > 0 ) {
            $this->response(array('status' => true, 'result' => $resultResolutionIssues), 200); exit;              
        } else {
            $this->response(array('status' => false, 'message' => 'No resolution issues are available.'), 300); exit;       
        }      
    }

    /**
     * Title       : contact_list
     * Description : Contact us list
     *   
     * @return Array
     */

    public function contact_list_get () 
    {
        // Checked user is authenticate or not
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }
        
        // Get contact list 
        $resultContactUs = $this->Common_model->select("contact_id as id, contact_name, contact_email as email, contact_phone_number as mobile_no, contact_subject as subject, contact_message as message", TB_CONTACT, array('isDeleted' => 0));
        if ( count($resultContactUs) > 0 ) {
            $this->response(array('status' => true, 'result' => $resultContactUs), 200); exit;              
        } else {
            $this->response(array('status' => false, 'message' => 'No contact details are available.'), 300); exit;       
        }      
    }

    /**
     * Title       : contact_us
     * Description : User contact us
     *   
     * @param String name 
     * @param String email 
     * @param String contact_no 
     * @param String message 
     * @return Array
     */

    public function contact_us_post () 
    {
        // Checked user is authenticate or not
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }

        $postData = $this->post();

        // Validation
        if ( trim($postData['name']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'name field is required.'), 200); exit; 
        }    
        if ( trim($postData['email']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Email field is required.'), 200); exit; 
        } 
        if ( trim($postData['contact_no']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Contact no field is required.'), 200); exit; 
        }
        if ( trim($postData['message']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Message field is required.'), 200); exit; 
        } 
        if ( trim($postData['createdBy']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Created by field is required.'), 200); exit; 
        } 

        
        $contactArr = array();
        $contactArr = array( 
            "contact_name" => ucfirst(trim($postData['name'])),
            "contact_email" => trim($postData['email']),
            "contact_phone_number" => trim($postData['contact_no']),
            "contact_message" => trim($postData['message']),
            "createdBy"    => trim($postData['createdBy'])
            );
        
        // insert contact 
        $resultContactUs = $this->Common_model->insert(TB_CONTACT, $contactArr);
        if ( $resultContactUs ) {
            $resultUserName = $this->Common_model->select("user_name", TB_USERS, array('user_id' => trim($postData['createdBy'])));


            $this->load->library("email");
            $this->load->helper('email_template_helper'); 
            $username = !empty($resultUserName)?$resultUserName[0]['user_name']:'';        
            $hostname = $this->config->item('hostname');
            $email = trim($postData['email']);
            $config['mailtype'] ='html';
            $config['charset'] ='iso-8859-1';
            $this->email->initialize($config);
            $from  = EMAIL_FROM; 
            $this->messageBody  = email_header();
            $this->messageBody  .= "Hello ".$username." 
            <br/><br/>Thank you for contact us, as soon as possible we will contact us. 
            
            ";           
            //if(empty($subject)) 
            $subject =  'Contact us of tulii';   
            $this->messageBody  .= email_footer();
            $this->email->from($from, FROM_NAME);
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->message($this->messageBody);
            $send = $this->email->send();
            
            $this->response(array('status' => true, 'message' => "Contact us has been inserted successfully."), 200); exit;              
        } else {
            $this->response(array('status' => false, 'message' => 'Something went wrong.'), 300); exit;       
        }      
    }


    /**
     * Title       : subject_list
     * Description : All subject list
     *   
     * @return Array
     */

    public function subject_list_get () 
    {
        // Checked user is authenticate or not
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300); exit;
        }
        
        // Get subject list 
        $resultSubjects = $this->Common_model->select("*", TB_SUBJECTS);
        if ( count($resultSubjects) > 0 ) {
            $this->response(array('status' => true, 'result' => $resultSubjects), 200); exit;              
        } else {
            $this->response(array('status' => false, 'message' => 'No subject are available.'), 300); exit;       
        }      
    }

    /**
     * Title       : teacher_list
     * Description : All teacher list
     *   
     * @return Array
     */

    public function teacher_list_get () 
    {
        // Checked user is authenticate or not
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300); exit;
        }
        
        // Get teacher list 
        $resultTeachers = $this->Common_model->select("*", TB_TEACHERS);
        if ( count($resultTeachers) > 0 ) {
            $this->response(array('status' => true, 'result' => $resultTeachers), 200); exit;              
        } else {
            $this->response(array('status' => false, 'message' => 'No teachers are available.'), 300); exit;       
        }      
    }


    // Store card details on Stripe payment gateway
    
    function save_card_post()
    {
        $postData = $_POST;
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }

        require_once APPPATH."third_party/stripe/init.php";
        \Stripe\Stripe::setApiKey("sk_test_pCPNKY3zT06WVxwqH5VBvETE");
        $t = \Stripe\Token::create(array( "card" => array(
            "number" => "4242424242424242",
            "exp_month" => 11,
            "exp_year" => 2018,
            "cvc" => "314"  )));

        $token  = $t->id;
        $name = 'Ram';
        $email = 'kiran@excepptionaire.co';
        $card_num = "4242424242424242";
        $card_cvc = "314";
        $card_exp_month = 11;
        $card_exp_year = 2018;

        $stripe = array(
          "secret_key"      => "sk_test_pCPNKY3zT06WVxwqH5VBvETE",
          "publishable_key" => "pk_test_i9V0Rd1zj1INzemrZvO6IUW0"
          );
        
        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        if($token !='')
        {
            //add customer to stripe
            $customer = \Stripe\Customer::create(array(
                'email' => $email,
                'source'  => $token
                ));
            if($customer->id !='')
            {

                $userdata = array(                    
                    'customer_id'            => $customer->id
                    );
                $user = $this->db->update(TB_USERS, $userdata, array('user_id' =>$postData['user_id']));

                $this->response(array('status' => true, 'customer_id'=>$customer->id, 'message' => 'You have successfully created customer.'), 200);exit;
            }else{
                $this->response(array('status' => true, 'message' => 'Something went wrong!!!.'), 200);exit;
            }

        }else{
            $this->response(array('status' => true, 'message' => 'You have added invalide card details.'), 200);exit;
        }            
    }


    public function makePayment_post()
    {
      $postData = $_POST;
         // echo "<pre>";print_r($_POST);die;
      if(is_authorized()) {
        $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
    }

    if("" == $postData['user_id'])
    {
      $this->response(array('status' => 'error','message' => 'please enter user id'));
  }

  if("" == $postData['amount'])
  {
      $this->response(array('status' => 'error','message' => 'please enter amount'));
  }

  if("" == $postData['booking_id'])
  {
      $this->response(array('status' => 'error','message' => 'please enter booking id'));
  }



  $getUserData = $this->common_model->select('*',TB_USERS,array('user_id' => $postData['user_id']));
          // echo "<pre>";print_r($getUserData);die;
  if(count($getUserData) > 0)
  {
      if('2' != $getUserData[0]['roleId'])
      {
          $this->response(array('status' => 'error','message' => 'Invalid User Type'));
          die();
      }
  }
  else
  {
      $this->response(array('status' => 'error','message' => 'No User Available'));
      die();
  }

  try
  {
    require_once APPPATH."third_party/stripe/init.php";
                // \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);


    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 14; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

                /*$itemName = "Stripe Donation dmo";
                $itemNumber = "PS123456";
                $itemPrice = $postData['amount'];*/
                //$currency = "usd";
                $orderID = $randomString;
               // echo "<pre>";print_r($orderID);
                $charge = \Stripe\Charge::create(array(
                   'customer' => $getUserData[0]['customer_id'],
                 'amount'   => ($postData['amount'] * 100), // cents to dollar conversion
                 'currency' => "usd",
                 'description' => "Ride booking",
                 'metadata' => array(
                     'item_id' => $postData['booking_id']
                     )
                 ));

                // echo "<pre>"; print_r($charge);die;

                if(null!= $charge)
                {
                    $data = array(

                        'user_id' => $postData['user_id'],
                        'booking_id' => $postData['booking_id'],
                        'order_id' => $orderID,
                        'status' => 'paid',
                        'sp_status' => 'pending',
                        'amount' => $postData['amount'],
                        'created_at' => date('Y-m-d h:i:s'),
                        'on_hold_reason' => ''
                        );

                    $insertData  = $this->common_model->insert(TB_PAYMENT,$data);

                    if($insertData)
                    {
                        $this->response(array('status' => 'success','message' => 'Payment completed'));
                    }
                    else
                    {
                     $this->response(array('status' => 'error','message' => 'Something went wrong !!'));
                 }
             }
         }
         catch (\Stripe\Error\Card $e)
         {
                // Card was declined.

            $this->response(array('status' => 'error','message' => $e->getJsonBody()));

        }
        catch (\Stripe\Error\ApiConnection $e)
        {
                // Network problem, perhaps try again.

            $this->response(array('status' => 'error','message' => $e->getJsonBody()));

        }
        catch (\Stripe\Error\InvalidRequest $e)
        {
                // You screwed up in your programming. Shouldn't happen!

            $this->response(array('status' => 'error','message' => $e->getJsonBody()));

        }
        catch (\Stripe\Error\Api $e)
        {
                // Stripe's servers are down!

            $this->response(array('status' => 'error','message' => $e->getJsonBody()));

        }
        catch (\Stripe\Error\Base $e)
        {
                // Something else that's not the customer's fault.

           $this->response(array('status' => 'error','message' => $e->getJsonBody()));
       }
   }

    /**
     * Title : payment_onhold
     * Description : payment onhold of service provider
     *
     * @param Integer payment_id
     * @param String reason
     * @return Array
     */

    public function payment_onhold_post ()
    {
        // Input data
        $postData = $this->post();

        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }  

        // Validation
        if ( trim($postData['payment_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'payment id field is required.'), 200); exit; 
        }    
        if ( trim($postData['reason']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Reason field is required.'), 200); exit; 
        }        
        
        // payment onhold of service provider 
        $resultUpdate = $this->Common_model->update(TB_PAYMENT, array('id' => $postData['payment_id']), array("on_hold_reason" => $postData['reason'], "status" => ONHOLD));
        if ( $resultUpdate ) {
            $this->response(array('status' => true, 'message' => 'Payment has been onhold successfully .'), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Something went wrong.'), 300);exit;
        }  
        
    }   


    /**
     * Title : payment_pending_list
     * Description : show the list of payment pending list
     *
     * @return Array     
     */ 

    public function payment_pending_list_get ()
    {
        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }  
        $payPendingList = $payList = array();
        $i = 0;

        $cond        = array(TB_BOOKINGS.'.booking_status' => COMPLETED, TB_USERS.".isDeleted" => 0, TB_PAYMENT.".status" => PENDING);
        $jointype    = array(TB_BOOKINGS => "LEFT", TB_USERS => "LEFT", TB_SERVICE_REQUESTS => "LEFT");
        $join        = array(TB_BOOKINGS=>TB_BOOKINGS.".booking_id = ".TB_PAYMENT.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_PAYMENT.".user_id", TB_SERVICE_REQUESTS => TB_SERVICE_REQUESTS.'.service_booking_id = '.TB_BOOKINGS.'.booking_id');
        $paymentData = $this->Common_model->selectJoin("id as payment_id, service_provider_user_id, user_name, booking_pick_up_location, booking_drop_off_location, booking_start_date, booking_price, status, user_rating",TB_PAYMENT,$cond,array(),$join,$jointype);
        
        if( count($paymentData) > 0 ) {
            
            foreach ($paymentData as $key => $value) {
                $payPendingList[$i]['payment_id']                = $value['payment_id'];
                $payPendingList[$i]['service_provider_user_id']  = $value['service_provider_user_id'];
                $payPendingList[$i]['Driver_name']               = $value['user_name'];
                $payPendingList[$i]['booking_pick_up_location']  = $value['booking_pick_up_location']; 
                $payPendingList[$i]['booking_drop_off_location'] = $value['booking_drop_off_location']; 
                $payPendingList[$i]['booking_start_date']        = $value['booking_start_date']; 
                $payPendingList[$i]['booking_price']             = $value['booking_price']; 
                $payPendingList[$i]['status']                    = $value['status']; 
                $payPendingList[$i]['user_rating']               = $value['user_rating'];                

                $i++;
            }
            $payList['pending_payment_list'] =  $payPendingList; 
            
            $this->response(array("status" => true, "result" => $payList), 200); exit;
            
        } else {
            $this->response(array("status" => false, "message" => 'Payment pending records not found.'), 300); exit;
        }
    }   

    /**
     * Title : payment_paid_list
     * Description : show the list of payment paid list
     *
     * @return Array     
     */ 

    public function payment_paid_list_get ()
    {
        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }  

        $payPaidList = $payList = array();
        $i = 0;

        $cond        = array(TB_BOOKINGS.'.booking_status' => COMPLETED, TB_USERS.".isDeleted" => 0, TB_PAYMENT.".status" => PAID);
        $jointype    = array(TB_BOOKINGS => "LEFT", TB_USERS => "LEFT", TB_SERVICE_REQUESTS => "LEFT");
        $join        = array(TB_BOOKINGS=>TB_BOOKINGS.".booking_id = ".TB_PAYMENT.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_PAYMENT.".user_id", TB_SERVICE_REQUESTS => TB_SERVICE_REQUESTS.'.service_booking_id = '.TB_BOOKINGS.'.booking_id');
        $paymentData = $this->Common_model->selectJoin("id as payment_id, service_provider_user_id, user_name, booking_pick_up_location, booking_drop_off_location, booking_start_date, booking_price, status, user_rating",TB_PAYMENT,$cond,array(),$join,$jointype);
        
        if( count($paymentData) > 0 ) {
            
            foreach ($paymentData as $key => $value) {
                $payPaidList[$i]['payment_id']                = $value['payment_id'];
                $payPaidList[$i]['service_provider_user_id']  = $value['service_provider_user_id'];
                $payPaidList[$i]['Driver_name']               = $value['user_name'];
                $payPaidList[$i]['booking_pick_up_location']  = $value['booking_pick_up_location']; 
                $payPaidList[$i]['booking_drop_off_location'] = $value['booking_drop_off_location']; 
                $payPaidList[$i]['booking_start_date']        = $value['booking_start_date']; 
                $payPaidList[$i]['booking_price']             = $value['booking_price']; 
                $payPaidList[$i]['status']                    = $value['status']; 
                $payPaidList[$i]['user_rating']               = $value['user_rating'];                

                $i++;
            }
            $payList['paid_payment_list'] =  $payPaidList; 
            
            $this->response(array("status" => true, "result" => $payList), 200); exit;
            
        } else {
            $this->response(array("status" => false, "message" => 'Payment paid records not found.'), 300); exit;
        }
    } 

    /**
     * Title : payment_onhold_list
     * Description : show the list of payment on-hold list
     *
     * @return Array     
     */ 

    public function payment_onhold_list_get ()
    {
        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }  

        $payOnholdList = $payList = array();
        $i = 0;

        $cond        = array(TB_BOOKINGS.'.booking_status' => COMPLETED, TB_USERS.".isDeleted" => 0, TB_PAYMENT.".status" => ONHOLD);
        $jointype    = array(TB_BOOKINGS => "LEFT", TB_USERS => "LEFT", TB_SERVICE_REQUESTS => "LEFT");
        $join        = array(TB_BOOKINGS=>TB_BOOKINGS.".booking_id = ".TB_PAYMENT.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_PAYMENT.".user_id", TB_SERVICE_REQUESTS => TB_SERVICE_REQUESTS.'.service_booking_id = '.TB_BOOKINGS.'.booking_id');
        $paymentData = $this->Common_model->selectJoin("id as payment_id, service_provider_user_id, user_name, booking_pick_up_location, booking_drop_off_location, booking_start_date, booking_price, status, user_rating",TB_PAYMENT,$cond,array(),$join,$jointype);
        
        if( count($paymentData) > 0 ) {
            
            foreach ($paymentData as $key => $value) {
                $payOnholdList[$i]['payment_id']                = $value['payment_id'];
                $payOnholdList[$i]['service_provider_user_id']  = $value['service_provider_user_id'];
                $payOnholdList[$i]['Driver_name']               = $value['user_name'];
                $payOnholdList[$i]['booking_pick_up_location']  = $value['booking_pick_up_location']; 
                $payOnholdList[$i]['booking_drop_off_location'] = $value['booking_drop_off_location']; 
                $payOnholdList[$i]['booking_start_date']        = $value['booking_start_date']; 
                $payOnholdList[$i]['booking_price']             = $value['booking_price']; 
                $payOnholdList[$i]['status']                    = $value['status']; 
                $payOnholdList[$i]['user_rating']               = $value['user_rating'];                

                $i++;
            }
            $payList['onhold_payment_list'] =  $payOnholdList; 
            
            $this->response(array("status" => true, "result" => $payList), 200); exit;
            
        } else {
            $this->response(array("status" => false, "message" => 'Payment on-hold records not found.'), 300); exit;
        }
    } 

    /**
    * Ashwini changes here
     * Title : add_group
     * Description : create new groups
     *
     * @param Integer user_id
     * @param Integer group_id
     * @param String group_name
     * @param String group_flag
     * @return Array
     */

    public function add_group_post ()
    {
        // Input data
        $postData = $this->post();

        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }  

        if(empty(trim($postData['group_flag'])))
        {
            $this->response(array("status" => "error", "message" => 'group flag field is required.'), 200); exit; 
        }

        // Validation
        if(trim($postData['group_flag']) != "parent_parent")
        {
            if ( trim($postData['user_id']) == "" ) { 
                $this->response(array("status" => "error", "message" => 'User id field is required.'), 200); exit; 
            }
        }    
        if ( trim($postData['group_name']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'group name field is required.'), 200); exit; 
        } else if ( is_group_exist(trim($postData['group_name'])) && empty($postData['group_id']) ) {
            $this->response(array("status" => "error", "message" => 'group name are already exist.'), 200); exit; 
        }

        $count = count(explode(',',$postData['user_id']));    

        if ( !empty($postData['group_id']) ) {

            // Group member data update 
            $resultUpdate = $this->Common_model->update(TB_GROUP, array('group_id' => $postData['group_id']), array("user_id" => $postData['user_id'], "group_name" => $postData['group_name'], "no_of_people" => $count, 'updated_date' => date("Y-m-d H:i:s"), "updated_by" => $postData['modify_by']));
            if ( $resultUpdate ) {
                $this->response(array('status' => true, 'message' => 'Group has been Updated successfully .'), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Something went wrong.'), 300);exit;
            }  
        } else {
            // insert group details
            if(trim($postData['group_flag']) == "parent_parent")
            {
                $all_user = '';
                $total_people = 0;
            }else
            {
                $all_user = trim($postData['user_id']);
                $total_people = $count;
            }

            $arrData    = array("user_id" => $all_user, "group_name" => $postData['group_name'], "no_of_people" => $total_people, "created_date" => date("Y-m-d H:i:s"), "created_by" => $postData['created_by']);
            $resultGrouData = $this->Common_model->insert(TB_GROUP, $arrData);
            if ($resultGrouData) {
                if(trim($postData['group_flag']) == "parent_parent")
                {
                    $each_user = explode(",",trim($postData['user_id']));
                    foreach ($each_user as $keyu => $valueu) 
                    {
                       $insertArr[] = array(
                        "group_id"=>$resultGrouData,
                        "sender_id"=>trim($postData['created_by']),
                        "receiver_id"=>$valueu,
                        "created_at"=>date("Y-m-d")
                        );
                   }
                   $result = $this->common_model->insert_batch(TB_TEMPORARY_GROUP,$insertArr);

               }



               $this->response(array("status" => true, "message" => 'Group has been created successfully.'), 200); exit;
           } else {
            $this->response(array("status" => false, "message" => 'Something went wrong'), 300); exit;
        }
    }
}

    /**
     * Title : group_list
     * Description : show the list of group name
     *
     * @return Array     
     */ 

    public function group_list_get ()
    {
        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }  
        $groupList = $groupMemers = array();
        $i = $j = 0;
        $cond     = array(TB_GROUP.'.isDeleted'=>1);
        $jointype = array(TB_USERS => "LEFT");
        $join     = array(TB_USERS => TB_USERS.".user_id=".TB_GROUP.".created_by");
        $resultOfGroupData = $this->Common_model->selectJoin(TB_GROUP.".user_id, group_id, group_name, no_of_people, created_by,user_name as createdBy",TB_GROUP,$cond,array(),$join,$jointype);
        
        if( count($resultOfGroupData) > 0 ) {
            foreach ($resultOfGroupData as $key => $value) {
                $groupMemers = array();
                $resultUsersName = $this->Common_model->select_where_in_with_no_quote("user_id, user_name, user_email", TB_USERS,"user_id",(!empty($value['user_id'])?$value['user_id']:0));    
                $groupList[$i]['group_id']     = $value['group_id'];
                $groupList[$i]['group_name']   = $value['group_name'];
                $groupList[$i]['no_of_people'] = $value['no_of_people'];
                $groupList[$i]['group_id']     = $value['group_id']; 
                $groupList[$i]['created_by']   = $value['createdBy'];
                
                foreach ($resultUsersName as $key1 => $value1) {
                    $groupMemers[$j]['id']           = $value1['user_id'];
                    $groupMemers[$j]['member_name']  = $value1['user_name'];
                    $groupMemers[$j]['member_email'] = $value1['user_email'];
                    $j++;           
                }   
                $groupList[$i]['group_members'] = $groupMemers; 
                $groupMemers = array();
                $j = 0;
                $i++;
            }
            
            if ( $resultOfGroupData ) {
                $this->response(array("status" => true, "result" => $groupList), 200); exit;
            } else {
                $this->response(array("status" => false, "message" => 'Something went wrong'), 300); exit;
            }
        } else {
            $this->response(array("status" => false, "message" => 'No groups found.'), 300); exit;
        }
    }   

    /**
     * Title : delete_group
     * Description : delete group  
     * 
     * @param Integer group_id
     * @return Array 
     */

    function delete_group_post() 
    {
        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // Input data
        $postData = $_POST; 

        // Validation
        if ( trim($postData['group_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Group id field is required.'), 200); exit; 
        }  

        // Group data is Soft delete 
        $resultDelete = $this->common_model->update(TB_GROUP,array('group_id' => $postData['group_id']),array('isDeleted' => "0", 'updated_date' => date("Y-m-d H:i:s")));
        if($resultDelete) {
            $this->response(array('status' => true, 'message' => 'Group has been deleted successfully .'), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Something went wrong.'), 300);exit;
        }                                   
    }

    /**
     * Author : Kiran Nyalpelli
     * Title : delete_group_member
     * Description : delete group member 
     * 
     * @param Integer group_id
     * @param Integer user_id
     * @return Array 
     */

    function delete_group_member_post() 
    {
        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // Input data
        $postData = $_POST; 

        // Validation
        if ( trim($postData['group_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Group id field is required.'), 200); exit; 
        }  
        if ( trim($postData['user_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'User id field is required.'), 200); exit; 
        }

        $resultOfGroupData = $this->Common_model->select("user_id",TB_GROUP , array('group_id'=>$postData['group_id'], 'isDeleted' => 1));
        
        if ( count($resultOfGroupData) > 0 ) {            
            $resultUserId = explode(",", $resultOfGroupData[0]['user_id']);
            
            if (in_array($postData['user_id'], $resultUserId)) {

                unset($resultUserId[array_search($postData['user_id'],$resultUserId)]);
                $resultUpdatedUserID = implode(',', $resultUserId);
            } else {
                $this->response(array('status' => false, 'message' => 'User id does not exists.'), 300); exit;                
            }
            $count = explode(',',$resultUpdatedUserID);
            $countMembers = (empty($count[0]) ? 0 : count(explode(',',$resultUpdatedUserID))) ;
            
            // Group member data is Soft delete 
            $resultDelete = $this->Common_model->update(TB_GROUP, array('group_id' => $postData['group_id']), array('user_id' => $resultUpdatedUserID, 'no_of_people' => $countMembers, 'updated_date' => date("Y-m-d H:i:s")));
            
            if($resultDelete) {
                $this->response(array('status' => true, 'message' => 'Group member has been deleted successfully.'), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Something went wrong.'), 300); exit;
            }  
        } else {
         $this->response(array('status' => false, 'message' => 'Group id does not apc_exists(keys).'), 300); exit; 
     }                                 
 }

    /**
     * Title : vehicle_details
     * Description : create new groups
     *
     * @param Integer user_id
     * @param Integer car_model
     * @param Integer role_id
     * @param String car_reg_no
     * @param String car_seating_capacity
     * @param String car_manufacturing_year
     * @param String car_pic
     * @param String car_number_plate_pic
     * @param String car_number_plate
     * @param String licence_scan_copy
     * @return Array
     */

    public function vehicle_details_post ()
    {
        // Input data
        $postData = $this->post();
        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        } 


        // Validation
        if ( trim($postData['user_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'User id field is required.'), 200); exit; 
        }    
        if ( trim($postData['car_model']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Car model field is required.'), 200); exit; 
        } 
        // if ( trim($postData['role_id']) == "" ) { 
        //     $this->response(array("status" => "error", "message" => 'Role id field is required.'), 200); exit; 
        // }
        if ( trim($postData['car_reg_no']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Car register no field is required.'), 200); exit; 
        }    
        if ( trim($postData['car_seating_capacity']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Car seating capacity field is required.'), 200); exit; 
        } 
        if ( trim($postData['car_manufacturing_year']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Car manufacturing year field is required.'), 200); exit; 
        } 
        if ( trim($postData['car_number_plate']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'car number plate field is required.'), 200); exit; 
        } 

        if(!isset($_FILES['car_pic']))
        {
            $this->response(array("status" => "error", "message" => 'Car pictures field is required.'), 200); exit;
        }
        else if(isset($_FILES['car_pic']) && count($_FILES['car_pic']['error']) == 1 && $_FILES['car_pic']['error'][0] > 0 ) {
            $this->response(array("status" => "error", "message" => 'Car pictures field is required.'), 200); exit; 
        } 

        if(!isset($_FILES['car_number_plate_pic']))
        {
            $this->response(array("status" => "error", "message" => 'Car number plate picture field is required.'), 200); exit; 
        }else if(isset($_FILES['car_number_plate_pic']) && count($_FILES['car_number_plate_pic']['error']) == 1 && $_FILES['car_number_plate_pic']['error'][0] > 0)
        { 
            $this->response(array("status" => "error", "message" => 'Car number plate picture field is required.'), 200); exit; 
        }

        if(!isset($_FILES['licence_scan_copy']))
        {
            $this->response(array("status" => "error", "message" => 'licence scan copy field is required.'), 200); exit;
        }else if(isset($_FILES['licence_scan_copy']) && count($_FILES['licence_scan_copy']['error']) == 1 && $_FILES['licence_scan_copy']['error'][0] > 0)
        {
            $this->response(array("status" => "error", "message" => 'licence scan copy field is required.'), 200); exit; 
        } 
        

        $fileUploaded = multiple_pics_uploads($_FILES);
        $certificate  = $fileUploaded['certificate'];
        $license      = $fileUploaded['license'];
        $car          = $fileUploaded['car'];
        $car_number   = $fileUploaded['car_number']; 

        // insert vehicle details
        $arrData = array(
            "user_id"                => $postData['user_id'],
            "car_model"              => $postData['car_model'],
            // "role_id"                => $postData['role_id'], 
            "car_reg_no"             => $postData['car_reg_no'],
            "car_seating_capacity"   => $postData['car_seating_capacity'],
            "car_manufacturing_year" => $postData['car_manufacturing_year'],
            "car_number_plate"       => $postData['car_number_plate']                        
            );     

        if ( !empty($postData['vehicle_id']) ) {    
            $resultOfVehicleDetails = $this->common_model->select("", TB_VEHICLES, array("vehicle_id" => $postData['vehicle_id']));
            $arrData["car_pic"]              = !empty($car)         ? $car         : $resultOfVehicleDetails[0]['car_pic'];
            $arrData["car_number_plate_pic"] = !empty($car_number)  ? $car_number  : $resultOfVehicleDetails[0]['car_number_plate_pic'];
            $arrData["licence_scan_copy"]    = !empty($license)     ? $license     : $resultOfVehicleDetails[0]['licence_scan_copy'];
            $arrData["certification_pics"]   = !empty($certificate) ? $certificate : $resultOfVehicleDetails[0]['certification_pics'];
            $arrData["updated_date"]         = date("Y-m-d H:i:s");     

            // Vehicle data update 
            $resultUpdate = $this->Common_model->update(TB_VEHICLES, array('vehicle_id' => $postData['vehicle_id']), $arrData);
            if ( $resultUpdate ) {
                $this->response(array('status' => true, 'message' => 'Vehicle details has been Updated successfully .'), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Something went wrong.'), 300);exit;
            }  
        } else {
            $arrData["car_pic"]              = $car;
            $arrData["car_number_plate_pic"] = $car_number;
            $arrData["licence_scan_copy"]    = $license;
            $arrData["certification_pics"]   = $certificate;          
            $arrData["is_default"]           = "1";
            $arrData["status"]               = "0";
            $arrData["create_date"]          = date("Y-m-d H:i:s");

            $resultVehicleData = $this->Common_model->insert(TB_VEHICLES, $arrData);
            if ( $resultVehicleData ) {
                $this->response(array("status" => true, "message" => 'Vehicle details has been inserted successfully.'), 200); exit;
            } else {
                $this->response(array("status" => false, "message" => 'Something went wrong'), 300); exit;
            }
        }
    }


     /**
     * Title : set_default_vehicle
     * Description : Set default vehicle
     *
     * @param Integer user_id
     * @param Integer vehicle_id
     * @return Array
     */

     public function set_default_vehicle_post ()
     {
        // Input data
        $postData = $this->post();

        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 400); exit;
        }

        $arrData = array(
            "is_default"                => '1'
            );
        
        // Vehicle data update 
        $resultUpdate = $this->Common_model->update(TB_VEHICLES, array('vehicle_id' => $postData['vehicle_id']), $arrData);
        if ( $resultUpdate ) {
            $this->response(array('status' => true, 'message' => 'Vehicle successfully set as default.'), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Something went wrong.'), 400);exit;
        }  

    }



    /**
     * Title : get_payment_details
     * Description : show the details of payment 
     *
     * @param Integer payment_id
     * @param Integer booking_id
     * @return Array     
     */ 

    public function get_payment_details_post ()
    {
        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }  

        //Input data
        $postData = $this->post();

         // Validation
        if ( trim($postData['payment_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Payment id field is required.'), 200); exit; 
        }    
        if ( trim($postData['booking_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Booking id field is required.'), 200); exit; 
        }

        $payPendingList = array();
        $i = 0;

        $cond        = array(TB_BOOKINGS.'.booking_status' => COMPLETED, TB_USERS.".isDeleted" => 0, TB_PAYMENT.".id" => $postData['payment_id'], TB_PAYMENT.".booking_id" => $postData['booking_id']);
        $jointype    = array(TB_BOOKINGS => "LEFT", TB_USERS => "LEFT", TB_SERVICE_REQUESTS => "LEFT");
        $join        = array(TB_BOOKINGS=>TB_BOOKINGS.".booking_id = ".TB_PAYMENT.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_PAYMENT.".user_id", TB_SERVICE_REQUESTS => TB_SERVICE_REQUESTS.'.service_booking_id = '.TB_BOOKINGS.'.booking_id');
        $paymentData = $this->Common_model->selectJoin("id as payment_id, service_provider_user_id, user_name, booking_pick_up_location, booking_drop_off_location, booking_start_date, booking_price, status, user_rating",TB_PAYMENT,$cond,array(),$join,$jointype);
        
        if( count($paymentData) > 0 ) {
            
            foreach ($paymentData as $key => $value) {
                $payPendingList[$i]['payment_id']                = $value['payment_id'];
                $payPendingList[$i]['service_provider_user_id']  = $value['service_provider_user_id'];
                $payPendingList[$i]['Driver_name']               = $value['user_name'];
                $payPendingList[$i]['booking_pick_up_location']  = $value['booking_pick_up_location']; 
                $payPendingList[$i]['booking_drop_off_location'] = $value['booking_drop_off_location']; 
                $payPendingList[$i]['booking_start_date']        = $value['booking_start_date']; 
                $payPendingList[$i]['booking_price']             = $value['booking_price']; 
                $payPendingList[$i]['status']                    = $value['status']; 
                $payPendingList[$i]['user_rating']               = $value['user_rating'];                

                $i++;
            }
            
            $this->response(array("status" => true, "result" => $payPendingList), 200); exit;
            
        } else {
            $this->response(array("status" => false, "message" => 'NO records found.'), 300); exit;
        }
    }

    /**
     * Title : upload_students
     * Description : To upload multiple students by csv
     *
     * @param Integer user_id
     * @param File csv_file
     * @return Array
     */
    public function upload_students_post()
    {
        $postData = $this->post();
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }

        $user_id = $postData['user_id'];

        // check file is set
        if(!isset($_FILES['csv_file']['tmp_name'])) {
            $this->response(array("status"=>false,"message"=> 'Please upload csv file.'), 300);exit;
        } else {
            $allowed =  array('csv');
            $filename = $_FILES['csv_file']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $this->response(array("status"=>false,"message"=> 'Invalid file format, Please upload CSV file.'), 300);exit;
            }else{
                $count=0;
                $total_records= 0;
                $fp = fopen($_FILES['csv_file']['tmp_name'],'r') or die("can't open file");
                while($csv_line = fgetcsv($fp,1024))
                {
                    $count++;
                    if($count == 1)
                    {
                        continue;
                    }//keep this if condition if you want to remove the first row
                   // echo "<pre>";print_r($csv_line);die;
                    for($i = 0, $j = count($csv_line); $i < $j; $i++)
                    {
                        $insert_csv = array();
                        $insert_csv['stud_name'] = $csv_line[0];
                        $insert_csv['stud_username'] = $csv_line[1];
                        $insert_csv['stud_age'] = $csv_line[2];
                        $insert_csv['stud_address'] = $csv_line[3];
                        $insert_csv['parent_name'] = $csv_line[4];
                        $insert_csv['parent_phone'] = $csv_line[5];
                        $insert_csv['parent_email'] = $csv_line[6];

                    }
                    $i++;

                    if((!empty($insert_csv['stud_name'])) && (!empty($insert_csv['stud_username'])) && (!empty($insert_csv['stud_age'])) && (!empty($insert_csv['stud_address'])) && (!empty($insert_csv['parent_name'])) && (!empty($insert_csv['parent_phone'])) && (!empty($insert_csv['parent_email'])) ){

                        $current_time = date( 'Y-m-d H:i:s', time());
                        $data_new[] = array(
                            $user_id,
                            "'".$insert_csv['stud_name']."'",
                            "'".$insert_csv['stud_username']."'",
                            "'".$insert_csv['stud_age']."'",
                            "'".$insert_csv['stud_address']."'",
                            "'".$insert_csv['parent_name']."'",
                            "'".$insert_csv['parent_phone']."'",
                            "'".$insert_csv['parent_email']."'",
                            "'".$current_time."'");
                        $total_records++;
                    }
                }

                $key_new = "user_id,stud_name,stud_username,stud_age,stud_address,parent_name,parent_phone,parent_email,created_date";
                $result = $this->common_model->insert_ignore(TB_STUDENT,$key_new,$data_new);
                // echo $this->db->last_query();die;
                fclose($fp) or die("can't close file");

                if(!empty($result)){
                    $this->response(array("status"=>true,"result"=> array('total_records_inserted'=>$result)), 200);exit;
                }

            }

        }

    } 

    /**
     * Title : add_student
     * Description : To add new student
     * 
     * @param Integer user_id
     * @param Integer role_id
     * @param Varchar stud_name
     * @param Integer stud_age
     * @param Varchar stud_address
     * @param Varchar parent_name
     * @param Integer parent_phone
     * @param Varchar parent_email
     * @return Array 
     */
    public function add_student_post()
    {
        $postData = $this->post();
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }

        $user_id = $postData['user_id'];
        $role_id = $postData['role_id'];
        $stud_name = $postData['stud_name'];
        $stud_age = $postData['stud_age'];
        $stud_address = $postData['stud_address'];
        $parent_name = $postData['parent_name'];
        $parent_phone = $postData['parent_phone'];
        $parent_email = $postData['parent_email'];

        $current_time = date( 'Y-m-d H:i:s', time());

        if((!empty($user_id)) && (!empty($role_id)) && (!empty($stud_name)) && (!empty($stud_age)) && (!empty($stud_address)) && (!empty($parent_name)) && (!empty($parent_phone)) && (!empty($parent_email)) ){

            $data = array(
                'user_id' => $user_id,
                'role_id' => $role_id,
                'stud_name' => $stud_name,
                'stud_age' => $stud_age,
                'stud_address' => $stud_address,
                'parent_name' => $parent_name,
                'parent_phone' => $parent_phone,
                'parent_email' => $parent_email,
                'created_date' => $current_time
                );

            $last_id = $this->db->insert(TB_STUDENT, $data);

            if($last_id){
                $this->response(array("status"=>true,"result"=> array('msg'=>'Student added successfully.', 'student_id'=>$last_id)), 200);exit;
            }else{
                $this->response(array("status"=>false,"message"=> 'Something went wrong.'), 300);exit;
            }

        }else{
            $this->response(array("status"=>false,"message"=> 'Please provide all required fields.'), 300);exit;
        }
    }

    /**
     * Title : update_student
     * Description : To edit existing student
     * 
     * @param Integer student_id
     * @param Integer user_id
     * @param Integer role_id
     * @param Varchar stud_name
     * @param Integer stud_age
     * @param Varchar stud_address
     * @param Varchar parent_name
     * @param Integer parent_phone
     * @param Varchar parent_email
     * @return Array 
     */
    public function update_student_post()
    {
        $postData = $this->post();
        if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 300);exit;
        }

        $student_id = $postData['student_id'];
        $user_id = $postData['user_id'];
        $role_id = $postData['role_id'];
        $stud_name = $postData['stud_name'];
        $stud_age = $postData['stud_age'];
        $stud_address = $postData['stud_address'];
        $parent_name = $postData['parent_name'];
        $parent_phone = $postData['parent_phone'];
        $parent_email = $postData['parent_email'];

        $current_time = date( 'Y-m-d H:i:s', time());

        // check student exist
        $db_student_id = $this->common_model->select("stud_id",TB_STUDENT,array("stud_id"=>$postData['student_id']));
        if(count($db_student_id) > 0) {

            if((!empty($student_id)) && (!empty($user_id)) && (!empty($role_id)) && (!empty($stud_name)) && (!empty($stud_age)) && (!empty($stud_address)) && (!empty($parent_name)) && (!empty($parent_phone)) && (!empty($parent_email)) ){

                $update_data = array(
                    'user_id' => $user_id,
                    'role_id' => $role_id,
                    'stud_name' => $stud_name,
                    'stud_age' => $stud_age,
                    'stud_address' => $stud_address,
                    'parent_name' => $parent_name,
                    'parent_phone' => $parent_phone,
                    'parent_email' => $parent_email,
                    'updated_date' => $current_time
                    );

                try {
                   $this->common_model->update(TB_STUDENT,array("stud_id"=>$student_id),$update_data);
               } catch (Exception $ex) {
                $this->response(array('status' => false, 'message' => $ex->getCode()),300);exit;
            }                
            $this->response(array('status' => true, 'message' => 'Student updated successfully.'), 200);exit;

                /*
                if($last_id){
                    $this->response(array("status"=>true,"result"=> array('msg'=>'Student updated successfully.', 'student_id'=>$last_id)), 200);exit;
                }else{
                    $this->response(array("status"=>false,"message"=> 'Something went wrong.'), 300);exit;
                }
                */

            }else{
                $this->response(array("status"=>false,"message"=> 'Please provide all required fields.'), 300);exit;
            }
        }else{
            // student dose not exist
            $this->response(array('status' => false, 'message' => "Student does not exist"),300);exit;
        }
    }


    /**
     * Title : delete_student
     * Description : delete student  
     * 
     * @param Integer student_id
     * @return Array 
     */

    function delete_student_post() 
    {
        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // Input data
        $postData = $_POST; 

        // Validation
        if ( trim($postData['student_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Student id field is required.'), 200); exit; 
        }  


        // check student exist
        $db_student_id = $this->common_model->select("stud_id",TB_STUDENT,array("stud_id"=>$postData['student_id']));
        if(count($db_student_id) > 0) {

            // student delete 
            $resultDelete = $this->common_model->delete(TB_STUDENT,array('stud_id' =>$postData['student_id']));
            if($resultDelete) {
                $this->response(array('status' => true, 'message' => 'Student has been deleted successfully .'), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Something went wrong.'), 300);exit;
            }   
        }else{
            // student dose not exist
            $this->response(array('status' => false, 'message' => "Student does not exist"),300);exit;
        }                                
    }

    /**
     * Title : add student to existing group
     * Description : Api for add new students in the existing group
     * 
     * @param Integer group_id
     * @param Integer student_id
     * @return Array 
     */
    public function student_add_group_post()
    {
        // Checked user are authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        // Input data
        $postData = $_POST; 

        $student_id = $postData['student_id'];
        $group_id = $postData['group_id'];

        // Validation
        if ( (trim($postData['student_id']) == "") || (trim($postData['group_id']) == "") ) { 
            $this->response(array("status" => "error", "message" => 'Please provide required parameters.'), 200); exit; 
        }

        // check student exist
        $db_student_id = $this->common_model->select("stud_id,stud_name,  parent_name,parent_phone",TB_STUDENT,array("stud_id"=>$postData['student_id']));
        if(count($db_student_id) > 0) {
            //check group exist

            $group_details = $this->common_model->select("*",TB_GROUP,array("group_id"=>$postData['group_id']));
            if(count($group_details) > 0) {
                foreach ($group_details as $key => $value) {

                    $groupMembers = $value['user_id'];
                    $no_of_people = $value['no_of_people'];

                    if($groupMembers!=""){
                        $members_arr = explode(',',$groupMembers);
                        if (in_array($student_id, $members_arr))
                        {
                            $this->response(array('status' => false, 'message' => "Student is member of this group"),300);exit;
                        }
                        else
                        {
                            //add student to member string
                            $members_str = $groupMembers.','.$student_id;
                        }
                    }else{
                        $members_str = $student_id;
                    }

                    $total_ppl = $no_of_people + 1;
                    // update student to group
                    $update_data = array(
                        'user_id' => $members_str,
                        'no_of_people' => $total_ppl
                        );

                    try {
                       $this->common_model->update(TB_GROUP,array("group_id"=>$group_id),$update_data);
                       $msg = 'Dear '.$db_student_id[0]['parent_name'].', Your child'.$db_student_id[0]['stud_name'].' is added in '.$group_details [0]['group_name'];

                       $to = $db_student_id[0]['parent_phone'];
                        //$to = '9766792917';
                       $responce = sendOTP($msg, trim($to));


                   } catch (Exception $ex) {
                    $this->response(array('status' => false, 'message' => $ex->getCode()),300);exit;
                }                
                $this->response(array('status' => true, 'message' => 'Student added in group successfully.'), 200);exit;


            }
        }else{
                // group dose not exist
            $this->response(array('status' => false, 'message' => "Group does not exist"),300);exit;
        }
    }else{
            // student dose not exist
        $this->response(array('status' => false, 'message' => "Student does not exist"),300);exit;
    }
}

    //----------- Ashwini Code start ---------------
    /**
     * Title : Student Listing
     * Description : listing of students for particular school
     *
     * @param Integer user_id
     * @return Array
     */
    public function student_listing_post()
    {
        $postData = $this->post();
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            if(trim($postData['user_id']) == "" ) { 
                $this->response(array("status" => "error", "message" => 'User id is required.'), 200); exit; 
            }else
            {
                $cond = array("user_id"=>trim($postData['user_id']));
                $ord = array("stud_name"=>"ASC");
                $result = $this->common_model->selectQuery("*",TB_STUDENT,$cond,$ord);
                if(!empty($result))
                {
                    $this->response(array("status" => "success", "data" => $result),200); exit;
                }else
                {
                    $this->response(array("status" => "error", "message" => 'Data not found.'), 200); exit; 
                }
            }
        }
    }

        /**
     * Title : Group Listing
     * Description : listing of group made by particular user
     *
     * @param Integer user_id
     * @return Array
     */
        public function group_listing_post()
        {
            $postData = $this->post();
            if(is_authorized())
            {
                $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
            }else
            {
                if(trim($postData['user_id']) == "" ) 
                { 
                    $this->response(array("status" => "error", "message" => 'User id is required.'), 200); exit; 
                }else
                {
                    $select = "group_id,group_name,no_of_people,isDeleted,created_date,updated_date";
                    $cond = array("created_by"=>trim($postData['user_id']));
                    $ord = array("group_name"=>"ASC");
                    $result = $this->common_model->selectQuery($select,TB_GROUP,$cond,$ord);
                    if(!empty($result))
                    {
                        $this->response(array("status" => "success", "data" => $result), 200); exit; 
                    }else
                    {
                        $this->response(array("status" => "error", "message" => 'Data not found.'), 200); exit; 
                    }
                }
            }
        }

    /**
     * Title : Display Group members
     * @param Integer group_id
     * @return Array
     */
    public function list_group_members_post()
    {
        $postData = $this->post();
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            if(trim($postData['group_id']) == "" ) 
            { 
                $this->response(array("status" => "error", "message" => 'Group id is required.'), 200); exit; 
            }else
            {
                $cond=array("group_id"=>trim($postData['group_id']));
                $getUser = $this->common_model->select("user_id",TB_GROUP,$cond);
                $stud_ids = $getUser[0]['user_id'];
                if(!empty($stud_ids))
                {
                    $result = $this->common_model->select_where_in_with_no_quote("stud_id,stud_name",TB_STUDENT,"stud_id",$stud_ids);
                    if(!(empty($result)))
                    {
                        $this->response(array("status" => "success", "data" => $result), 200); exit;
                    }else
                    {
                        $this->response(array("status" => "error", "message" => 'Data not found.'), 200); exit;        
                    }
                }else
                {
                   $this->response(array("status" => "error", "message" => 'Data not found.'), 200); exit; 
               }
           }
       }
   }

   
    /**
     * Title : Group shifting
     * Desc : Shift members from one group to another group
     * @param Integer from_group_id
     * @param Integer to_group_id
     * @param Integer student_id
     * @param String status
     * @param Date temp_date
     * @return Array
     */

    public function shift_group_post()
    {
        $postData = $this->post();
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            $from_group_id = trim($postData['from_group_id']);
            $to_group_id = trim($postData['to_group_id']);
            $student_id = trim($postData['student_id']);
            $status = trim($postData['status']);
            $temp_date = trim($postData['temp_date']);

            if($from_group_id == "" )
            {
                $this->response(array("status" => "error", "message" => 'From Group id is required.'), 200); exit;
            }
            if($to_group_id == "" )
            {
                $this->response(array("status" => "error", "message" => 'To Group id is required.'), 200); exit;
            }
            if($student_id == "" )
            {
                $this->response(array("status" => "error", "message" => 'Student id is required.'), 200); exit;
            }
            if($status == "" )
            {
                $this->response(array("status" => "error", "message" => 'Status is required.'), 200); exit;
            }
            if($status == "1" && $temp_date == "")
            {
                $this->response(array("status" => "error", "message" => 'Temporary Date is required.'), 200); exit;
            }


            $cond1 = array("group_id"=>$from_group_id);
            $fromStud = $this->common_model->select("*",TB_GROUP,$cond1);
            $from_students=$fromStud[0]['user_id'];
            $from_stud_arr = explode(',',$from_students);

            $cond2 = array("group_id"=>$to_group_id);
            $toStud = $this->common_model->select("*",TB_GROUP,$cond2);
            $to_students=$toStud[0]['user_id'];
            $to_stud_arr = explode(',',$to_students);

            //------ To check if student already in To-group ------
            if(in_array($student_id,$to_stud_arr))
            {
                $this->response(array("status" => "error", "message" => 'Student already exists in this group.'), 200); exit;
            }else
            {
                $temp_date_new = ($status == "1") ? $temp_date : "";
                $insertArr=array(
                    "from_group_id"=>$from_group_id,
                    "to_group_id"=>$to_group_id,
                    "student_id"=>$student_id,
                    "shift_status"=>$status,
                    "temp_date"=>$temp_date_new,
                    "created_at"=>date("Y-m-d H:i:s")
                    );
                $result = $this->common_model->insert(TB_GROUP_SHIFT,$insertArr);
                if($result)
                {
                    //---- Remove student from 1st group & update no of people --
                    if (($key = array_search($student_id, $from_stud_arr)) !== false) {
                        unset($from_stud_arr[$key]);
                    }
                    $new_from_people = count($from_stud_arr);
                    $new_from_stud = implode(',',$from_stud_arr);
                    $upArr1 = array("user_id"=>$new_from_stud,"no_of_people"=>$new_from_people);
                    $upfromGrp = $this->common_model->update(TB_GROUP,$cond1,$upArr1);

                    //---- Add student to 2nd group & update no of people --
                    array_push($to_stud_arr,$student_id);
                    $new_to_people = count($to_stud_arr);
                    $new_to_stud = implode(',',$to_stud_arr);
                    $upArr2 = array("user_id"=>$new_to_stud,"no_of_people"=>$new_to_people);
                    $uptoGrp = $this->common_model->update(TB_GROUP,$cond2,$upArr2);

                    if($upfromGrp && $uptoGrp)
                    {
                        $this->response(array("status" => "success", "message" => 'Student shifted successfully.'), 200); exit;
                    }else
                    {
                        $this->response(array("status" => "error", "message" => 'Oops!!! Something went wrong.'), 200); exit;
                    }
                }
                else
                {
                    $this->response(array("status" => "error", "message" => 'Oops!!! Something went wrong.'), 200); exit;
                }
            }
        }
    }

    /**
     * Title : Cron Group shifting
     * Desc : Automatically shift students to old group after temporary date
     */

    public function cron_shift_group_get()
    {
        $postData = $this->post();
        $today = date("Y-m-d");
        $getStudent = $this->common_model->select("*",TB_GROUP_SHIFT,array("temp_date"=>$today,"shift_status"=>"1"));
        if(!empty($getStudent))
        {
            foreach ($getStudent as $key => $value)
            {
                $cond1 = array("group_id"=>$value['to_group_id']);
                $fromStud = $this->common_model->select("*",TB_GROUP,$cond1);
                $from_students=$fromStud[0]['user_id'];
                $from_stud_arr = explode(',',$from_students);

                $cond2 = array("group_id"=>$value['from_group_id']);
                $toStud = $this->common_model->select("*",TB_GROUP,$cond2);
                $to_students=$toStud[0]['user_id'];
                $to_stud_arr = explode(',',$to_students);

            //---- Remove student from to group & update no of people --
                if (($key = array_search($value['student_id'], $from_stud_arr)) !== false) {
                    unset($from_stud_arr[$key]);
                }
                $new_from_people = count($from_stud_arr);
                $new_from_stud = implode(',',$from_stud_arr);
                $upArr1 = array("user_id"=>$new_from_stud,"no_of_people"=>$new_from_people);
                $upfromGrp = $this->common_model->update(TB_GROUP,$cond1,$upArr1);
                    // echo $this->db->last_query();die;

                    //---- Add student to 2nd group & update no of people --
                array_push($to_stud_arr,$value['student_id']);
                $new_to_people = count($to_stud_arr);
                $new_to_stud = implode(',',$to_stud_arr);
                $upArr2 = array("user_id"=>$new_to_stud,"no_of_people"=>$new_to_people);
                $uptoGrp = $this->common_model->update(TB_GROUP,$cond2,$upArr2);

                if($upfromGrp && $uptoGrp)
                {
                    $this->common_model->delete(TB_GROUP_SHIFT,array("shift_id"=>$value['shift_id']));

                }
            }
        }
    }


    /**
     * Title : Accept / Reject group request
     * @param Integer user_id
     * @param Integer group_id
     * @param String status
     * @return Array
     */

    public function change_request_status_post()
    {
        $postData = $this->post();
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            if(empty(trim($postData['user_id'])))
            {
                $this->response(array("status" => "error", "message" => 'User id is required.'), 200); exit;
            }
            if(empty(trim($postData['group_id'])))
            {
                $this->response(array("status" => "error", "message" => 'Group id is required.'), 200); exit;
            }
            if(empty(trim($postData['status'])))
            {
                $this->response(array("status" => "error", "message" => 'Status is required.'), 200); exit;
            }

            $status = trim($postData['status']);

            if($status == "accept")
            {
                $cond1 = array("group_id"=>trim($postData['group_id']));
                $Stud = $this->common_model->select("*",TB_GROUP,$cond1);
                $all_user=$Stud[0]['user_id'];
                if(!empty($all_user))
                {
                    $all_user_arr = explode(',',$all_user);
                }else
                {
                    $all_user_arr = array();
                }


                array_push($all_user_arr,trim($postData['user_id']));
                $total_people = count($all_user_arr);
                $new_people = implode(',',$all_user_arr);

                $upArr = array("user_id"=>$new_people,"no_of_people"=>$total_people);
                $uptoGrp = $this->common_model->update(TB_GROUP,$cond1,$upArr);
                if($uptoGrp)
                {
                    $this->common_model->delete(TB_TEMPORARY_GROUP,array("group_id"=>trim($postData['group_id']),"receiver_id"=>trim($postData['user_id'])));
                    $this->response(array("status" => "success", "message" => "You are added in group successfully."), 200); exit;
                }
            }
            else if($status == "reject")
            {
                $this->common_model->delete(TB_TEMPORARY_GROUP,array("group_id"=>trim($postData['group_id']),"receiver_id"=>trim($postData['user_id'])));
                $this->response(array("status" => "success", "message" => "You have rejected group."), 200); exit;
            }else
            {
             $this->response(array("status" => "error", "message" => "Oops!!! Something went wrong."), 200); exit;
         }
     }
 }

    /**
     * Title : Standard listing
     * @return Array
     */

    public function standard_listing_get()
    {
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            $all_standard = unserialize(STANDARD);
            if(!empty($all_standard))
            {
                $this->response(array("status" => "success","data" => $all_standard), 300); exit;
            }else
            {
                $this->response(array("status" => "error","message" => 'Standards are not defined.'), 300); exit;
            }
        } 
    }

    /**
     * Title : Save tutor standard
     * @param Integer tutor_id
     * @param Integer standard_id
     * @return Array
     */
    public function save_tutor_standard_post()
    {
        $postData = $this->post();
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            if(empty(trim($postData['tutor_id'])))
            {
                $this->response(array("status" => "error", "message" => 'Tutor id is required.'), 200); exit;
            }
            if(empty(trim($postData['standard_id'])))
            {
                $this->response(array("status" => "error", "message" => 'Standard id is required.'), 200); exit;
            }

            $all_std = explode(",",trim($postData['standard_id']));
            foreach ($all_std as $keys => $values) 
            {
                $chkQuery = $this->common_model->select("*",TB_TUTOR_STANDARD,
                    array("tutor_id"=>trim($postData['tutor_id']),"std_id"=>$values));
                if(count($chkQuery) > 0)
                {

                }else
                {
                    $insertArr = array(
                        "tutor_id"=>trim($postData['tutor_id']),
                        "std_id"=>$values,
                        "is_deleted"=>"0",
                        "created_at"=>date("Y-m-d H:i:s")
                        );
                    $ins_std =$this->common_model->insert(TB_TUTOR_STANDARD,$insertArr);               
                }
            }
            $this->response(array("status" => "success", "message" => 'Standard added successfully.'), 200); exit;
        }
    }

    /**
     * Title : Delete tutor standard
     * @param Integer tutor_id
     * @param Integer ts_id
     * @return Array
     */

    public function delete_tutor_standard_post()
    {
        $postData = $this->post();
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            if(empty($postData['tutor_id']))
            {
                $this->response(array("status" => "error","message" => 'Tutor id is required.'), 300); exit;
            }
            if(empty($postData['ts_id']))
            {
                $this->response(array("status" => "error","message" => 'Tutor standard id is required.'), 300); exit;
            }

            $updtArr = array("is_deleted"=>"1");
            $cond = array("tutor_id"=>trim($postData['tutor_id']),"ts_id"=>trim($postData['ts_id']));
            $updtQuery = $this->common_model->update(TB_TUTOR_STANDARD,$cond,$updtArr);
            if($updtQuery)
            {
                $this->response(array("status" => "success", "message" => 'Standard deleted successfully.'), 200); exit;
            }else
            {
                $this->response(array("status" => "error","message" => 'Oops!!! Something went wrong.'), 300); exit;
            }
        }
    }

    // -------- Chatting between parent and care user ------
    // ****** Check care driver is providing service to same parent to whom he is chatting ******
    /**
     * Title : Send messages
     * @param Integer sender_id
     * @param Integer receiver_id
     * @param String message
     * @return Array
     */
    public function sendMessage_post()
    {
        $postData = $this->post();    
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            if(empty($postData['sender_id']))
            {
                $this->response(array("status" => "error","message" => 'Sender id is required.'), 300); exit;
            }
            if(empty($postData['receiver_id']))
            {
                $this->response(array("status" => "error","message" => 'Receiver id is required.'), 300); exit;
            }
            if(trim(empty($postData['message'])))
            {
                $this->response(array("status" => "error","message" => 'Message is required.'), 300); exit;
            }

            $headers = apache_request_headers();
            $auth_token = $headers['Authorizationtoken'];
            $checkToken = $this->common_model->select("user_id",TB_USERS,array("user_device_token"=>$auth_token));
            $user_id = $checkToken[0]['user_id'];

            if($user_id == $postData['sender_id'] || $user_id == $postData['receiver_id'])
            {

                $insertArr = array(
                    "sender_id"=>$postData['sender_id'],
                    "receiver_id"=>$postData['receiver_id'],
                    "message"=>trim($postData['message']),
                    "message_seen"=>"0",
                    "created_at"=>date("Y-m-d H:i:s")
                    );
                $result = $this->common_model->insert(TB_CHAT,$insertArr);
                if($result)
                {
                    // $getDevice = $this->common_model->select("user_device_id",TB_USERS,array("user_id"=>$postData['receiver_id']));
                    // $device_id = $getDevice[0]['user_device_id'];
                // $message  = "You have new message"; 
                // $myData   = array("message"=>trim($postData['message']));
                // $this->sendPushNotification($device_id,$message,$myData);
                }
            }else
            {
                $this->response(array("status" => "error", "message" => 'Bad user to access request.'), 200); exit;
            }
        }
    }

    /**
     * Title : Get messages
     * @param Integer sender_id
     * @param Integer receiver_id
     * @return Array
     */
    public function getMessages_post()
    {
        $postData = $this->post();
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
           if(empty($postData['sender_id']))
           {
            $this->response(array("status" => "error","message" => 'Sender id is required.'), 300); exit;
        }
        if(empty($postData['receiver_id']))
        {
            $this->response(array("status" => "error","message" => 'Receiver id is required.'), 300); exit;
        }


        $headers = apache_request_headers();
        $auth_token = $headers['Authorizationtoken'];
        $checkToken = $this->common_model->select("user_id",TB_USERS,array("user_device_token"=>$auth_token));
        $user_id = $checkToken[0]['user_id'];

        if($user_id == $postData['sender_id'] || $user_id == $postData['receiver_id'])
        {
            $all_ids = $postData['sender_id'].",".$postData['receiver_id'];

            $this->db->select("*");
            $this->db->from(TB_CHAT);
            $this->db->where("sender_id IN($all_ids) AND receiver_id IN($all_ids)");
            $this->db->order_by('created_at','ASC');
            $query = $this->db->get();
            $result = $query->result_array();
            if(!empty($result))
            {
              foreach($result as $key => $value)
              {
                  $result[$key]['sender_id'] = $this->getUserName($result[$key]['sender_id']);
                  $result[$key]['receiver_id'] = $this->getUserName($result[$key]['receiver_id']);
                  $clear_by = explode(",",$result[$key]['clear_by']);
                  if(in_array($user_id,$clear_by))
                  {
                    $result[$key]['message'] = "Message is deleted";
                }
            }
            $this->response(array("status" => "success", "message" => $result), 200); exit;
        }else
        {
            $this->response(array("status" => "error", "message" => 'Messages not found.'), 200); exit;
        }
    }else
    {
        $this->response(array("status" => "error", "message" => 'Bad user to access request.'), 200); exit;
    }
}
}

public function getUserName($user_id)
{
    $getSender = $this->common_model->select("user_name",TB_USERS,array("user_id"=>$user_id));
    return $getSender[0]['user_name'];
}

 /**
     * Title : Delete messages
     * @param Integer user_id
     * @param Integer chat_id
     * @return Array
     */
 public function deleteMessages_post()
 {
    $postData = $this->post();
    if(is_authorized())
    {
        $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
    }else
    {
        if(empty($postData['user_id']))
        {
            $this->response(array("status" => "error","message" => 'User id is required.'), 300); exit;
        }
        if(empty($postData['chat_id']))
        {
            $this->response(array("status" => "error","message" => 'Chat id is required.'), 300); exit;
        }

        $headers = apache_request_headers();
        $auth_token = $headers['Authorizationtoken'];
        $checkToken = $this->common_model->select("user_id",TB_USERS,array("user_device_token"=>$auth_token));
        $user_id = $checkToken[0]['user_id'];

        if($user_id == $postData['user_id'])
        {
            $all_chat_ids = explode(",",trim($postData['chat_id']));
            foreach ($all_chat_ids as $key => $value)
            {
                $getClear = $this->common_model->select("clear_by",TB_CHAT,array("chat_id"=>$value));
                if(!empty($getClear[0]['clear_by']))
                {
                    $new_clear = $getClear[0]['clear_by'].",".$postData['user_id'];
                }else
                {
                    $new_clear = $postData['user_id'];
                }

                $updtArr = array("clear_by"=>$new_clear);
                $cond = array("chat_id"=>$value);
                $updateq = $this->common_model->update(TB_CHAT,$cond,$updtArr);
            }
            if($updateq)
            {
                $this->response(array("status" => "success", "message" => "Message deleted successfully."), 200); exit;
            }else
            {
                $this->response(array("status" => "error", "message" => "Oops!!! Something went wrong."), 200); exit;
            }
        }else
        {
            $this->response(array("status" => "error", "message" => 'Bad user to access request.'), 200); exit;
        }

    }
}

    /**
     * Title : Update messages
     * @param Integer chat_id
     * @param String message
     * @return Array
     */
    public function updateMessage_post()
    {
        $postData = $this->post();
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            if(empty($postData['chat_id']))
            {
                $this->response(array("status" => "error","message" => 'Chat id is required'), 300); exit;
            }
            if(empty(trim($postData['chat_id'])))
            {
                $this->response(array("status" => "error","message" => 'Message is required'), 300); exit;
            }

            $updtArr = array("message"=>trim($postData['message']));
            $cond = array("chat_id"=>$postData['chat_id']);
            $result = $this->common_model->update(TB_CHAT,$cond,$updtArr);
            if($result)
            {
                $this->response(array("status" => "success","message" => 'Message is updated successfully'), 200); exit;
            }else
            {
                $this->response(array("status" => "error","message" => 'Oops!!! Something went wrong.'), 300); exit;
            }
        }
    }

    //---------- Cronjob function to delete messages in chat after 7 days -------
    public function cron_delete_messages_get()
    {
       $postData = $this->post();
       $today = date("Y-m-d");
       $getData = $this->common_model->select("DATEDIFF('$today',date(created_at)) AS days,chat_id",TB_CHAT);
       foreach ($getData as $key => $value)
       {
        if($value['days'] == "7")    
        {
            $this->common_model->delete(TB_CHAT,array("chat_id"=>$value['chat_id']));
        }
    }
}

    //---------- Ashwini Code end here ----------




    /**
     * Title : Service provider unavailability add/update functionality API
     * @param Integer sp_id(User_id)
     * @param Varchar date
     * @param Integer unavail_id
     * @return Array
     */  

    public function add_unavilability_post()
    {
        $postData = $this->post();
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            if($postData['unavail_id'] =='')
            {
                $UAdate = str_replace(array('[',']'),'',$postData['date']);
                $unvaildate = explode(',', $UAdate);
                $count = count($unvaildate);
                foreach ($unvaildate as $key => $date) {
                    $insertArr=array(
                        "sp_id"=>$postData['sp_id'],
                        "status"=>'1',
                        "date"=>$date,
                        "created_date"=>date("Y-m-d H:i:s")
                        );
                    $result = $this->common_model->insert(TB_DRIVER_UNAVAILABILITY,$insertArr);
                }
                if ($result) {
                   $this->response(array("status" => "success", "message" => 'Tulii accepted your unavailability for "'.$count.'" days.'), 200); exit; 
               }
           }else{
            $UAdate = str_replace(array('[',']'),'',$postData['date']);
            $unvaildate = explode(',', $UAdate);
            $count = count($unvaildate);
            foreach ($unvaildate as $key => $date) {
                $updateArr=array(
                    "sp_id"=>$postData['sp_id'],
                    "status"=>'1',
                    "date"=>$date,
                    "created_date"=>date("Y-m-d H:i:s")
                    );

                $result = $this->common_model->update(TB_DRIVER_UNAVAILABILITY,array("unavail_id"=>$postData['unavail_id']),$updateArr);
            }
            if ($result) {
               $this->response(array("status" => "success"), 200); exit; 
           }
       }            
   }
}

    /**
     * Title : Service provider list of unavailable users
     * @param Integer sp_id(User_id)
     * @return Array
     */

    public function view_unavilability_post()
    {
        $postData = $this->post();
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            $cond1 = array("sp_id"=>$postData['sp_id'],'date>='=>date("Y-m-d"));
            $result = $this->common_model->select("*",TB_DRIVER_UNAVAILABILITY,$cond1);            
            if ($result) {
               $this->response(array("status" => "success", "data" => $result), 200); exit; 
           }else{
            $this->response(array("status" => "error", "message" => 'No records found.'), 200); exit;
        }
    }
}

       /**
     * Title : Service provider delete unavailable users
     * @param Integer sp_id(User_id)
     * @return Array
     * Need to set cron job for delete unavailble users.
     */


       public function delete_unavilability_post()
       {
        $postData = $this->post();
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            $resultDelete = $this->common_model->delete(TB_DRIVER_UNAVAILABILITY,array('sp_id' =>$postData['sp_id'],'date<'=>date("Y-m-d")));                       
            if ($resultDelete) {
               $this->response(array("status" => "success"), 200); exit; 
           }else{
            $this->response(array("status" => "error", "message" => 'Something went wrong!!!.'), 200); exit;
        }
    }
}

public function sendSMS_post()
{

    $msg = 'Dear Parent,<br> Ram is added in Technical group';
        //$msg = 12345;
    $from = 'TFCTOR';
    $to = 9822707586;
    $SendAt = date("Y-m-d");
    $responce = sendOTP($msg, trim($to),$from);
    echo "<pre>";print_r($responce);die;
}

public function car_category_post()
{
    if(is_authorized())
    {
        $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
    }else
    {
        $cond1 = array("is_deleted"=>'0');
        $result = $this->common_model->select("*",TB_CAR_CATEGORY,$cond1);;            
        if ($result) {
           $this->response(array("status" => "success", "data" => $result), 200); exit; 
       }else{
        $this->response(array("status" => "error", "message" => 'No records found.'), 200); exit;
    }
}
}


    /**
     * Title : Tutor subject rate (add/update) functionality API
     * @param Integer tutor_id(User id)
     * @param Integer cat_id(Tutor category id)
     * @param Integer sub_id(Subject id)
     * @param Float rate
     * @param Integer tutor_sub_id(For update info)
     * @return Array
     */  

    public function subject_rate_post()
    {
        $postData = $this->post();
        $subject_rate = json_decode($postData['subject_rate'],true);
        if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 300); exit;
        }else
        {
            if($postData['tutor_sub_id'] =='')
            {
                foreach ($subject_rate['request_data'] as $key => $catrate) {
                   $catid = $catrate['cat_id'];
                   foreach ($catrate['cat_details'] as $key => $sub_rate) {
                    $insertArr=array(
                        "cat_id"=>$catid,
                        "sub_id"=>$sub_rate['sub_id'],
                        "tutor_id"=>$postData['tutor_id'],
                        "rate_per_hour"=>$sub_rate['rate'],
                        "created_at"=>date("Y-m-d H:i:s")
                        );
                    $result = $this->common_model->insert(TB_SUBJECT_MASTER,$insertArr);
                }
            }
            if ($result) {
               $this->response(array("status" => "success", "message" => 'Subject rate added successfully.'), 200); exit; 
           }
       }else{
        foreach ($subject_rate['request_data'] as $key => $catrate) {
            $catid = $catrate['cat_id'];
            foreach ($catrate['cat_details'] as $key => $sub_rate) {
               $updateArr=array(
                "cat_id"=>$catid,
                "sub_id"=>$sub_rate['sub_id'],
                "tutor_id"=>$postData['tutor_id'],
                "rate_per_hour"=>$sub_rate['rate'],
                "created_at"=>date("Y-m-d H:i:s")
                );

               $result = $this->common_model->update(TB_SUBJECT_MASTER,array("id"=>$postData['tutor_sub_id']),$updateArr);
           }   
           
       }
       if ($result) {
           $this->response(array("status" => "success", "message" => 'Subject rate updated successfully.'), 200); exit; 
       }
   }            
}
}

    /**
     * Title : tutor_list
     * Description : tutor list
     *  
     * @param  Integer tutor_id
     * @return Array
     */

    public function tutor_list_post()
    {   
        // Checked user is authenticate or not
        if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 300); exit;
        } 

        $postData = $this->post();
        // checked inputs are not emtpy
        if ( !empty($postData['tutor_id']) ) {
            $cond = array("tbl_tutor_subjects.status" => 1 ,"tbl_tutor_subjects.is_deleted" => "0","tbl_tutor_subjects.id" => $postData['tutor_id']);
            $join = array(TB_TUTOR_CATEGORY => TB_TUTOR_CATEGORY.".id = ".TB_TUTOR_SUBJECTS.".cat_id",TB_USERS=>TB_TUTOR_SUBJECTS.".tutor_id  = ".TB_USERS.".user_id");
            $tutorData = $this->Common_model->select_join("tbl_tutor_subjects.id,user_name,category_name,tutor_id", TB_TUTOR_SUBJECTS, $cond, array(), array(), $join,"tutor_id");            
        } else {
            $cond = array("tbl_tutor_subjects.status" => 1 ,"tbl_tutor_subjects.is_deleted" => "0");
            $join = array(TB_TUTOR_CATEGORY => TB_TUTOR_CATEGORY.".id = ".TB_TUTOR_SUBJECTS.".cat_id",TB_USERS=>TB_TUTOR_SUBJECTS.".tutor_id  = ".TB_USERS.".user_id");
            $tutorData = $this->Common_model->select_join("tbl_tutor_subjects.id,user_name,category_name,tutor_id", TB_TUTOR_SUBJECTS, $cond, array(), array(), $join,"tutor_id");            
        }
        
        $arrayList = array();
        $i = 0;
        // result
        if ( count($tutorData) > 0 ) {
            foreach ($tutorData as $key => $value) {
                $cond1    = array("tbl_tutor_subjects.tutor_id"=>$value['tutor_id']);       
                $join1    = array(TB_TUTOR_SUBCATEGORY => TB_TUTOR_SUBJECTS.".sub_id  = ".TB_TUTOR_SUBCATEGORY.".id");
                $subjPriceData = $this->Common_model->select_join("rate_per_hour,subcategory_name", TB_TUTOR_SUBJECTS, $cond1, array(), array(), $join1);
                $arrayList[$i]['id']            = $value['id'];
                $arrayList[$i]['tutor_name']    = $value['user_name'];
                $arrayList[$i]['category_name'] = $value['category_name'];                
                $arrayList[$i]['subject_price_list'] = $subjPriceData;
                $i++;
            }
            $this->response(array("status" => true, "result" => $arrayList), 200); exit;
        } else {
            $this->response(array("status" => false, "message" => 'Tutor data not found.'), 300); exit;
        }
    }
}
