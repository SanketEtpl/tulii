function schoolList(){
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"school_management/School_controller/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
               
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
                $("input[type=search]").keydown(function(event) {
                  k = event.which;
                  if (k === 32 && !this.value.length)
                      event.preventDefault();
                });
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
            $("#table").css({"opacity":"1"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,5], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    }); 
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  schoolList();
}

function clearViewSearch() 
{ 
  $("input[type=search]").val("");
  viewSchoolStudents();
}

function viewSchoolStudents($this) // view of students
{
  var student = $('#student').DataTable();
  $('#student').empty();
  student.destroy();
  $('#student').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"school_management/School_controller/viewStudentList",
            "data": {"key":$($this).attr("data-row-id")},
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
               
                $("input[type=search]").on("keyup",function(event) {
                if($("#clearView").length == 0) {
                   if($(this).val() != ""){
                    $("#student_filter label").append('<div id="clearView"><button class="btn btn-primary" type="button" id="clearViewText" onClick="clearViewSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clearView").remove();      
              }); 
                $("input[type=search]").keydown(function(event) {
                  k = event.which;
                  if (k === 32 && !this.value.length)
                      event.preventDefault();
                });
             },
            complete: function(){              
              $('#viewStudentModal').modal('show', {backdrop: 'static'});              
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [0,1,2,3,4,5,6], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    }); 
}

/*function viewSchoolStudents($this) // view of students
{
  if($($this).attr("data-row-id")) {            
    $( "h3" ).html( "<h3 class='modal-title'><b>View Student</b><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></h3>" );
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"school_management/School_controller/viewStudentList",
         beforeSend: function() {
             //$('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        //console.log(json);
          if(json.status == "success"){
             $("#studentPanel tbody").html(json.rows);
          $('#viewStudentModal').modal('show', {backdrop: 'static'});
         } else {
           toastr.error(json.msg); 
         }
      });
  }else{
    toastr.error("Something went wrong!!!.");         
  }
}*/