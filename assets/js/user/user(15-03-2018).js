function checkEmail()
{
  $(".email1").remove();
  var user_key = $("#user_key").val();
  var result = 0;
  if(user_key !="")
    return result;
  else
  {
    var email = $("#email").val();  
    $.ajax({
      type:"POST",
      url:baseURL+"childcare_provider/Childcare/check_email_availability",
      data:{'email':email,'user_key':user_key},
      dataType:"json",
      async:false,          
      success:function(response){        
        if(response.status == true)
        {
          $(".email1").remove(); 
          $("#email").parent().append("<div class='email1' style='color:red;'>"+ response.message +"</div>");
          $(".email2").remove();
          result = 1;
        }
        else
        {       
          $(".email1").remove();       
        }     
      }
    });  
  }
  return result;
}

function userStatus(userId,user_status,active_id,inActive_id)
{  
  if(confirm('Are you sure want to change status ?'))
  {
    $.ajax({
      type : "POST",
      dataType : "json",
      url : baseURL + "user_management/UserController/userStatusChange",
      data : { userId : userId, user_status: user_status} 
      }).success(function(data){   
      if(user_status == 1) { 
        
        var str_success="User profile has been actived successfully";
        var str_error="User profile has been activation failed";

        $(active_id).removeClass("btn-active-disable");
        $(active_id).addClass("btn-active-enable");

        $(inActive_id).removeClass("btn-inactive-enable");
        $(inActive_id).addClass("btn-inactive-disable");

        $(active_id).prop('disabled', true);
        $(inActive_id).prop('disabled', false);
        } else {
          $(active_id).removeClass("btn-active-enable");
          $(active_id).addClass("btn-active-disable");

          $(inActive_id).removeClass("btn-inactive-disable ");
          $(inActive_id).addClass("btn-inactive-enable");

          var str_success="User profile has been inactived successfully";
          var str_error="User profile has been inactivation failed";

          $(active_id).prop('disabled', false);
          $(inActive_id).prop('disabled', true);
        }
      if(data.status = true) {
        toastr.success(str_success);
       }
      else if(data.status = false) { 
          toastr.error(str_error);
        }
      else {
          toastr.error("Access denied..!");
        }
    }); 
  //$("#LoadingDiv").css({"display":"none"});
  }  
}

function deleteUserData($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are you sure want to delete user data ?'))
    {
      $("#LoadingDiv").css({"display":"block"});          
      $.ajax({
            type: "POST",
            dataType: "json",
           url: baseURL+"user_management/UserController/deleteUser",
            data: {"key":$($this).attr("data-row-id")},
        }).success(function (json) {
            if(json.status == "success"){
              $("#LoadingDiv").css({"display":"none"});
              $($this).parents("tr:first").remove();
              toastr.success(json.msg);
              $("#table").dataTable().fnDraw();
              
             }else{
                toastr.error(json.msg);
                $("#LoadingDiv").css({"display":"none"});
             	}
        });
     } 
  } else { 	   
    toastr.error("User something get wrong.");  	
  }
}   

function editUserData($this)
{
  $('#userForm')[0].reset();
   $(".fname").remove();
    $(".email2").remove();
    $(".email1").remove();
    $(".userPassword").remove();
    $(".userCpassword").remove();
    $(".phone").remove();
    $(".userRole").remove();
    $("h3").html('<h3 class="modal-title"><b>Edit parent</b></h3>');
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"user_management/UserController/viewUserInfo",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#user_key").val($($this).attr("data-row-id"));
            $("#fname").val(json.parentData.user_name);            
            $("#email").val(json.parentData.user_email);
            $("#contactNumber").val(json.parentData.user_phone_number);   
            $("#userRole").val(json.parentData.roleId);
            $("#hidePwd").css({"display":"none"});
            $('#userModal').modal('show', {backdrop: 'static'});        
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  } else {
    toastr.error("User something get wrong.");
    $("#user_key").val('');
    $('#userModal').modal('show', {backdrop: 'static'});
  }
}

$(document).ready(function(){

  $('#btnUserSave').click(function(){            
    var userName = $("#fname").val();
    var email = $("#email").val();
    var phone = $("#contactNumber").val();
    var password = $("#userPassword").val();
    var cpassword= $("#userCpassword").val();
    var editData = $("#user_key").val();
    var userRole= $("#userRole").val();

    var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,50}$/i;
    var flag = 0;
    if(email == ''){      
      flag = 1;
      $(".email2").remove(); 
      $(".email1").remove(); 
      $("#email").parent().append("<div class='email2' style='color:red;'>Email field is required</div>");
    }else{
      if( !emailPattern.test(email)){
        $(".email2").remove(); 
        $(".email1").remove(); 
        $("#email").parent().append("<div class='email2' style='color:red;'>Incorrect email. Please try again</div>");
       flag = 1;
      }else{        
        $(".email1").remove(); 
        $(".email2").remove(); 
      }
    }
    if(editData == ""){
      if(password == ''){
        $(".userPassword").remove();  
        $("#userPassword").parent().append("<div class='userPassword' style='color:red;'>Password field is required.</div>");
      flag = 1;
      }else{
        $(".userPassword").remove();        
      }

      if(cpassword == ''){
        $(".userCpassword").remove();  
        $("#userCpassword").parent().append("<div class='userCpassword' style='color:red;'>Confirm password field is required.</div>");
        flag = 1;
      } else if(cpassword != password) {
        $(".userCpassword").remove();  
        $("#userCpassword").parent().append("<div class='userCpassword' style='color:red;'>Password & Confirm password didn't match.</div>");
        flag = 1;
      } else {
        $(".userCpassword").remove();        
      }
    }
    if(phone == ''){     
      flag = 1;
      $(".phone").remove();  
      $("#contactNumber").parent().append("<div class='phone' style='color:red;'>Mobile no field is required.</div>");
    }
    else
    {
      if(phone.length < 10)
      {
        $(".phone").remove();  
        $("#contactNumber").parent().append("<div class='phone' style='color:red;'>Mobile no field must contain minimum 10 digits .</div>");
       flag = 1;
      }
      else
      { 
        $(".phone").remove();  
      }   
    }

    if(userName == ''){
      $(".fname").remove();  
      $("#fname").parent().append("<div class='fname' style='color:red;'>Full name field is required.</div>");
      flag = 1;
    }else{
      $(".fname").remove();        
    }

    if(userRole == '' || userRole == null){
      $(".userRole").remove();  
      $("#userRole").parent().append("<div class='userRole' style='color:red;'>Role field is required.</div>");
      flag = 1;
    }else{
      $(".userRole").remove();          
    }     
  
    var ckk = checkEmail();
    if(flag == 1 || ckk == 1)
      return false;    
    else
    {
      $("#LoadingDiv").css({"display":"block"});
      $.ajax({
        type:"POST",
        url:baseURL+"user_management/UserController/addUser",            
        data:{"editData":editData,"userRole":userRole,"password":password,"phone":phone,"email":email,"userName":userName},
        dataType:"json",
        async:false,
        success:function(response){                     
          if(response.status == "success")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#userModal").modal('hide');
            $("#table").dataTable().fnDraw();
            toastr.success(response.message);                                      
          }
          else
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#userModal").modal('hide');
            toastr.error(response.message);                                       
          }
        }
      }); 
    }   
  });  
  $('#addPopUp').on("click",function(){
    $("#hidePwd").css({"display":"block"});
    $("h3").html('<h3 class="modal-title"><b>Add parent</b></h3>');
    $("#user_key").val("");
    $(".fname").remove();
    $(".email2").remove();
    $(".email1").remove();
    $(".userPassword").remove();
    $(".userCpassword").remove();
    $(".phone").remove();
    $(".userRole").remove();
    $("#userForm")[0].reset();
    $("#userModal").modal('show');
  });
});
