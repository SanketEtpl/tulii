function deleteCareDriver($this)
{
  $("#LoadingDiv").css({"display":"block"});        
  if($($this).attr("data-row-id")){
    if(confirm('Are you sure you want to delete?'))
    {          
      $.ajax({
            type: "POST",
            dataType: "json",
           url: baseURL+"service_management/Service_tutoring/deleteCareDriverInfo",
            data: {"key":$($this).attr("data-row-id")},
        }).success(function (json) {
            if(json.status == "success"){
              $($this).parents("tr:first").remove();
                $("#LoadingDiv").css({"display":"none"});
                toastr.success(json.msg);
                $("#table").dataTable().fnDraw();              
             }else{
                $("#LoadingDiv").css({"display":"none"});
                toastr.error(json.msg);               
              }
        });
     } 
      $("#LoadingDiv").css({"display":"none"});
  } else {   
   $("#LoadingDiv").css({"display":"none"}); 
    toastr.error("Something went wrong!!!.");      
  }
}   

function viewCareDriver($this)
{
  $("#LoadingDiv").css({"display":"block"});                
  if($($this).attr("data-row-id")){            
    $.ajax({
          type: "POST",
          dataType: "json",
         url: baseURL+"service_management/Service_tutoring/viewCareDriverInfo",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        console.log(json);
          if(json.status == "success"){
          $("#LoadingDiv").css({"display":"none"});                
          $("#care_driver_name").text(json.careDriverdData.sph_cd_name);
          $("#care_driver_age").text(json.careDriverdData.sph_cd_age);
          $("#care_driver_exp").text(json.careDriverdData.sph_cd_experience);
          $("#care_driver_totalnodays").text(json.careDriverdData.sph_total_no_days);
          $("#care_driver_date").text(json.careDriverdData.sph_date);
          $("#care_driver_start_date").text(json.careDriverdData.sph_start_date);
          $("#care_driver_end_date").text(json.careDriverdData.sph_end_date);
          $("#care_driver_rating").text(json.careDriverdData.sph_rating);
          $("#care_driver_raise_an_issues").text(json.careDriverdData.sph_raise_an_issues);
          if(json.careDriverdData.sph_types == 3)
            $("#care_driver_type").text("Tutoring");
          else
            $("#care_driver_type").text("None");

          if(json.imgExist == 0) {
            $("#sph_cd_profile_picture img").attr("src","assets/images/img-not-found.jpg");
          } else {
            if(json.careDriverdData.sph_cd_profile_picture != "")            
              $("#sph_cd_profile_picture img").attr("src",json.careDriverdData.sph_cd_profile_picture);
            else
              $("#sph_cd_profile_picture img").attr("src","assets/images/avatar-img.png");
          } 
          if(json.careDriverdData.sph_cd_gender == "female")
            $("#user_gender").text("Female");
          else if(json.careDriverdData.sph_cd_gender == "male")
            $("#user_gender").text("Male"); 
          else
            $("#user_gender").text("-");           
          $('#carpoolModal').modal('show', {backdrop: 'static'});

         }
         else{
           toastr.error(json.msg);   
          $("#LoadingDiv").css({"display":"none"});
         }
      });
  }else{
     $("#LoadingDiv").css({"display":"none"});
    toastr.error("Something went wrong!!!.");         
  }
  $("#LoadingDiv").css({"display":"none"});
} 

$(document).ready(function(){
  $('#addPopUp').on("click",function(){
    $("#carpoolModal").modal('show');                              
    });
});
