function driverCurrentLocationMapList(){    
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"map_management/Driver_location_map/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
               
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
                $("input[type=search]").keydown(function(event) {
                  k = event.which;
                  if (k === 32 && !this.value.length)
                      event.preventDefault();
                });
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
            $("#table").css({"opacity":"1"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,3,4], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    }); 
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  driverCurrentLocationMapList();
}

function viewDriverCurLocMap($this)
{
  if($($this).attr('data-row-id')){            
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"map_management/Driver_location_map/viewDriverLocMap",
          beforeSend: function() {          
         },
        complete: function(){        
      },
        data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {    
        if(json.status == "success"){
          var lat = parseFloat(json.latLng.loc_lat);
          var lng = parseFloat(json.latLng.loc_long);

          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: {lat: lat, lng: lng}
          });
          var geocoder = new google.maps.Geocoder;
          var infowindow = new google.maps.InfoWindow;        
          geocodeLatLng(geocoder, map, infowindow);       
      
          function geocodeLatLng(geocoder, map, infowindow) {
            var latlng = {lat: lat, lng: lng};
            geocoder.geocode({'location': latlng}, function(results, status) {
              if (status === 'OK') {
                if (results[0]) {
                  map.setZoom(11);
                  var marker = new google.maps.Marker({
                    position: latlng,
                    map: map
                  });
                  infowindow.setContent(results[0].formatted_address);
                  infowindow.open(map, marker);
                } else {
                  window.alert('No results found');
                }
              } else {
                window.alert('Geocoder failed due to: ' + status);
              }
            });
          }
          $('#viewdriverCurrentMapModal').modal('show', {backdrop: 'static'});
        }        
      });
  } else {     
    toastr.error("Something went wrong!!!.");         
  }
}