function bookingRideMapList(){    
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"map_management/Booking_ride_map/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
               
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
                $("input[type=search]").keydown(function(event) {
                  k = event.which;
                  if (k === 32 && !this.value.length)
                      event.preventDefault();
                });
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
            $("#table").css({"opacity":"1"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,3,4], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    }); 
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  bookingRideMapList();
}

function viewBookingRide(id,type)
{
  /*var id = $($this).attr("data-row-id");
  var type = $($this).attr("data-booking-type");*/
  if(id){            
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"map_management/Booking_ride_map/viewMapList",
          beforeSend: function() {          
         },
        complete: function(){        
      },
        data: {"key":id,"bookingType":type},
      }).success(function (json) {    
      //console.log(json);    
        if(json.status == "success"){
          var srSlitLatLng = json.latLng.pick_latlong;
          var sourceLatlng = srSlitLatLng.split(",");
          var dsSlitLatLng = json.latLng.drop_latlong;
          var destLatlng = dsSlitLatLng.split(",");
          
          var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 14,
                  center: new google.maps.LatLng(parseFloat(sourceLatlng[0]), parseFloat(sourceLatlng[1])),
                  //mapTypeId: 'terrain'
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var obj = json.rideMap;
                /*console.log(obj);
                alert(obj);*/
                var mapLatLng = [];
                var text,obj,lng,lat;
                $.each(obj, function (i,data) {
                  $.each(data, function (key, val) {
                    var value = parseFloat(val);
                    if(key == "lat") {
                      lat = value;
                    } else {
                      lng = value;
                    }
                  });
                  obj = {lat: lat, lng: lng};
                  mapLatLng.push(obj);
                }); 
                
                var flightPlanCoordinates = mapLatLng;
                
                var carPath = new google.maps.Polyline({
                  path: flightPlanCoordinates,
                  geodesic: true,
                  strokeColor: '#FF0000',
                  strokeOpacity: 1.0,
                  strokeWeight: 2
                });

                var contentStringSource = '<div id="content">'+'<div id="siteNotice">'+'<div id="bodyContent">'+'<p><b>Source location</b><br/> '+json.latLng.pick_up_point+'</p>'+'</div>'+'</div>';

                var infowindow = new google.maps.InfoWindow({
                  content: contentStringSource
                });

                /** Start point marker **/
                var marker = new google.maps.Marker({
                  position: {lat: parseFloat(sourceLatlng[0]), lng: parseFloat(sourceLatlng[1])},
                  map: map,
                  icon: 'http://162.144.204.188/~etpl2013/Demo/tulii/assets/images/green-dot.png',
                  //title:"Source"                  
                });

                  /** driver ride point marker **/
              /*  var marker = new google.maps.Marker({
                  position: {lat: 18.561656, lng: 73.784648},
                  map: map,
                  icon: 'http://162.144.204.188/~etpl2013/Demo/tulii/assets/images/car.png'
                });
              */                
                marker.addListener('click', function() {
                  infowindow.open(map, marker);
                });
                /*console.log($.type(destLatlng[0]));
                console.log($.type(obj.lat));*/
                //if( obj.lat >= parseFloat(destLatlng[0])  && obj.lng >= parseFloat(destLatlng[1]) ) {
                  var contentStringDestination = '<div id="content">'+
                    '<div id="siteNotice">'+
                    '<div id="bodyContent">'+
                    '<p><b>Destination location</b><br/>'+json.latLng.drop_point+'</p>'+
                    '</div>'+
                    '</div>';

                  var infowindow1 = new google.maps.InfoWindow({
                    content: contentStringDestination
                  });
                  
                  var markerDt = new google.maps.Marker({
                    position: {lat: parseFloat(destLatlng[0]), lng: parseFloat(destLatlng[1])},
                    map: map,
                    icon: 'http://162.144.204.188/~etpl2013/Demo/tulii/assets/images/blue-dot.png'                    
                  });
                   markerDt.addListener('click', function() {
                    infowindow1.open(map, markerDt);
                  });            
                //}
                carPath.setMap(map);
                interval(id,type);
          //$("#latlngArray").val(json.mapData);                
          $('#viewMapModal').modal('show', {backdrop: 'static'});
        }        
      });
  }else{     
    toastr.error("Something went wrong!!!.");         
  }  
  
}

function interval(id,type){  
  setInterval(function(){
    viewBookingRide(id,type);
  }, 180000);  
}
