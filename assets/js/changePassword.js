/**
 * File : changePassword.js
 * 
 * This file contain the validation of changepassword form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Rajendra pawar
 */

$(document).ready(function(){
	
	var changePassForm = $("#changePass");
	
	var validator = changePassForm.validate({
		
		rules:{


			oldPassword : {required : true},
			newPassword : {required : true},
			cNewPassword : {required : true, equalTo: "#newPassword"}
			
		},
		messages:{
			oldPassword :{ required : "This field is required" },
			newPassword :{ required : "This field is required" },
			cNewPassword : {required : "This field is required", equalTo: "The Confirm new password field does not match the New password field" }						
		}
	});
});
