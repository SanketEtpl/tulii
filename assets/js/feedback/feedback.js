function feedbackList(){   
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"feedback_management/Feedback/feedback_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
               
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
                $("input[type=search]").keydown(function(event) {
                  k = event.which;
                  if (k === 32 && !this.value.length)
                      event.preventDefault();
                });
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
            $("#table").css({"opacity":"1"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,3], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    }); 
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  feedbackList();
}

function deleteFeedback($this)
{
  if($($this).attr("data-row-id")) {
      var kay= "'"+ $($this).attr("data-row-id") +"'";
      $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $(".inner_body").html('<b>Are you sure you want to delete this feedback?</b>');
      $(".inner_footer").html('<button type="button" onclick="delFeedback('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
      $('.commanPopup').modal('show');       
  } else {
    toastr.error("Something went wrong!!!.");           
  }
}  

function delFeedback(key){ 
  $.ajax({
        type: "POST",
        dataType: "json",
        beforeSend: function() {
          $('#LoadingDiv').show();
        },
        complete: function(){
          $('#LoadingDiv').hide();
        },
          url: baseURL+"feedback_management/Feedback/deleteFeedback",
            data: {"key":key},
        }).success(function (json) {
            if(json.status == "success") {
              toastr.success(json.msg); // success message
              $("#table").dataTable().fnDraw();
            } else {
              toastr.error(json.msg); // error message
            }
      });
    $('.commanPopup').modal('hide');    
}

function editFeedback($this) // edit feedback data
{
  //$('#feedbackForm')[0].reset(); //reset of feedback list value 
  $(".feedback").remove();
  $(".roleType").remove();
  $( ".headerTitle" ).html( "<h3 class='modal-title'><b>Edit feedback topic</b></h3>" );
  if($($this).attr("data-row-id")){ // get feedback id
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"feedback_management/Feedback/viewFeedback",
      data: {"key":$($this).attr("data-row-id")},
    }).success(function (json) {
      if(json.status == "success"){            
          $("#user_key").val($($this).attr("data-row-id"));
          $("#feedback").val(json.feedbackData.user_feedback);
          $("#roleType").val(json.feedbackData.role_id);
        } else {
            toastr.error(json.msg);
        }
    });
    } else {
      toastr.error("Feedback id is not empty.");
      $("#user_key").val('');    
  }   
  $('#feedbackModal').modal('show');
}

$(document).ready(function(){
  $('#feedbackForm')[0].reset();
  $('#btnFeedback').click(function() { // save record of feedback data
    var feedback = $("#feedback").val();
    var roleType = $("#roleType").val();
    var user_key = $("#user_key").val();
    var flag = 0;
    if(feedback == ''){
      $(".feedback").remove();  
      $("#feedback").parent().append("<div class='feedback' style='color:red;'>Feedback field is not empty.</div>");
        flag = 1;
    } else {
        $(".feedback").remove();          
    }

    if(roleType == ''){
      $(".roleType").remove();  
      $("#roleType").parent().append("<div class='roleType' style='color:red;'>Please select role.</div>");
      flag = 1;
    } else {
      $(".roleType").remove();          
    }

    if(flag == 1)
      return false;    
    else
    { 
      $.ajax({
        type:"POST",
        url:baseURL+"feedback_management/Feedback/addUpdateFeedback",  
        beforeSend: function() {            
        },
        complete: function(){        
        },          
        data:{"editId":user_key,"feedback":feedback,"roleType":roleType},
        dataType:"json",
        async:false,
        success:function(response){                     
        if(response.status == "success")
        {  
          $("#feedbackModal").modal('hide');
          $("#table").dataTable().fnDraw();
          toastr.success(response.message);                                      
        } else {
            $("#feedbackModal").modal('show');
            toastr.error(response.message);                                       
          }
        }
      });    
    }   
  });  
    
  $('#addFeedbackPopUp').on("click",function(){
    $( ".headerTitle" ).html( "<h3 class='modal-title'><b>Add feedback topic</b></h3>" );
    $(".feedback").remove();
    $(".roleType").remove();
    $("#user_key").val("");
    $("#feedbackForm")[0].reset(); 
    $("#feedbackModal").modal('show');   
  });  
});