/**
 * @author Rajendra Pawar
 */

jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deleteCareDriver", function(){
		var userId = $(this).data("userid");
		$("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	    $(".inner_body").html('Are you sure you want to delete?');
		$(".inner_footer").html('<button type="button" onclick="deleteCareDriver('+userId+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
		$('.commanPopup').modal('show');	
	});
	
	jQuery(document).on("click", ".editCareDriver", function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "careDriver/editCareDriverDetails";
	  		$.ajax({
			type : "POST",
			async: false,
			dataType : "json",
			url : hitURL,
			beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
			data : { userId : userId }
		}).success(function (json) {
	   	  console.log(json);
	   	   
        $("#ModalLabel").html('<i class="fa fa-users"></i> Care Driver Management/Care Driver<small> Edit</small><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	    $(".inner_body").html(json.data);
		$(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><button type="button" onclick="checkEmilExist(\'submit\');" class="btn btn-primary">Save changes</button>');
		$('.commanPopup').modal('show');	
	 });			
	});	

  });

 			

function careDriverStatus(userId,user_status,active_id,inActive_id)
{

jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL = baseURL + "careDriver/careDriverStatus",
			beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
			data : { userId : userId, user_status: user_status} 
			}).done(function(data){
				console.log(data);
				if(user_status == 1) { 
					var str_success="Care driver successfully actived";
					var str_error="Care driver activation failed";

					$(active_id).removeClass("btn-active-disable");
					$(active_id).addClass("btn-active-enable");

					$(inActive_id).removeClass("btn-inactive-enable");
					$(inActive_id).addClass("btn-inactive-disable");

					$(active_id).prop('disabled', true);
					$(inActive_id).prop('disabled', false);
			    	}
				else{

					$(active_id).removeClass("btn-active-enable");
					$(active_id).addClass("btn-active-disable");

					$(inActive_id).removeClass("btn-inactive-disable ");
					$(inActive_id).addClass("btn-inactive-enable");

					var str_success="Care driver successfully inactived";
				    var str_error="Care driver inactivation failed";

				    $(active_id).prop('disabled', false);
					$(inActive_id).prop('disabled', true);
					}
				if(data.status == true) {toastr.success(str_success);

				}
				else if(data.status == false) { toastr.error(str_error); }
				else { alert("Access denied..!"); }
			});	
}

function checkEmilExist(info)
{
$("#email_error_id").css("display", "none");	
var email = $('#email').val();
var userId = $('#userId').val();
jQuery.ajax({
			type : "POST",
			dataType : "json",
			 async: false,
			url : baseURL + "careDriver/checkEmilExist",
			beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
			data : { userId : userId, email : email} 
			}).done(function(data){
				console.log(data);
				if(data.status == true) {
				$("#email_error_id").css("display", "inline");
				}
				else if(data.status == false) { 
					$("#email_error_id").css("display", "none");
					if(info =="submit") {
					reLoadList();
				   }					
				}
				else { alert("Access denied..!"); }
			});
}


function loadList()
{		
	var table = $('#careDriverList-grid').DataTable();
            $('#careDriverList-grid').empty();
            table.destroy();

       $('#careDriverList-grid').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"careDriver/careDriverList",
            beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,4,5 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });	
}
function reLoadList(){
  var validator = jQuery( "#editcareDriverFrm" ).validate({
		
		rules:{
			fname :{ required : true },
			email : { required : true, email : true,},
			password : {equalTo: "#cpassword"},
			cpassword : {equalTo: "#password"},
			mobile : { required : true, digits : true }
		},
		messages:{
			fname :{ required : "This field is required" },
			email : { required : "This field is required", email : "Please enter valid email address"},
			password : {equalTo: "The password field does not match with confirm password field"},
			cpassword : {equalTo: "The Confirm password field does not match with password field"},
			mobile : { required : "This field is required", digits : "Please enter numbers only" }
		}
	});
 if(validator.element( "#fname" ) && validator.element( "#email" ) && validator.element( "#password" ) && validator.element( "#cpassword" ) && validator.element( "#mobile" )){

if($('#password').val() == $('#cpassword').val()) {
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			async: false,
			url : baseURL + "careDriver/editCareDriver",
			beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
			data : { userId : $('#userId').val(),
			         email  : $('#email').val(),
			         mobile : $('#mobile').val(),
			         fname  : $('#fname').val(),
			         password  : $('#password').val(),
			         cpassword  : $('#cpassword').val()
			} 
			}).done(function(data){
				console.log(data);
				if(data.status == true) {toastr.success("Care driver successfully updated");}
				else if(data.status == false) { toastr.error("Care driver updation failed"); }
				else { alert("Access denied..!"); }

			});
	 $('.commanPopup').modal('hide');		
	loadList();
}
}
}
function addCareDriver()
{

 var validator = jQuery( "#addcareDriverfrm" ).validate({
		
		rules:{
			fname_new :{ required : true },
			email_new : { required : true, email : true, remote : { url : baseURL + "careDriver/checkEmilExist", type :"post"} },
			password_new : { required : true , equalTo: "#cpassword_new"},
			cpassword_new : {required : true, equalTo: "#password_new"},
			mobile_new : { required : true, digits : true }
		},
		messages:{
			fname_new :{ required : "This field is required" },
			email_new : { required : "This field is required", email : "Please enter valid email address", remote : "Email already taken" },
			password_new : { required : "This field is required" ,equalTo: "The password field does not match with confirm password field" },
			cpassword_new : {required : "This field is required", equalTo: "The Confirm password field does not match with password field" },
			mobile_new : { required : "This field is required", digits : "Please enter numbers only" }
		}
	});
  if(validator.element( "#fname_new" ) && validator.element( "#email_new" ) && validator.element( "#password_new" ) && validator.element( "#cpassword_new" ) && validator.element( "#mobile_new" )){

	jQuery.ajax({
			type : "POST",
			dataType : "json",
			async: false,
			url : baseURL + "careDriver/addCareDriver",
			beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
			data : { fname_new  : $('#fname_new').val(),
			         email_new  : $('#email_new').val(),
			         mobile_new : $('#mobile_new').val(),			         
			         password_new  : $('#password_new').val(),
			         cpassword_new  : $('#cpassword_new').val()
			} 
			}).done(function(data){
				console.log(data);
				if(data.status == true) {toastr.success("Care driver successfully added");}
				else if(data.status == false) { toastr.error("Care driver addtion failed"); }
				else { alert("Access denied..!"); }

			});
	$('.commanPopup').modal('hide');	
	loadList();
}
}
function openAddCarePopup(){
	hitURL = baseURL + "careDriver/addCareDriverPopup";
	  		$.ajax({
			type : "POST",
			async: false,
			dataType : "json",
			url : hitURL,
			beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
			data : {}
		}).success(function (json) {
	   	  console.log(json);	   	   
  $("#ModalLabel").html('<i class="fa fa-users"></i> Care Driver Management/Care Driver<small> Add</small><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
 $(".inner_body").html(json.data);
 $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><button type="button" onclick="addCareDriver();" class="btn btn-primary">Save</button>');
 $('.commanPopup').modal('show');
	 }); 

}
function deleteCareDriver(userId)
{
$('.commanPopup').modal('hide');		
jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : baseURL + "careDriver/deleteCareDriver",
			beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
			data : { userId : userId } 
			}).done(function(data){
				console.log(data);				
				if(data.status == true) {toastr.success("Care driver successfully deleted");}
				else if(data.status == false) { toastr.error("Care driver deletion failed"); }
				else { alert("Access denied..!"); }
			});	
	loadList();			
}