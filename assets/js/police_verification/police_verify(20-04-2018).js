$(document).ready(function(){
	$('#policeVerifyForm')[0].reset();
  	$('#btnUploadDocuments').click(function() { // save record of subject list  
	    var uploadDocuments = $("#uploadDocuments").val();
      var verify_no = $("#verify_no").val();
	    var user_key = $("#user_key").val();
	    var flag = 0;
	    var ext = $('#uploadDocuments').val().split('.').pop().toLowerCase();   

    	if(uploadDocuments == ''){
		    $(".uploadDocuments").remove();  
	    	$("#uploadDocuments").parent().append("<div class='uploadDocuments' style='color:red;'>Please upload documents.</div>");
	      	flag = 1;
	    } else {
	    	$(".uploadDocuments").remove();
	    } 

      if(verify_no == ''){
        $(".verify_no").remove();  
        $("#verify_no").parent().append("<div class='verify_no' style='color:red;'>Verification no field is required.</div>");
          flag = 1;
      } else {
        $(".verify_no").remove();
      } 

	    if(flag == 1)
	      	return false;    
	    else
	    {      
	      	//$("#LoadingDiv").css({"display":"block"});
	      	var formData = new FormData($('#policeVerifyForm')[0]);
	      	$.ajax({
		        type:"POST",
		        url:baseURL+"police_verification/Police_verify_doc/addDocuments",            
		        dataType:"json",
		        data: formData,	
		        cache: false,
		        contentType: false,
		        processData: false,		        	        
		        success:function(response){                     
		          	if(response.status == "success")
		          	{
			            $("#LoadingDiv").css({"display":"none"});
			            $("#policeVerifyModal").modal('hide');
			            $("#table").dataTable().fnDraw();
			            toastr.success(response.message);                                      
		         	} else if(response.status == "warning") {
			            $("#LoadingDiv").css({"display":"none"});
			            $("#policeVerifyModal").modal('show');
			            toastr.warning(response.message); 
			        } else {
			            $("#LoadingDiv").css({"display":"none"});
			            $("#policeVerifyModal").modal('show');
			            toastr.error(response.message);                                       
		          	}
		        }
	      	});    
	    }   
	});  

  $('#addPopUp').on("click",function(){
    $( "h3" ).html( "<h3 class='modal-title'><b>Add documents</b></h3>" );
    $(".uploadDocuments").remove();    
    $("#showFilename").html("No file chosen");
    $("#user_key").val("");
    $("#dvPreview").remove();
    $("#btnUploadDocuments").attr('disabled',false);
    $("#policeVerifyForm")[0].reset();
    $("#policeVerifyModal").modal('show');    
  });

   $(document).on("click", ".viewPic", function(){
    var baseurl = $(this).data("baseurl");
    var picname = $(this).data("picname");
    var error_msg = $(this).data("error");
    var headerMsg = $(this).data("headermsg");
    var imgPath = $(this).data("imgpath");
   if(picname){

    var array = picname.split("|");
   // var cnt =1 ;
    var temp= '';
     $("#modalHeader").html(headerMsg+'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
     $("#modalBody").html('<div id="image-gallery"><div class="image-container"></div><img src="'+imgPath+'assets/images/left.svg" class="prev"/><img src="'+imgPath+'assets/images/right.svg"  class="next"/><div class="footer-info"><span class="current"></span>/<span class="total"></span></div></div> ');
     $("#modalFooter").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
     $('.photoGallery').modal('show'); 
     
      a = new Array();
     for (var i in array){
      var http = new XMLHttpRequest();
      http.open('HEAD', baseurl+array[i], false);
      http.send();
      if(http.status != 404) {
      a[i]=baseurl+array[i]; 
      } else{
      a[i]=imgPath+'assets/images/not_found.png';   
      }
     
     }
    
    var images = Array();
    for(var j=0;j<a.length;j++)
    {
      images.push({small:a[j],big:a[j]}); 
    }  
       
     var curImageIdx = 1,
        total = images.length;
    var wrapper = $('#image-gallery'),
        curSpan = wrapper.find('.current');
    var viewer = ImageViewer(wrapper.find('.image-container'));
 
    //display total count
    wrapper.find('.total').html(total);
 
    function showImage(){
        var imgObj = images[curImageIdx - 1];
        viewer.load(imgObj.small, imgObj.big);
        curSpan.html(curImageIdx);

    }
 
    wrapper.find('.next').click(function(){
         curImageIdx++;
        if(curImageIdx > total) curImageIdx = 1;
        showImage();
    });
 
    wrapper.find('.prev').click(function(){
         curImageIdx--;
        if(curImageIdx < 0) curImageIdx = total;
        showImage();
    });
 
    //initially show image
    showImage();  
     } else{
     toastr.error(error_msg); 
     }
  });

});

function userStatus_popup(userId,user_status,active_id,inActive_id)
{
 
var temp = "userStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
$("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
$(".inner_body").html('<b>Are you sure want to change status ?</b>');
$(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
$('.commanPopup').modal('show');
}
function userStatus(userId,user_status,active_id,inActive_id)
{ 
 $('.commanPopup').modal('hide');
     $.ajax({
      type : "POST",
      dataType : "json",
      url : baseURL + "police_verification/Police_verify_doc/policeVerifyStatusChange",
      data : { userId : userId, user_status: user_status} 
      }).success(function(data){   
      if(user_status == 1) { 
        
        var str_success="Document verification has been completed successfully";
        var str_error="Document verification has been complete failed";

        $("#"+active_id).removeClass("btn-active-disable");
        $("#"+active_id).addClass("btn-active-enable");

        $("#"+inActive_id).removeClass("btn-inactive-enable");
        $("#"+inActive_id).addClass("btn-inactive-disable");

        $("#"+active_id).prop('disabled', true);
        $("#"+inActive_id).prop('disabled', false);
        } else {
          $("#"+active_id).removeClass("btn-active-enable");
          $("#"+active_id).addClass("btn-active-disable");

          $("#"+inActive_id).removeClass("btn-inactive-disable ");
          $("#"+inActive_id).addClass("btn-inactive-enable");

          var str_success="Document verification has been Incompleted successfully";
          var str_error="Document verification has been Incomplete failed";

          $("#"+active_id).prop('disabled', false);
          $("#"+inActive_id).prop('disabled', true);
        }
      if(data.status = true) {
        toastr.success(str_success);
       }
      else if(data.status = false) { 
          toastr.error(str_error);
        }
      else {
          toastr.error("Access denied..!");
        }
    }); 
  //$("#LoadingDiv").css({"display":"none"});
 }

function downloadDocuments($this)
{
  if($($this).attr("data-row-id")){            
    $.ajax({
          type: "POST",
          dataType: "json",
         url: baseURL+"police_verification/Police_verify_doc/downloadDoc",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
       // console.log(json);
          if(json.status == "success"){ 
            var array = Array();
            var array = json.downloadData.police_verify_doc.split('|');
            var imagePath=""
            for(var i=0; i< array.length; i++)
            {
               imagePath += '<div><a href="'+json.imagePath+array[i]+'" download="'+array[i]+'"><img src="'+json.imagePath+array[i]+'" style="width:400px;height:250px" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download documents"></a><a href="'+json.imagePath+array[i]+'" download="'+array[i]+'" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-download-alt"></span> Download</a></div>';
            }
            $("#documentsDown").html(imagePath);          
            $('#downloadModal').modal('show', {backdrop: 'static'});

         } else {
           toastr.error(json.msg); 
         }

      });
  }else{
    toastr.error("Document verification something get wrong.");         
  }
}

function deleteDocuments($this)
{
  if($($this).attr("data-row-id")){    
    $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL+"police_verification/Police_verify_doc/downloadDoc",
        data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        //console.log(json);
          if(json.status == "success"){ 
            var array = Array();
            var array = json.downloadData.police_verify_doc.split('|');
            var imagePath=""
            $("#user_key").val(json.downloadData.user_id);
            $("#copyOfImgPath").val(json.downloadData.police_verify_doc);
            for(var i=0; i< array.length; i++)
            {
               imagePath += '<div id="'+"image_"+i+'"><img src="'+json.imagePath+array[i]+'" style="width:400px;height:250px" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete documents"><a data-image-id="'+"image_"+i+'" data-row-userid="'+json.downloadData.user_id+'" data-row-image-path="'+array[i]+'" onclick="deleteDocumentsImage(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><span class="glyphicon glyphicon-remove-circle"></span></a></div>';
            } 
            $("#deleteDocuments").html(imagePath);          
            $('#deleteModal').modal('show', {backdrop: 'static'});

         } else {
           toastr.error(json.msg); 
         }

      });
  }else{
    toastr.error("Document verification something get wrong.");         
  }
}

function deleteDocumentsImage($this) {
  if($($this).attr("data-row-userid")){    
    $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL+"police_verification/Police_verify_doc/deleteImage",
        data: {"image_id":$($this).attr("data-image-id"),"image":$($this).attr("data-row-image-path"),"user_id":$($this).attr("data-row-userid")},
      }).success(function (json) {
          if(json.status == "success"){ 
            if(json.deleteStaus == 1) {
              $(json.imageName).remove();
              toastr.success(json.msg); 
            } else {
               toastr.error("Image not delete successfully .");  
            }
         } else {
           toastr.error(json.msg); 
         }
      });
  }else{
    toastr.error("Document verification something get wrong.");         
  }

}