$(document).ready(function(){
	$('#policeVerifyForm')[0].reset();
  	$('#btnUploadDocuments').click(function() { // save record of subject list  
	    var uploadDocuments = $("#uploadDocuments").val();
	    var user_key = $("#user_key").val();
	    var flag = 0;
	    var ext = $('#uploadDocuments').val().split('.').pop().toLowerCase();   

    	if(uploadDocuments == ''){
		    $(".uploadDocuments").remove();  
	    	$("#uploadDocuments").parent().append("<div class='uploadDocuments' style='color:red;'>Please upload documents.</div>");
	      	flag = 1;
	    } else {
	    	$(".uploadDocuments").remove();
	    } 

	    if(flag == 1)
	      	return false;    
	    else
	    {      
	      	//$("#LoadingDiv").css({"display":"block"});
	      	var formData = new FormData($('#policeVerifyForm')[0]);
	      	$.ajax({
		        type:"POST",
		        url:baseURL+"police_verification/Police_verify_doc/addDocuments",            
		        dataType:"json",
		        data: formData,	
		        cache: false,
		        contentType: false,
		        processData: false,		        	        
		        success:function(response){                     
		          	if(response.status == "success")
		          	{
			            $("#LoadingDiv").css({"display":"none"});
			            $("#policeVerifyModal").modal('hide');
			            $("#table").dataTable().fnDraw();
			            toastr.success(response.message);                                      
		         	} else if(response.status == "warning") {
			            $("#LoadingDiv").css({"display":"none"});
			            $("#policeVerifyModal").modal('show');
			            toastr.warning(response.message); 
			        } else {
			            $("#LoadingDiv").css({"display":"none"});
			            $("#policeVerifyModal").modal('show');
			            toastr.error(response.message);                                       
		          	}
		        }
	      	});    
	    }   
	});  

  $('#addPopUp').on("click",function(){
    $( "h3" ).html( "<h3 class='modal-title'><b>Add documents</b></h3>" );
    $(".uploadDocuments").remove();    
    $("#showFilename").html("No file chosen");
    $("#user_key").val("");
    $("#dvPreview").remove();
    $("#btnUploadDocuments").attr('disabled',false);
    $("#policeVerifyForm")[0].reset();
    $("#policeVerifyModal").modal('show');    
  });

   $(document).on("click", ".viewPic", function(){
    var baseurl = $(this).data("baseurl");
    var picname = $(this).data("picname");
    var error_msg = $(this).data("error");
    var headerMsg = $(this).data("headermsg");
    var imgPath = $(this).data("imgpath");
   if(picname){

    var array = picname.split("|");
   // var cnt =1 ;
    var temp= '';
     $("#modalHeader").html(headerMsg+'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
     $("#modalBody").html('<div id="image-gallery"><div class="image-container"></div><img src="'+imgPath+'assets/images/left.svg" class="prev"/><img src="'+imgPath+'assets/images/right.svg"  class="next"/><div class="footer-info"><span class="current"></span>/<span class="total"></span></div></div> ');
     $("#modalFooter").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
     $('.photoGallery').modal('show'); 
     
      a = new Array();
     for (var i in array){
      var http = new XMLHttpRequest();
      http.open('HEAD', baseurl+array[i], false);
      http.send();
      if(http.status != 404) {
      a[i]=baseurl+array[i]; 
      } else{
      a[i]=imgPath+'assets/images/not_found.png';   
      }
     
     }
    
    var images = Array();
    for(var j=0;j<a.length;j++)
    {
      images.push({small:a[j],big:a[j]}); 
    }  
       
     var curImageIdx = 1,
        total = images.length;
    var wrapper = $('#image-gallery'),
        curSpan = wrapper.find('.current');
    var viewer = ImageViewer(wrapper.find('.image-container'));
 
    //display total count
    wrapper.find('.total').html(total);
 
    function showImage(){
        var imgObj = images[curImageIdx - 1];
        viewer.load(imgObj.small, imgObj.big);
        curSpan.html(curImageIdx);

    }
 
    wrapper.find('.next').click(function(){
         curImageIdx++;
        if(curImageIdx > total) curImageIdx = 1;
        showImage();
    });
 
    wrapper.find('.prev').click(function(){
         curImageIdx--;
        if(curImageIdx < 0) curImageIdx = total;
        showImage();
    });
 
    //initially show image
    showImage();  
     } else{
     toastr.error(error_msg); 
     }
  });

});