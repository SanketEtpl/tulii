/*

Author : Rajendra pawar 
Page :  generalManagement.js
Description : generalManagement  use for general management module functionality

*/
jQuery(document).ready(function(){
  jQuery('#LoadingDiv').show();
		jQuery(document).on("click", ".deleteContact", function(){
	var contactId = $(this).data("contactid");
		$("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	    $(".inner_body").html('<b> Are you sure you want to delete?</b>');
		$(".inner_footer").html('<button type="button" onclick="delContact('+contactId+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
		$('.commanPopup').modal('show');
	});

	
	jQuery(document).on("click", ".updateAbout", function(){
	var about_id = $('#about_id').val();	
	var message_body = CKEDITOR.instances['about_data'].getData();
	hitURL = baseURL + "aboutUs/updateAbout",
	        jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			 beforeSend: function() {
            // $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
			data : { message_body : message_body, about_id : about_id } 
			}).done(function(data){
				console.log(data);
				if(data.status == true) { 
				toastr.success("About Us has been updated successfully."); }
				else if(data.status == false) {toastr.error("About Us updation failed."); }
				else { toastr.error("Access denied..!"); }
			});
		
	});
		

	
	jQuery(document).on("click", ".updateTerms", function(){
	var terms_id = $('#terms_id').val();	
	var message_body = CKEDITOR.instances['terms_data'].getData();
	hitURL = baseURL + "terms/updateTerms",
	        jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			beforeSend: function() {
            // $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
			data : { message_body : message_body, terms_id : terms_id } 
			}).done(function(data){
				console.log(data);
				if(data.status == true) { 
				toastr.success("Terms & Condition has been updated successfully."); }
				else if(data.status == false) {toastr.error("Terms & Condition updation failed."); }
				else { toastr.error("Access denied..!"); }
			});
		
	});

	jQuery(document).on("click", ".deleteRating", function(){
		var ratingid = $(this).data("ratingid");
		$("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	    $(".inner_body").html('<b>Are you sure you want to delete?</b>');
		$(".inner_footer").html('<button type="button" onclick="delRating('+ratingid+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
		$('.commanPopup').modal('show');
	});


  jQuery(document).on("click", ".viewContact", function(){
    var contactId = $(this).data("contactid"),
      hitURL = baseURL + "contactUs/viewContactDetails";
        $.ajax({
      type : "POST",
      async: false,
      dataType : "json",
      url : hitURL,
      beforeSend: function() {        
            // $('#LoadingDiv').show();
             },
             complete: function(){
        $("#LoadingDiv").hide();
        },
      data : { contactId : contactId }
    }).success(function (json) {
        console.log(json);
    $("#ModalLabel").html('Message<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html(json.data);
    $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
    $('.commanPopup').modal('show');  
   });      
  }); 
});


function loadRatingList()
{		
	var table = $('#rating-grid').DataTable();
        $('#rating-grid').empty();
        table.destroy();
        $('#rating-grid').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"rating/ratingList",
            beforeSend: function() {
                var search = $("input[type=search]").val();
                if(search=="")
                
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
            },
            complete: function(){
     		    $('#LoadingDiv').hide();
            $(".display").css({"opacity":"1"});
  			},
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
            { 
                "targets": [ 0,4], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],
 
    });	
}
function delContact(contactId)
{			
   
	jQuery.ajax({
	type : "POST",
	dataType : "json",
	url : baseURL + "contactUs/deleteContact",
	beforeSend: function() {
    //$('#LoadingDiv').show();
     },
     complete: function(){
		$('#LoadingDiv').hide();
		},
	data : { contactId : contactId } 
	}).done(function(data){
		//console.log(data);
        //alert(data);
		if(data.status == true) {
            //$("#LoadingDiv").css({"display":"none"});
            $(this).parents("tr:first").remove();
            $("#contact-grid").dataTable().fnDraw();
            toastr.success("Contact has been deleted successfully.");
        }
		else if(data.status == false) { toastr.error("Contact deletion failed."); }
		else { toastr.error("Access denied..!"); }
	});
	$('.commanPopup').modal('hide');
    $("#LoadingDiv").css({"display":"none"});
	//loadContactList();		
    
}

function delRating(ratingid)
{
	// $("#LoadingDiv").css({"display":"block"});
	jQuery.ajax({
	type : "POST",
	dataType : "json",
	url : baseURL + "rating/deleteRating",
	beforeSend: function() {
    // $('#LoadingDiv').show();
     },
     complete: function(){
		$('#LoadingDiv').hide();
		},
	data : { ratingId : ratingid } 
	}).done(function(data){
		//console.log(data);
		if(data.status == true) {
           // alert("success");
            
            $(this).parents("tr:first").remove();
            $("#rating-grid").dataTable().fnDraw();
            $("#LoadingDiv").css({"display":"none"});
            toastr.success("Rating has been deleted successfully.");
        }
		else if(data.status == false) { toastr.error("Rating deletion failed."); }
		else { toastr.error("Access denied..!"); }
	});	
	$('.commanPopup').modal('hide');    
    $("#LoadingDiv").css({"display":"none"});
	//loadRatingList();
}
function loadContactList()
{		
	var table = $('#contact-grid').DataTable();
    $('#contact-grid').empty();
    table.destroy();
       $('#contact-grid').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"contactUs/contactlist",
            beforeSend: function() {
                var search = $("input[type=search]").val();
                if(search=="")
                 //$('#LoadingDiv').show();
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#contact-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
            },
            complete: function(){
     		    $('#LoadingDiv').hide();
            $(".display").css({"opacity":"1"});
  			},
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,4], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });	
}