function deleteNotification($this)
{
  if($($this).attr("data-row-id")){
    var kay= "'"+ $($this).attr("data-row-id") +"'";
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure you want to delete this notification?</b>');
    $(".inner_footer").html('<button type="button" onclick="delNotification('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');       
    
  } else {
 toastr.error("Something went wrong!!!.");      
 }
}  
function delNotification(key){
 
 $.ajax({
          type: "POST",
          dataType: "json",
          beforeSend: function() {
             //$('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
         url: baseURL+"notification_management/Notification_list/deleteNotification",
          data: {"key":key},
        }).success(function (json) {
          if(json.status == "success"){
            toastr.success(json.msg); // success message
            $("#table").dataTable().fnDraw();
            }else{
            toastr.error(json.msg); // error message
          }
      });
    $('.commanPopup').modal('hide');
    //notificationList();
}
function notificationList()
{
 var table = $('#table').DataTable();
            $('#table').empty();
            table.destroy();

       $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
          "ajax": {
        "url": baseURL+"notification_management/Notification_list/ajax_list",
       beforeSend: function() {
          var search = $("input[type=search]").val();
          if(search == "")
          //$("#LoadingDiv").css({"display":"block"}); 
            $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
         },
        complete: function(){
        $("#LoadingDiv").css({"display":"none"}); 
        $("#table").css({"opacity":"1"}); 
      },
        "type": "POST"
    },
    
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [0,4,5], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  }); 
}   

function viewNotification($this)
{
  if($($this).attr("data-row-id")){            
    $.ajax({
          type: "POST",
          dataType: "json",
         url: baseURL+"notification_management/Notification_list/viewNotifInfo",
          beforeSend: function() {
          var search = $("input[type=search]").val();
          //if(search == "")
          //$("#LoadingDiv").css({"display":"block"}); 
         },
        complete: function(){
        $("#LoadingDiv").css({"display":"none"}); 
      },
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        console.log(json);
          if(json.status == "success"){
          $("#message").text(json.notifData.n_content);
          $("#name").text(json.notifData.user_name);
          $("#mobile").text(json.notifData.user_phone_number);
          if(json.notifData.n_read == 1)
            $("#isReading").text("Unread");
          else
            $("#isReading").text("Read");
          $('#notifListModal').modal('show', {backdrop: 'static'});

         }        
      });
  }else{     
    toastr.error("Something went wrong!!!.");         
  }  
} 
