$(document).ready(function(){
  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
      if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if(k === 32 && !this.value.length)
      event.preventDefault();
  });
});

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  loadTicketsList();
}

function loadTicketsList()
{
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.    
    "ajax": {
      "url": baseURL+"ticket_management/Ticket/ticketList",
      beforeSend: function() {
        var search = $("input[type=search]").val();
        if(search=="")          
          $("input[type=search]").on("keyup",function(event) {
          if($("#clear").length == 0) {
            if($(this).val() != ""){
              $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
            } 
          }
          if($(this).val() == "")  
          $("#clear").remove();      
        }); 
        $("input[type=search]").keydown(function(event) {
          k = event.which;
          if (k === 32 && !this.value.length)
            event.preventDefault();
        }); 
      },
      complete: function(){
        $("#LoadingDiv").css({"display":"none"}); 
        $("#table").css({'opacity':"1"});
      },
        "type": "POST"
    },
    
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [0,4,5], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  }); 
} 
 

function viewIssue($this)
{
  if($($this).attr("data-row-id")){            
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"ticket_management/Ticket/viewIssue",
      beforeSend: function() {        
      },
      complete: function(){        
      },
      data: {"key":$($this).attr("data-row-id"),"app_id":$($this).data('app-id')},
    }).success(function (json) {        
    if(json.status == "success"){
      $("#ModalLabel").html('Ticket Details<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $(".inner_body").html(json.data);
      $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
      $('.commanPopup').modal('show');
    } else {
      toastr.error(json.msg);            
    }
  });
  } else {     
    toastr.error("Something Went wrong!!!.");         
  }  
}

function changeIssueStatus_popup(userId,user_status,active_id,inActive_id)
{ 
  var temp = "changeIssueStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
  $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
  $(".inner_body").html('<b>Are you sure you want to change status?</b>');
  $(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
  $('.commanPopup').modal('show');
}

function changeIssueStatus(tId,ticket_status,active_id,inActive_id)
{  
  $('.commanPopup').modal('hide');
  $.ajax({
    type : "POST",
    dataType : "json",
    beforeSend: function() {             
    },
    complete: function(){        
    },
    url : baseURL + "ticket_management/Ticket/changeIssueStatus",
    data : { tId : tId, ticket_status: ticket_status} 
    }).done(function(data){
    if(ticket_status == 1) { 
      var str_success = "Ticket has been resolved.";
      var str_error = "Ticket has been resolved failed.";

      $("#"+active_id).removeClass("btn-success");
      $("#"+active_id).addClass("btn-success");

      $("#"+inActive_id).removeClass("btn-success");
      $("#"+inActive_id).addClass("btn-success");

      $("#"+active_id).prop('disabled', true);
      $("#"+inActive_id).prop('disabled', false);
      $("#table").dataTable().fnDraw();
      if(data.status = true) {
        toastr.success(str_success);
      } else if(data.status = false) {
        toastr.error(str_error);        
      } else {
        toastr.error("Access denied..!");
      }
    } else {
      toastr.error("Direct access denied..!");
    }    
  }); 
}  
