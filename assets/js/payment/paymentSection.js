function paymentSectionList()
{
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  
  $('#table').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
    
    "ajax": {
      "url": baseURL+"payment_management/Payment_section/ajax_list",
      beforeSend: function() {
        var search = $("input[type=search]").val();
        if(search=="")             
        $("input[type=search]").on("keyup",function(event) {
          if($("#clear").length == 0) {
             if($(this).val() != ""){
              $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
            } 
          }
          if($(this).val() == "")  
          $("#clear").remove();      
        }); 
        $("input[type=search]").keydown(function(event) {
          k = event.which;
          if (k === 32 && !this.value.length)
              event.preventDefault();
        });
      },
      complete: function() {
        $("#LoadingDiv").css({"display":"none"}); 
        $("#table").css({"opacity":"1"}); 
      },
      "type": "POST"
    },  
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
      "targets": [ 0,8 ], //first column / numbering column
      "orderable": false, //set not orderable
    },
  ],
});  
} 

function payNow($this){ 
   $.ajax({
      type: "POST",
      dataType: "json",
      beforeSend: function() {
         $('#LoadingDiv').show();
         },
         complete: function(){
    $('#LoadingDiv').hide();
    },
       url: baseURL+"payment_management/Payment_section/payToServiceProvider",
      data: {"payId":$($this).attr("data-row-id")},
    }).success(function (json) {
      if(json.status == "success"){
        toastr.success(json.msg); // success message
        $("#table").dataTable().fnDraw();
        }else{
        toastr.error(json.msg); // error message
      }
  }); 
}  

function payOnHold($this) // delete record of price list
{
  if($($this).attr("data-row-id")){
    var id= "'"+ $($this).attr("data-row-id") +"'";
    $("#ModalLabel").html('Payment On hold<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html("<div class='col-md-12 col-sm-12 col-xs-12'><b>Tell me reason why payment on-hold ?</b><div class='form-group'><label class='control-label' for='lbl_sub_title'>Sub title<span class='required_star'>*</span></label><textarea placeholder='Reason' required='true' id='reason' name='reason' class='form-control description_valid'></textarea></div></div>");
    $(".inner_footer").html('<button type="button" onclick="onHold('+ id +');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');
  } else {     
        toastr.error("Something went wrong!!!.");    
  }
}

function onHold(id){ 
  var reason = $("#reason").val();
  var flag = 0;
  if(reason == ''){
    $(".reason").remove();  
    $("#reason").parent().append("<div class='reason' style='color:red;'>Reason field is required.</div>");
    flag = 1;
  }else{
    $(".reason").remove();          
  }
  if(flag == 1) {
    return false;
  } else {
   $.ajax({
      type: "POST",
      dataType: "json",
      beforeSend: function() {
         $('#LoadingDiv').show();
         },
         complete: function(){
    $('#LoadingDiv').hide();
    },
      url: baseURL+"payment_management/Payment_section/paymentOnHoldOfSP",
      data: {"id":id,"reason":reason},
    }).success(function (json) {
      if(json.status == "success"){
        toastr.success(json.msg); // success message
        $("#table").dataTable().fnDraw();
        }else{
        toastr.error(json.msg); // error message
      }
  });
  $('.commanPopup').modal('hide'); 
  }   
}   

function viewPayment($this) // view of faq
{
  if($($this).attr("data-row-id")) {            
    $( "h3" ).html( "<h3 class='modal-title'><b>View Payment section</b><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></h3>" );
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"payment_management/Payment_section/viewPaymentList",
         beforeSend: function() {
             //$('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        //console.log(json);
          if(json.status == "success"){
          $("#viewDriverName").text(json.payData.user_name);
          $("#viewSourceLoc").text(json.payData.booking_pick_up_location);
          $("#viewDestinationLoc").text(json.payData.booking_drop_off_location); 
          $("#viewDate").text(json.payData.booking_start_date);
          $("#viewPrice").text(json.payData.booking_price);
          $("#viewStatus").text(json.payData.status);
          $("#viewRating").text(json.payData.user_rating);
          $("#viewTripStartTime").text(json.payData.booking_pick_up_time);
          $("#viewTripEndTime").text(json.payData.booking_drop_off_time);
          
          
          $('#viewPaymentModal').modal('show', {backdrop: 'static'});
         } else {
           toastr.error(json.msg); 
         }
      });
  }else{
    toastr.error("Something went wrong!!!.");         
  }
}