function paymentOnholdList()
{
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  
  $('#table').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
    
    "ajax": {
      "url": baseURL+"payment_management/Payment_onhold/ajax_list",
      beforeSend: function() {
        var search = $("input[type=search]").val();
        if(search=="")             
        $("input[type=search]").on("keyup",function(event) {
          if($("#clear").length == 0) {
             if($(this).val() != ""){
              $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
            } 
          }
          if($(this).val() == "")  
          $("#clear").remove();      
        }); 
        $("input[type=search]").keydown(function(event) {
          k = event.which;
          if (k === 32 && !this.value.length)
              event.preventDefault();
        });
      },
      complete: function() {
        $("#LoadingDiv").css({"display":"none"}); 
        $("#table").css({"opacity":"1"}); 
      },
      "type": "POST"
    },  
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
      "targets": [ 0,8 ], //first column / numbering column
      "orderable": false, //set not orderable
    },
  ],
});  
} 

function payNow($this) { 
   $.ajax({
      type: "POST",
      dataType: "json",
      beforeSend: function() {
         $('#LoadingDiv').show();
         },
         complete: function(){
    $('#LoadingDiv').hide();
    },
       url: baseURL+"payment_management/Payment_onhold/payToServiceProvider",
      data: {"payId":$($this).attr("data-row-id")},
    }).success(function (json) {
      if(json.status == "success"){
        toastr.success(json.msg); // success message
        $("#table").dataTable().fnDraw();
        }else{
        toastr.error(json.msg); // error message
      }
  }); 
}  

