/**
 * @author Kiran N.
 */

function checkOldPwd()
{
	//alert("hello");
	$(".inputOldPassword1").remove(); 
	 $(".inputOldPassword").remove();
 	var oldPwd = $("#inputOldPassword").val();
 	var result = 0;
    $.ajax({
      type:"POST",
      url:baseURL+"user/check_old_password",
      data:{'oldPassword':oldPwd},
      dataType:"json",
      async:false,          
      success:function(response){        
        if(response.status == true)
        {
        	if($("#inputOldPassword").val() == "")
        	{
        		$(".inputOldPassword1").remove();  
		      	$("#inputOldPassword").parent().append("<div class='inputOldPassword1' style='color:red;'>Old password field is required.</div>");
		      	result = 1;
        	} else {
        		$(".inputOldPassword1").remove(); 
		        $("#inputOldPassword").parent().append("<div class='inputOldPassword1' style='color:red;'>"+ response.message +"</div>");
		        result = 1;
        	}	          
        }
        else
        {         	
          $(".inputOldPassword1").remove();       
        }     
      }
    });  
    return result;
}

$(document).ready(function(){

	setInterval(function(){ 
		$.ajax({
		    type:"POST",
		    url:baseURL+"login/sessionExpire",
		    data:{},
		    dataType:"json",
		    async:false,          
		    success:function(response){ 
		    	//console.log(response);    
		    	if(response.status == "logout")
		    	{
		    		window.location.href = baseURL+"login";
		    	} 
		    }
	    });  

	}, 30000);

	setInterval(function(){ 
		$.ajax({
		    type:"POST",
		    url:baseURL+"user_management/UserController/notificationStatus",
		    data:{},
		    dataType:"json",
		    async:false,          
		    success:function(response){ 		    	
		    	if(response.status == "success")
		    	{
		    		toastr.success(response.message);
		    	} 
		    }
	    });  

	}, 30000);

	$('#loadChangePass').on("click",function(){
		$(".inputOldPassword1").remove();
		$(".inputOldPassword").remove();
		$(".inputPassword1").remove();
		$(".inputPassword2").remove();
		$("#changePwdForm")[0].reset();
		$("h3.mainHeader").html("<h3 class='modal-title'>Change Password</h3>");
		$("h4.setnewpass").html("<h4 class='setnewpass'>Set new password for your account</h4>");
		//$("h3").html("<h3 class='modal-title'>Change Password</h3>");
		//$("h4").html("<h4 class='modal-title'>Change password<small>Set new password for your account</small></h4>");
	   
	    $("#changePwdModal").modal('show');	   
	});	

	$('#btnChagePwdSave').on("click",function(){
		var inputOldPassword = $("#inputOldPassword").val();
		var inputPassword1 = $("#inputPassword1").val();
		var inputPassword2 = $("#inputPassword2").val();
		$(".inputOldPassword1").remove();
		var flag = 0;
		if(inputPassword1 == ''){     
		    flag = 1;
		    $(".inputPassword1").remove();  
		    $("#inputPassword1").parent().append("<div class='inputPassword1' style='color:red;'>Password field is required.</div>");
	    }
	    else
	    {		    
		    $(".inputPassword1").remove();  		       
	    }

	    if(inputPassword2 == ''){
	     	$(".inputPassword2").remove();  
	      	$("#inputPassword2").parent().append("<div class='inputPassword2' style='color:red;'>Please enter your new password and confirm password.</div>");
	      	flag = 1;
	    } else if(inputPassword1 != inputPassword2) {
	    	$(".inputPassword2").remove();  
	      	$("#inputPassword2").parent().append("<div class='inputPassword2' style='color:red;'>Passwords didn't match. Please try again.</div>");
	      	flag = 1;
	    }
	    else {
	        $(".inputPassword2").remove();          
	    }
	    var chk = checkOldPwd();
	    if(flag == 1 || chk == 1)
      		return false;    
	    else
	    {
	    	$.ajax({
			    type:"POST",
			    url:baseURL+"changePassword",
			    data:{'newPassword':inputPassword1},
			    dataType:"json",
			    async:false,          
			    success:function(response){        
			    if(response.status == true)
			    {			     	
		            $("#success").css("display","block");
		            $("#success").html("<strong>"+response.message+"</strong>");
		            setInterval(function(){ $("#success").css("display","none"); $("#changePwdModal").modal('hide'); }, 5000); 
			    } else {       			         	
			            $("#error").css("display","block");
			            $("#error").html("<strong>"+response.message+"</strong>");
			            setInterval(function(){ $("#error").css("display","none"); $("#changePwdModal").modal('show'); }, 5000); 
			        }     
			    }
		    });  
		}
	});
	
	/*jQuery(document).on("click", ".deleteUser", function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "deleteUser",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this user ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("User successfully deleted"); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	jQuery(document).on("click", ".searchList", function(){
		
	});*/
	
	// add plus & minus symbol to menu
/*	$(".sidebar-menu a ").click(function () {
	   $(".fa-plus",this).toggleClass("fa-minus");
	    if (!$(".fa-plus").is(":visible")) {
	        $('.fa-plus').toggleClass('fa-minus');      
	    }
	})
*/

/*$('.treeview').click(function () {
    //var _this = $(this);
    //alert("hello");
    if($(this).hasClass('active')) {
      $(this).removeClass('active');
      $(this).children().find('treeview-menu').css({"display":"none"});
    } else {
      $(this).parent().addClass('active');
      $(this).children().find('treeview-menu').css({"display":"block"});
    }
  }) 
*/
/*$(".sidebar-menu .treeview a").click(function () {
	$(this).parent().addClass("active");
});*/
// $(".sidebar-menu a").click(function () {
//   	$(".fa-plus",this).toggleClass("fa-minus");
//     if (!$(".fa-plus").is(":visible")) {
//         $('.fa-plus').toggleClass('fa-minus');      
//     }    

//     $(".sidebar-menu").find(".active").removeClass("active");
//     $(".sidebar-menu").find("span i:first-child").removeClass("fa-minus");
//     $(".sidebar-menu").find(".treeview-menu").css({"display":"none"});
//     $(".sidebar-menu").find("span i:first-child").addClass("fa-plus");
//     //$(".treeview").children().find('treeview-menu').css({"display":"block"});
//     //$(".sidebar-menu").find("span i:first").addClass("fa-minus");
//     //$(this).find("span i:first-child").remClass("fa-minus");

//     $(this).parent().addClass("active");
//     $(this).nextAll("ul.treeview-menu").css({"display":"block"});
//     $(this).find("span i:first-child").addClass("fa-minus");


   // $(!this).parent().removeClass("active");

    /*if($(this).hasClass("fa-plus")) {
    	$(this,' li span i ').addClass('fa-minus');   
    }*/

  /*  $(".sidebar ").each(function(){
    	//alert("hello");

    	if($(this).closest("li").hasClass("active")) {        		
    		$(this).closest("ul li").find("span i:first").toggleClass("fa-minus"); 			
    	}
    	if(!$(this).closest("ul li").hasClass("active")) {    	 	
    		$(this).closest("ul li").find("span i:first").addClass("fa-plus"); 				
    	}
    	
    });*/  
/*     $(".sidebar ul li").each(function(){ 
     	$(!this).removeClass("active");
     });*/


// });

$(".sidebar-menu a").click(function () {
  	$(".fa-plus",this).toggleClass("fa-minus");
    if (!$(".fa-plus").is(":visible")) {
        $('.fa-plus').toggleClass('fa-minus');      
    }    
    //alert($(this).parent().attr("class"));
    $(".sidebar-menu a").parent().removeClass('active');
    $(this).parent().addClass('active');
    if($(this).find('.treeview-menu').hasClass('menu-open'))
    	$(this).find("span i:first").toggleClass("fa-minus"); 
    /*if($(this).hasClass("fa-plus")) {
    	$(this,' li span i ').addClass('fa-minus');   
    }*/

/*    $(".sidebar ul li").each(function(){
    	if($(this).hasClass("active")) {    		
    		$(this).find("span i:first").toggleClass("fa-minus"); 			
    	}
    	 if(!$(this).hasClass("active")) {    	 	
    		$(this).find("span i:first").addClass("fa-plus"); 				
    	}
    }); 
    $(".sidebar-menu ul").each(function(){
    	if($(this).hasClass("active")) {    		
    		$(this).find("span i:first").toggleClass("fa-minus"); 			
    	}
    	 if(!$(this).hasClass("active")) {    	 	
    		$(this).find("span i:first").addClass("fa-plus"); 				
    	}
    }); */
});


/*$(".sidebar ul li").on("click",function(){ 
	$(this).addClass("removeActiveClass");	
});*/
});


