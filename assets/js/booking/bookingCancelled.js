function clearSearchRide() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  loadBookingCancelledRideList();
}

function clearSearchCare()
{
  $("input[type=search]").val("");
  $("#clear").remove();
  loadBookingCancelledCare();  
}

function clearSearchTutor()
{
  $("input[type=search]").val("");
  $("#clear").remove();
  loadBookingCancelledTutor();    
}

function loadBookingCancelledRideList()
{
  var bookingCancelledRideList = $('#bookingCancelledRideList').DataTable();
  $('#bookingCancelledRideList').empty();
  bookingCancelledRideList.destroy();
  $('#bookingCancelledRideList').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [],
    // Load data for the table's content from an Ajax source
      "ajax": {
        "url": baseURL+"booking_management/Booking_cancelled/rideList",
        beforeSend: function() {
           // $("#LoadingDiv").css({"display":"block"});
          var search = $("input[type=search]").val();
          if(search=="")
           $("input[type=search]").on("keyup",function(event) {

            if($("#clear").length == 0) {
               if($(this).val() != ""){
                $("#bookingCancelledRideList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearchRide()"><i class="fa fa-times-circle"></i></button></div>');    
              } 
            }
            if($(this).val() == "")  
            $("#clear").remove();      
          }); 
          $("input[type=search]").keydown(function(event) {
            k = event.which;
            if (k === 32 && !this.value.length)
                event.preventDefault();
          });
         },
        complete: function(){
       $("#LoadingDiv").css({"display":"none"}); 
        $("#bookingCancelledRideList").css({"opacity":"1"});
      },
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 0, 7 ], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  }); 
}

function loadBookingCancelledCare()
{
  var bookingCancelledCareList = $('#bookingCancelledCareList').DataTable();
  $('#bookingCancelledCareList').empty();
  bookingCancelledCareList.destroy();
  $('#bookingCancelledCareList').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [],
    // Load data for the table's content from an Ajax source
      "ajax": {
        "url": baseURL+"booking_management/Booking_cancelled/careList",
        beforeSend: function() {
           // $("#LoadingDiv").css({"display":"block"});
          var search = $("input[type=search]").val();
          if(search=="")
           $("input[type=search]").on("keyup",function(event) {

            if($("#clear").length == 0) {
               if($(this).val() != ""){
                $("#bookingCancelledCareList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearchCare()"><i class="fa fa-times-circle"></i></button></div>');    
              } 
            }
            if($(this).val() == "")  
            $("#clear").remove();      
          }); 
          $("input[type=search]").keydown(function(event) {
            k = event.which;
            if (k === 32 && !this.value.length)
                event.preventDefault();
          });
         },
        complete: function(){        
      },
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 0,7 ], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  }); 
}

function loadBookingCancelledTutor()
{
  var bookingCancelledTutorList = $('#bookingCancelledTutorList').DataTable();
  $('#bookingCancelledTutorList').empty();
  bookingCancelledTutorList.destroy();
  $('#bookingCancelledTutorList').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [],
    // Load data for the table's content from an Ajax source
      "ajax": {
        "url": baseURL+"booking_management/Booking_cancelled/tutorList",
        beforeSend: function() {
           // $("#LoadingDiv").css({"display":"block"});
          var search = $("input[type=search]").val();
          if(search=="")
           $("input[type=search]").on("keyup",function(event) {

            if($("#clear").length == 0) {
               if($(this).val() != ""){
                $("#bookingCancelledTutorList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearchTutor()"><i class="fa fa-times-circle"></i></button></div>');    
              } 
            }
            if($(this).val() == "")  
            $("#clear").remove();      
          }); 
          $("input[type=search]").keydown(function(event) {
            k = event.which;
            if (k === 32 && !this.value.length)
                event.preventDefault();
          });
         },
        complete: function(){        
      },
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 0,6 ], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  }); 
}

function viewBookingScheduleData($this,type)
{
  if($($this).attr("data-row-id")){
  $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"booking_management/Booking_cancelled/viewBookingInfo",
      beforeSend: function() {
      },
      complete: function(){      
    },
      data: {"key":$($this).attr("data-row-id"),"type":type},
  }).success(function (json) {
    if(json.status == "success"){   
      $("#ModalLabel").html('Booking '+json.type+' Cancelled Details<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $(".inner_body").html(json.data);
      $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
      $('.commanPopup').modal('show');  
    } else {
          toastr.error(json.msg,"Error:");
      }
    });
  } else {
    toastr.error("Something went wrong!!!.");    
  }
}

$(document).ready(function(){  
  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#bookingCancelledRideList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearchRide()"><i class="fa fa-times-circle"></i></button></div>');    
        $("#bookingCancelledCareList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearchCare()"><i class="fa fa-times-circle"></i></button></div>');    
        $("#bookingCancelledTutorList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearchTutor()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
      event.preventDefault();
  });
});