function deleteRating($this)
{
  if($($this).attr("data-row-id")){
    var kay= "'"+ $($this).attr("data-row-id") +"'";
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure you want to delete this rating?</b>');
    $(".inner_footer").html('<button type="button" onclick="delRating('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');       
    
  } else {
    toastr.error("Something went wrong!!!.");              
  }
}

function delRating(key){ 
  $.ajax({
    type: "POST",
    dataType: "json",
    beforeSend: function() {
    },
    complete: function(){  
    },
    url: baseURL+"general_management/Rating_controller/deleteRating",
    data: {"key":key},
  }).success(function (json) {
    if(json.status == "success"){
      toastr.success(json.msg); // success message
      $("#table").dataTable().fnDraw();
    } else if(json.status == "warning") {
      toastr.warning(json.msg); // error message
    } else{
      toastr.error(json.msg); // error message
    }
});
$('.commanPopup').modal('hide');    
}

function ratingList()
{
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.    
    "ajax": {
      "url": baseURL+"general_management/Rating_controller/ajax_list",
      beforeSend: function() {
      var search = $("input[type=search]").val();
      if(search=="")             
      $("input[type=search]").on("keyup",function(event) {
        if($("#clear").length == 0) {
            if($(this).val() != ""){
            $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
          } 
        }
        if($(this).val() == "")  
        $("#clear").remove();      
      }); 
      $("input[type=search]").keydown(function(event) {
        k = event.which;
        if(k === 32 && !this.value.length)
          event.preventDefault();
      });
    },
    complete: function(){
      $("#LoadingDiv").css({"display":"none"}); 
    },
    "type": "POST"
    },
    
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
      "targets": [ 0,3 ], //first column / numbering column
      "orderable": false, //set not orderable
    },
    ],
  });  
} 

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();  
  ratingList();
}

function viewFAQ($this) // view of faq
{
  if($($this).attr("data-row-id")){            
    $( "h3" ).html( "<h3 class='modal-title'><b>View FAQ</b><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></h3>" );
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"general_management/Rating_controller/viewFAQ",
      beforeSend: function() {
      },
      complete: function(){      
      },
      data: {"key":$($this).attr("data-row-id"),"role_name":$($this).data('role-name')},
      }).success(function (json) {       
      if(json.status == "success") {
        $("#viewTitle").text(json.faqData.faq_title);
        $("#viewSubTitle").text(json.faqData.faq_sub_title);
        $("#viewDescription").text(json.faqData.faq_description); 
        $("#viewRole").text(json.role_name);
        $('#viewFaqModal').modal('show', {backdrop: 'static'});
      } else {
        toastr.error(json.msg); 
      }
    });
  }else{
    toastr.error("Something went wrong!!!.");         
  }
} 

function viewDesc($this) // view of faq
{
  if($($this).attr("data-row-id")){            
    $( "h3" ).html( "<h3 class='modal-title'><b>View FAQ Description</b><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></h3>" );
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"general_management/Rating_controller/viewFAQ",
      beforeSend: function() {      
      },
      complete: function(){        
      },
      data: {"key":$($this).attr("data-row-id"),"role_name":$($this).data('role-name')},
      }).success(function (json) {        
      if(json.status == "success"){
        $("#viewDescription1").text(json.faqData.faq_description); 
        $('#viewFaqModal1').modal('show', {backdrop: 'static'});
      } else {
        toastr.error(json.msg); 
      }
    });
  } else {
    toastr.error("Something went wrong!!!.");         
  }
} 

function editRating($this) // edit Rating list data
{  
  $('#ratingForm')[0].reset(); //reset of Rating list value 
  $(".title").remove();
  $(".userRole").remove();  
  $( "h3" ).html( "<h3 class='modal-title'><b>Edit Rating title</b></h3>" );
  if($($this).attr("data-row-id")){ // get Rating list id 
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"general_management/Rating_controller/viewRating",
      beforeSend: function() {      
      },
      complete: function() {      
      },
      data: {"key":$($this).attr("data-row-id"),"role_name":$($this).data('role-name')},
      }).success(function (json) {
      if(json.status == "success"){            
        $("#user_key").val($($this).attr("data-row-id"));
        $('#userRole').val(json.ratingData.role_id).attr('selected','selected');         
        $("#title").val(json.ratingData.rating_type);        
        $('#ratingModal').modal('show', {backdrop: 'static'});        
      } else {
        toastr.error(json.msg);
      }
    });
  } else {
    $("#user_key").val('');
    $('#ratingModal').modal('show', {backdrop: 'static'});
  }
}

$(document).ready(function(){
  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
      if($(this).val() != ""){
      $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
  k = event.which;
  if (k === 32 && !this.value.length)
    event.preventDefault();
  });

  $('#ratingForm')[0].reset();
  $('#btnRating').click(function(){ // save record of faq list  
    var title = $("#title").val();
    var user_key = $("#user_key").val();
    var userRole = $("#userRole").val();
    var flag = 0;
    if(userRole == ''){
      $(".userRole").remove();  
      $("#userRole").parent().append("<div class='userRole' style='color:red;'>Please select role.</div>");
      flag = 1;
    }else{
      $(".userRole").remove();          
    }

    if(title == ''){
      $(".title").remove();  
      $("#title").parent().append("<div class='title' style='color:red;'>Title field is required.</div>");
      flag = 1;
    }else{
      $(".title").remove();          
    }

    if(flag == 1)
      return false;    
    else
    {      
      $.ajax({
        type:"POST",
        url:baseURL+"general_management/Rating_controller/addRating",            
        data:{"editData":user_key, "title":title,"userRole":userRole},
        dataType:"json",
        async:false,
        success:function(response){                     
          if(response.status == "success")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#ratingModal").modal('hide');
            $("#table").dataTable().fnDraw();
            toastr.success(response.message);                                      
          } else {
            $("#LoadingDiv").css({"display":"none"});
            $("#ratingModal").modal('show');
            toastr.error(response.message);                                       
          }
        }
      });    
    }   
  });  

  $('#addPopUp').on("click",function(){
    $( "h3" ).html( "<h3 class='modal-title'><b>Add Rating Title</b></h3>" );
    $(".title").remove();    
    $(".userRole").remove();    
    $("#user_key").val("");
    $("#ratingForm")[0].reset();
    $("#ratingModal").modal('show');    
  });
});