<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class my404 extends CI_Controller{
    function __construct() {
        parent::__construct();
    }
    
    function index(){
    	  $data["main_content"] = '404'; 
        $this->load->view('error404', $data);
    }
}
?>
