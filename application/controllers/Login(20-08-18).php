<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Login extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->load->model('login_model');
        $this->load->library("email"); 
        $this->load->helper("email_template"); 
        $this->load->helper('string');

    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->isLoggedIn();
    }
    
    /**
     * This function used to check the user is logged in or not
     */
    function isLoggedIn()
    {
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            $this->load->view('login');
        }
        else
        {
            redirect('/dashboard');
        }
    }
    
    
    /**
     * This function used to logged in user
     */
    public function loginMe()
    {
        // print_r( 'hii' );exit;
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|xss_clean|trim');
        $this->form_validation->set_rules('password', 'Password', 'required');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            
            $result = $this->login_model->loginMe($email, $password);
          
            if(count($result) > 0)
            {
                foreach ($result as $res)
                {
                    $sessionArray = array('userId'=>$res->user_id,                    
                                            'role'=>$res->roleId,
                                            'roleText'=>$res->role,
                                            'name'=>$res->user_name,
                                            'isLoggedIn' => TRUE
                                    );
                                    
                    $this->session->set_userdata($sessionArray);
                    
                    redirect('/dashboard');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                
                redirect('/login');
            }
        }
    }

    public function sessionExpire()
    {
        $dataExistCheck = $this->session->userdata("userId");    
        if(empty($dataExistCheck)) {
            echo(json_encode(array('status'=>'logout')));
        } else {
            echo(json_encode(array('status'=>'continue')));
        }
    }

    /**
     * This function used to load forgot password view
     */
    public function forgotPassword()
    {
        $this->load->view('forgotPassword');
    }
    
    /**
     * This function used to generate reset password request link
     */
     public function resetPasswordUser()
    {   
        $status = '';
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('login_email','Email','trim|required|valid_email|xss_clean');
                
        if($this->form_validation->run() == FALSE)
        {
            $this->forgotPassword();
        }
        else 
        {
            $email = $this->input->post('login_email');
            $data = $this->Common_model->select('user_id,user_email,user_status',TB_USERS,array('user_email'=>$email));
           // print_r($data);exit;
           if(empty($data))
            {
               $status = 'invalid';
                setFlashData($status, "This email is not registered with us.");
            }
            else if($data[0]['user_status']=='0' || $data[0]['user_status']==0)
            {   
                $status = 'invalid';
                setFlashData($status, "Your account is deactive, please cantact to admin.");                
            }        
            else
            {           
                //$encoded_email = urlencode($email);    
                //$userId = base64_encode($data[0]['user_id']);
                $userId =$data[0]['user_id'];
                $reset_token = random_string('unique');
                $updateData = array('token' =>$reset_token);
                $where = array('user_id' =>$data[0]['user_id'] ); 
                $result = $this->Common_model->update(TB_USERS,$where,$updateData); 
                $reset_url = base_url() . "resetPasswordConfirmUser/" . $userId . "/" . $reset_token;
                $hostname = $this->config->item('hostname');
                $config['mailtype'] ='html';
                $config['charset'] ='iso-8859-1';
                $this->email->initialize($config);
                $from  = EMAIL_FROM; 
                $this->messageBody  = email_header();
                $this->messageBody  .= '<tr> 
                    <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                        <p>Hello,</p>
                            <p>
                            A request has been made to reset your password for Tulii account.<br>
                            Please click the link below to reset your password.</p>
                            <p>
                            <p>Reset Password Link : </p>
                            <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href="'.$reset_url.'">Reset Password</a>
                            </p>
                        <p>
                            If the above link does not work, please copy and paste the following link into your browser.</p>
                            <p>Copy link :</p>
                            <a href="'.$reset_url.'">'.$reset_url.'</a>
                            
                        </td>
                    </tr>
                    ';                      
                $this->messageBody  .= email_footer();
                $this->email->from($from, $from);
                $this->email->to($email);
                $this->email->subject('Reset Password Link');
                $this->email->message($this->messageBody);
                $send = $this->email->send();
                //echo "successfully".$send;exit;
                if($send) 
                {
                    $status = "send";
                    setFlashData($status, "Reset password link sent successfully, please check mails.");
                }       
                else
                {
                    $status = 'notsend';
                    setFlashData($status, "Email has been failed to send, try again.");                
                    
                }
            }
            redirect('/forgotPassword');
        }       
    }

    // This function used to reset the password 
    public function resetPasswordConfirmUser($id,$token)
    {
        $userdata = $this->Common_model->select("user_id,user_email,user_status",TB_USERS,array("user_id"=>trim($id),"token"=>trim($token)));
        $data['mydata']=array('token'=>$token,'user_id'=>$id);   
        if(count($userdata) > 0 )
        {
            // Clear token for one time user change password 
            $datareset = array('token' =>'');
            $cond1 = array('user_id' =>$id); 
            $result = $this->Common_model->update(TB_USERS,$cond1,$datareset);          
            $this->load->view("newPassword",$data);
        }
        else
        {
            $status = 'notsend';
            setFlashData($status, "You reset password link has been expired, already use!");                
            redirect("forgotPassword");
        }
    }

    // Update token
    /*function update_token()
    {
        $ftoken=$this->input->post('token');
        $fid = $this->input->post('user_id');
        $datareset1 = array('token' =>$ftoken);
        $cond1 = array('user_id' =>$fid); 
        $result = $this->Common_model->update(TB_USERS,$cond1,$datareset1);
    }*/

    // Change user password 
    public function createPasswordUser()
    {
        $postData = $this->input->post();
        $data['mydata']=array('token'=>$postData['token'],'user_id'=>$postData['user_id']);
        if(empty($postData['token']) && empty($postData['user_id'])){
            redirect('login');
        } else {               
            $datareset = array('user_password' =>  getHashedPassword($postData['password']));
            $cond1 = array('user_id' =>  $postData['user_id']); 
            // Chnage password successfully
            $result = $this->Common_model->update(TB_USERS,$cond1,$datareset);                
            if($result)
            {
                $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status'=>true,'message' => 'Change password has been successfully.', 'redirect'=>base_url('login'))));
            }
            else
            {  
                $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status'=>false,'message' => 'Error occured,Please try again.', 'redirect'=>base_url('login'))));
                                       
            }                  
        }            
    }
}

?>