<?php
/*
* @author : kiran N.
* description: manage the payment history data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Payment_history extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Payament history management', 
            'isActive'  => 'active'
        );
        $this->load->model('PaymentHistory','Payment_history');
        $this->load->library('M_pdf');
        $this->load->helper('download');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');  
            $this->load->view('includes/header',$this->data);
            $this->load->view('payment_management/paymentHistory',$this->data);
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of FAQ list data
    {
        if(is_user_logged_in()){            
            $paymentData = $this->Payment_history->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;

            foreach ($paymentData as $payValue) {
                $payID = $payValue->payment_id;
                $base_64 = base64_encode($payID);
                $pid = rtrim($base_64, '=');
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($payValue->user_name);
                $row[] = $payValue->booking_pick_up_location;
                $row[] = $payValue->booking_drop_off_location;
                $row[] = $payValue->booking_start_date;                               
                $row[] = "$".$payValue->booking_price;                
                $row[] = $payValue->user_rating;
                $row[] = ucfirst($payValue->status);
                $row[] ='   
                            <a data-id="'.$i.'" data-row-id="'.$pid.'" onclick="generateInvoice(this)" class="btn btn-sm btn-info"  href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" target="" data-original-title="Generate invoice">Generate invoice                                
                            </a>                           
                        ';
           
                $data[] = $row;             
                $i++;
            }
     
            $output = array(

                "draw" => $_POST['draw'],
                "recordsTotal" => $this->Payment_history->count_all(),
                "recordsFiltered" => $this->Payment_history->count_filtered(),
                "data" => $data,

            );

            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }
    public function invoiceGenerate()
    {
        $base_64 = $this->uri->segment(4) . str_repeat('=', strlen($this->uri->segment(4)) % 4);
        $payId = base64_decode($base_64);
        $data = '';
        if(!empty($payId)) {
            //echo $this->encrypt->decode($postData["payId"]);exit;
            $cond = array(TB_BOOKINGS.'.booking_status' => COMPLETED, TB_USERS.".isDeleted" => 0, TB_PAYMENT.".status" => "paid", TB_PAYMENT.".id" => $payId);
            $jointype=array(TB_BOOKINGS => "LEFT", TB_USERS => "LEFT", TB_SERVICE_REQUESTS => "LEFT");
            $join = array(TB_BOOKINGS=>TB_BOOKINGS.".booking_id = ".TB_PAYMENT.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_PAYMENT.".user_id", TB_SERVICE_REQUESTS => TB_SERVICE_REQUESTS.'.service_booking_id = '.TB_BOOKINGS.'.booking_id');
            $paymentData = $this->Common_model->selectJoin("id as payment_id, CONCAT( 'T-', LPAD(id,7,'0') ) as invoice_no,service_provider_user_id, user_name, booking_pick_up_location, booking_drop_off_location, tbl_payment.updated_at,booking_start_date,booking_pick_up_time,booking_drop_off_time,order_id,user_email,amount, user_address,booking_price, status, user_rating",TB_PAYMENT,$cond,array(),$join,$jointype);
            //print_r($paymentData);exit;
            $data['user_name'] = !empty($paymentData[0]['user_name']) ?$paymentData[0]['user_name']:"";
            $data['booking_pick_up_location'] = !empty($paymentData[0]['booking_pick_up_location']) ?$paymentData[0]['booking_pick_up_location']:"N/A";
            $data['booking_drop_off_location'] = !empty($paymentData[0]['booking_drop_off_location']) ?$paymentData[0]['booking_drop_off_location']:"N/A";
            $data['booking_start_date'] = !empty($paymentData[0]['booking_start_date']) ?$paymentData[0]['booking_start_date']:"N/A";
            $data['booking_price'] = !empty($paymentData[0]['booking_price']) ?$paymentData[0]['booking_price']:"N/A";
            $data['status'] = !empty($paymentData[0]['status']) ?$paymentData[0]['status']:"N/A";
            $data['booking_pick_up_time'] = !empty($paymentData[0]['booking_pick_up_time']) ?$paymentData[0]['booking_pick_up_time']:"N/A";
            $data['booking_drop_off_time'] = !empty($paymentData[0]['booking_drop_off_time']) ?$paymentData[0]['booking_drop_off_time']:"N/A";
            $data['order_id'] = !empty($paymentData[0]['order_id']) ?$paymentData[0]['order_id']:"N/A";
            $data['user_address'] = !empty($paymentData[0]['user_address']) ?$paymentData[0]['user_address']:"N/A";
            $data['user_email'] = !empty($paymentData[0]['user_email']) ?$paymentData[0]['user_email']:"N/A";
            $data['invoice_no'] = !empty($paymentData[0]['invoice_no']) ?$paymentData[0]['invoice_no']:"N/A";
            $data['updated_at'] = !empty($paymentData[0]['updated_at']) ?$paymentData[0]['updated_at']:"N/A";
            $data['amount'] = !empty($paymentData[0]['amount']) ?$paymentData[0]['amount']:"N/A";
            // echo "<pre>";print_r($data);die;
            $data['bookingDT'] = $data['booking_start_date']." / ".$data['booking_pick_up_time'] ;
            $data['quantity'] = '1';
            $data['description'] = "From location: ".$data['booking_pick_up_location']." To location: ".$data['booking_drop_off_location'];
            $html = $this->load->view('payment_management/invoice', $data, true);
            // echo "<pre>";print_r($html);die;
                    // create some HTML content
            // $html = pdfGenerator($user_name,$booking_start_date." / ".$booking_pick_up_time,$order_id,$user_address,$user_email,$invoice_no,$updated_at,$amount,"From location :".$booking_pick_up_location." To location :".$booking_drop_off_location,$amount,1,$amount);
            // echo "<pre>";print_r($html);die;
            // $html .= '<b>Tulii project</b>';


        
           // print_r($html);exit;
                // $title = 'Tulii invoice of trip';
                // $subject = 'Invoice'; 
                // $filename = "Invoice_".date("Y_m_d_H_i_s").".pdf";
                
                // invoiceGenerateInPDF($html, $title, $subject, $filename, $invoice_no);

            $pdfFilePath = "Invoice_".date("Y_m_d_H_i_s").".pdf";
            $this->m_pdf->pdf->WriteHTML($html);
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "I");
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please provide payment id.")); exit;   
                }          
    }
}