<?php
/*
* @author : kiran N.
* description: manage the payment data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Payment_section extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->load->helper("cias"); 
        $this->data = array(
            'pageTitle' => 'Tulii : Payament section management', 
            'isActive'  => 'active'
        );
        $this->load->model('PaymentSection_model','payment_section');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');  
            $this->load->view('includes/header',$this->data);
            $this->load->view('payment_management/paymentSection',$this->data);
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of FAQ list data
    {
        if(is_user_logged_in()){            
            $paymentData = $this->payment_section->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;

            foreach ($paymentData as $payValue) {
                $payID = $this->encrypt->encode($payValue->payment_id);
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($payValue->user_name);
                $row[] = $payValue->booking_pick_up_location;
                $row[] = $payValue->booking_drop_off_location;
                $row[] = $payValue->booking_start_date;                               
                $row[] = "$".$payValue->booking_price;
                $row[] = ucfirst($payValue->status);
                $row[] = $payValue->user_rating;
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$payID.'" class="btn btn-sm btn-info" onclick="viewPayment(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>                
                            <a data-id="'.$i.'" data-row-id="'.$payID.'" class="btn btn-sm btn-info" onclick="payNow(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pay now">Pay Now                                
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$payID.'" class="btn btn-sm btn-danger" onclick="payOnHold(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="On hold">On-hold                                
                            </a>                           
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(

                "draw" => $_POST['draw'],
                "recordsTotal" => $this->payment_section->count_all(),
                "recordsFiltered" => $this->payment_section->count_filtered(),
                "data" => $data,

            );

            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function payToServiceProvider() // payment to service provider
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {                
                $postData = $this->input->post();  

                if(!empty($postData["payId"])) {
                    require_once APPPATH.'third_party/stripe/init.php';
                    \Stripe\Stripe::setApiKey('sk_test_pCPNKY3zT06WVxwqH5VBvETE');
                    // Get all data trip & parent details
                    $cond    = array("id" => $this->encrypt->decode($postData["payId"]));      
                    $join    = array(TB_USERS => TB_USERS.".user_id = ".TB_PAYMENT.".user_id", TB_BOOKINGS => TB_BOOKINGS.".booking_id = ".TB_PAYMENT.".booking_id");
                    $resultUserDetails = $this->Common_model->select_join("amount, user_name, roleId, user_device_token, booking_start_date, booking_pick_up_location, booking_drop_off_location, booking_pick_up_time, booking_drop_off_time, user_rating, customer_id", TB_PAYMENT, $cond, array(), array(), $join);                 
                    $amount = !empty($resultUserDetails[0]['amount']) ? $resultUserDetails[0]['amount'] :"";
                    $accountID = !empty($resultUserDetails[0]['customer_id']) ? $resultUserDetails[0]['customer_id'] :"";
                    $roleId = !empty($resultUserDetails[0]['roleId']) ? $resultUserDetails[0]['roleId'] :"";
                    $user_name = !empty($resultUserDetails[0]['user_name']) ? $resultUserDetails[0]['user_name'] :"";
                    $user_device_token = !empty($resultUserDetails[0]['user_device_token']) ? $resultUserDetails[0]['user_device_token'] :"";
                    $booking_start_date = !empty($resultUserDetails[0]['booking_start_date']) ? $resultUserDetails[0]['booking_start_date'] :"";
                    $booking_pick_up_location = !empty($resultUserDetails[0]['booking_pick_up_location']) ? $resultUserDetails[0]['booking_pick_up_location'] :"";
                    $booking_drop_off_location = !empty($resultUserDetails[0]['booking_drop_off_location']) ? $resultUserDetails[0]['booking_drop_off_location'] :"";
                    $booking_pick_up_time = !empty($resultUserDetails[0]['booking_pick_up_time']) ? $resultUserDetails[0]['booking_pick_up_time'] :"";
                    $booking_drop_off_time = !empty($resultUserDetails[0]['booking_drop_off_time']) ? $resultUserDetails[0]['booking_drop_off_time'] :"";
                    $user_rating = !empty($resultUserDetails[0]['user_rating']) ? $resultUserDetails[0]['user_rating'] :"";
                    try {
                    // transfer amount to service provider account
                    $transfer = \Stripe\Transfer::create(array(
                    "amount" => $amount*100,
                    "currency" => "usd",
                    "destination" => $accountID
                    ));
                    $message   = "Hi ".$user_name; 
                    $myData    = array("message" => "Trip booking details <br /> Trip date : '".$booking_start_date."' <br /> Pick-up location : '".$booking_pick_up_location."'<br /> Drop-off location :'".$booking_drop_off_location."'<br /> Pick-up time :'".$booking_pick_up_time."'<br /> Drop-off time :'".$booking_drop_off_time."'<br /> User rating :'".$user_rating."'<br /> Amount :'".$amount."'<br /> Payment status : Paid");
                    sendPushNotification($user_device_token, $message, $myData, $roleId);
                    $paidPayment = $this->Common_model->update(TB_PAYMENT, array('id' => $this->encrypt->decode($postData['payId'])), array("status" => "paid", "updated_at" => date("Y-m-d H:i:s")));
                    if($paidPayment){                                                
                        echo json_encode(array("status"=>"success","action"=>"pay","msg"=>"Payment has been paid successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"pay","msg"=>"Please try again.")); exit; 
                    }
                   

                    } catch(\Stripe\Error\Card $e) {
                        // Since it's a decline, \Stripe\Error\Card will be caught
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        echo json_encode(array("status"=>"error","action"=>"pay","msg"=>$err['message'])); exit;  
                    } catch (\Stripe\Error\RateLimit $e) {
                        // Too many requests made to the API too quickly
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        echo json_encode(array("status"=>"error","action"=>"pay","msg"=>$err['message'])); exit;  
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        // Invalid parameters were supplied to Stripe's API
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        echo json_encode(array("status"=>"error","action"=>"pay","msg"=>$err['message'])); exit;  
                    } catch (\Stripe\Error\Authentication $e) {
                        // Authentication with Stripe's API failed
                        // (maybe you changed API keys recently)
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        echo json_encode(array("status"=>"error","action"=>"pay","msg"=>$err['message'])); exit;  
                    } catch (\Stripe\Error\ApiConnection $e) {
                        // Network communication with Stripe failed
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        echo json_encode(array("status"=>"error","action"=>"pay","msg"=>$err['message'])); exit;  
                    } catch (\Stripe\Error\Base $e) {
                        // Display a very generic error to the user, and maybe send
                        // yourself an email
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        echo json_encode(array("status"=>"error","action"=>"pay","msg"=>$err['message'])); exit;  
                    } catch (Exception $e) {
                        // Something else happened, completely unrelated to Stripe
                        $body = $e->getJsonBody();
                        $err = $body['error'];
                        echo json_encode(array("status"=>"error","action"=>"pay","msg"=>$err['message'])); exit;  
                    }                   
                } else {
                    echo json_encode(array("status"=>"error","action"=>"pay","msg"=>"Please provider payment id.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function paymentOnHoldOfSP() // payment to service provider
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if(!empty($postData["id"])) {
                    $onHoldPayment = $this->Common_model->update(TB_PAYMENT, array('id' => $this->encrypt->decode($postData['id'])), array("status" => "onhold", "on_hold_reason" => $postData['reason']));
                    if($onHoldPayment){                                                
                        echo json_encode(array("status"=>"success","action"=>"pay","msg"=>"Payment has been on-hold successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"pay","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"pay","msg"=>"Please provider payment id.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewPaymentList() // view data of payment  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if(!empty($postData["key"])) {

                    $cond = array(TB_BOOKINGS.'.booking_status' => COMPLETED, TB_USERS.".isDeleted" => '0', TB_PAYMENT.".status" => "pending", TB_PAYMENT.".id" => $this->encrypt->decode($postData["key"]));
                    $jointype=array(TB_BOOKINGS => "LEFT", TB_USERS => "LEFT", TB_SERVICE_REQUESTS => "LEFT");
                    $join = array(TB_BOOKINGS=>TB_BOOKINGS.".booking_id = ".TB_PAYMENT.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_PAYMENT.".user_id", TB_SERVICE_REQUESTS => TB_SERVICE_REQUESTS.'.service_booking_id = '.TB_BOOKINGS.'.booking_id');
                    $paymentData = $this->Common_model->selectJoin("id as payment_id, service_provider_user_id, user_name, booking_pick_up_location, booking_drop_off_location, booking_start_date, booking_pick_up_time, booking_drop_off_time, booking_price, status, user_rating",TB_PAYMENT,$cond,array(),$join,$jointype);
                               
                    if($paymentData) {                                                
                        echo json_encode(array("status"=>"success","action"=>"view","payData"=>$paymentData[0])); exit; 
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please provide payment id.")); exit;   
                }
            }
            else{
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 
}