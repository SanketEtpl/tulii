<?php
/*
* @author : kiran N.
* description: manage the faq data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Faq_controller extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : FAQ management', 
            'isActive'  => 'active'
        );
        $this->load->model('Faq_model','faq');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');  
            $this->data['roleData'] = $this->Common_model->select('*',TB_ROLES,array("roleID !="=>ROLE_ADMIN,"roleId !="=>ROLE_PARENTS));      
            $this->data['categories'] = $this->Common_model->select('category,category_id',TB_CATEGORIES,array("status"=>1));                          
            $this->load->view('includes/header',$this->data);
            $this->load->view('faq_management/faq',$this->data);
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    //get categories
    public function get_category()
    {       
        $postData = $this->input->post();
        $dataCategory = $this->Common_model->select("category,category_id",TB_CATEGORIES,array("status"=>1,"role_id"=>$postData['roleID']));
        $categoryJSON='';
        // if record are exist
        if(count($dataCategory) > 0)
        {
            $categoryJSON .= '<option value="" disable="" selected="" hidden="">-- Select category --</option>';
            foreach ($dataCategory as $key => $name) {
                $categoryJSON.= '<option value="'.$name['category_id'].'">'.$name['category'].'</option>';
            } 
            $status ='true';             
        }
        else
        {
            $categoryJSON.= '<option value="">-- Category not available --</option>';
            $status ='false';
        }
        // convert in json format
        echo json_encode(array('category'=>$categoryJSON,"status"=>$status));       
    }

        //get categories
    public function get_subcategory()
    {       
        $postData = $this->input->post();
        $dataCategory = $this->Common_model->select("sub_category_id,name",TB_SUBCATEGORIES,array("status"=>1,"category_id"=>$postData['categoryID']));
        $categoryJSON='';
        //print_r($dataCategory);exit;
        // if record are exist
        if(count($dataCategory) > 0)
        {
            $categoryJSON .= '<option value="" disable="" selected="" hidden="">-- Select sub category --</option>';
            foreach ($dataCategory as $key => $name) {
                $categoryJSON.= '<option value="'.$name['sub_category_id'].'">'.$name['name'].'</option>';
            }     
            $status ='true';          
        }
        else
        {
            $categoryJSON.= '<option value="">-- Category not available --</option>';
            $status ='false';
        }
        // convert in json format
        echo json_encode(array('status'=>$status,'subCategory'=>$categoryJSON));       
    }

    public function ajax_list() // list of FAQ list data
    {
        if(is_user_logged_in()){            
            $faqData = $this->faq->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            foreach ($faqData as $faq) {
                $userId = $this->encrypt->encode($faq->id);
                $titleFaq = $subTitleFaq = $descFaq= $titleCut= $descCut="";
                if (strlen($faq->faq_title) > 100) {                                                    
                    $titleCut = substr($faq->faq_title, 0, 100);
                    $titleFaq = substr($titleCut, 0, strrpos($titleCut, ' ')).'...'; 
                } else {
                    $titleFaq = $faq->faq_title;
                }

               if (strlen($faq->faq_sub_title) > 100) {
                $titleSubCut = substr($faq->faq_sub_title, 0, 100);
                $subTitleFaq = substr($titleSubCut, 0, strrpos($titleSubCut, ' ')).'...'; 
               } else{
                $subTitleFaq = $faq->faq_sub_title;
            }                

                if (strlen($faq->faq_description) > 150) {                                                    
                    $descCut = substr($faq->faq_description, 0, 150);
                    $descFaq = substr($descCut, 0, strrpos($descCut, ' ')).'<a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-primary" onclick="viewDesc(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Read more">
                                <i class="fa fa-eye"> Read more</i>
                            </a>'; 
                } else {
                    $descFaq = $faq->faq_description;
                }
                
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $titleFaq;
                $row[] = $subTitleFaq;
                $row[] = $descFaq;
                $row[] = $faq->role;                               
                $row[] = $faq->category;
                $row[] = $faq->name;
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="viewFAQ(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>                
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editFAQ(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger" onclick="deleteFaq(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->faq->count_all(),
                            "recordsFiltered" => $this->faq->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function deleteFAQ() // delete record of FAQ 
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->delete(TB_FAQ,array('id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"FAQ record has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    /*public function viewFAQ() // view data of FAQ  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $cond = array(TB_CATEGORIES.'.status'=>1,TB_SUBCATEGORIES.'.status'=>1,TB_FAQ.'.id'=>$this->encrypt->decode($postData['key']));
                    $jointype=array(TB_CATEGORIES=>"LEFT",TB_SUBCATEGORIES=>"LEFT");
                    $join = array(TB_CATEGORIES=>TB_CATEGORIES.".category_id = ".TB_FAQ.".category_id",TB_SUBCATEGORIES=>TB_SUBCATEGORIES.".sub_category_id = ".TB_FAQ.".subcategory_id");
                    $faqData = $this->Common_model->selectJoin(TB_FAQ.".category_id,".TB_FAQ.".subcategory_id,name,category,faq_description,faq_title,faq_sub_title",TB_FAQ,$cond,array(),$join,$jointype);
                    $dataSubCategory = $this->Common_model->select("sub_category_id,name",TB_SUBCATEGORIES,array('category_id'=>$faqData[0]['category_id']));
                    $subCategoryJSON='';
                    if($dataSubCategory > 0)
                    {
                        $subCategoryJSON .= '<option value="">Select sub category</option>';
                        foreach ($dataSubCategory as $key => $category) {
                            $subCategoryJSON.= '<option value="'.$category['sub_category_id'].'">'.$category['name'].'</option>';
                        }               
                    }
                    else
                    {
                        $subCategoryJSON.= '<option value="">Sub category not available</option>';
                    }                 
                    if($faqData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","faqData"=>$faqData[0],"subCategoryJSON"=>$subCategoryJSON)); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            }
            else{
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } */
    public function viewFAQ() // view data of FAQ  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $cond = array(TB_FAQ.'.id'=>$this->encrypt->decode($postData['key']));
                    $jointype=array(TB_CATEGORIES=>"LEFT",TB_SUBCATEGORIES=>"LEFT",TB_ROLES=>"LEFT");
                    $join = array(TB_CATEGORIES=>TB_CATEGORIES.".category_id = ".TB_FAQ.".category_id",TB_SUBCATEGORIES=>TB_SUBCATEGORIES.".sub_category_id = ".TB_FAQ.".subcategory_id",TB_ROLES=>TB_ROLES.'.roleId = '.TB_FAQ.'.role_id');
                    $faqData = $this->Common_model->selectJoin(TB_FAQ.".category_id,".TB_FAQ.".subcategory_id,tbl_faq.role_id,name,category,faq_description,faq_title,faq_sub_title,role",TB_FAQ,$cond,array(),$join,$jointype);
                    //echo $this->db->last_query();

                    $categoryJSON='';
                    $subCategoryJSON='';
                  //  print_r($faqData);die();
                   // if($faqData[0]['role_id'] == ROLE_CARE_DRIVER) {
                        $dataCategory = $this->Common_model->select("category,category_id",TB_CATEGORIES,array('role_id'=>$faqData[0]['role_id']));
                        if($dataCategory > 0) {
                            $categoryJSON .= '<option value="" disable="" selected="" hidden="">Select category</option>';
                            foreach ($dataCategory as $key => $category) {
                                $categoryJSON.= '<option value="'.$category['category_id'].'">'.$category['category'].'</option>';
                            }               
                        } else {
                            $categoryJSON.= '<option value="">Category not available</option>';
                        }  

                        $dataSubCategory = $this->Common_model->select("sub_category_id,name",TB_SUBCATEGORIES,array('category_id'=>$faqData[0]['category_id']));
                        if($dataSubCategory > 0) {
                            $subCategoryJSON .= '<option value="" disable="" selected="" hidden="">Select sub category</option>';
                            foreach ($dataSubCategory as $key => $category) {
                                $subCategoryJSON.= '<option value="'.$category['sub_category_id'].'">'.$category['name'].'</option>';
                            }               
                        } else {
                            $subCategoryJSON.= '<option value="">Sub category not available</option>';
                        }                 
                   // }
                    if($faqData) {                                                
                        echo json_encode(array("status"=>"success","action"=>"view","faqData"=>$faqData[0],"subCategoryJSON"=>$subCategoryJSON,"categoryJSON"=>$categoryJSON)); exit; 
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            }
            else{
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

    // add & update record of faq list
    public function addFAQ()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();
                if($postData['userRole'] == ROLE_TUTOR || $postData['userRole'] == SCHOOL || $postData['userRole'] == ORGANIZATION) {
                    $category = "";
                    $subCategory = "";
                } else {
                    $category = $postData['categories'];
                    $subCategory = $postData['subCategory'];
                }   
                if(!empty($postData["editData"])) { // update data
                    $updateArr = array(  
                        "faq_title" =>$postData['title'],
                        "faq_sub_title" =>$postData['sub_title'],
                        "faq_description" =>$postData['description'],
                        "category_id"=> $category,
                        "subcategory_id"=>$subCategory,
                        "role_id"=>$postData['userRole'],  
                        "update_on"=>date("Y-m-d H:s:i")  
                    );
                    $updateId = $this->Common_model->update(TB_FAQ,array('id'=>$this->encrypt->decode($postData['editData'])),$updateArr);
                    if($updateId){
                        echo json_encode(array("status"=>"success","action"=>"update","message"=>"FAQ has been updated successfully.")); exit;   
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                    }
                } else {       
                    if(!empty($postData)){ // add data               
                    $data = array(
                        "faq_title" =>$postData['title'],
                        "faq_sub_title" =>$postData['sub_title'],
                        "faq_description" =>$postData['description'],
                        "category_id"=> $category,
                        "subcategory_id"=>$subCategory,
                        "role_id"=>$postData['userRole'],                 
                        "created_on"=>date("Y-m-d H:s:i")           
                        );
                    $insertId = $this->Common_model->insert(TB_FAQ,$data);
                    if($insertId){
                        echo json_encode(array("status"=>"success","action"=>"insert","message"=>"FAQ has been added successfully.")); exit;    
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
                }
                else{
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the faq information.")); exit;    
                    }
                }                   
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 
}