<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class User extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->load->model('data_model');
        $this->load->library("email"); 
        $this->load->helper("email_template");         
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Tulii : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the user list
     */
    function userListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
           $this->load->model('data_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->data_model->userListingCount($searchText);

			$returns = $this->paginationCompress ( "userListing/", $count, 5 );
            
            $data['userRecords'] = $this->data_model->userListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'Tulii : User Listing';
            $this->loadViews("users", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('data_model');
            $data['roles'] = $this->data_model->getUserRoles();
            
            $this->global['pageTitle'] = 'Tuli : Add New User';

            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->data_model->checkEmailExists($email);
        } else {
            $result = $this->data_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('password','Password','required|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $name = ucwords(strtolower($this->input->post('fname')));
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->input->post('mobile');
                
                $userInfo = array('user_email'=>$email, 'user_password'=>getHashedPassword($password), 'roleId'=>$roleId, 'user_name'=> $name,
                                    'user_phone_number'=>$mobile, 'createdBy'=>$this->vendorId, 'created_at'=>date('Y-m-d H:i:s'));
                
                $this->load->model('data_model');
                $result = $this->data_model->addNewUser($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New User created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('addNew');
            }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('userListing');
            }
            
            $data['roles'] = $this->data_model->getUserRoles();
            $data['userInfo'] = $this->data_model->getUserInfo($userId);
            
            $this->global['pageTitle'] = 'Tulii : Edit User';
            
            $this->loadViews("editOld", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $userId = $this->input->post('userId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userId);
            }
            else
            {
                $name = ucwords(strtolower($this->input->post('fname')));
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->input->post('mobile');
                
                $userInfo = array();
                
                if(empty($password))
                {
                    $userInfo = array('user_email'=>$email, 'roleId'=>$roleId, 'user_name'=>$name,
                                    'user_phone_number'=>$mobile, 'updatedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s'));
                }
                else
                {
                    $userInfo = array('user_email'=>$email, 'user_password'=>getHashedPassword($password), 'roleId'=>$roleId,
                        'user_name'=>ucwords($name), 'user_phone_number'=>$mobile, 'updatedBy'=>$this->vendorId, 
                        'updated_at'=>date('Y-m-d H:i:s'));
                }
                
                $result = $this->data_model->editUser($userInfo, $userId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('userListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s'));
            
            $result = $this->data_model->deleteUser($userId, $userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    /**
    * This function is used to check email availability 
    */    
    public function check_old_password()
    {
        $postData = $this->input->post();
        // check old password are already exist or not
        $resultPas = $this->data_model->matchOldPassword($this->vendorId, $postData['oldPassword']);
        if(empty($resultPas))
        {
            $this->output
            ->set_content_type("application/json")
            ->set_output(json_encode(array('status'=>true,'message' => 'Your old password not correct.')));           
        }
        else
        {
            $this->output
            ->set_content_type("application/json")
            ->set_output(json_encode(array('status'=>false)));                          
        }           
    }

    /**
     * This function is used to load the change password screen
     */
    function loadChangePass()
    {
        $this->global['pageTitle'] = 'Tulii : Change Password';
        
        $this->loadViews("changePassword", $this->global, NULL, NULL);
    }
    
    /**
     * This function is used to change the password of the user
    */
    public function changePassword()
    {
         if(is_ajax_request())
        {
            if(is_user_logged_in()){
            $postData = $this->input->post();
            $parnetData = $this->Common_model->select("user_email,user_name",TB_USERS,array('user_id'=>$this->session->userdata('userId')));
            $usersData = array('user_password'=>getHashedPassword($postData['newPassword']), 'updatedBy'=>$this->vendorId,
                                'updated_at'=>date('Y-m-d H:i:s'));                
            $result = $this->data_model->changePassword($this->vendorId, $usersData);
            $hostname = $this->config->item('hostname');            
            $usrname = $parnetData[0]['user_name'];
            $config['mailtype'] ='html';
            $config['charset'] ='iso-8859-1';
            $this->email->initialize($config);
            $from  = EMAIL_FROM; 
            $this->messageBody  = email_header();
            $this->messageBody  .= "Hello ".$usrname." 
                                    <br/><br/>Your password has been changed successfully.                                      
                                     ";
            
            $this->messageBody  .= email_footer();
             //echo $this->messageBody;
             //die;
             $this->email->from($from, $from);
             $this->email->to($parnetData[0]['user_email']);

             $this->email->subject('Your change password');
             $this->email->message($this->messageBody); 
                 
             $this->email->send();
            if ($result)
                echo(json_encode(array('status'=>TRUE,'message' => 'Your password has been updated successfully.'))); 
            else
                echo(json_encode(array('status'=>FALSE,'message' => 'Password updated failed, please try again.'))); 
            }else{
                echo json_encode(array("status"=>"logout"));
            }
        }  
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Tulii : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>