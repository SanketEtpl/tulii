<?php
/*
* @author : kiran N.
* description: manage the child care provider data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Childcare extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Childcare provider'        
        );
        $this->load->model('Childcare_model','Childcare');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');                                
            $this->load->view('childcare_provider/childcare',$this->data);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of child care data
    {
        if(is_user_logged_in()){            
            $list = $this->Childcare->get_datatables();        
            //print_r($list);exit;
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id=1;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            foreach ($list as $childcareData) {
                $userId = $this->encrypt->encode($childcareData->user_id);  
                $button ="";  
                if($childcareData->user_status == 1)
                {   
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="userStatus('.$childcareData->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');" ';
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="userStatus('.$childcareData->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');"';
                } 
                else
                {   
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="userStatus('.$childcareData->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');"';
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="userStatus('.$childcareData->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');" ';
                }
                $button ='<button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button>&nbsp;';
                $button.='<button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button>&nbsp;';
                $type="";               
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $childcareData->user_name;
                $row[] = $childcareData->user_email;
                $row[] = $childcareData->user_phone_number;
                $row[] = $childcareData->user_gender;
                $row[] = $childcareData->user_address;
                $row[] = $button;
                $row[] ='  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="viewChildcare(this)" href="javascript:void(0)">
                                <i class="fa fa-eye"></i>
                            </a>
                			 <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editChildData(this)" href="javascript:void(0)">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteChildcare(this)" href="javascript:void(0)">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->Childcare->count_all(),
                            "recordsFiltered" => $this->Childcare->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function childcareStatusChange() // chnage the status fo parent active or inactive
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_USERS,array("user_id"=>$postData['userId']),array('user_status'=>$postData['user_status'],'updatedBy'=>$this->session->userdata('userId'), 'updated_at'=>date('Y-m-d H:i:s')));            
            if ($result)
                echo(json_encode(array('status'=>TRUE))); 
            else
                echo(json_encode(array('status'=>FALSE))); 
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }        
    }

    public function deleteChildcare() // delete record of parent & also kids data
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->delete(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Childcare has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    // check email availability 
    public function check_email_availability()
    {
        if(is_user_logged_in()) {
            $postData = $this->input->post();
            // check email & user type are already exist or not
            $data = $this->Common_model->select('user_id,user_email',TB_USERS,array('user_email'=>$postData['email']));
            if($data)
            {
                $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status'=>true,'message' => 'Email ID already exists.')));           
            }
            else
            {
                $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status'=>false)));                          
            }  
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }         
    }

     public function viewUserInfo() // view data of user 
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $childData = $this->Common_model->select("user_id,user_email,user_name,user_phone_number,user_gender,user_address",TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    if($childData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","childData"=>$childData[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            }
            else{
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 
    // add & update record of parent data
    public function addUser()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();
                if(!empty($postData["editData"])) { // update data
                    $updateArr = array(  
                        "user_email" =>$postData['email'],
                        "user_address"=>$postData['address'],
                        "user_name"=>$postData['userName'],
                        "user_phone_number"=>$postData['phone'],
                        "user_gender"=> $postData['gender'],                        
                        "updated_at"=>date("Y-m-d H:s:i")  
                    );
                    $updateId = $this->Common_model->update(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['editData'])),$updateArr);
                    if($updateId){
                        echo json_encode(array("status"=>"success","action"=>"update","message"=>"Childcare provider has been updated successfully.")); exit;   
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                    }
                } else {       

                    if(!empty($postData)){ // add data               
                    $data = array(
                        "user_email" =>$postData['email'],
                        "user_address"=>$postData['address'],
                        "user_name"=>$postData['userName'],
                        "user_phone_number"=>$postData['phone'],
                        "user_status"=>"1",
                        "roleId" => 5,
                        "user_gender"=> $postData['gender'], 
                        "is_user_available"=>"available",
                        "created_at"=>date("Y-m-d H:s:i")           
                        );
                    $recordInsertUser = $this->Common_model->insert(TB_USERS,$data);
                  
                    if($recordInsertUser){
                        echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Childcare provider has been inserted successfully ")); exit;    
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
                }
                else{
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the user information")); exit;    
                    }
                }  
            }
            else{
                $this->session->sess_destroy();
                redirect('login');
            } 
        }     
    } 
}