<?php
/*
* @ author : kiran N.
* @ date : 14-03-2018
* @ description: Slider management
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Slider extends CI_Controller
{

	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Slider management',
            'isActive'  => 'active'        
        );
        $this->load->model('Slider_model','slider');
        $this->load->helper(array('form', 'url'));
        $this->load->library('image_lib');
        $config['allowed_types'] =   "gif|png|jpg|jpeg|JPG|JPEG|PNG";  
        $config['max_size']      =   "2097152"; 
        $config['upload_path']   =  "./uploads/banner" ;   
        $this->load->library('upload', $config);

    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');   
            $this->load->view('includes/header',$this->data);     
            $this->load->view('slider/slider_list',$this->data);
            $this->load->view('includes/footer');
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of slider data
    {
        if(is_user_logged_in()){            
            $list = $this->slider->get_datatables(); 

            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id=1;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            foreach ($list as $sliderData) {      
                $userId = $this->encrypt->encode($sliderData->id);
                $button ="";  
               if($sliderData->banner_status == 1) //Active or Inactive status button
                {   
                    $a= $sliderData->id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="sliderStatus_popup('.$a.');" ';

                    $b= $sliderData->id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="sliderStatus_popup('.$b.');"';
                } 
                else
                {   
                    $c= $sliderData->id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="sliderStatus_popup('.$c.');"';

                     $d= $sliderData->id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="sliderStatus_popup('.$d.');" ';
                }
                $button ='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Active"><button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button></span>&nbsp;';
                $button.='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="In-active"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button></span>&nbsp;';
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $sliderData->title;
                $row[] = file_exists($sliderData->banner_img) ? "<img src='".$sliderData->banner_img."' data-toggle='tooltip' width='100' height='50' data-placement='right' title='".$sliderData->title."'>":"<img src='assets/images/img-not-found.jpg' width='100' height='50' data-toggle='tooltip' data-placement='right' title='No image'>";                
                $row[] = $button;
                $row[] = '  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editBanner(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>  
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteBanner(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';       
                $data[] = $row;   
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;          
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->slider->count_all(),
                            "recordsFiltered" => $this->slider->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function sliderStatus() // chnage the status fo slider active or inactive
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_BANNER,array("id"=>$postData['userId']),array('banner_status'=>$postData['user_status'], 'updated_at'=>date('Y-m-d H:i:s')));            
            if ($result)
                echo(json_encode(array('status'=>TRUE))); 
            else
                echo(json_encode(array('status'=>FALSE))); 
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }        
    }


    public function deleteBanner() // delete record of banner list
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $getImage = $this->Common_model->select("banner_img",TB_BANNER,array('id'=>$this->encrypt->decode($postData['key'])));
                    if(file_exists($getImage[0]['banner_img']))
                        unlink($getImage[0]['banner_img']);
                    $deleteId = $this->Common_model->delete(TB_BANNER,array('id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Slider image has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewBanner() // view record of banner details
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $getImage = $this->Common_model->select("title,banner_img",TB_BANNER,array('id'=>$this->encrypt->decode($postData['key'])));
                    if($getImage[0]){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","bannerData"=>$getImage[0])); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    // add record of banner list
    public function addBanner()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $doc ="";
                $postData = $this->input->post();               
                if(!empty($postData['user_key'])){
                    $deleteImage = $this->Common_model->select("banner_img",TB_BANNER,array('id'=>$this->encrypt->decode($postData['user_key'])));
                    if(file_exists($deleteImage[0]['banner_img']))
                        unlink($deleteImage[0]['banner_img']);
                    
                   
                    if(isset($_FILES["slider_img"]["name"]))   
                    {   
                        $file_name = $_FILES["slider_img"]["name"];
                        $original_file_name = $file_name;
                        $random = rand(1, 100000);
                        $date = date("Y-m-d-H-s-i");
                        $makeRandom = $random;
                        $file_name_rename = $makeRandom;
                        $explode = explode('.', $file_name);
                        if(count($explode) >= 2)
                        {
                            $new_file = $file_name_rename.$date.'.'.$explode[1];               
                            $config['upload_path'] = "./uploads/banner/";
                            $config['allowed_types'] ="gif|jpeg|png|jpg";
                            $config['file_name'] = $new_file;
                            $config['max_size'] = '2097152';
                            $config['overwrite'] = FALSE;
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            if(!$this->upload->do_upload("slider_img")) {
                            $error = $this->upload->display_errors();
                                echo json_encode(array("status"=>"error","action"=>"update","message"=>$error)); exit;  
                            } else {
                            $doc = 'uploads/banner/'.$new_file;
                            }
                        }
                    }           
                    else
                    {
                        $doc ="";   
                    }                
                    if(!empty($postData)){ // add data               
                        $data = array(
                            "banner_img" =>$doc,
                            "title" =>$postData['slider_name'],
                            "updated_at"=>date("Y-m-d H:s:i")           
                            );
                        $recordUpdateSlider = $this->Common_model->update(TB_BANNER,array('id'=>$this->encrypt->decode($postData['user_key'])),$data);
                        if($recordUpdateSlider){
                            echo json_encode(array("status"=>"success","action"=>"update","message"=>"Slider image has been updated successfully.")); exit;    
                            } else {
                            echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                        }
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please fill the slider information.")); exit;    
                    }   
                } else {
                    if(isset($_FILES["slider_img"]["name"]))   
                    {   
                        $file_name = $_FILES["slider_img"]["name"];
                        $original_file_name = $file_name;
                        $random = rand(1, 100000);
                        $date = date("Y-m-d-H-s-i");
                        $makeRandom = $random;
                        $file_name_rename = $makeRandom;
                        $explode = explode('.', $file_name);
                        if(count($explode) >= 2)
                        {
                            $new_file = $file_name_rename.$date.'.'.$explode[1];               
                            $config['upload_path'] = "./uploads/banner/";
                            $config['allowed_types'] ="gif|jpeg|png|jpg";
                            $config['file_name'] = $new_file;
                            $config['max_size'] = '2097152';
                            $config['overwrite'] = FALSE;
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            if(!$this->upload->do_upload("slider_img")) {
                            $error = $this->upload->display_errors();
                                echo json_encode(array("status"=>"error","action"=>"update","message"=>$error)); exit;  
                            } else {
                            $doc = 'uploads/banner/'.$new_file;
                            }
                        }
                    }           
                    else
                    {
                        $doc ="";   
                    }                
                    if(!empty($postData)){ // add data               
                        $data = array(
                            "banner_img" =>$doc,
                            "title" =>$postData['slider_name'],
                            "banner_status"=>1,
                            "created_at"=>date("Y-m-d H:s:i")           
                            );
                        $recordInsertUser = $this->Common_model->insert(TB_BANNER,$data);
                        if($recordInsertUser){
                            echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Slider image has been inserted successfully.")); exit;    
                            } else {
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                        }
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the slider information.")); exit;    
                    }             
                }                                    
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 
}