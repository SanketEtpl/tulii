<?php 

/*

Author : Rajendra pawar 
Page :  TutorList_controller.php
Description : tutorList controller use for tutor managment module

*/

if(!defined('BASEPATH')) exit('No direct script access allowed');
 
 require APPPATH . '/libraries/BaseController.php';

class TutorList_controller extends BaseController
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->load->model('tutor_management/tutor_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->isLoggedIn();   
    }
    public function index()
    {
    	$this->global['pageTitle'] = 'Tulii : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
            
    }

  function listOperation()
  { 
        if($this->isAdmin() == TRUE)
        {
          $this->global['pageTitle'] = 'Tulii : Tutor Listing';
          $this->loadViews("tutor_management/tutorList", $this->global, NULL , NULL);
           
        }
        else
        {
         $this->loadThis();     	      
           
    }

}

  function tutorList()
  { 
        if($this->isAdmin() == TRUE)
        {
         $list = $this->tutor_model->get_datatables();  

        $data = array();
        $no = $_POST['start'];
        foreach($list as $record)
          {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $record->user_name;
            $row[] = $record->kid_name;
            $row[] = $record->tutor_name;
            $row[] = $record->tutor_number;
            $row[] = '<a data-tutorid="'.$record->tutor_id.'"  class="btn btn-sm btn-info viewTutor"  href="javascript:void(0)"><i class="fa fa-eye"></i></a>';
            
            $data[] = $row;
          }

          $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->tutor_model->count_all(),
                        "recordsFiltered" => $this->tutor_model->count_filtered(),
                        "data" => $data,
                );          

          echo json_encode($output);
           
        }
        else
        {
           $this->loadThis();
    }

}
 function viewTutorDetails()
    {
        if($this->isAdmin() == TRUE)
        {
           $tutorId = $this->input->post('tutorId');
            $result=$this->tutor_model->select_query("user_name,user_email,user_phone_number,user_gender,user_address, user_no_of_kids,kid_name,kid_gender,kid_age,tutor_name,tutor_number,Start_date,end_date,duration,total_day,total_price",TB_USERS,array("tutor_id"=>$tutorId));
                if ($result > 0) { 

                 $user_name= $result[0]['user_name'];
                 $user_email=$result[0]['user_email'];
                 $user_phone_number=$result[0]['user_phone_number'];
                 $gender = $result[0]['user_gender'];
                 $user_address = $result[0]['user_address'];
                 $user_no_of_kids = $result[0]['user_no_of_kids'];

                $kid_name = $result[0]['kid_name'];
                $kid_gender = $result[0]['kid_gender'];

                $tutor_name = $result[0]['tutor_name'];
                $tutor_number = $result[0]['tutor_number'];
                $Start_date = $result[0]['Start_date'];
                $end_date = $result[0]['end_date'];
                $duration = $result[0]['duration'];
                $total_day = $result[0]['total_day'];
                $total_price = $result[0]['total_price'];
                 $data='<div class="box">
                     <form role="form"  method="post" id="editcareDriverFrm" name="editcareDriverFrm">
                        <div class="box-body">
                            <div class="row"><h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Parent Details</h4>               
                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Full Name: </label>
                                        <lable>'.$user_name.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email address: </label>
                                        <lable>'.$user_email.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number: </label>
                                        <lable>'.$user_phone_number.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="gender">Gender: </label>
                                        <lable>'.$gender.'</lable>
                                    </div>
                                </div>
                            </div>
                          
                               <div class="row"><h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Kid\'s Details</h4>               
                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="kid">Kid Name: </label>
                                        <lable>'.$kid_name.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="kid_gender">Kid Gender: </label>
                                        <lable>'.$kid_gender.'</lable>
                                    </div>
                                </div>
                            </div>
                            



                               <div class="row"><h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Tutor Details</h4>               
                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="Tutor\'s Name">Tutor\'s Name: </label>
                                        <lable>'.$tutor_name.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Tutor\'s Number">Tutor\'s Number: </label>
                                        <lable>'.$tutor_number.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Start_date">Start Date: </label>
                                        <lable>'.$Start_date.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="end_date">End Date: </label>
                                         <lable>'.$end_date.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="duration">Duration: </label>
                                        <lable>'.$duration.'</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="total_day">Total Days: </label>
                                        <lable>'.$total_day.'</label>
                                    </div>
                                </div>
                               </div>
                                 <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                         <label for="total_price">Total Price : </label>
                                        <lable>'.$total_price.'</label>
                                    </div>
                                </div>
                               </div>


                        </div><!-- /.box-body -->   
                         </form>                
                </div>';   
               echo json_encode(array("status" => TRUE,"data" => $data)); 
               }
            else { echo(json_encode(array("status"=>FALSE))); } 
       }
        else
        {
         echo(json_encode(array('status'=>'access')));                           
          
        }
    }
}