<?php
/*
* @author : kiran N.
* description: manage the tutor list of subcategory data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Tutor_subcategory extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Tutor sub-category list',
            'isActive' => 'active'         
        );
        $this->load->model('Tutor_subcategory_model','tutor_subcategory');
    }

    public function index()
    {
        if(is_user_logged_in()) {
            $this->data['categoryList'] = $this->Common_model->select("tut_cat_id as id, category_name", TB_TUTOR_CATEGORY, array('is_deleted' => '0', 'status' => '1'));
            $this->load->view("includes/header", $this->data);
            $this->load->view('tutor_management/tutorSubcategory', $this->data);   
            $this->load->view('includes/footer');                    
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function subcategoryList() // list of price list data
    {
        if(is_user_logged_in()){   
            $resutlTutorSubCategoryList = $this->tutor_subcategory->get_datatables(); 
            $data = array();
            $no = $_POST['start'];
            $i = 1;             
            foreach ($resutlTutorSubCategoryList as $tcValue) {
                $tcId = $this->encrypt->encode($tcValue->id);
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($tcValue->category_name);
                $row[] = ucfirst($tcValue->subcategory_name);
                $row[] = $tcValue->rate_per_hour;
                $row[] = '                    
                    <a data-id="'.$i.'" data-row-id="'.$tcId.'" class="btn btn-sm btn-info" onclick="editTutorSubCategory(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a data-id="'.$i.'" data-row-id="'.$tcId.'" class="btn btn-sm btn-danger" onclick="deleteTutorSubCategory(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>                           
                ';
                $data[] = $row;                             
                $i++;
            }
     
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->tutor_subcategory->count_all(),
                "recordsFiltered" => $this->tutor_subcategory->count_filtered(),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function deleteTutorSubCategory() // delete record of tutor sub-category list
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $checkTutorCatAssign = $this->Common_model->select("tutor_booking_id", TB_TUTOR_BOOKING, array('subcat_id' => trim($this->encrypt->decode($postData['key']))));
                    if(count($checkTutorCatAssign) > 0) {
                        echo json_encode(array("status" => "warning" ,"action" => "delete", "msg" => "This tutor category assigned someone in tutor booking table.")); exit; 
                    } else {
                        $data = array(
                            "is_deleted" => "1",
                            "update_date" => date('Y-m-d H:i:s')  
                        );
                        $deleteId = $this->Common_model->update(TB_TUTOR_SUBCATEGORY,array('tut_sub_cat_id'=>$this->encrypt->decode($postData['key'])), $data);
                        if($deleteId){                                                
                            echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Tutor sub-category record has been deleted successfully.")); exit;  
                        }else{
                            echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                        }
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewTutorSubCategory() // view tutor data of subcategory  
    {
        if(is_ajax_request()) {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if(! empty($postData["key"])){               
                    $resultViewTutorData = $this->Common_model->select("tut_sub_cat_id, tut_cat_id, subcategory_name, rate_per_hour", TB_TUTOR_SUBCATEGORY, array('tut_sub_cat_id' => $this->encrypt->decode($postData["key"])));
                    if(count($resultViewTutorData) > 0) {                                                
                        echo json_encode(array("status" => "success", "action" => "view","tutorData" => $resultViewTutorData[0])); exit;
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

    // add tutor sub-category
    public function addTutorSubCategory()
    {
        if(is_ajax_request()) {
            if(is_user_logged_in()) {
                $postData = $this->input->post(); 
                if(!empty($postData['edit_id'])) {
                    $checkExists = $this->Common_model->select("tut_sub_cat_id", TB_TUTOR_SUBCATEGORY, array('subcategory_name' => trim($postData['tutor_sub_category_name']),'tut_cat_id' => trim($postData['tutorCategory']), 'tut_sub_cat_id !=' => $postData['edit_id'], 'is_deleted' => '0')); 
                    if(count($checkExists) > 0){
                        echo json_encode(array("status" => "warning","message" => "Tutor sub-category already exists.")); exit;    
                    } else {                                              
                        $data = array(
                            "tut_cat_id" => trim($postData['tutorCategory']),
                            "subcategory_name" => trim($postData['tutor_sub_category_name']),
                            "rate_per_hour" => trim($postData['rate_per_hour']),
                            "update_date" => date('Y-m-d H:i:s')                            
                        );
                        $resultTutorSubCatUpdated = $this->Common_model->update(TB_TUTOR_SUBCATEGORY, array('tut_sub_cat_id' => $postData['edit_id']), $data);
                        if($resultTutorSubCatUpdated) {
                            echo json_encode(array("status" => "success", "action" => "update", "message" => "Tutor sub-category has been updated successfully.")); exit;    
                        } else {
                            echo json_encode(array("status" => "error","action" => "update", "message" => "Please try again.")); exit; 
                        }
                    }
                } else {
                    $checkExists = $this->Common_model->select("tut_sub_cat_id", TB_TUTOR_SUBCATEGORY, array('subcategory_name' => trim($postData['tutor_sub_category_name']),'tut_cat_id' => trim($postData['tutorCategory']), 'is_deleted' => '0')); 
                    if(count($checkExists) > 0){
                        echo json_encode(array("status" => "warning","message" => "Tutor sub-category already exists.")); exit;    
                    } else {                                              
                        $data = array(
                            "tut_cat_id" => trim($postData['tutorCategory']),
                            "subcategory_name" => trim($postData['tutor_sub_category_name']),
                            "rate_per_hour" => trim($postData['rate_per_hour']),
                            "is_deleted" => "0",
                            "status" => "1",
                            "created_date" => date('Y-m-d H:i:s')        
                        );
                        $resultTutorCatAdd = $this->Common_model->insert(TB_TUTOR_SUBCATEGORY,$data);

                        if($resultTutorCatAdd) {
                            echo json_encode(array("status" => "success", "action" => "insert", "message" => "Tutor sub-category has been added successfully.")); exit;    
                        } else {
                            echo json_encode(array("status" => "error","action" => "insert", "message" => "Please try again.")); exit; 
                        }
                    }    
                }                     
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 
}