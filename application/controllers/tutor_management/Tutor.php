<?php 
/*
Author : Kiran n 
Page :  Tutor.php
Description : Tutor details
*/
if(!defined('BASEPATH')) exit('No direct script access allowed'); 
require APPPATH . '/libraries/BaseController.php';
class Tutor extends BaseController
{
	public function __construct()
  {
    ob_start();
    parent::__construct();
    $this->load->model('Tutor_model','tutor');
    $this->load->model('common_model');
    $this->load->model('login_model');
    $this->data = array(
      'pageTitle' => 'Tulii : Tutor management',
      'isActive' => 'active'        
    );
    $this->isLoggedIn();   
  }
  public function index()
  {    
    if($this->isAdminOrParent() == TRUE){
      if(is_user_logged_in()) {                                
        $this->load->helper('url');
        $this->load->view('includes/header',$this->data);
        $this->load->view('tutor_management/tutor',$this->data);
        $this->load->view('includes/footer');
      } else {
        $this->session->sess_destroy();
        redirect('login');
      }
    } else {
      $this->loadThis();    
    }      
  }

  public function tutorList()
  { 
    if(is_user_logged_in()){            
      $list = $this->tutor->get_datatables();  
      $data = array();
      $no = $_POST['start'];
      $i = 1;
      foreach ($list as $rec) {
          $userId = $this->encrypt->encode($rec->id);
          $no++;
          $row = array();
          $row[] = $no;
          $row[] = $rec->teacher_name;
          $temp=''; 
          $list_id = $this->tutor->select_where_in('sub_name',TB_SUBJECTS,$rec->sub_id);
          if($list_id){
         
         foreach($list_id as $dt)
         {
          if($temp == ''){
          $temp .=  $dt['sub_name'] ;     
          }else{
          $temp .= " , ". $dt['sub_name']; 
          }               
         } 
         }
          $row[] = $temp;                         
          /*$row[] ='  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editSubject(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                          <i class="fa fa-pencil"></i>
                      </a>                                                
                  ';*/
          $data[] = $row;             
          $i++;
      }

      $output = array(
                      "draw" => $_POST['draw'],
                      "recordsTotal" => $this->tutor->count_all(),
                      "recordsFiltered" => $this->tutor->count_filtered(),
                      "data" => $data,
              );
      //output to json format
      echo json_encode($output);
  }
  else{
      $this->session->sess_destroy();
      redirect('login');
  }
  }  
}
?>