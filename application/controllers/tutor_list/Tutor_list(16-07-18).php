<?php
/*
* @author : kiran N.
* description: manage the tutor list of tutors category & subcategory data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Tutor_list extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Tutor list',
            'isActive' => 'active'         
        );
        $this->load->model('Tutor_list_model','tutor');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url'); 
            //$this->data['tutors'] = $this->Common_model->select("user_id,user_name",TB_USERS,array("roleId" => ROLE_TUTOR));
            $this->data['tutorCat'] = $this->Common_model->select("*",TB_TUTOR_CATEGORY);
            $this->load->view("includes/header",$this->data);
            $this->load->view('tutor_list/tutor_details',$this->data);                       
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of price list data
    {
        if(is_user_logged_in()){   
            $list = $this->tutor->get_datatables(); 
            $data = array();
            $no = $_POST['start'];
            $i = 1;             
            $status_id=1;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            foreach ($list as $rec) {
                $userId = $this->encrypt->encode($rec->tutor_id);
                $button ="";  
                if($rec->status == 1)
                {   
                    $a= $rec->tutor_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="tutorStatus_popup('.$a.');" ';

                    $b= $rec->tutor_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="tutorStatus_popup('.$b.');"';
                } 
                else
                {   
                    $c= $rec->tutor_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="tutorStatus_popup('.$c.');"';

                    $d= $rec->tutor_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="tutorStatus_popup('.$d.');" ';
                }
                $button ='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Active"><button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button></span>&nbsp;';
                $button.='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="In-active"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button></span>&nbsp;';
                
                $cond    = array("tbl_tutor_subjects.tutor_id"=> $rec->tutor_id);       
                $join    = array(TB_TUTOR_SUBCATEGORY => TB_TUTOR_SUBJECTS.".sub_id  = ".TB_TUTOR_SUBCATEGORY.".id");
                $subjPriceData = $this->Common_model->select_join("subcategory_name", TB_TUTOR_SUBJECTS, $cond, array(), array(), $join);
                $subcategoryData=''; 
                if(count($subjPriceData)) {               
                    foreach($subjPriceData as $dt)
                    {
                        if($subcategoryData == ''){
                            $subcategoryData .=  ucfirst($dt['subcategory_name']);     
                        } else {
                            $subcategoryData .= " , ". ucfirst($dt['subcategory_name']); 
                        }               
                    } 
                } 
                $subcategoryData = !empty($subcategoryData) ? $subcategoryData : "N/A";              
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($rec->user_name);
                $row[] = ucfirst($rec->category_name);
                $row[] = trim($subcategoryData," ,");
                $row[] = $button;                                      
                $row[] = '<a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="viewTutor(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>';
               /* $row[] ='  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editTutor(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger" onclick="deleteTutor(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';*/
                $data[] = $row;             
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->tutor->count_all(),
                            "recordsFiltered" => $this->tutor->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function tutorStatusChange() // chnage the status fo tutor active or inactive
    {
        if(is_user_logged_in()) { 
            if(is_ajax_request()) {
                $postData = $this->input->post();
                $result = $this->Common_model->update(TB_TUTOR_SUBJECTS,array("tutor_id"=>$postData['tId']),array('status'=>$postData['t_status'], 'updated_at'=>date('Y-m-d H:i:s')));            
                if ($result) {                   
                    echo(json_encode(array('status'=>TRUE))); 
                }    
                else
                    echo(json_encode(array('status'=>FALSE)));
            }     
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }   
    }

/*    public function deleteTutor() // delete record of tutor list
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->delete(TB_TUTOR_MASTER,array('id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Tutor record has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewTutor() // view data of tutor  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if(! empty($postData["key"])){               
                    $cond    = array("tbl_tutor_master.id"=>$this->encrypt->decode($postData['key']));       
                    $join    = array(TB_TUTOR_CATEGORY => TB_TUTOR_CATEGORY.".id = ".TB_TUTOR_MASTER.".category_id",TB_TUTOR_SUBCATEGORY=>TB_TUTOR_MASTER.".subcategory_id  = ".TB_TUTOR_SUBCATEGORY.".id",TB_USERS=>TB_TUTOR_MASTER.".user_id  = ".TB_USERS.".user_id");
                    $tutorData = $this->Common_model->select_join("tbl_tutor_master.id,tbl_tutor_master.category_id,tbl_tutor_master.user_id,subcategory_id", TB_TUTOR_MASTER, $cond, array(), array(), $join);
                
                    if($tutorData) {                                                
                        echo json_encode(array("status"=>"success","action"=>"view","tutorData"=>$tutorData[0])); exit;//,"subjects"=>$subjects[0],"teachers"=>$teachers[0])); exit; 
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } */

    public function viewTutorDetails() // view data of tutor  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if(! empty($postData["key"])){               
                    $cond    = array("tbl_tutor_subjects.tutor_id"=>$this->encrypt->decode($postData['key']));       
                    $join    = array(TB_TUTOR_CATEGORY => TB_TUTOR_CATEGORY.".id = ".TB_TUTOR_SUBJECTS.".cat_id",TB_USERS=>TB_TUTOR_SUBJECTS.".tutor_id  = ".TB_USERS.".user_id");
                    $tutorData = $this->Common_model->select_join("tbl_tutor_subjects.id,user_name,category_name,tutor_id", TB_TUTOR_SUBJECTS, $cond, array(), array(), $join,"tutor_id");

                    $cond1    = array("tbl_tutor_subjects.tutor_id"=>$tutorData[0]['tutor_id']);       
                    $join1    = array(TB_TUTOR_SUBCATEGORY => TB_TUTOR_SUBJECTS.".sub_id  = ".TB_TUTOR_SUBCATEGORY.".id");
                    $subjPriceData = $this->Common_model->select_join("rate_per_hour,subcategory_name", TB_TUTOR_SUBJECTS, $cond1, array(), array(), $join1);
                    $html = '<table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th>Subjects</th>
                                    <th>Price</th>                                    
                                  </tr>
                                </thead>
                                <tbody>';
                    if(count($subjPriceData) > 0) {
                        foreach ($subjPriceData as $key => $value) { 
                        $subject =  !empty($value['subcategory_name']) ? ucfirst($value['subcategory_name']) : "N/A";   
                        $price =  !empty($value['rate_per_hour']) ? ucfirst($value['rate_per_hour']) : "N/A";                          
                        $html .= "<tr>
                                    <td>".$subject."</td>
                                    <td>$".$price."</td>                                    
                                  </tr>                                  
                                ";
                        }
                    } else {
                        $html .="";
                    }
                    $html .='</tbody>                    
                    </table>';
                    if(count($tutorData)) {                                          
                        echo json_encode(array("status"=>"success","action"=>"view","tutorData"=>$tutorData[0],"subjectPrice" => $html)); exit;//,"subjects"=>$subjects[0],"teachers"=>$teachers[0])); exit; 
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"No data found.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }

     // add tutor category
    public function addTutorCategory()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post(); 
                $checkExists = $this->Common_model->select("id",TB_TUTOR_CATEGORY,array('category_name'=>trim($postData['category_name']))); 
                if(count($checkExists) > 0){
                    echo json_encode(array("status"=>"warning","message"=>"Tutor category already exists.")); exit;    
                } else {

                          
                        if(!empty($postData)){               
                        $data = array(
                            "category_name" =>$postData['category_name']         
                            );
                        $recordInsert = $this->Common_model->insert(TB_TUTOR_CATEGORY,$data);
                        if($recordInsert){
                            echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Tutor category has been added successfully.")); exit;    
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                        }
                    } else {
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the tutor information.")); exit;    
                    }
                   
                }     
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 

    // add tutor sub-category
    public function addTutorSubCategory()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                
                $checkExists = $this->Common_model->select("id",TB_TUTOR_SUBCATEGORY,array('subcategory_name'=>trim($postData['tutor_sub_category_name']),'category_id'=>trim($postData['category_id']))); 
                if(count($checkExists) > 0){
                    echo json_encode(array("status"=>"warning","message"=>"Tutor sub-category already exists.")); exit;    
                } else {                     
                          
                      if(!empty($postData)){               
                        $data = array(
                            "category_id" => $postData['category_id'],
                            "subcategory_name" => $postData['tutor_sub_category_name']         
                        );
                        $recordInsert = $this->Common_model->insert(TB_TUTOR_SUBCATEGORY,$data);
                        if($recordInsert){
                            echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Tutor sub-category has been added successfully.")); exit;    
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                        }
                    } else {
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the tutor information.")); exit;    
                    }
                   
                }     
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 

        // add & update tutor 
/*    public function addTutor()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();  
                if(!empty($postData['user_key'])) {
                     $updateArr = array(                             
                            "category_id" => $postData['category'],
                            "subcategory_id" => $postData['sub_category'],                           
                        );
                        $updateId = $this->Common_model->update(TB_TUTOR_MASTER,array('id'=>$this->encrypt->decode($postData['user_key'])),$updateArr);

                        if($updateId){
                            echo json_encode(array("status"=>"success","action"=>"update","message"=>"Tutor has been updated successfully.")); exit;   
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                        }
                } else {
                    $checkExists = $this->Common_model->select("id",TB_TUTOR_MASTER,array('user_id'=>trim($postData['tutorName']))); 
                    if(count($checkExists) > 0){
                        echo json_encode(array("status"=>"warning","message"=>"Tutor already exists.")); exit;    
                    } else {               
                        $subcategory = !empty($postData['sub_category']) ? implode(',', $postData['sub_category']):"";
                        if(!empty($postData)){               
                            $data = array(
                                "user_id" => $postData['tutorName'],
                                "category_id" => $postData['category'],
                                "subcategory_id" => $subcategory,
                                "created_at" => date("Y-m-d H:i:s")         
                            );
                            $recordInsert = $this->Common_model->insert(TB_TUTOR_MASTER,$data);
                            if($recordInsert){
                                echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Tutor has been added successfully.")); exit;    
                            } else {
                                echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                            }
                        } else {
                                echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the tutor information.")); exit;    
                        }      
                    }   
                }    
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } */

/*    public function showTutorSubcategory(){
        if(is_user_logged_in()) {
            $postData = $this->input->post();
            if(isset($postData['category_id'])){             
                $list = $this->Common_model->select('id,subcategory_name',TB_TUTOR_SUBCATEGORY,array("category_id"=>$postData['category_id'],"status" => 1));
            } else {
                $list = $this->Common_model->select_join("tbl_tutor_subcategory.id,subcategory_name",TB_TUTOR_MASTER,array("tbl_tutor_master.id"=>$this->encrypt->decode($postData['key'])),array(),array(),array(TB_TUTOR_SUBCATEGORY=>'tbl_tutor_subcategory.category_id = tbl_tutor_master.category_id'),null);
            }
            if(count($list) > 0) {
                $data=''; 
                foreach($list as $dt)
                {
                    $data .="<option value='".$dt['id']."'> ".ucfirst($dt['subcategory_name'])."</option>";  
                } 
                $output = array(
                    "status" => 'success',
                    "data" => $data,
                ); 
            } else {
                $output = array(
                    "status" => 'error',
                    "data" =>'',
                );   
            }
            echo json_encode($output);   
        } 
    }*/
}