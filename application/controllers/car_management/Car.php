<?php
/*
* @author : Ram K.
* description: manage the payment data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Car extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Car management', 
            'isActive'  => 'active'
        );
        // $this->load->model('PaymentSection_model','payment_section');
        $this->load->model("car_category_model");
        $this->load->model("common_model");
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');  
            $this->load->view('includes/header',$this->data);
            $this->load->view('car_management/car',$this->data);
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of FAQ list data
    {
        if(is_user_logged_in()){
          
            $catData = $this->car_category_model->get_datatables();  
            //echo "<pre>";print_r($catData);die;
            $data = array();
            $no = $_POST['start'];
            $i = 1;

            foreach ($catData as $catValue) {
                $catID = $this->encrypt->encode($catValue->cat_id);
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($catValue->cat_name);
                $row[] = $catValue->cat_rate;
                //$row[] = $catValue->capacity;
                $row[] = $catValue->created_date;                
                $row[] ='  <a data-id="'.$i.'" data-row-id="'.$catID.'" class="btn btn-sm btn-info" onclick="editCatData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$catID.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteCatList(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(

                "draw" => $_POST['draw'],
                "recordsTotal" => $this->car_category_model->count_all(),
                "recordsFiltered" => $this->car_category_model->count_filtered(),
                "data" => $data,

            );

            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

     // add & update record of price list
    public function addCarCategory()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();
                $cond = array("cat_name" => $postData['categories']);
                $priceListData = $this->Common_model->select("distinct(cat_name)",TB_CAR_CATEGORY,$cond);
                if(empty($postData['editData']) && count($priceListData) > 0) {                    
                    echo json_encode(array("status"=>"warning","message"=>$priceListData[0]['cat_name']." category is already exists.")); exit;   
                }
                else
                {
                    if(!empty($postData["editData"])) { // update data
                        $updateArr = array(
                            "cat_name" =>$postData['categories'],
                            "cat_rate" =>$postData['rate'],
                            //"capacity "=> $postData['car_capacity'],
                            "created_date"=>date("Y-m-d H:s:i")
                        );
                        $updateId = $this->Common_model->update(TB_CAR_CATEGORY,array('cat_id'=>$this->encrypt->decode($postData['editData'])),$updateArr);
                        if($updateId){
                            echo json_encode(array("status"=>"success","action"=>"update","message"=>"Category has been updated successfully.")); exit;
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit;
                        }
                    } else {
                        if(!empty($postData)){ // add data
                        $data = array(
                            "cat_name" =>$postData['categories'],
                            "cat_rate" =>$postData['rate'],
                            //"capacity "=> $postData['car_capacity'],
                            "created_date"=>date("Y-m-d H:s:i")
                            );
                        $recordInsertUser = $this->Common_model->insert(TB_CAR_CATEGORY,$data);
                        if($recordInsertUser){
                            echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Category has been inserted successfully.")); exit;
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit;
                        }
                    }
                    else{
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the user information.")); exit;
                        }
                    }
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

     public function viewCatInfo() // view data of price  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $subCat ="";
                    $catData = $this->Common_model->select("cat_id,cat_name,cat_rate",TB_CAR_CATEGORY,array('cat_id'=>$this->encrypt->decode($postData['key'])));
                    if(!empty($catData[0]['cat_id']))
                    if($catData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","catData"=>$catData[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            }
            else{
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }

    public function deleteCatList() // delete record of cat list
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){             

                    $updateArr = array(                            
                            "is_deleted "=> '1',
                            "created_date"=>date("Y-m-d H:s:i")
                        );
                        $updateId = $this->Common_model->update(TB_CAR_CATEGORY,array('cat_id'=>$this->encrypt->decode($postData['key'])),$updateArr);
                        
                    if($updateId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Category has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }  
}