<?php
/*
* @author : kiran N.
* page : service kids and youth controller
* description: show the all service kids and youth data & management module
*/
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Service_kids_and_youth extends CI_Controller {
 
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Service kids and youth'        
        );
        $this->load->model('Servicekidsyouth_model','kidsandyouth');
    }
 
    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');    
            $this->load->view('service_management/kids_and_youth',$this->data);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        } 
    }
 
    public function ajax_list() // list of kids and youth data
    {
        if(is_user_logged_in()){
            $list = $this->kidsandyouth->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            foreach ($list as $KAYData) {
                $userId = $this->encrypt->encode($KAYData->sph_id);  
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $KAYData->sph_cd_name;
                $row[] = $KAYData->sph_cd_gender;
                $row[] = $KAYData->sph_cd_age;
                $row[] = $KAYData->sph_cd_experience;
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="viewCareDriver(this)" href="javascript:void(0)">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteCareDriver(this)" href="javascript:void(0)">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;               
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->kidsandyouth->count_all(),
                            "recordsFiltered" => $this->kidsandyouth->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        } 
    }

    public function deleteCareDriverInfo() // delete record of kids and youth & also kids data
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $profileData = $this->Common_model->select("sph_cd_profile_picture",TB_SERVICE_PROVIDER_HISTORY,array('sph_id'=>$this->encrypt->decode($postData['key'])));
                    if(file_exists($profileData[0]['sph_cd_profile_picture']))
                    unlink($profileData[0]['sph_cd_profile_picture']);
                    $deleteId = $this->Common_model->delete(TB_SERVICE_PROVIDER_HISTORY,array('sph_id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Kids and youth record has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }

    public function viewCareDriverInfo() // view data of parent & no. of kids
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                $imgExist =0;  
                if($postData["key"]){
                    $careDriverdData = $this->Common_model->select("*",TB_SERVICE_PROVIDER_HISTORY,array('sph_id'=>$this->encrypt->decode($postData['key'])));
                    if(file_exists($careDriverdData[0]['sph_cd_profile_picture']))
                    $imgExist =1;  
                    if($careDriverdData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","careDriverdData"=>$careDriverdData[0],'imgExist'=>$imgExist)); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }   
}