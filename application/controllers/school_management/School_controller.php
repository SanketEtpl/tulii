<?php
/*
* @author : Kiran.
* description: manage the school data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class School_controller extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : School management', 
            'isActive'  => 'active'
        );
        $this->load->model("School_model","school");
        $this->load->model("Pagination_school_model");
        $this->load->model("common_model");   
        $this->load->library('pagination');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->view('includes/header',$this->data);
            $this->load->view('school_management/school');
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of school list data
    {
        if(is_user_logged_in()){          
            $schoolData = $this->school->get_datatables();              
            $data = array();
            $no = $_POST['start'];
            $i = 1;

            foreach ($schoolData as $schoolValue) {
                $userID = $schoolValue->user_id;
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($schoolValue->user_name);
                $row[] = $schoolValue->user_phone_number;
                $row[] = $schoolValue->user_email;
                $row[] = $schoolValue->user_address;                
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userID.'" class="btn btn-sm btn-info" onclick="viewSchoolStudents(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>                
                                                     
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(

                "draw" => $_POST['draw'],
                "recordsTotal" => $this->school->count_all(),
                "recordsFiltered" => $this->school->count_filtered(),
                "data" => $data,

            );

            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function viewStudentList() // view data of students  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();     
                $id = !empty($postData['key']) ? $postData['key'] : $this->session->userdata('schoolId');
                if( $id) {
                    $sId = $id;
                    $this->session->set_userdata(array("schoolId" =>$sId));
                    $studData = $this->Pagination_school_model->get_datatables($sId);              
                    $data = array();
                    $no = $_POST['start'];
                    $i = 1;

                    foreach ($studData as $value) {
                        $row = array();
                        $row[] = $value->role_id;
                        $row[] = ucfirst($value->stud_username);
                        $row[] = $value->stud_age;
                        $row[] = $value->stud_address;
                        $row[] = $value->parent_name;
                        $row[] = $value->parent_phone;
                        $row[] = $value->parent_email;
                        $data[] = $row;             
                        $i++;
                    }
             
                    $output = array(

                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Pagination_school_model->count_all($sId),
                        "recordsFiltered" => $this->Pagination_school_model->count_filtered($sId),
                        "data" => $data,

                    );

                    //output to json format
                    echo json_encode($output);                                  
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please provide school id.")); exit;   
                }

            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 
}