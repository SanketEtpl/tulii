<?php
/*
* @author : kiran N.
* description : show the parent,school and organization users management controller
* Created_date : 23-07-2018
*/
defined('BASEPATH') OR exit('No direct script access allowed'); 
class ServiceUserController extends CI_Controller {
 
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Service user management',
            'isActive' => 'active'        
        );
        $this->load->model('ServiceUserModel','service_user');        
    }
 
    public function index()
    {
        if (is_user_logged_in()){   
            $this->load->view('includes/header',$this->data);
            $this->load->view('user/serviceUser');
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of parent,school and organization data
    {   
        if(is_user_logged_in()) {
            $resultServiceUserList = $this->service_user->get_datatables();        
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id = 1;
            $rowActive_id = "active".$status_id;
            $rowInActive_id = "inActive".$status_id;
            foreach ($resultServiceUserList as $userData) {
                $userId = $this->encrypt->encode($userData->service_user_id);  
                $button ="";  
                if($userData->user_status == 1)
                {   
                    $a= $userData->service_user_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="userStatus_popup('.$a.');" ';

                    $b= $userData->service_user_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="userStatus_popup('.$b.');"';
                } 
                else
                {   
                    $c= $userData->service_user_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="userStatus_popup('.$c.');"';

                    $d= $userData->service_user_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="userStatus_popup('.$d.');" ';
                }
                $button ='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Active"><button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button></span>&nbsp;';
                $button.='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="In-active"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button></span>&nbsp;';
                $newRecord = $userData->new_record_status == '1'?'<button type="button" class="btn btn-secondary new-record" data-row-id="'.$userId.'" onclick="newRecord(this)" data-toggle="tooltip" data-placement="top" title="" data-original-title="New">New</button>&nbsp;&nbsp;':"&nbsp;&nbsp;";
                
                $no++;
                $row = array();
                $row[] = $no." ".$newRecord;
                $row[] = $userData->user_fullname;
                $row[] = $userData->user_email;
                $row[] = $userData->user_phone_number;
                $row[] = $userData->role_name;
                $row[] = $button;
                $row[] = '
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-primary" onclick="viewServiceUserData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>                           
                        ';
                
                $data[] = $row;
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;
                $i++;
            }
     
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->service_user->count_all(),
                "recordsFiltered" => $this->service_user->count_filtered(),
                "data" => $data,
            );

            //output to json format
            echo json_encode($output);
        } else {
            echo(json_encode(array('status'=>'logout')));
            $this->session->sess_destroy();
            redirect('login');
        } 
    }

    public function newRecord() // new record change to old record status
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_SERVICE_USERS,array("service_user_id"=>$this->encrypt->decode($postData['key'])),array('new_record_status'=>'0'));            
            if ($result)
                echo(json_encode(array('status'=>"success"))); 
            else
                echo(json_encode(array('status'=>"error"))); 
            } else {
                echo(json_encode(array('status'=>'logout')));
                $this->session->sess_destroy();
                redirect('login');
            } 
        }        
    }

    public function userStatusChange() // chnage the status fo parent active or inactive
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {             
                $postData = $this->input->post();
                $result = $this->Common_model->update(TB_SERVICE_USERS,array("service_user_id"=>$postData['userId']),array('user_status'=>$postData['user_status'],'updated_at'=>date('Y-m-d H:i:s')));            
                $subject = "";
                if($result) {
                    if($postData['user_status'] == "1") {
                        $msg = "Admin activated your account. Now you can access your account.";
                        $subject = "Your tulii account activated";
                    } else {
                        $subject = "Your tulii account deactivated";
                        $msg = "Admin deactivated your account due to unsufficient information. You need to contact to admin for more information.";
                    }
                    $userDetails = $this->Common_model->select('service_user_id,user_email,user_fullname,user_device_id',TB_SERVICE_USERS,array('service_user_id'=>$postData['userId']));      
                    $message = '<tr> 
                    <td style="font-size:16px;word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                        <p>Hello '.$userDetails[0]['user_fullname'].',</p>
                            <p>
                            '.$msg.' <br/>
                            Thank & regards.<br/>            
                            Tulii Admin         
                            </p>                    
                        </td>
                    </tr>
                    ';  
                    sendEmail($userDetails[0]['user_email'],$userDetails[0]['user_fullname'],$message,$subject);
                    
                   /* $message = "Your tulii account status"; 
                    $myData = array("message"=>$msg);

                    $allDeviceId = explode(",", $userDetails[0]['user_device_id']);
                    foreach ($allDeviceId as $key => $value) {
                        $this->sendPushNotification($value,$message,$myData);                    
                    }*/
                    echo(json_encode(array('status'=>TRUE))); 
                } else {
                    echo(json_encode(array('status'=>FALSE))); 
                }           
            } else {
                echo(json_encode(array('status'=>'logout')));
                $this->session->sess_destroy();
                redirect('login');
            }
        }        
    }   

    public function viewServiceUserInfo() // view data of user 
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {    
                $postData = $this->input->post();  
                if($postData["key"]){
                    $resultServiceUserData = $this->Common_model->select("service_user_id, organization_name, user_address, user_email, user_fullname, user_phone_number, user_type_id, user_gender, user_birth_date",TB_SERVICE_USERS,array('service_user_id'=>$this->encrypt->decode($postData['key'])));
                    if($resultServiceUserData){  
                        
                        if($resultServiceUserData[0]['user_type_id'] == ROLE_PARENTS)
                        {
                            $parentData = $childDetails = array();   
                            $parentData['parnet_name'] = !empty($resultServiceUserData[0]['user_fullname']) ? $resultServiceUserData[0]['user_fullname'] : "N/A";
                            $parentData['parent_email'] = !empty($resultServiceUserData[0]['user_email']) ? $resultServiceUserData[0]['user_email'] : "N/A";
                            $parentData['parnet_phone'] = !empty($resultServiceUserData[0]['user_phone_number']) ? $resultServiceUserData[0]['user_phone_number'] : "N/A";
                            $parentData['parnet_Gender'] = !empty($resultServiceUserData[0]['user_gender']) ? $resultServiceUserData[0]['user_gender'] : "N/A";
                            $parentData['address'] = !empty($resultServiceUserData[0]['user_address']) ? $resultServiceUserData[0]['user_address'] : "N/A";
                            $parentData['user_birth_date'] = $resultServiceUserData[0]['user_birth_date'] != "0000-00-00" ? $resultServiceUserData[0]['user_birth_date'] : "N/A";
                            $resultChildDetails = $this->Common_model->select("pass_username, pass_fullname, pass_dob, pass_gender",TB_PASSENGER,array('pass_status'=>'1','is_deleted'=>'0','service_user_id'=>$resultServiceUserData[0]['service_user_id']));
                            $count = 0;
                            if( count($resultChildDetails) > 0) {
                                
                                foreach ($resultChildDetails as $key => $value) {
                                    if($value['pass_dob'] != "0000-00-00") {
                                        $today    = date('Y-m-d');
                                        $ageDiff  = date_diff(date_create($value['pass_dob']), date_create($today));
                                        $userAge  = $ageDiff->format('%y');
                                    } else {
                                        $userAge = "N/A"; 
                                    }
                                    $childDetails[$count]['username'] = $value['pass_username'];
                                    $childDetails[$count]['childname'] = ucfirst($value['pass_fullname']);
                                    $childDetails[$count]['gender'] = ucfirst($value['pass_gender']);
                                    $childDetails[$count]['age'] = $userAge;                                                                     
                                    $count++;   
                                }
                            }
                            echo json_encode(array("status"=>"success","action"=>"view","userType"=>"parent","noOfChilds"=>$count,"parentData"=>$parentData, "childData" => $childDetails)); exit; 
                        } else if($resultServiceUserData[0]['user_type_id'] == ORGANIZATION){
                            $orgDetails = array();   
                            $orgDetails['orgName'] = !empty($resultServiceUserData[0]['organization_name']) ? $resultServiceUserData[0]['organization_name'] : "N/A";
                            $orgDetails['email'] = !empty($resultServiceUserData[0]['user_email']) ? $resultServiceUserData[0]['user_email'] : "N/A";
                            $orgDetails['address'] = !empty($resultServiceUserData[0]['user_address']) ? $resultServiceUserData[0]['user_address'] : "N/A";
                            echo json_encode(array("status"=>"success","action"=>"view","userType"=>"org","orgDetails"=>$orgDetails)); exit; 
                        } else {
                            $schoolDetails = array();   
                            $schoolDetails['schoolName'] = !empty($resultServiceUserData[0]['organization_name']) ? $resultServiceUserData[0]['organization_name'] : "N/A";
                            $schoolDetails['email'] = !empty($resultServiceUserData[0]['user_email']) ? $resultServiceUserData[0]['user_email'] : "N/A";
                            $schoolDetails['address'] = !empty($resultServiceUserData[0]['user_address']) ? $resultServiceUserData[0]['user_address'] : "N/A";
                            echo json_encode(array("status"=>"success","action"=>"view","userType"=>"school","schoolDetails"=>$schoolDetails)); exit;    
                        }                                                       
                        
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please provider user id and try again.")); exit;   
                }
            } else {
                echo(json_encode(array('status'=>'logout')));
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }   

    /*public function sendPushNotification($device_id,$message,$data)
    {     
        $content = array(
            "en" => $message
        );
        $fields = array(
            'app_id' => ONSIGNALKEY,
            'include_player_ids' =>(array)$device_id,
            'data' => $data,
            'contents' => $content
        );
        
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
         'Authorization: Basic OGViZTZkMjgtMjRlYy00YWQ0LWIzMWYtNmE2ZWI5MDhmZTUw'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);  
       // print_r($response);exit;            
        curl_close($ch);
    }*/
}