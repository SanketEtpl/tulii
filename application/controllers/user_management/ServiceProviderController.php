<?php
/*
* @author : kiran N.
* description : show the service provider and tutor users management controller
* Created_date : 24-07-2018
*/
defined('BASEPATH') OR exit('No direct script access allowed'); 
class ServiceProviderController extends CI_Controller {
 
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Service provider management',
            'isActive' => 'active'        
        );
        $this->load->model('ServiceProviderModel','service_provider');        
    }
 
    public function index()
    {
        if (is_user_logged_in()){   
            $this->load->view('includes/header',$this->data);
            $this->load->view('user/serviceProvider');
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of service provider and tutor data
    {   
        if(is_user_logged_in()) {
            $resultServiceProviderList = $this->service_provider->get_datatables();
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id = 1;
            $rowActive_id = "active".$status_id;
            $rowInActive_id = "inActive".$status_id;
            foreach ($resultServiceProviderList as $userData) {
                $userId = $this->encrypt->encode($userData->service_provider_id);  
                $button ="";  
                if($userData->sp_status == 1)
                {   
                    $a= $userData->service_provider_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="userStatus_popup('.$a.');" ';

                    $b= $userData->service_provider_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="userStatus_popup('.$b.');"';
                } 
                else
                {   
                    $c= $userData->service_provider_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="userStatus_popup('.$c.');"';

                    $d= $userData->service_provider_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="userStatus_popup('.$d.');" ';
                }
                $button ='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Active"><button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button></span>&nbsp;';
                $button.='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="In-active"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button></span>&nbsp;';
                $newRecord = $userData->new_record_status == '1'?'<button type="button" class="btn btn-secondary new-record" data-row-id="'.$userId.'" onclick="newRecord(this)" data-toggle="tooltip" data-placement="top" title="" data-original-title="New">New</button>&nbsp;&nbsp;':"&nbsp;&nbsp;";
                
                $no++;
                $row = array();
                $row[] = $no." ".$newRecord;
                $row[] = $userData->sp_fullname;
                $row[] = $userData->sp_email;
                $row[] = $userData->sp_phone_number;
                $row[] = $userData->role_name;
                $row[] = $button;
                $row[] = '
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" data-user-type="'.$userData->sp_user_type.'" class="btn btn-sm btn-primary viewServiceProvider" data-userId="'.$userId.'" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>                           
                        ';
                
                $data[] = $row;
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;
                $i++;
            }
     
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->service_provider->count_all(),
                "recordsFiltered" => $this->service_provider->count_filtered(),
                "data" => $data,
            );

            //output to json format
            echo json_encode($output);
        } else {
            echo(json_encode(array('status'=>'logout')));
            $this->session->sess_destroy();
            redirect('login');
        } 
    }

    public function newRecord() // new record change to old record status
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_SERVICE_PROVIDER,array("service_provider_id"=>$this->encrypt->decode($postData['key'])),array('new_record_status'=>'0'));            
            if ($result)
                echo(json_encode(array('status'=>"success"))); 
            else
                echo(json_encode(array('status'=>"error"))); 
            } else {
                echo(json_encode(array('status'=>'logout')));
                $this->session->sess_destroy();
                redirect('login');
            } 
        }        
    }

    public function userStatusChange() // chnage the status fo parent active or inactive
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {             
                $postData = $this->input->post();
                $result = $this->Common_model->update(TB_SERVICE_PROVIDER,array("service_provider_id"=>$postData['userId']),array('sp_status'=>$postData['user_status'],'updated_at'=>date('Y-m-d H:i:s')));            
                $subject = "";
                if($result) {
                    if($postData['user_status'] == "1") {
                        $msg = "Admin activated your account. Now you can access your account.";
                        $subject = "Your tulii account activated";
                    } else {
                        $subject = "Your tulii account deactivated";
                        $msg = "Admin deactivated your account due to unsufficient information. You need to contact to admin for more information.";
                    }
                    $userDetails = $this->Common_model->select('service_provider_id,sp_email,sp_fullname,sp_device_id',TB_SERVICE_PROVIDER,array('service_provider_id'=>$postData['userId']));      
                    $message = '<tr> 
                    <td style="font-size:16px;word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                        <p>Hello '.$userDetails[0]['sp_fullname'].',</p>
                            <p>
                            '.$msg.' <br/>
                            Thank & regards.<br/>            
                            Tulii Admin         
                            </p>                    
                        </td>
                    </tr>
                    ';  
                    sendEmail($userDetails[0]['sp_email'],$userDetails[0]['sp_fullname'],$message,$subject);                  

                    echo(json_encode(array('status'=>TRUE))); 
                } else {
                    echo(json_encode(array('status'=>FALSE))); 
                }           
            } else {
                echo(json_encode(array('status'=>'logout')));
                $this->session->sess_destroy();
                redirect('login');
            }
        }        
    }   

    public function viewServiceProviderTutorInfo() // view data of service provider Driver
    {
        if(is_ajax_request()) {

            if(is_user_logged_in()) {   
                $userId = $this->encrypt->decode($this->input->post('userId'));   
                $userType = $this->input->post('userType');
                $titleHeader = ($userType == 4) ? "Service Provider Tutor Details" : "Service Provider Chaperone Details";        
                $join = array(TB_ATTRIBUTE_VALUES => TB_ATTRIBUTE_VALUES.'.service_provider_id='.TB_SERVICE_PROVIDER.'.service_provider_id', TB_ATTRIBUTE_MASTER => TB_ATTRIBUTE_MASTER.'.attribute_id ='.TB_ATTRIBUTE_VALUES.'.attribute_id');
                $resultSPTutorViewList = $this->Common_model->select_join(TB_ATTRIBUTE_VALUES.".attribute_id, sp_fullname, sp_email, sp_phone_number, sp_address, sp_gender, DATE_FORMAT(sp_birth_date,'%m/%d/%Y')as user_birth_date, DATE_FORMAT(FROM_DAYS(DATEDIFF(CURDATE(), sp_birth_date)), '%Y')+0 AS user_age, sp_prof_pic, sp_cover_pic, attribute_value", TB_SERVICE_PROVIDER, array("service_provider_ut.service_provider_id" => $userId, "is_deleted" => "0", "is_block" => "0"),array(),array(),
                $join,null);
                if ($resultSPTutorViewList) { 
                    $schoolOrganization = $degree = $specialization = $education_from_date = $education_to_date = $certification_name = $certificate_doc = $police_verify_status = $police_verify_doc = $verification_no = $user_name = $user_phone_number = $user_age = $user_birth_date = $user_gender = $sp_cover_pic = $sp_prof_pic = $user_address = $user_email = "N/A";
                        foreach ($resultSPTutorViewList as $key => $value) {
                            $user_name= $value['sp_fullname'];
                            $user_gender = $value['sp_gender'];
                            $user_birth_date = $value['user_birth_date'];
                            $user_age = $value['user_age'];
                            $user_phone_number = $value['sp_phone_number'];
                            $user_email = $value['sp_email'];
                            $user_address = $value['sp_address'];
                            $sp_prof_pic = !empty($value['sp_prof_pic']) ? $value['sp_prof_pic'] : "N/A";
                            $sp_cover_pic = !empty($value['sp_cover_pic']) ? $value['sp_cover_pic'] : "N/A";                           
                            switch ($value['attribute_id']) {                              
                                case 8:
                                    $schoolOrganization = $value['attribute_value'];
                                    break;
                                case 9:
                                    $degree = $value['attribute_value'];
                                    break;
                                case 10:
                                    $specialization = $value['attribute_value'];
                                    break;
                                case 11:
                                    $education_from_date = $value['attribute_value'];
                                    break;
                                case 12:
                                    $education_to_date = $value['attribute_value'];
                                    break;    
                                case 13:
                                    $certification_name = $value['attribute_value'];
                                    break;                                
                                case 14:
                                    $certificate_doc = $value['attribute_value'];
                                    break;
                                case 15:
                                    $police_verify_status = $value['attribute_value'];
                                    break;
                                case 16:
                                    $police_verify_doc = $value['attribute_value'];
                                    break;
                                case 17:
                                    $verification_no = $value['attribute_value'];
                                    break;         
                                default:                                    
                                    break;
                            }
                        }
                    //}


                $data= '
                    <div class="box"> 
                        <div class="box-body">
                            <div class="row">
                                <div class="board">                    
                                    <div class="board-inner">
                                        <ul class="nav nav-tabs" id="myTab">
                                        <div class="liner"></div>

                                        <li class="active">
                                            <a href="#personal" data-toggle="tab" title="Personal information">
                                                <span class="round-tabs one">
                                                    <i class="glyphicon glyphicon-user"></i>
                                                </span> 
                                            </a>
                                        </li>                                      


                 <li><a href="#education" data-toggle="tab" title="Education">
                     <span class="round-tabs three">
                          <i class="glyphicon glyphicon-education"></i>
                     </span> </a>
                     </li>     
                     
                     </ul></div>

                     <div class="tab-content">
                      <div class="tab-pane fade in active" id="personal">

                  
                         <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Name: </label>
                                        <lable>'.$user_name.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Gender: </label>
                                        <lable>'.$user_gender.'</lable>
                                    </div>
                                </div>
                            </div>

                             <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Date Of Birth: </label>
                                        <lable>'.$user_birth_date.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Age: </label>
                                        <lable>'.$user_age.'</lable>
                                    </div>
                                </div>
                            </div>

                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Contact No: </label>
                                        <lable>'.$user_phone_number.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E-Mail: </label>
                                        <lable>'.$user_email.'</lable>
                                    </div>
                                </div>
                            </div>


                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Address: </label>
                                        <lable>'.$user_address.'</lable>
                                    </div>                                    
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Profile Image : </label>
                                        <lable><a data-baseUrl="'.base_url().SERVICE_PROVIDER_TUTOR.'/'.$userId.'/'.'" data-picName="'.$sp_prof_pic.'" data-error="profile image not available." data-headerMsg="Profile picture" data-imgPath="'.base_url().'" class="btn btn-sm btn-info viewPic" href="javascript:void(0)"><i class="fa fa-eye"></i></a></lable>
                                    </div>                                    
                                </div> 
                            </div>       
                            <div class="row">       
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Cover Image : </label>
                                        <lable><a data-baseUrl="'.base_url().SERVICE_PROVIDER_TUTOR.'/'.$userId.'/'.'" data-picName="'.$sp_cover_pic.'" data-error="Cover image not available." data-headerMsg="Cover picture" data-imgPath="'.base_url().'" class="btn btn-sm btn-info viewPic" href="javascript:void(0)"><i class="fa fa-eye"></i></a></lable>
                                    </div>                                    
                                </div> 
                            </div>           
                        </div>
                        <div class="tab-pane fade" id="education">
                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>School/College : </label>
                                        <lable>'.$schoolOrganization.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Degree  : </label>
                                        <lable>'.$degree.'</lable>
                                    </div>
                                </div>
                            </div>

                             <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Specialization : </label>
                                        <lable>'.$specialization.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>From : </label>
                                        <lable>'.$education_from_date.'</lable>
                                    </div>
                                </div>
                            </div>

                             <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>To : </label>
                                        <lable>'.$education_to_date.'</lable>
                                    </div>                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Certification Name : </label>
                                        <lable>'.$certification_name.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Police verify status : </label>
                                        <lable>'.$police_verify_status.'</lable>
                                    </div>                                    
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Police verify no: </label>
                                        <lable>'.$verification_no.'</lable>
                                    </div>                                    
                                </div>                                
                            </div>

                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Police verify doc : </label>
                                        <lable><a data-baseUrl="'.base_url().SERVICE_PROVIDER_TUTOR.'/'.$userId.'/'.'" data-picName="'.$police_verify_doc.'" data-error="Police verify doc." data-headerMsg="Police verify doc" data-imgPath="'.base_url().'" class="btn btn-sm btn-info viewPic" href="javascript:void(0)"><i class="fa fa-eye"></i></a></lable>
                                    </div>                                    
                                </div>                               
                            </div>

                        </div>                    
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>'; 
               echo json_encode(array("status" => TRUE,"data" => $data, "titleHeader" => $titleHeader));             
          }
            else { echo(json_encode(array("status"=>FALSE))); } 
                    
            } else {
                echo(json_encode(array('status'=>'logout')));
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }

    public function viewServiceProviderDriverInfo() // view data of service provider Driver
    {
        if(is_ajax_request()) {

            if(is_user_logged_in()) {                               
                $userId = $this->encrypt->decode($this->input->post('userId'));          
                $join = array(TB_SERVICE_PROVIDER_VEHICLE => TB_SERVICE_PROVIDER_VEHICLE.'.service_provider_id='.TB_SERVICE_PROVIDER.'.service_provider_id', TB_VEHICLE_CAT => TB_VEHICLE_CAT.'.vehicle_category_id ='.TB_SERVICE_PROVIDER_VEHICLE.'.vehicle_category_id');
                $resultServiceProviderViewList = $this->Common_model->select_join("sp_fullname, sp_email, sp_phone_number, sp_address, sp_gender, DATE_FORMAT(sp_birth_date,'%m/%d/%Y')as user_birth_date, DATE_FORMAT(FROM_DAYS(DATEDIFF(CURDATE(), sp_birth_date)), '%Y')+0 AS user_age, sp_prof_pic, sp_cover_pic, vehicle_category_name, description, seating_capacity, car_model, car_reg_no, car_manufacturing_year, car_pic, car_number_plate_pic, car_number_plate, licence_scan_copy, rate_per_mile, certification_pics", TB_SERVICE_PROVIDER, array("service_provider_ut.service_provider_id" => $userId, "service_provider_ut.is_deleted" => "0", "is_block" => "0"),array(),array(),
                $join,null);
                if ($resultServiceProviderViewList) { 
                    $resultSPDetails = $this->Common_model->select_join(TB_ATTRIBUTE_VALUES.".attribute_id, attribute_value, attribute_description", TB_ATTRIBUTE_VALUES, array("service_provider_id" => $userId, "status" => "1"), array(), array(), array(TB_ATTRIBUTE_MASTER => TB_ATTRIBUTE_MASTER.".attribute_id =".TB_ATTRIBUTE_VALUES.".attribute_id"), null);
                    $is_care = $care_admin_approved = $is_your_own_car = $driving_license_type = $license_no = $lic_valid_from_date = $lic_valid_until_date = $schoolOrganization = $degree = $specialization = $education_from_date = $education_to_date = $certification_name = $certificate_doc = $police_verify_status = $police_verify_doc = $verification_no = "N/A";
                    if(count($resultSPDetails) > 0) {
                        foreach ($resultSPDetails as $key => $value) {
                            switch ($value['attribute_id']) {
                                case 1:
                                    $is_care = $value['attribute_value'];
                                    break;                                
                                case 2:
                                    $care_admin_approved = $value['attribute_value'];
                                    break;
                                case 3:
                                    $is_your_own_car = $value['attribute_value'];
                                    break;
                                case 4:
                                    $driving_license_type = $value['attribute_value'];
                                    break;
                                case 5:
                                    $license_no = $value['attribute_value'];
                                    break;
                                case 6:
                                    $lic_valid_from_date = $value['attribute_value'];
                                    break;    
                                case 7:
                                    $lic_valid_until_date = $value['attribute_value'];
                                    break;                                
                                case 8:
                                    $schoolOrganization = $value['attribute_value'];
                                    break;
                                case 9:
                                    $degree = $value['attribute_value'];
                                    break;
                                case 10:
                                    $specialization = $value['attribute_value'];
                                    break;
                                case 11:
                                    $education_from_date = $value['attribute_value'];
                                    break;
                                case 12:
                                    $education_to_date = $value['attribute_value'];
                                    break;    
                                case 13:
                                    $certification_name = $value['attribute_value'];
                                    break;                                
                                case 14:
                                    $certificate_doc = $value['attribute_value'];
                                    break;
                                case 15:
                                    $police_verify_status = $value['attribute_value'];
                                    break;
                                case 16:
                                    $police_verify_doc = $value['attribute_value'];
                                    break;
                                case 17:
                                    $verification_no = $value['attribute_value'];
                                    break;         
                                default:                                    
                                    break;
                            }
                        }
                    }

                $user_name= !empty($resultServiceProviderViewList[0]['sp_fullname'])?$resultServiceProviderViewList[0]['sp_fullname']:"N/A";
                $user_gender = !empty($resultServiceProviderViewList[0]['sp_gender'])?$resultServiceProviderViewList[0]['sp_gender']:"N/A";
                $user_birth_date = !empty($resultServiceProviderViewList[0]['user_birth_date'])?$resultServiceProviderViewList[0]['user_birth_date']:"N/A";
                $user_age = !empty($resultServiceProviderViewList[0]['user_age'])?$resultServiceProviderViewList[0]['user_age']:"N/A";
                $user_phone_number=!empty($resultServiceProviderViewList[0]['sp_phone_number'])?$resultServiceProviderViewList[0]['sp_phone_number']:"N/A";
                $user_email = !empty($resultServiceProviderViewList[0]['sp_email'])?$resultServiceProviderViewList[0]['sp_email']:"N/A";
                $user_address = !empty($resultServiceProviderViewList[0]['sp_address'])?$resultServiceProviderViewList[0]['sp_address']:"N/A";
                $sp_prof_pic = !empty($resultServiceProviderViewList[0]['sp_prof_pic'])?$resultServiceProviderViewList[0]['sp_prof_pic']:"N/A";
                $sp_cover_pic = !empty($resultServiceProviderViewList[0]['sp_cover_pic'])?$resultServiceProviderViewList[0]['sp_cover_pic']:"N/A";
                $car_model = !empty($resultServiceProviderViewList[0]['car_model'])?$resultServiceProviderViewList[0]['car_model']:"N/A";
                $car_register_no = !empty($resultServiceProviderViewList[0]['car_reg_no'])?$resultServiceProviderViewList[0]['car_reg_no']:"N/A";
                $car_seating_capacity = !empty($resultServiceProviderViewList[0]['seating_capacity'])?$resultServiceProviderViewList[0]['seating_capacity']:"N/A";
                $vehicle_category_name = !empty($resultServiceProviderViewList[0]['vehicle_category_name'])?$resultServiceProviderViewList[0]['vehicle_category_name']:"N/A";
                $description = !empty($resultServiceProviderViewList[0]['description'])?$resultServiceProviderViewList[0]['description']:"N/A";
                $rate_per_mile = !empty($resultServiceProviderViewList[0]['rate_per_mile'])?$resultServiceProviderViewList[0]['rate_per_mile']:"N/A";
                $car_manufacturing_year = !empty($resultServiceProviderViewList[0]['car_manufacturing_year'])?$resultServiceProviderViewList[0]['car_manufacturing_year']:"N/A"; 
                $car_number_plate = !empty($resultServiceProviderViewList[0]['car_number_plate'])?$resultServiceProviderViewList[0]['car_number_plate']:"N/A"; 
                $car_number_plate_pic = !empty($resultServiceProviderViewList[0]['car_number_plate_pic'])?$resultServiceProviderViewList[0]['car_number_plate_pic']:"N/A"; 
                $car_pic = !empty($resultServiceProviderViewList[0]['car_pic'])?$resultServiceProviderViewList[0]['car_pic']:"N/A"; 
                $license_pic = !empty($resultServiceProviderViewList[0]['licence_scan_copy'])?$resultServiceProviderViewList[0]['licence_scan_copy']:"N/A"; 
                $certificate = !empty($resultServiceProviderViewList[0]['certification_pics'])?$resultServiceProviderViewList[0]['certification_pics']:"N/A"; 
                $data= '
                    <div class="box"> 
                        <div class="box-body">
                            <div class="row">
                                <div class="board">                    
                                    <div class="board-inner">
                                        <ul class="nav nav-tabs" id="myTab">
                                        <div class="liner"></div>

                                        <li class="active">
                                            <a href="#personal" data-toggle="tab" title="Personal information">
                                                <span class="round-tabs one">
                                                    <i class="glyphicon glyphicon-user"></i>
                                                </span> 
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#driver" data-toggle="tab" title="Driver Details">
                                                <span class="round-tabs two">
                                                    <i class="fa fa-car"></i>
                                                </span> 
                                            </a>
                                        </li>


                 <li><a href="#education" data-toggle="tab" title="Education">
                     <span class="round-tabs three">
                          <i class="glyphicon glyphicon-education"></i>
                     </span> </a>
                     </li>     
                     
                     </ul></div>

                     <div class="tab-content">
                      <div class="tab-pane fade in active" id="personal">

                  
                         <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Name: </label>
                                        <lable>'.$user_name.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Gender: </label>
                                        <lable>'.$user_gender.'</lable>
                                    </div>
                                </div>
                            </div>

                             <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Date Of Birth: </label>
                                        <lable>'.$user_birth_date.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Age: </label>
                                        <lable>'.$user_age.'</lable>
                                    </div>
                                </div>
                            </div>

                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Contact No: </label>
                                        <lable>'.$user_phone_number.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E-Mail: </label>
                                        <lable>'.$user_email.'</lable>
                                    </div>
                                </div>
                            </div>


                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Address: </label>
                                        <lable>'.$user_address.'</lable>
                                    </div>                                    
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Profile Image : </label>
                                        <lable><a data-baseUrl="'.base_url().SERVICE_PROVIDER_DRIVER.'/'.$userId.'/'.'" data-picName="'.$sp_prof_pic.'" data-error="profile image not available." data-headerMsg="Profile picture" data-imgPath="'.base_url().'" class="btn btn-sm btn-info viewPic" href="javascript:void(0)"><i class="fa fa-eye"></i></a></lable>
                                    </div>                                    
                                </div> 
                            </div>       
                            <div class="row">       
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Cover Image : </label>
                                        <lable><a data-baseUrl="'.base_url().SERVICE_PROVIDER_DRIVER.'/'.$userId.'/'.'" data-picName="'.$sp_cover_pic.'" data-error="Cover image not available." data-headerMsg="Cover picture" data-imgPath="'.base_url().'" class="btn btn-sm btn-info viewPic" href="javascript:void(0)"><i class="fa fa-eye"></i></a></lable>
                                    </div>                                    
                                </div> 
                            </div>           
                        </div>

                        <div class="tab-pane fade" id="driver">  
                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Is care : </label>
                                        <lable>'.$is_care.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Care admin approve: </label>
                                        <lable>'.$care_admin_approved.'</lable>
                                    </div>
                                </div>
                            </div>                       
                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Do you have your own car ? : </label>
                                        <lable>'.$is_your_own_car.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Driving License: </label>
                                        <lable>'.$driving_license_type.'</lable>
                                    </div>
                                </div>
                            </div>

                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>License Number: </label>
                                        <lable>'.$license_no.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Valid From: </label>
                                        <lable>'.$lic_valid_from_date.'</lable>
                                    </div>
                                </div>
                            </div>


                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Valid Until: </label>
                                        <lable>'.$lic_valid_until_date.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Car Model: </label>
                                        <lable>'.$car_model.'</lable>
                                    </div>
                                </div>
                            </div>

                             <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Car Reg. No: </label>
                                        <lable>'.$car_register_no.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Car Seating Capacity: </label>
                                        <lable>'.$car_seating_capacity.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Police verify status: </label>
                                        <lable>'.$police_verify_status.'</lable>
                                    </div>                                    
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Rate per mile: </label>
                                        <lable>'.$rate_per_mile.'</lable>
                                    </div>                                    
                                </div>                                
                            </div>
                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Car Manufacturing Year: </label>
                                        <lable>'.$car_manufacturing_year.'</lable>
                                    </div>                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Car Number Plate Image : </label>
                                        <lable><a data-baseUrl="'.base_url().SERVICE_PROVIDER_DRIVER.'/'.$userId.'/'.'" data-picName="'.$car_number_plate_pic.'" data-error="Car number plate picture not available." data-headerMsg="Car number plate picture" data-imgPath="'.base_url().'" class="btn btn-sm btn-info viewPic" href="javascript:void(0)"><i class="fa fa-eye"></i></a></lable>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Car number plate: </label>
                                        <lable>'.$car_number_plate.'</lable>
                                    </div>                                    
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Police verify doc : </label>
                                        <lable><a data-baseUrl="'.base_url().SERVICE_PROVIDER_DRIVER.'/'.$userId.'/'.'" data-picName="'.$police_verify_doc.'" data-error="Police verify doc." data-headerMsg="Police verify doc" data-imgPath="'.base_url().'" class="btn btn-sm btn-info viewPic" href="javascript:void(0)"><i class="fa fa-eye"></i></a></lable>
                                    </div>                                    
                                </div>

                            </div>

                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Car Image : </label>
                                        <lable><a data-baseUrl="'.base_url().SERVICE_PROVIDER_DRIVER.'/'.$userId.'/'.'" data-picName="'.$car_pic.'" data-error="Car picture not available." data-headerMsg="Car picture" data-imgPath="'.base_url().'" class="btn btn-sm btn-info viewPic" href="javascript:void(0)"><i class="fa fa-eye"></i></a></lable>
                                    </div>                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Scanned Copy Of License  : </label>
                                        <lable><a data-baseUrl="'.base_url().SERVICE_PROVIDER_DRIVER.'/'.$userId.'/'.'" data-picName="'.$license_pic.'" data-error="Scanned copy of license not available." data-headerMsg="Scanned copy of license" data-imgPath="'.base_url().'" class="btn btn-sm btn-info viewPic" href="javascript:void(0)"><i class="fa fa-eye"></i></a></lable>
                                    </div>
                                </div>
                            </div>                                                   
                        </div>

                        <div class="tab-pane fade" id="education">
                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>School/College : </label>
                                        <lable>'.$schoolOrganization.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Degree  : </label>
                                        <lable>'.$degree.'</lable>
                                    </div>
                                </div>
                            </div>

                             <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Specialization : </label>
                                        <lable>'.$specialization.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>From : </label>
                                        <lable>'.$education_from_date.'</lable>
                                    </div>
                                </div>
                            </div>

                             <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>To : </label>
                                        <lable>'.$education_to_date.'</lable>
                                    </div>                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Certification Name : </label>
                                        <lable>'.$certification_name.'</lable>
                                    </div>
                                </div>
                            </div>

                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Certification Image : </label>
                                        <lable><a data-baseUrl="'.base_url().SERVICE_PROVIDER_DRIVER.'/'.$userId.'/'.'" data-picName="'.$certificate.'" data-error="Certificate not available." data-headerMsg="Certificate" data-imgPath="'.base_url().'" class="btn btn-sm btn-info viewPic" href="javascript:void(0)"><i class="fa fa-eye"></i></a></lable>
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>                    
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>'; 
               echo json_encode(array("status" => TRUE,"data" => $data, "titleHeader" => "Service Provider Driver Details"));             
          }
            else { echo(json_encode(array("status"=>FALSE))); }        
            } else {
                echo(json_encode(array('status'=>'logout')));
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }   
}