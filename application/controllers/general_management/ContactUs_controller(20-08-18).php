<?php 

/*

Author : Rajendra pawar 
Page :  ContactUs_controller.php
Description : Contact Us controller use for contact us functionality

*/


if(!defined('BASEPATH')) exit('No direct script access allowed');
 
 require APPPATH . '/libraries/BaseController.php';

class ContactUs_controller extends BaseController
{
	public function __construct()
    {
       ob_start();
        parent::__construct();
        $this->load->model('general_management/contact_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
        $this->isLoggedIn();   
    }
    public function index()
    {
        $this->data = array(
            'pageTitle' => 'Tulii : General management',
            'isActive' => 'active'         
        );
        $this->load->view("includes/header",$this->data);
        $this->load->view('dashboard',$this->data);            
        $this->load->view("includes/footer");   
    	//$this->global['pageTitle'] = 'Tulii : General management';
      //$this->loadViews("dashboard", $this->global, NULL , NULL);
            
    }

  function contactDetails()
  { 
    if($this->isAdmin() == TRUE)
    {
        $this->data = array(
            'pageTitle' => 'Tulii : General management',
            'isActive' => 'active'            
        );
        $this->load->view("includes/header",$this->data);
        $this->load->view('general_management/contactUs',$this->data);            
        $this->load->view("includes/footer");

     // $this->global['pageTitle'] = 'Tulii : General management';
      //$this->loadViews("general_management/contactUs", $this->global, NULL, NULL);
    }
    else
    {
      $this->loadThis();          	
    }

}
   function deleteContact()
    {  

        if($this->isAdmin() == TRUE)
        {
          $contactId = $this->input->post('contactId');
            $contactInfo = array('isDeleted'=>1, 'updated_at'=>date('Y-m-d H:i:s'),
                'updatedBy'=>$this->vendorId);
            
            $result = $this->contact_model->deleteContact($contactId, $contactInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
        else
        {
          echo(json_encode(array('status'=>'access')));    
        }
    }
    function contactlist()
    {
      if($this->isAdmin() == TRUE)
        {
          $list = $this->contact_model->get_datatables();

        $data = array();
        $no = $_POST['start'];
        
         foreach($list as $record)
                        {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $record->contact_name;
            $row[] = $record->contact_email;
            $row[] = $record->contact_phone_number;
            //$row[] = '<a class="btn btn-sm btn-info viewContact" href="javascript:void(0)" data-contactid="'. $record->contact_id .'"   data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>'; 
            //$record->contact_message;
            $row[] = '<a class="btn btn-sm btn-info viewContact" href="javascript:void(0)" data-contactid="'. $record->contact_id .'"   data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>
            <a class="btn btn-sm btn-danger deleteContact" href="javascript:void(0)" data-contactid="'. $record->contact_id .'"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
         }

          $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->contact_model->count_all(),
                        "recordsFiltered" => $this->contact_model->count_filtered(),
                        "data" => $data,
                );          

          echo json_encode($output);
           
        }
        else
        {
        
      $this->loadThis();
    }   
           
    }
    function viewContactDetails()
    {
        if($this->isAdmin() == TRUE)
        {
            $contactId = $this->input->post('contactId');
          
            $result=$this->common_model->select("contact_message",TB_CONTACT,array("contact_id"=>$contactId));
            

                if ($result > 0) { 

                $contact_message= $result[0]['contact_message'];

                $data='<div class="box">
                     <form role="form"  method="post">
                        <div class="box-body">
                            <div class="row">                          
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                      <lable>'.$contact_message.'</lable>
                                    </div>
                                    
                                </div>
                              </div>                              

                        </div><!-- /.box-body -->   
                         </form>                
                </div>'; 
               echo json_encode(array("status" => TRUE,"data" => $data));             
          }
            else { echo(json_encode(array("status"=>FALSE))); }        
        }
        else
        {
          echo(json_encode(array('status'=>'access')));                 
        }
    }
}