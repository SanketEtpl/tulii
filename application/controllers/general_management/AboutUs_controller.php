<?php 

/*
Author : Kiran  
Page :  AboutUs_controller.php
Description : About Us controller use for About us functionality
*/
if(!defined('BASEPATH')) exit('No direct script access allowed');
 
  class AboutUs_controller extends CI_Controller
  {
	  public function __construct()
    {
      ob_start();
      parent::__construct();
      $this->data = array(
        'pageTitle' => 'Tulii : General management of about us',
        'isActive' => 'active',
        'userRecords' => $this->Common_model->select("*", TB_ABOUT, array("status" => "1"))         
      );
    }
    
    // Default view page
    public function index()
    {
      if (is_user_logged_in()) {
        $this->load->view("includes/header",$this->data);
        $this->load->view('general_management/aboutUs',$this->data);            
        $this->load->view("includes/footer");   
      } else {
        $this->session->sess_destroy();
        redirect('login');
      }     
    }

  // About us updated here
  function updateAbout()
  { 
    if(is_ajax_request()) {
      if(is_user_logged_in()) {
        $msg = '';
        if(!empty($this->input->post('about_id'))) {
          $cond = array("about_id" => $this->input->post('about_id')); 
          $updateInfo = array('message' => $this->input->post('message_body'), 'update_date' =>date('Y-m-d H:i:s'));
          $resultAboutUs = $this->Common_model->update(TB_ABOUT, $cond, $updateInfo);  
          $msg = "About-us has been updated successfully.";
        } else {
          $insertInfo = array('message' => $this->input->post('message_body'), 'created_date' =>date('Y-m-d H:i:s'));
          $resultAboutUs = $this->Common_model->insert(TB_ABOUT, $insertInfo);    
          $msg = "About-us has been inserted successfully.";
        }
        // update result
        if($resultAboutUs) { 
          echo(json_encode(array('status' => TRUE, 'msg' => $msg))); 
        } else { 
          echo(json_encode(array('status' => FALSE, 'msg' => 'Something went wrong.'))); 
        }
      } else {
        echo(json_encode(array('status'=>'logout')));
        $this->session->sess_destroy();
        redirect('login');
      } 
    }     
  }
}

