<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Home_controller extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Common_model');
    $this->load->library("email"); 
    $this->load->helper("email_template"); 
    $this->load->helper('string');      
  }
  public function index()
  {
    $banner = $this->Common_model->fetch_no_of_records("*",TB_BANNER,array("banner_status"=>'1'),5,0);
    $this->load->view("frontend/index",array("banner"=>$banner));
  }

  public function about()
  {
    $this->load->view("frontend/about");
  }
  
  public function services()
  {
    $this->load->view("frontend/services");
  }
  
  public function safety()
  {
    $this->load->view("frontend/safety");
  }

  public function faq()
  {
    $faqData = $this->Common_model->select("*",TB_FAQ);
    $this->load->view("frontend/faq",array("faq"=>$faqData));
  }

  public function career()
  {
    $this->load->view("frontend/career");
  }

  public function pricing()
  {
    $this->load->view("frontend/pricing");
  }

  public function contact()
  {
    $this->load->view("frontend/contact");
  }

  public function videos()
  {
    $this->load->view("frontend/videos");
  }

  public function download()
  {
    $this->load->view("frontend/download");
  }

  public function contactUs() 
  {
    $postData = $this->input->post();
    $data = array(
      "contact_name" =>$postData['fname'],
      "contact_email"=>$postData['email'],
      "contact_phone_number"=>$postData['mobileno'],
      "contact_message"=>$postData['message'],
      "contact_subject"=>$postData['subject'],      
      "created_at"=>date("Y-m-d H:s:i")           
      );
    $contactUs = $this->Common_model->insert(TB_CONTACT,$data);
    if($contactUs){
      $hostname = $this->config->item('hostname');
      $config['mailtype'] ='html';
      $config['charset'] ='iso-8859-1';
      $this->email->initialize($config);
      $from  = EMAIL_FROM; 
      $this->messageBody  = email_header();
      $this->messageBody  .= '<tr> 
          <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
              <p>Hello,</p>
                  <p> Thanks for contact us as soon as possible we will be contact soon.</p>
              </td>
          </tr>
          ';                      
      $this->messageBody  .= email_footer();
      $this->email->from($from, $from);
      $this->email->to($postData['email']);
      $this->email->subject('Tulii admin');
      $this->email->message($this->messageBody);
      $send = $this->email->send();      
      echo json_encode(array("status"=>true,"action"=>"insert","message"=>"Your message has been send successfully, as soon as possible we will contact soon.")); exit;    
    }else{
      echo json_encode(array("status"=>false,"action"=>"insert","message"=>"Please try again.")); exit; 
    }
  } 
}

