<?php 
/*
Author : Rajendra pawar 
Page :  service_controller.php
Description : service provider details
*/
if(!defined('BASEPATH')) exit('No direct script access allowed'); 
 require APPPATH . '/libraries/BaseController.php';

class Service_controller extends BaseController
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model('service_provider/service_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
        $this->isLoggedIn();   
    }
    public function index()
    {
    	$this->global['pageTitle'] = 'Tulii : Dashboard';        
        $this->loadViews("dashboard", $this->global, NULL , NULL);            
    }

  function service()
  { 
        if($this->isAdminOrServiceProvider() == TRUE)
        {
         $this->global['pageTitle'] = 'Tulii : Service provider';
         $this->loadViews("service_provider/service", $this->global, NULL , NULL);         
        }
        else
        {         
         $this->loadThis();          
    }

}
 

  function serviceList()
  { 
        if($this->isAdminOrServiceProvider() == TRUE)
        {
         $category = $this->input->post('category');    
         $list = $this->service_model->get_datatables(array("tbl_subcategories.status ="=>1,"tbl_categories.status"=>1,"tbl_users.isDeleted"=>0,"tbl_roles.role"=>"Service provider","tbl_subcategories.name"=>$category));  
        $data = array();
        $no = $_POST['start'];
        $status_id=1;
        
        foreach($list as $record)
          {
            $button ="";
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id; 
            if($record->user_status == 1)
                       {  
                        $a= $record->user_id.",1,'$rowActive_id','$rowInActive_id'";
                        $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="serviceStatus_popup('.$a.');" ';
                           
                           $b= $record->user_id.",0,'$rowActive_id','$rowInActive_id'";
                         $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="serviceStatus_popup('.$b.');"';  } 
                    else{ 
                        $c= $record->user_id.",1,'$rowActive_id','$rowInActive_id'";
                        $active_btn_class='class="btn btn-sm btn-active-disable" onclick="serviceStatus_popup('.$c.');"';
                        $d= $record->user_id.",0,'$rowActive_id','$rowInActive_id'";
                          $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="serviceStatus_popup('.$d.');" ';
                      }

                $button ='<button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button>&nbsp;';
                $button.='<button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button>';
            
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $record->user_name;
            $row[] = $record->user_email;
            $row[] = $record->user_phone_number;
            $row[] = $record->user_gender;
            $row[] = $button;
            $row[] = '<a data-userid="'.$record->user_id.'"  class="btn btn-sm btn-info viewService"  href="javascript:void(0)"><i class="fa fa-eye"></i></a>
            <a data-userid="'. $this->encrypt->encode($record->user_id).'" data-category="'.$category.'"  class="btn btn-sm btn-danger deleteService" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';            
            $data[] = $row;
           $status_id ++; 
          }

          $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->service_model->count_all(),
                       "recordsFiltered" => $this->service_model->count_filtered(array("tbl_subcategories.status ="=>1,"tbl_categories.status"=>1,"tbl_roles.role"=>"Service provider","tbl_users.isDeleted"=>0,"tbl_subcategories.name"=>$category)),
                        "data" => $data,
                         );          

          echo json_encode($output);         
        }
        else
        {
      $this->loadThis();   
    }

}
  function viewServiceDetails()
    {
        if($this->isAdminOrServiceProvider() == TRUE)
        {
            $userId = $this->input->post('userId');
          
            $result=$this->common_model->select_join("user_name,user_email,user_phone_number,user_gender,user_address,driving_license_type,license_no,valid_from_date,valid_until_date,
            car_model,car_register_no,car_age,name,category",TB_USERS,array("user_id"=>$userId),array(),array(),
            array(TB_CATEGORIES=>'tbl_users.user_category_id = tbl_categories.category_id',TB_SUBCATEGORIES=>'tbl_users.user_sub_category_id = tbl_subcategories.sub_category_id'),null);
            

                if ($result > 0) { 

                $user_name= $result[0]['user_name'];
                $user_phone_number=$result[0]['user_phone_number'];
                $user_email = $result[0]['user_email'];
                $user_gender = $result[0]['user_gender'];
                $user_address = $result[0]['user_address'];
                $driving_license_type = $result[0]['driving_license_type'];
                $license_no = $result[0]['license_no'];

                if($result[0]['valid_from_date'] != '0000-00-00') {
                $valid_from_date =  date("d/m/Y", strtotime($result[0]['valid_from_date']));
                } else {$valid_from_date = '';}
                if($result[0]['valid_until_date'] != '0000-00-00') {
                $valid_until_date = date("d/m/Y", strtotime($result[0]['valid_until_date'])); 
                } else {$valid_until_date = '';}                
                
                $car_model = $result[0]['car_model'];
                $car_register_no = $result[0]['car_register_no'];
                $car_age = $result[0]['car_age'];
                $category = $result[0]['category'];
                $sub_category = $result[0]['name'];

                $data='<div class="box">
                     <form role="form"  method="post">
                        <div class="box-body">
                            <div class="row">                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label>Name: </label>
                                        <lable>'.$user_name.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobile: </label>
                                        <lable>'.$user_phone_number.'</lable>
                                    </div>
                                </div>
                            </div>

                            <div class="row">      
                            <div class="col-md-6">                                
                                    <div class="form-group">
                                    <label>Email: </label>
                                    <lable>'.$user_email.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label >Gender: </label>
                                        <lable>'.$user_gender.'</lable>
                                    </div>
                                </div>
                            </div>

                             <div class="row">      
                            <div class="col-md-6">                                
                                    <div class="form-group">
                                    <label>Address: </label>
                                    <lable>'.$user_address.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label >Driving License Type: </label>
                                        <lable>'.$driving_license_type.'</lable>
                                    </div>
                                </div>
                            </div>

                            <div class="row">      
                            <div class="col-md-6">                                
                                    <div class="form-group">
                                    <label>License Number: </label>
                                    <lable>'.$license_no.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label >Valid From Date: </label>
                                        <lable>'.$valid_from_date.'</lable>
                                    </div>
                                </div>
                            </div>

                            <div class="row">      
                            <div class="col-md-6">                                
                                    <div class="form-group">
                                    <label>Valid Until Date: </label>
                                    <lable>'.$valid_until_date.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label >Car Model: </label>
                                        <lable>'.$car_model.'</lable>
                                    </div>
                                </div>
                            </div>

                            <div class="row">      
                            <div class="col-md-6">                                
                                    <div class="form-group">
                                    <label>Car Register Number: </label>
                                    <lable>'.$car_register_no.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label >Car Age: </label>
                                        <lable>'.$car_age.'</lable>
                                    </div>
                                </div>
                            </div>
                            
                             <div class="row">      
                            <div class="col-md-6">                                
                                    <div class="form-group">
                                    <label>Category: </label>
                                    <lable>'.$category.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label >Sub Category: </label>
                                        <lable>'.$sub_category.'</lable>
                                    </div>
                                </div>
                            </div>
                               

                        </div><!-- /.box-body -->   
                         </form>                
                </div>'; 
               echo json_encode(array("status" => TRUE,"data" => $data));             
          }
            else { echo(json_encode(array("status"=>FALSE))); }        
        }
        else
        {
          echo(json_encode(array('status'=>'access')));                 
        }
    }
    function serviceStatus()
    {
        if($this->isAdminOrServiceProvider() == TRUE)
        {
           $userId = $this->input->post('userId');
            $user_status = $this->input->post('user_status');
            $result = $this->common_model->update(TB_USERS,array("user_id"=>$userId),array('user_status'=>$user_status,'updatedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s')));
            
            if ($result > 0) { 
             echo(json_encode(array('status'=>TRUE))); 

          }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
        else
        {
         echo(json_encode(array('status'=>'access')));  
        }
    }
    public function deleteServiceUser() 
    {
        if(is_ajax_request())
        {
             if($this->isAdminOrServiceProvider() == TRUE){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->update(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])),array('isDeleted'=>1));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Service provider has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                echo(json_encode(array('status'=>'access')));
            }
        }
    }
}
?>