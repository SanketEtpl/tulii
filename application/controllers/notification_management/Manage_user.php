<?php
/*
* @author : kiran N.
* page : Notification list controller
* description: Show the all data about notification list
*/
defined('BASEPATH') OR exit('No direct script access allowed');
 require APPPATH . '/libraries/BaseController.php'; 
class Manage_user extends BaseController {
 
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Notification management',
            'isActive'  => 'active'      
        );
        $this->load->model('Manage_user_model','manageUser');
        $this->isLoggedIn();   
    }
 
    public function index()
    {
        if($this->isAdmin() == TRUE){
            $this->load->helper('url'); 
            $this->load->view('includes/header',$this->data);   
            $this->load->view('notification/manage_user',$this->data);
            $this->load->view('includes/footer');
        } else {
            $this->loadThis();       
        } 
    }
 
    public function ajax_list() // list of after school care data
    {
        if($this->isAdmin() == TRUE){
            $list = $this->manageUser->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
          /*  $checkbox="";
            $isChecked = $_POST['ischecked'];
            if($isChecked == "true")
            $checkbox = "checked";  */ 
            foreach ($list as $userData) {
                $userId = $this->encrypt->encode($userData->user_id);  
                $no++;
                $row = array();
                //onclick="selectAllCheckBox(this);" 
                $row[] = '<div class="custombox"><input type="checkbox"  name="notification_id" value="'.$userData->user_id.'" class="chkListOfUsers" ><span class="checkspan"></span></div>';//$no;
                $row[] = $userData->user_email;
                $row[] = $userData->user_name;
                $row[] = $userData->user_phone_number;
                $row[] = $userData->role; 
               /* $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="viewNotification(this)" href="javascript:void(0)">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteNotification(this)" href="javascript:void(0)">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';*/
                $data[] = $row;               
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->manageUser->count_all(),
                            "recordsFiltered" => $this->manageUser->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
             echo(json_encode(array('status'=>'access')));    
        } 
    }

    public function deleteNotification() // delete record of notification
    {
        if(is_ajax_request())
        {
           if($this->isAdmin() == TRUE){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->delete(TB_NOTIFICATION,array('n_id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Notification list has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                echo(json_encode(array('status'=>'access')));    
            } 
        }
    }

    public function viewNotifInfo() // view data of notification
    {
        if(is_ajax_request())
        {
            if($this->isAdmin() == TRUE){
                $postData = $this->input->post();     
                if($postData["key"]){
                    $cond = array("tbl_notification.n_id" => $this->encrypt->decode($postData['key']));
                    $jointype=array("tbl_users"=>"INNER");
                    $join = array("tbl_users"=>"tbl_users.user_id = tbl_notification.user_id");
                    $notifListData = $this->Common_model->selectJoin("n_content,user_name,user_phone_number,n_read,user_email,roleId",TB_NOTIFICATION,$cond,array(),$join,$jointype);
                    if($notifListData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","notifData"=>$notifListData[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                echo(json_encode(array('status'=>'access')));    
            } 
        }
    }   

    public function sendNotification()
    {
        if(is_ajax_request())
        {
            if($this->isAdmin() == TRUE){
                $postData = $this->input->post();
                $arr =explode(',',$postData['user_key']);
                $pushNotif = array();
                if(!empty($arr))
                {                    
                    foreach ($arr as $key => $value) {
                          $pushNotif[] = array(
                            'n_content' =>$postData['message'],
                            'n_read' => 1,
                            'user_id' =>$value
                            );               
                    }
                    $sendInBatch = $this->Common_model->insert_batch(TB_NOTIFICATION,$pushNotif);
                    if($sendInBatch){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","msg"=>"Notification has been sent successfully.")); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }

                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Something went wrong.")); exit;   
                }

                } else {
                echo(json_encode(array('status'=>'access')));    
            } 
        }    
    }
}