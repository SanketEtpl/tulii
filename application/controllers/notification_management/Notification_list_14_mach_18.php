<?php
/*
* @author : kiran N.
* page : Notification list controller
* description: Show the all data about notification list
*/
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Notification_list extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Notification list'        
        );
        $this->load->model('Notification_list_model','notification');
    }
 
    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');    
            $this->load->view('notification/notification_list',$this->data);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        } 
    }
 
    public function ajax_list() // list of after school care data
    {
        if(is_user_logged_in()){
            $list = $this->notification->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            foreach ($list as $notifData) {
                $userId = $this->encrypt->encode($notifData->n_id);  
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $notifData->n_content;
                $row[] = $notifData->user_name;
                $row[] = $notifData->user_phone_number;
                $row[] = $notifData->n_read == 1 ? 'Unread' : 'Read';
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="viewNotification(this)" href="javascript:void(0)">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteNotification(this)" href="javascript:void(0)">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;               
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->notification->count_all(),
                            "recordsFiltered" => $this->notification->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        } 
    }

    public function deleteNotification() // delete record of notification
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->delete(TB_NOTIFICATION,array('n_id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Notification list has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }

    public function viewNotifInfo() // view data of notification
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();     
                if($postData["key"]){
                    $cond = array("tbl_notification.n_id" => $this->encrypt->decode($postData['key']));
                    $jointype=array("tbl_users"=>"INNER");
                    $join = array("tbl_users"=>"tbl_users.user_id = tbl_notification.user_id");
                    $notifListData = $this->Common_model->selectJoin("n_content,user_name,user_phone_number,n_read,user_email,roleId",TB_NOTIFICATION,$cond,array(),$join,$jointype);
                    if($notifListData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","notifData"=>$notifListData[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }   
}