<?php
/*
* @author : Ram K.
* description: manage the payment data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Group extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Group management', 
            'isActive'  => 'active'
        );
        // $this->load->model('PaymentSection_model','payment_section');
        $this->load->model("group_model");
        $this->load->model("common_model");
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');  
            $this->load->view('includes/header',$this->data);
            $this->load->view('group_management/group',$this->data);
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of FAQ list data
    {
        //echo "<pre>";print_r('hii');die;
        if(is_user_logged_in()){
          
            $paymentData = $this->group_model->get_datatables();  
            
            $data = array();
            $no = $_POST['start'];
            $i = 1;

            foreach ($paymentData as $payValue) {
                $payID = $this->encrypt->encode($payValue->group_id);
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($payValue->group_name);
                $row[] = $payValue->user_name;
                $row[] = $payValue->created_date;                
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$payID.'" class="btn btn-sm btn-info" onclick="viewMember(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>                
                                                     
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(

                "draw" => $_POST['draw'],
                "recordsTotal" => $this->group_model->count_all(),
                "recordsFiltered" => $this->group_model->count_filtered(),
                "data" => $data,

            );

            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function viewMembersList() // view data of payment  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();             
                if(!empty($postData["key"])) {

                    $group_details = $this->common_model->select("user_id,group_name",TB_GROUP,array("group_id"=>$this->encrypt->decode($postData["key"])));
                    $checkResultIsNotEmpty = !empty($group_details[0]['user_id']) ? $group_details[0]['user_id'] : 0 ;
                    $group_details1 = $this->common_model->select_where_in_with_no_quote("user_id,user_name",TB_USERS,'user_id',$checkResultIsNotEmpty);

                    $rows = '';
                    if (!empty($group_details1)) {
                    foreach ($group_details1 as $member) {
                        
                            $rows .= '                                        
                            <tr>                                              
                              <td  class="text-left">' . $member['user_name'] . '</td>
                              <td  class="text-left">' . $group_details[0]['group_name'] . '</td>
                            </tr>';
                    }
                } else {
                    $rows .= '<tr><td colspan="10" align="center">No Records Available.</td></tr>';
                }

                    if($rows) {                                                
                        echo json_encode(array("status"=>"success","action"=>"view","rows"=>$rows)); exit; 
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                    exit; 
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please provide group id.")); exit;   
                }
            }
            else{
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 
}