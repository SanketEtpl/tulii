<?php
/*
* @author : kiran N.
* page : Ticket controller
* description: show the all ticket data 
*/

defined('BASEPATH') OR exit('No direct script access allowed');   
class Ticket extends CI_Controller {
 
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Tickets',
            'isActive'  => 'active'       
        );
        $this->load->model('Tickets_model','tickets');         
    }
 
    public function index()
    {
        if(is_user_logged_in()) {
            $this->load->view('includes/header',$this->data); 
            $this->load->view('ticket_management/tickets');
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        } 
    }
 
    public function ticketList() // list of issues data
    {
        if(is_user_logged_in()) {
            $resultTicketList = $this->tickets->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id = 1;
            $rowActive_id = "active".$status_id;
            $rowInActive_id = "inActive".$status_id;
            foreach ($resultTicketList as $resData) {
                $tId = $this->encrypt->encode($resData->ticket_id);  
                $button ="";  
                if($resData->status == "0") {   
                    $a= $resData->ticket_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class='class="btn btn-sm btn-warning" onclick="changeIssueStatus_popup('.$a.');" ';
                    $button = '<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Peding"><button id="'.$rowActive_id.'" '.$active_btn_class.'>Pedding</button></span>&nbsp;';
                } else {   
                    $d= $resData->ticket_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class=' disabled class="btn btn-sm btn-success" onclick="changeIssueStatus_popup('.$d.');" ';
                    $button = '<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Resolved"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>Resolved</button></span>&nbsp;';
                }                
                
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $resData->ticket_subject;
                $row[] = $resData->ticket_service;
                $row[] = $resData->ticket_comment;
                $row[] = $button;
                $row[] =
                    '   
                        <a data-id="'.$i.'" data-row-id="'.$tId.'" data-app-id="'.$resData->app_id_from.'" class="btn btn-sm btn-info" onclick="viewIssue(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i>
                        </a>                                                      
                    ';
                $data[] = $row;    
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;           
                $i++;
            }
     
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->tickets->count_all(),
                "recordsFiltered" => $this->tickets->count_filtered(),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        } 
    }

    public function changeIssueStatus() // chnage the status of issue pending or resolved
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_TICKET, array("ticket_id" => $postData['tId']),array('status'=>$postData['ticket_status'], 'update_date' => date('Y-m-d H:i:s')));
            if($result)
                echo(json_encode(array('status' => TRUE))); 
            else
                echo(json_encode(array('status' => FALSE))); 
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }        
    }

    public function viewIssue() // view data of ticket information
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $imgExist =0; 
                $postData = $this->input->post();                   
                if(!empty($postData["key"])) {
                    $cond = array("ticket_id" => $this->encrypt->decode($postData["key"]));
                    $jointype = array(TB_BOOKING => "LEFT",TB_SERVICE_USERS => "LEFT", TB_SERVICE_PROVIDER => "LEFT", TB_CARE_BOOKING => "LEFT", TB_TUTOR_BOOKING => "LEFT", TB_RIDE_BOOKING => "LEFT", TB_TUTOR_CATEGORY => "LEFT", TB_TUTOR_SUBCATEGORY => "LEFT");
                    if($postData["app_id"] == 1) {
                        $join = array(TB_SERVICE_USERS => TB_SERVICE_USERS.".service_user_id = ".TB_TICKET.".ticket_to", TB_SERVICE_PROVIDER => TB_SERVICE_PROVIDER.".service_provider_id = ".TB_TICKET.".ticket_from", TB_BOOKING => TB_BOOKING.".booking_id=".TB_TICKET.".booking_id", TB_CARE_BOOKING => TB_CARE_BOOKING.".booking_id =".TB_TICKET.".booking_id", TB_TUTOR_BOOKING => TB_TUTOR_BOOKING.".booking_id=".TB_TICKET.".booking_id", TB_RIDE_BOOKING => TB_RIDE_BOOKING.".booking_id =".TB_TICKET.".booking_id", TB_TUTOR_CATEGORY => TB_TUTOR_CATEGORY.".tut_cat_id =".TB_TUTOR_BOOKING.".cat_id", TB_TUTOR_SUBCATEGORY => TB_TUTOR_SUBCATEGORY.".tut_sub_cat_id = ".TB_TUTOR_BOOKING.".subcat_id");
                        $resultTicketDetails = $this->Common_model->selectJoin("user_fullname, sp_fullname, ticket_subject, ticket_service, ticket_comment, ticket_ut.status, ticket_attachment, pick_up_point, drop_point, ride_start_date, ride_end_date, care_start_date, care_end_date, care_location, tut_start_date, tut_end_date, category_name, subcategory_name", TB_TICKET, $cond, array(), $join, $jointype);
                    } else {
                        $join = array(TB_SERVICE_USERS => TB_SERVICE_USERS.".service_user_id = ".TB_TICKET.".ticket_from", TB_SERVICE_PROVIDER => TB_SERVICE_PROVIDER.".service_provider_id = ".TB_TICKET.".ticket_to", TB_BOOKING => TB_BOOKING.".booking_id=".TB_TICKET.".booking_id", TB_CARE_BOOKING => TB_CARE_BOOKING.".booking_id =".TB_TICKET.".booking_id", TB_TUTOR_BOOKING => TB_TUTOR_BOOKING.".booking_id=".TB_TICKET.".booking_id", TB_RIDE_BOOKING => TB_RIDE_BOOKING.".booking_id =".TB_TICKET.".booking_id", TB_TUTOR_CATEGORY => TB_TUTOR_CATEGORY.".tut_cat_id =".TB_TUTOR_BOOKING.".cat_id", TB_TUTOR_SUBCATEGORY => TB_TUTOR_SUBCATEGORY.".tut_sub_cat_id = ".TB_TUTOR_BOOKING.".subcat_id");
                        $resultTicketDetails = $this->Common_model->selectJoin("user_fullname, sp_fullname, ticket_subject, ticket_service, ticket_comment, ticket_ut.status, ticket_attachment, pick_up_point, drop_point, ride_start_date, ride_end_date, care_start_date, care_end_date, care_location, tut_start_date, tut_end_date, category_name, subcategory_name", TB_TICKET, $cond, array(), $join, $jointype);
                    }

                    if(count($resultTicketDetails) > 0) {          
                        $ticket_from = $ticket_to = "";
                        if($postData['app_id'] == 1) {
                            $ticket_from = $resultTicketDetails[0]['sp_fullname'];
                            $ticket_to = $resultTicketDetails[0]['user_fullname'];
                        } else {
                            $ticket_from = $resultTicketDetails[0]['user_fullname'];
                            $ticket_to = $resultTicketDetails[0]['sp_fullname'];
                        }
                        $link = $fileLink = "";
                        
                        if(!empty($resultTicketDetails[0]['ticket_attachment'])) {
                            if(file_exists("uploads/ticket/".$resultTicketDetails[0]['ticket_attachment']))
                                $fileLink = base_url()."uploads/ticket/".$resultTicketDetails[0]['ticket_attachment'];
                            else 
                                $fileLink = base_url()."assets/images/img-not-found.jpg";
                            $link = 
                            '<div class="row">                                   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tia">Ticket Issue Attachment: </label>
                                        <a target="_blank" rel="noopener noreferrer" href="'.$fileLink.'"><b>Attachment Link</b></a>
                                    </div>
                                </div>                                
                            </div>
                            '; 
                        } else {
                            $link = 
                            '<div class="row">                                   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tia">Ticket Issue Attachment: </label>
                                        <b>N/A</b>
                                    </div>
                                </div>                                
                            </div>
                            '; 
                        }

                        $status = $resultTicketDetails[0]['status'] == 0 ? "Pending" : "Resolved";
                        $bookingData = "";
                        if(!empty($resultTicketDetails[0]['pick_up_point']) && !empty($resultTicketDetails[0]['drop_point'])) {
                            $bookingData = 
                            '
                            <div class="row"> 
                                <h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Issue Of Ride Details</h4>                                   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ride_start_date">Start date: </label>
                                        <lable>'.$resultTicketDetails[0]['ride_start_date'].'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ride_end_date">End date: </label>
                                        <lable>'.$resultTicketDetails[0]['ride_end_date'].'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">                                   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pick_up_point">Pick up point: </label>
                                        <lable>'.$resultTicketDetails[0]['pick_up_point'].'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="drop_point">Drop point: </label>
                                        <lable>'.$resultTicketDetails[0]['drop_point'].'</lable>
                                    </div>
                                </div>
                            </div>
                            ';
                        } else if(!empty($resultTicketDetails[0]['care_location'])) {
                            $bookingData = 
                            '<div class="row">       
                                <h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Issue Of Care Details</h4>                            
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="care_start_date">Start date: </label>
                                        <lable>'.$resultTicketDetails[0]['care_start_date'].'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ticket_service">End date: </label>
                                        <lable>'.$resultTicketDetails[0]['care_end_date'].'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="care_location">Care location: </label>
                                        <lable>'.$resultTicketDetails[0]['care_location'].'</lable>
                                    </div>
                                </div>
                            </div>
                            ';
                        } else {
                            $bookingData = 
                            '<div class="row">        
                                <h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Issue Of Tutor Details</h4>                                                       
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tut_start_date">Start date: </label>
                                        <lable>'.$resultTicketDetails[0]['tut_start_date'].'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tut_end_date">End date: </label>
                                        <lable>'.$resultTicketDetails[0]['tut_end_date'].'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">        
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="category_name">Tutor category name: </label>
                                        <lable>'.$resultTicketDetails[0]['category_name'].'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="subcategory_name">Tutor sub-category name: </label>
                                        <lable>'.$resultTicketDetails[0]['subcategory_name'].'</lable>
                                    </div>
                                </div>
                            </div>
                            ';
                        }
                        
                        $data = '
                        <div class="box">
                            <form role="form"  method="post" id="ticketInfo" name="ticketInfo">
                                <div class="box-body">
                                    <div class="row">
                                        <h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Ticket Issue Details</h4>               
                                        <div class="col-md-6">                                
                                            <div class="form-group">
                                                <label for="ticket_from">Ticket From: </label>
                                                <lable>'.$ticket_from.'</lable>
                                            </div>                              
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="ticket_to">Ticket To: </label>
                                                <lable>'.$ticket_to.'</lable>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">                                   
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="ticket_subject">Ticket Subject: </label>
                                                <lable>'.$resultTicketDetails[0]['ticket_subject'].'</lable>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="ticket_service">Ticket Service: </label>
                                                <lable>'.$resultTicketDetails[0]['ticket_service'].'</lable>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">                                   
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="ticket_comment">Ticket Comment: </label>
                                                <lable>'.$resultTicketDetails[0]['ticket_comment'].'</lable>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="status">Ticket Status: </label>
                                                <lable>'.$status.'</lable>
                                            </div>
                                        </div>
                                    </div>
                                    '.$link.'
                                    '.$bookingData.'             
                                </div><!-- /.box-body -->   
                            </form>                
                        </div>'; 
                        echo json_encode(array("status" => 'success',"data" => $data));  
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Booking details not found.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }   
}