<?php
/*
* @author : Kiran.
* description: manage the Organization data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Organization_controller extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Organization management', 
            'isActive'  => 'active'
        );
        $this->load->model("Organization_models","Organization");
        $this->load->model("common_model");
        $this->load->model("Pagination_models");   
        $this->load->library('pagination');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->view('includes/header',$this->data);
            $this->load->view('organization/organization');
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of Organization list data
    {
        if(is_user_logged_in()){          
            $orgData = $this->Organization->get_datatables();              
            $data = array();
            $no = $_POST['start'];
            $i = 1;

            foreach ($orgData as $value) {
                $userID = $value->user_id;
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($value->user_name);
                $row[] = $value->user_phone_number;
                $row[] = $value->user_email;
                $row[] = $value->user_address;                
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userID.'" class="btn btn-sm btn-info" onclick="viewOrgEmp(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>                
                                                     
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(

                "draw" => $_POST['draw'],
                "recordsTotal" => $this->Organization->count_all(),
                "recordsFiltered" => $this->Organization->count_filtered(),
                "data" => $data,

            );

            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function viewEmpList() // view data of Organization members  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();     
                $id = !empty($postData['key']) ? $postData['key'] : $this->session->userdata('orgId');        
                if( $id) {
                    $orgId = $id;
                    $this->session->set_userdata(array("orgId" =>$orgId));
                    $orgData = $this->Pagination_models->get_datatables($orgId);              
                    $data = array();
                    $no = $_POST['start'];
                    $i = 1;

                    foreach ($orgData as $value) {
                        //$userID = $this->encrypt->encode($value->stud_id);
                        //$no++;
                        $row = array();
                        //$row[] = $no;
                        $row[] = ucfirst($value->stud_username);
                        $row[] = $value->parent_email;
                        $row[] = $value->parent_phone;
                        $row[] = $value->stud_address;                
                        /*$row[] ='   <a data-id="'.$i.'" data-row-id="'.$userID.'" class="btn btn-sm btn-info" onclick="viewOrgEmp(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                        <i class="fa fa-eye"></i>
                                    </a>                
                                                             
                                ';*/
                        $data[] = $row;             
                        $i++;
                    }
             
                    $output = array(

                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Pagination_models->count_all($orgId),
                        "recordsFiltered" => $this->Pagination_models->count_filtered($orgId),
                        "data" => $data,

                    );

                    //output to json format
                    echo json_encode($output);                                  
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please provide organization id.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 
}