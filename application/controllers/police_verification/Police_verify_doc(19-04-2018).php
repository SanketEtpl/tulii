<?php
/*
* @author : kiran N.
* description: manage the police verify documentation of service provider docs.
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Police_verify_doc extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model("common_model");
        $this->data = array(
            'pageTitle' => 'Tulii : Police verification'        
        );
        $this->load->model('Police_verify_model','police');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');               
            $this->load->view('police_verification/police_verify_doc',$this->data);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of price list data
    {
        if(is_user_logged_in()){            
            $list = $this->police->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $docFiles = array();
            $imgPath ="";
            $folderName = $this->session->userdata("name")."_".$this->session->userdata("userId");
            $uploaddir = base_url()."uploads/police_verification/$folderName/";
            foreach ($list as $rec) {
                $userId = $this->encrypt->encode($rec->user_id);
                $docFiles[] = explode("|", $rec->police_verify_doc);
                $docFiles = $docFiles ? $docFiles[0]: array();
                if(!empty($docFiles)){
                    foreach ($docFiles as $key => $value) {
                       $imgPath.= "<img src='".$uploaddir.$value."' width='100px' height='60px' style='margin-right:5px;margin-top:0px'>";
                        /*$imgPath.= "<img src=\"<?php echo $uploaddir$value;?>\" width=\"100px\">"; */
                    }
                }
               /* echo $imgPath;
                die;*/
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $rec->user_name;
                $row[] = $rec->user_phone_number;
                $row[] = $imgPath;  
                $row[] = $rec->police_verify_status;                            
                $row[] ='  <a data-id="'.$i.'" data-row-id="'.$userId.'" data-error="Images not available." data-headermsg="Service provider documents" data-imgpath="'.base_url().'" data-picname="'.$rec->police_verify_doc.'" class="btn btn-sm btn-info viewPic" data-baseurl="'.$uploaddir.'" href="javascript:void(0)">
                                <i class="fa fa-eye"></i>
                            </a>                                                     
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->police->count_all(),
                            "recordsFiltered" => $this->police->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function viewSubject() // view data of subject  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $subData = $this->Common_model->select("id,teacher_id,sub_id",TB_SUBJECT_MASTER,array('id'=>$this->encrypt->decode($postData['key'])));
                   // $subjects = $this->Common_model->select('id,sub_name',TB_SUBJECTS);                          
                   // $teachers = $this->Common_model->select('id,teacher_name',TB_TEACHERS);                          
                    if($subData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","subData"=>$subData[0])); exit;//,"subjects"=>$subjects[0],"teachers"=>$teachers[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

    // add & update record of documents
    public function addDocuments()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                if(isset($_FILES['uploadDocuments']['tmp_name']) =='') {
                   $this->response(array("status"=>"error","message"=> 'Please upload documents.'), 300);exit;  
                } else {
                    $folderName = $this->session->userdata("name")."_".$this->session->userdata("userId");
                    $uploaddir = "./uploads/police_verification/$folderName/";
                   
                    $upload_certification = $documents ="";                       
                    $temp = "";
                    $fileImages = $_FILES;
                    //print_r($fileImages);exit;
                    $allFiles = array();
                    if(!empty($fileImages)) {
                        $j =1;                          
                        foreach ($fileImages as $key => $value) {
                            $cpt = count($_FILES[$key]['name']);
                            $temp = 'document_img';
                            $files_name = array();
                            for($i=0; $i<$cpt; $i++){
                                $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
                                $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
                                $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
                                $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
                                $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
                                $files_name[$i]['name'] = $_FILES[$temp]['name'];
                                $files_name[$i]['type'] = $_FILES[$temp]['type'];
                                $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                                $files_name[$i]['error'] = $_FILES[$temp]['error'];
                                $files_name[$i]['size'] = $_FILES[$temp]['size'];
                            }
                            $allFiles[$temp]=$files_name;
                            $j++;                            
                        }
                      //  print_r($allFiles);die;
                        foreach ($allFiles as $key => $value) {
                            $i = 0;                                
                            for($j=0;$j < count($value); $j++) { 
                                 if (strcmp($key,'document_img') == 0 ) {
                                    $upload_certification = $this->common_model->multiple_image_upload($value[$j],$uploaddir); 
                                    
                                 if(is_array($upload_certification))
                                    {
                                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Fail to upload documents.")); exit; 
                                    } else{
                                     $documents .= "|".$upload_certification;    
                                 }
                                                                           
                               
                                } 
                                $i++;                                                     
                            }
                        }             
                                       
                        $userdata = array(
                            'police_verify_status'=>"Pending",
                            'police_verify_doc'=>trim($documents,"|")                         
                        );                            
                        $user = $this->db->update(TB_USERS, $userdata,array("user_id"=>$this->session->userdata("userId")));
                        if ($user) {
                           echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Documents has been inserted successfully ")); exit;    
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
               } else {
                $this->response(array("status"=>false,"message"=> 'Please upload documents.'), 300);exit;  
               }         

            }             
                
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }                      
        }                      
    } 
}