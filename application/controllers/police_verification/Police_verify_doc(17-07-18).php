<?php
/*
* @author : kiran N.
* description: manage the police verify documentation of service provider docs.
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Police_verify_doc extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model("common_model");
        $this->load->helper("cias");
        $this->data = array(
            'pageTitle' => 'Tulii : Document verification management',        
            'isActive'=>'active'
        );
        $this->load->model('Police_verify_model','police');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');  
            $this->load->view('includes/header',$this->data);             
            $this->load->view('police_verification/police_verify_doc',$this->data);
            $this->load->view('includes/footer');
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of price list data
    {
        if(is_user_logged_in()){            
            $list = $this->police->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id=1;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            $docFiles = array();
            $imgPath ="";
            
            foreach ($list as $rec) {
                $userId = $this->encrypt->encode($rec->user_id);
                $folderName = str_replace(' ', '_',$rec->user_name)."_".$rec->user_id;
                $uploaddir = base_url()."uploads/police_verification/$folderName/";                    
                $docFiles = explode("|", $rec->police_verify_doc);
                $imgPath="";
                $url ="";
                if(count($docFiles) != 0){
                    foreach ($docFiles as $key => $value) {                        
                        if(!empty($value)){
                            if(!file_exists($uploaddir.$value)) 
                                $url = $uploaddir.$value;                                
                            else 
                                $url = 'uploads/No-image-available.jpg';    
                            
                            $imgPath.= "<img src='".$url."' width='100px' height='60px' style='margin-right:5px;margin-top:0px'>";        
                        } 
                        
                    }
                } 
                /*echo $imgPath;
                die;*/
                $button ="";  
                if($rec->police_verify_status == 1)
                {   
                    $a= $rec->user_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="userStatus_popup('.$a.');" ';

                    $b= $rec->user_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="userStatus_popup('.$b.');"';
                } 
                else
                {   
                    $c= $rec->user_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="userStatus_popup('.$c.');"';

                    $d= $rec->user_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="userStatus_popup('.$d.');" ';
                }
                $button ='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Completed"><button id="'.$rowActive_id.'" '.$active_btn_class.'>Complete</button></span>&nbsp;';
                $button.='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Incompleted"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>Incompleted</button></span>&nbsp;';

                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $rec->user_name;
                $row[] = $rec->user_phone_number;
                $row[] = $imgPath;  
                $row[] = $button;                            
                $row[] = '  
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" data-row-username="'.$rec->user_name.'" class="btn btn-sm addbtn" onclick="addPoliceDoc(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add">
                                    <i class="fa fa-plus"></i>
                                </a> 
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" data-row-username="'.$rec->user_name.'" class="btn btn-sm btn-info" onclick="editPoliceDoc(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a> 
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" data-row-username="'.$rec->user_name.'" data-error="No images are available." data-headermsg="View of service provider documents" data-imgpath="'.base_url().'" data-picname="'.$rec->police_verify_doc.'" class="btn btn-sm btn-info viewPic" data-baseurl="'.$uploaddir.'" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>  
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" data-row-username="'.$rec->user_name.'" class="btn btn-sm btn-default" onclick="downloadDocuments(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                <i class="fa fa-download"></i>
                            </a> 
                                                                           
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->police->count_all(),
                            "recordsFiltered" => $this->police->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function downloadDoc() // view data of subject  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $folderName = str_replace(' ', '_',$postData["username"])."_".$this->encrypt->decode($postData['key']);
                    $uploaddir = base_url()."uploads/police_verification/$folderName/";
                    $downloadData = $this->Common_model->select("user_id,police_verify_doc",TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    if(empty($downloadData[0]['police_verify_doc'])) {
                         echo json_encode(array("status"=>"error","action"=>"view","msg"=>"No images are available.")); exit;   
                    }else {                        
                        if($downloadData){                                                
                            echo json_encode(array("status"=>"success","action"=>"view","downloadData"=>$downloadData[0],"imagePath"=>$uploaddir)); exit;//,"subjects"=>$subjects[0],"teachers"=>$teachers[0])); exit; 
                        }else{
                            echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                        }
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

    public function getDetailsOfServiceProvider()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){                    
                    $downloadData = $this->Common_model->select("user_id,driving_license_type,license_no,valid_from_date,valid_until_date,car_model,car_register_no,car_pic,license_pic,car_seating_capacity,total_experience,car_number_plate",TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    if(count($downloadData) > 0){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","downloadData"=>$downloadData[0])); exit;//,"subjects"=>$subjects[0],"teachers"=>$teachers[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }

    public function policeVerifyStatusChange() // chnage the status fo police verification complete & incomplete
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
            //if($this->isAdmin() == TRUE){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_USERS,array("user_id"=>$postData['userId']),array('police_verify_status'=>$postData['user_status']));            
            if ($result)
                echo(json_encode(array('status'=>TRUE))); 
            else
                echo(json_encode(array('status'=>FALSE))); 
           /* } else {
                echo(json_encode(array('status'=>'access')));
            }*/
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }  
        }        
    }

    public function deleteImage() // delete image of service provider
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["user_id"]){
                    $getImage = $this->Common_model->select("user_id,police_verify_doc",TB_USERS,array('user_id'=>$postData['user_id']));
                    $imgArr = array();
                    $newPath="";
                    $image = $getImage ? $getImage[0]['police_verify_doc']:"";
                    $imgArr = explode("|",$image);
                    $folderName = str_replace(' ', '_',$postData["username"])."_".$postData['user_id'];
                    $uploaddir = "./uploads/police_verification/$folderName/";
                    $status =0;
                    if(!empty($imgArr)) {
                        foreach ($imgArr as $key => $value) {
                            if($value == $postData["image"]) {
                                if(file_exists($uploaddir.$postData["image"]))
                                unlink($uploaddir.$postData["image"]);
                                $status = 1;
                            } else {
                                $newPath .= "|".$value;
                            }
                        }
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                    $updateId = $this->Common_model->update(TB_USERS,array('user_id'=>$postData['user_id']),array('police_verify_doc'=>trim($newPath,"|")));
                    if($updateId){                                                
                        echo json_encode(array("status"=>"success","msg"=>"Documents has been deleted successfully.","action"=>"delete","downloadData"=>trim($newPath,"|"),"deleteStaus"=>$status,"imageName"=>"#".$postData["image_id"])); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function editDocFile()
    {
        if(is_ajax_request()) {
            if(is_user_logged_in()) {
                    
                $upload_certification = $carDocuments=$licDoc ="";                       
                $temp = "";
                $fileImages = $_FILES;
                $postData = $this->input->post();
                //print_r($fileImages); die;
               // print_r($postData); exit;
                $allFiles = array();
                if(!empty($fileImages)) {
                    $j =1;                          
                    foreach ($fileImages as $key => $value) {
                        $cpt = count($_FILES[$key]['name']);                       
                        $temp = $key;
                        $files_name = array();
                        $k=0;
                        for($i=0; $i<$cpt; $i++){
                            $_FILES[$k][$temp]['name']= $_FILES[$key]['name'][$i];
                            $_FILES[$k][$temp]['type']= $_FILES[$key]['type'][$i];
                            $_FILES[$k][$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
                            $_FILES[$k][$temp]['error']= $_FILES[$key]['error'][$i];
                            $_FILES[$k][$temp]['size']= $_FILES[$key]['size'][$i];    
                            $files_name[$i]['name'] = $_FILES[$k][$temp]['name'];
                            $files_name[$i]['type'] = $_FILES[$k][$temp]['type'];
                            $files_name[$i]['tmp_name'] = $_FILES[$k][$temp]['tmp_name'];
                            $files_name[$i]['error'] = $_FILES[$k][$temp]['error'];
                            $files_name[$i]['size'] = $_FILES[$k][$temp]['size'];
                            $k++;
                        }
                        $allFiles[$temp]=$files_name;
                        $j++;                            
                    }
                    $folderName = str_replace(' ', '_',$postData['user_name'])."_".$this->encrypt->decode($postData['user_id']);
                    $uploaddir = "./uploads/police_verification/$folderName/";
                    
                   /* $files = glob($uploaddir."*");
                    foreach($files as $file){ // iterate files
                      if(is_file($file))
                        unlink($file); // delete file
                    }*/
                   // print_r($allFiles);exit;
                    foreach ($allFiles as $key => $value) {
                        //echo $key;exit;
                        $i = 0;                                
                        for($j=0;$j < count($value); $j++) { 
                             if (strcmp($key,'uploadCarImage') == 0 ) {
                                $upload_certification = multiple_image_upload($value[$j],$uploaddir); 
                                
                             if(is_array($upload_certification))
                                {
                                    echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Fail to upload documents.")); exit; 
                                } else{
                                 $carDocuments .= "|".$upload_certification;    
                                }
                            } else if(strcmp($key, 'uploadScannedCpofLic') == 0) {
                                $upload_certification = multiple_image_upload($value[$j],$uploaddir); 
                                
                             if(is_array($upload_certification))
                                {
                                    echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Fail to upload documents.")); exit; 
                                } else{
                                 $licDoc .= "|".$upload_certification;    
                                }
                            } else {

                            } 
                            $i++;                                                     
                        }
                    }            
                                
                    $userdata = array(
                        'driving_license_type'=>$postData['drivingLicense'],
                        'license_no'=>$postData['license_no'],
                        'valid_from_date'=>$postData['valid_from'],
                        'valid_until_date'=>$postData['valid_until'],
                        'car_model'=>$postData['car_model'],
                        'car_register_no'=>$postData['car_reg_no'],
                        'car_number_plate'=>$postData['car_no_plate'],
                        'total_experience'=>$postData['total_experience'],
                        'car_seating_capacity'=>$postData['car_seating_capacity'],
                        'car_pic'=>trim($carDocuments,"|"),
                        'license_pic'=>trim($licDoc,"|"),                         
                    );                            
                    $user = $this->db->update(TB_USERS, $userdata,array("user_id"=>$this->encrypt->decode($postData['user_id'])));
                    if ($user) {
                            echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Documents has been inserted successfully ")); exit;    
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
               } else {
                echo json_encode(array("status"=>"error","message"=> 'Please upload documents.'), 300);exit;  
               }         

            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }                      
        }   
    }

    // add & update record of documents
    public function addDocuments()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                if(isset($_FILES['uploadDocuments']['tmp_name']) =='') {
                   echo json_encode(array("status"=>"error","message"=> 'Please upload documents.'), 300);exit;  
                } else {
                    $postData = $this->input->post();
                    $folderName = str_replace(' ', '_',$postData['add_user_name'])."_".$this->encrypt->decode($postData['user_key']);
                    $uploaddir = "./uploads/police_verification/$folderName/";
                    
                    $upload_certification = $documents ="";                       
                    $temp = "";
                    $fileImages = $_FILES;
                    //print_r($fileImages);exit;
                    $allFiles = array();
                    if(!empty($fileImages)) {
                        $j =1;                          
                        foreach ($fileImages as $key => $value) {
                            $cpt = count($_FILES[$key]['name']);
                            $temp = 'document_img';
                            $files_name = array();
                            for($i=0; $i<$cpt; $i++){
                                $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
                                $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
                                $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
                                $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
                                $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
                                $files_name[$i]['name'] = $_FILES[$temp]['name'];
                                $files_name[$i]['type'] = $_FILES[$temp]['type'];
                                $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                                $files_name[$i]['error'] = $_FILES[$temp]['error'];
                                $files_name[$i]['size'] = $_FILES[$temp]['size'];
                            }
                            $allFiles[$temp]=$files_name;
                            $j++;                            
                        }
                      /*  $files = glob($uploaddir."*");
                        foreach($files as $file){ // iterate files
                          if(is_file($file))
                            unlink($file); // delete file
                        }*/
                        
                        foreach ($allFiles as $key => $value) {
                            $i = 0;                                
                            for($j=0;$j < count($value); $j++) { 
                                 if (strcmp($key,'document_img') == 0 ) {
                                    $upload_certification = multiple_image_upload($value[$j],$uploaddir); 
                                    
                                 if(is_array($upload_certification))
                                    {
                                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Fail to upload documents.")); exit; 
                                    } else{
                                     $documents .= "|".$upload_certification;    
                                    }
                                } 
                                $i++;                                                     
                            }
                        }             
                                       
                        $userdata = array(
                            'police_verify_status'=>"0",
                            'verification_no'=>$postData["verify_no"],
                            'police_verify_doc'=>trim($documents,"|")                         
                        );                            
                        $user = $this->db->update(TB_USERS, $userdata,array("user_id"=>$this->encrypt->decode($postData['user_key'])));
                        if ($user) {
                            $service_provider_device_id = $this->common_model->select("user_name,user_device_id",TB_USERS,array("user_id"=>$this->session->userdata("userId")));
                            $userName = $service_provider_device_id[0]['user_name']? $service_provider_device_id[0]['user_name']:"Hello,";
                            $message = $userName." your documents verification by tulii."; 
                            $device_id = isset($service_provider_device_id[0]['user_device_id'])?$service_provider_device_id[0]['user_device_id']:"";
                            $myData = array("message"=>"Your documents has been verify successfully. ");
                            $response = $this->sendPushNotification($device_id,$message,$myData);
                            echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Documents has been inserted successfully ")); exit;    
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
               } else {
               echo json_encode(array("status"=>false,"message"=> 'Please upload documents.'), 300);exit;  
               }         

            }             
                
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }                      
        }                      
    } 

    public function sendPushNotification($device_id,$message,$data)
    {     
        $content = array(
            "en" => $message
        );
        $fields = array(
            'app_id' => ONSIGNALKEYSP,
            'include_player_ids' =>(array)$device_id,
            'data' => $data,
            'contents' => $content
        );
        
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
         'Authorization: Basic OGViZTZkMjgtMjRlYy00YWQ0LWIzMWYtNmE2ZWI5MDhmZTUw'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);     
        return $response;            
        curl_close($ch);
    }
}