<?php
/*
* @author : kiran N.
* description: manage the police verify documentation of service provider docs.
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Police_verify_doc extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model("common_model");
        $this->data = array(
            'pageTitle' => 'Tulii : Police verification'        
        );
        $this->load->model('Police_verify_model','police');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');               
            $this->load->view('police_verification/police_verify_doc',$this->data);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of price list data
    {
        if(is_user_logged_in()){            
            $list = $this->police->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id=1;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            $docFiles = array();
            $imgPath ="";
            $folderName = $this->session->userdata("name")."_".$this->session->userdata("userId");
            $uploaddir = base_url()."uploads/police_verification/$folderName/";
            foreach ($list as $rec) {
                $userId = $this->encrypt->encode($rec->user_id);
                $docFiles[] = explode("|", $rec->police_verify_doc);
                $docFiles = $docFiles[0] ? $docFiles[0]: array();
                if(!empty($docFiles[0])){
                    foreach ($docFiles as $key => $value) {
                       $imgPath.= "<img src='".$uploaddir.$value."' width='100px' height='60px' style='margin-right:5px;margin-top:0px'>";
                    }
                }
                $button ="";  
                if($rec->police_verify_status == 1)
                {   
                    $a= $rec->user_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="userStatus_popup('.$a.');" ';

                    $b= $rec->user_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="userStatus_popup('.$b.');"';
                } 
                else
                {   
                    $c= $rec->user_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="userStatus_popup('.$c.');"';

                    $d= $rec->user_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="userStatus_popup('.$d.');" ';
                }
                $button ='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Completed"><button id="'.$rowActive_id.'" '.$active_btn_class.'>Complete</button></span>&nbsp;';
                $button.='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Incompleted"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>Incompleted</button></span>&nbsp;';
                
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $rec->user_name;
                $row[] = $rec->user_phone_number;
                $row[] = $imgPath;  
                $row[] = $button;                            
                $row[] ='  <a data-id="'.$i.'" data-row-id="'.$userId.'" data-error="No images are available." data-headermsg="Service provider documents" data-imgpath="'.base_url().'" data-picname="'.$rec->police_verify_doc.'" class="btn btn-sm btn-info viewPic" data-baseurl="'.$uploaddir.'" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>  
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-primary" onclick="downloadDocuments(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                <i class="fa fa-download"></i>
                            </a>  
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="deleteDocuments(this)" class="btn btn-sm btn-danger" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>                                                     
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->police->count_all(),
                            "recordsFiltered" => $this->police->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function downloadDoc() // view data of subject  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $folderName = $this->session->userdata("name")."_".$this->session->userdata("userId");
                    $uploaddir = base_url()."uploads/police_verification/$folderName/";
                    $downloadData = $this->Common_model->select("user_id,police_verify_doc",TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    if(empty($downloadData[0]['police_verify_doc'])) {
                         echo json_encode(array("status"=>"error","action"=>"view","msg"=>"No images are available.")); exit;   
                    }else {                        
                        if($downloadData){                                                
                            echo json_encode(array("status"=>"success","action"=>"view","downloadData"=>$downloadData[0],"imagePath"=>$uploaddir)); exit;//,"subjects"=>$subjects[0],"teachers"=>$teachers[0])); exit; 
                        }else{
                            echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                        }
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

    public function policeVerifyStatusChange() // chnage the status fo police verification complete & incomplete
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
            //if($this->isAdmin() == TRUE){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_USERS,array("user_id"=>$postData['userId']),array('police_verify_status'=>$postData['user_status']));            
            if ($result)
                echo(json_encode(array('status'=>TRUE))); 
            else
                echo(json_encode(array('status'=>FALSE))); 
           /* } else {
                echo(json_encode(array('status'=>'access')));
            }*/
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }  
        }        
    }

    public function deleteImage() // delete image of service provider
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["user_id"]){
                    $getImage = $this->Common_model->select("user_id,police_verify_doc",TB_USERS,array('user_id'=>$postData['user_id']));
                    $imgArr = array();
                    $newPath="";
                    $image = $getImage ? $getImage[0]['police_verify_doc']:"";
                    $imgArr = explode("|",$image);
                    $folderName = $this->session->userdata("name")."_".$this->session->userdata("userId");
                    $uploaddir = "./uploads/police_verification/$folderName/";
                    $status =0;
                    if(!empty($imgArr)) {
                        foreach ($imgArr as $key => $value) {
                            if($value == $postData["image"]) {
                                if(file_exists($uploaddir.$postData["image"]))
                                unlink($uploaddir.$postData["image"]);
                                $status = 1;
                            } else {
                                $newPath .= "|".$value;
                            }
                        }
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }

                    $updateId = $this->Common_model->update(TB_USERS,array('user_id'=>$postData['user_id']),array('police_verify_doc'=>trim($newPath,"|")));
                    if($updateId){                                                
                        echo json_encode(array("status"=>"success","msg"=>"Documents has been deleted successfully.","action"=>"delete","downloadData"=>$newPath,"deleteStaus"=>$status,"imageName"=>"#".$postData["image_id"])); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    // add & update record of documents
    public function addDocuments()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                if(isset($_FILES['uploadDocuments']['tmp_name']) =='') {
                   $this->response(array("status"=>"error","message"=> 'Please upload documents.'), 300);exit;  
                } else {
                    $folderName = $this->session->userdata("name")."_".$this->session->userdata("userId");
                    $uploaddir = "./uploads/police_verification/$folderName/";
                   
                    $upload_certification = $documents ="";                       
                    $temp = "";
                    $fileImages = $_FILES;
                    //print_r($fileImages);exit;
                    $allFiles = array();
                    if(!empty($fileImages)) {
                        $j =1;                          
                        foreach ($fileImages as $key => $value) {
                            $cpt = count($_FILES[$key]['name']);
                            $temp = 'document_img';
                            $files_name = array();
                            for($i=0; $i<$cpt; $i++){
                                $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
                                $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
                                $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
                                $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
                                $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
                                $files_name[$i]['name'] = $_FILES[$temp]['name'];
                                $files_name[$i]['type'] = $_FILES[$temp]['type'];
                                $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                                $files_name[$i]['error'] = $_FILES[$temp]['error'];
                                $files_name[$i]['size'] = $_FILES[$temp]['size'];
                            }
                            $allFiles[$temp]=$files_name;
                            $j++;                            
                        }
                      //  print_r($allFiles);die;
                        foreach ($allFiles as $key => $value) {
                            $i = 0;                                
                            for($j=0;$j < count($value); $j++) { 
                                 if (strcmp($key,'document_img') == 0 ) {
                                    $upload_certification = $this->common_model->multiple_image_upload($value[$j],$uploaddir); 
                                    
                                 if(is_array($upload_certification))
                                    {
                                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Fail to upload documents.")); exit; 
                                    } else{
                                     $documents .= "|".$upload_certification;    
                                    }
                                } 
                                $i++;                                                     
                            }
                        }             
                                       
                        $userdata = array(
                            'police_verify_status'=>"0",
                            'verification_no'=>$this->input->post("verify_no"),
                            'police_verify_doc'=>trim($documents,"|")                         
                        );                            
                        $user = $this->db->update(TB_USERS, $userdata,array("user_id"=>$this->session->userdata("userId")));
                        if ($user) {
                           echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Documents has been inserted successfully ")); exit;    
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
               } else {
                $this->response(array("status"=>false,"message"=> 'Please upload documents.'), 300);exit;  
               }         

            }             
                
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }                      
        }                      
    } 
}