<?php
/*
* @author : kiran N.
* description: manage the price of rides data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Price_list extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Price list'        
        );
        $this->load->model('Price_list_model','priceList');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');       
            $this->data['ride_types'] = $this->Common_model->select('*',TB_PRICE_MASTER);                          
            $this->load->view('price_management/price_list',$this->data);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of price list data
    {
        if(is_user_logged_in()){            
            $list = $this->priceList->get_datatables();        
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            foreach ($list as $plData) {
                $userId = $this->encrypt->encode($plData->rp_id);
                $pkm=$phr ="None"; 
                if(!empty($plData->rp_per_km))
                $pkm = $plData->rp_per_km;
                if(!empty($plData->rp_per_hour))
                $phr = $plData->rp_per_hour;  
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $plData->pm_name;                              
                $row[] = $pkm;
                $row[] = $phr;
                $row[] = $plData->rp_cost;                
                $row[] ='  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editPriceData(this)" href="javascript:void(0)">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deletePriceList(this)" href="javascript:void(0)">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->priceList->count_all(),
                            "recordsFiltered" => $this->priceList->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function deletePriceList() // delete record of price list
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->delete(TB_RIDE_PRICE,array('rp_id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Price record has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewPriceInfo() // view data of price  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $priceData = $this->Common_model->select("rp_id,rp_cost,rp_per_km,pm_id",TB_RIDE_PRICE,array('rp_id'=>$this->encrypt->decode($postData['key'])));
                    if($priceData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","priceData"=>$priceData[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            }
            else{
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

    // add & update record of price list
    public function addPrice()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();
                $cond = array("tbl_ride_price.pm_id" => $postData['rideType']);
                $jointype=array("tbl_price_master"=>"LEFT");
                $join = array("tbl_price_master"=>"tbl_price_master.pm_id = tbl_ride_price.pm_id");
                $priceListData = $this->Common_model->selectJoin("distinct(pm_name)",TB_RIDE_PRICE,$cond,array(),$join,$jointype);
                if(empty($postData['editData']) && count($priceListData) > 0) {                    
                    echo json_encode(array("status"=>"warning","message"=>$priceListData[0]['pm_name']." is price data already exist")); exit;   
                }                
                else
                {  
                   // print_r($postData);exit;
                    $phr = $pkm = $cost = '';
                    if($postData['rideType'] == 1 || $postData['rideType'] == 3 || $postData['rideType'] == 5 ) {
                        $phr = 1;   
                        $cost = $postData['price_per_hour'];
                    }

                    if($postData['rideType'] == 2 || $postData['rideType'] == 4) {
                        $pkm = 1;   
                        $cost = $postData['price_per_km'];
                    }

                    if(!empty($postData["editData"])) { // update data
                        $updateArr = array(  
                            "pm_id" =>$postData['rideType'],
                            "rp_cost"=> $cost,
                            "rp_per_hour"=>$phr,
                            "rp_per_km" => $pkm,
                            "updated_at"=>date("Y-m-d H:s:i")  
                        );
                        $updateId = $this->Common_model->update(TB_RIDE_PRICE,array('rp_id'=>$this->encrypt->decode($postData['editData'])),$updateArr);
                        if($updateId){
                            echo json_encode(array("status"=>"success","action"=>"update","message"=>"Price has been updated successfully.")); exit;   
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                        }
                    } else {       
                        if(!empty($postData)){ // add data               
                        $data = array(
                            "pm_id" =>$postData['rideType'],
                            "rp_cost"=> $cost,
                            "rp_per_hour"=>$phr,
                            "rp_per_km" => $pkm,                 
                            "created_at"=>date("Y-m-d H:s:i")           
                            );
                        $recordInsertUser = $this->Common_model->insert(TB_RIDE_PRICE,$data);
                        if($recordInsertUser){
                            echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Price has been inserted successfully ")); exit;    
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                        }
                    }
                    else{
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the user information")); exit;    
                        }
                    }  
                } 
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 
}