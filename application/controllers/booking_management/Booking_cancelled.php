<?php 
/*
Author : Kiran n 
Page :  Booking_cancelled.php
Description : booking cancelled of ride, care and tutor data show. 
*/
if(!defined('BASEPATH')) exit('No direct script access allowed'); 
 
class Booking_cancelled extends CI_Controller
{
	public function __construct()
  {
    ob_start();
    parent::__construct();
    $this->load->model('Booking_model','booking');    
    $this->data = array(
      'pageTitle' => 'Tulii : Booking management of booking cancelled details',
      'isActive' => 'active'         
    );
  }

  public function index()
  {
    if(is_user_logged_in()) {
    	$this->load->view("includes/header",$this->data);      
      $this->load->view("booking_management/bookingCancelled");
      $this->load->view("includes/footer");
    } else {
      $this->session->sess_destroy();
      redirect('login');
    }  
  }

  function rideList()
  {     
    if(is_user_logged_in()) {
      $table = TB_RIDE_BOOKING;
      $select = " DISTINCT(booking_master_ut.booking_id), pick_up_point, drop_point, ride_start_date, ride_end_date, estimated_distance, estimated_cost";
      $where_cond = array(TB_RIDE_BOOKING.".ride_status" => "6", TB_BOOKING_DETAILS.".sub_book_status" => "6", TB_BOOKING_MASTER.".booking_status" => "6", TB_BOOKING_MASTER.".is_deleted" => "0");
      $order = array(TB_BOOKING_MASTER.'.booking_id' => 'desc');
      $column_order = array(null,'pick_up_point','drop_point','ride_start_date','ride_end_date','estimated_distance','estimated_cost');
      $column_search = array('pick_up_point','drop_point','ride_start_date','ride_end_date','estimated_distance','estimated_cost');
      $join = array(TB_BOOKING_MASTER => TB_BOOKING_MASTER.".booking_id = ".TB_RIDE_BOOKING.".booking_id", TB_BOOKING_DETAILS => TB_BOOKING_DETAILS.".booking_id = ".TB_BOOKING_MASTER.".booking_id");
      $resultRideList = $this->booking->get_datatables($select, $table, $where_cond, $order, $column_order, $column_search, $join);  
      $data = array();
      $no = $_POST['start'];
      $i = 1;
      foreach ($resultRideList as $rideCancelled) {
        $bookingId = $this->encrypt->encode($rideCancelled->booking_id);  
        $no++;
        $row = array();
        $row[] = $no;
        $row[] = $rideCancelled->pick_up_point;
        $row[] = $rideCancelled->drop_point;
        $row[] = $rideCancelled->ride_start_date;
        $row[] = $rideCancelled->ride_end_date;
        $row[] = $rideCancelled->estimated_distance;
        $row[] = "$".$rideCancelled->estimated_cost;
        $row[] = '
                  <a data-id="'.$i.'" data-row-id="'.$bookingId.'" class="btn btn-sm btn-primary" onclick="viewBookingScheduleData(this,\'ride\')" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                      <i class="fa fa-eye"></i>
                  </a>                           
              ';
        
        $data[] = $row;
        $i++;
      }

      $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => $this->booking->count_all($table),
        "recordsFiltered" => $this->booking->count_filtered($select, $table, $where_cond, $order, $column_order, $column_search, $join),
        "data" => $data,
      );
      //output to json format
      echo json_encode($output);
    } else {
      echo(json_encode(array('status'=>'logout')));
      $this->session->sess_destroy();
      redirect('login');
    } 
  }  
  
  function careList()
  {     
    if(is_user_logged_in()) {
      $table = TB_CARE_BOOKING;
      $select = " DISTINCT(booking_master_ut.booking_id), care_location, care_start_date, care_end_date, care_start_time, care_end_time, estimated_cost";
      $where_cond = array(TB_CARE_BOOKING.".care_booking_status" => "6", TB_BOOKING_DETAILS.".sub_book_status" => "6", TB_BOOKING_MASTER.".booking_status" => "6", TB_BOOKING_MASTER.".is_deleted" => "0");
      $order = array(TB_BOOKING_MASTER.'.booking_id' => 'desc');
      $column_order = array(null,'care_location','care_start_date','care_end_date','care_start_time','care_end_time','estimated_cost');
      $column_search = array('care_location','care_start_date','care_end_date','care_start_time','care_end_time','estimated_cost');
      $join = array(TB_BOOKING_MASTER => TB_BOOKING_MASTER.".booking_id = ".TB_CARE_BOOKING.".booking_id", TB_BOOKING_DETAILS => TB_BOOKING_DETAILS.".booking_id = ".TB_BOOKING_MASTER.".booking_id");
      $resultCareList = $this->booking->get_datatables($select, $table, $where_cond, $order, $column_order, $column_search, $join);  
      $data = array();
      $no = $_POST['start'];
      $i = 1;
      foreach ($resultCareList as $careCancelled) {
        $bookingId = $this->encrypt->encode($careCancelled->booking_id);  
        $no++;
        $row = array();
        $row[] = $no;
        $row[] = $careCancelled->care_location;
        $row[] = $careCancelled->care_start_date;
        $row[] = $careCancelled->care_end_date;
        $row[] = $careCancelled->care_start_time;
        $row[] = $careCancelled->care_end_time;
        $row[] = "$".$careCancelled->estimated_cost;
        $row[] = '
                  <a data-id="'.$i.'" data-row-id="'.$bookingId.'" class="btn btn-sm btn-primary" onclick="viewBookingScheduleData(this,\'care\')" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                      <i class="fa fa-eye"></i>
                  </a>                           
              ';
        
        $data[] = $row;
        $i++;
      }

      $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => $this->booking->count_all($table),
        "recordsFiltered" => $this->booking->count_filtered($select, $table, $where_cond, $order, $column_order, $column_search, $join),
        "data" => $data,
      );
      //output to json format
      echo json_encode($output);
    } else {
      echo(json_encode(array('status'=>'logout')));
      $this->session->sess_destroy();
      redirect('login');
    } 
  }

  function tutorList()
  {     
    if(is_user_logged_in()) {
      $table = TB_TUTOR_BOOKING;
      $select = " DISTINCT(booking_master_ut.booking_id), category_name, subcategory_name, tut_start_date, tut_end_date, estimated_cost";
      $where_cond = array(TB_TUTOR_BOOKING.".tution_booking_status" => "6", TB_BOOKING_DETAILS.".sub_book_status" => "6", TB_BOOKING_MASTER.".booking_status" => "6", TB_BOOKING_MASTER.".is_deleted" => "0");
      $order = array(TB_BOOKING_MASTER.'.booking_id' => 'desc');
      $column_order = array(null,'category_name','subcategory_name','tut_start_date','tut_end_date','estimated_cost');
      $column_search = array('category_name','subcategory_name','tut_start_date','tut_end_date','estimated_cost');
      $join = array(TB_BOOKING_MASTER => TB_BOOKING_MASTER.".booking_id = ".TB_TUTOR_BOOKING.".booking_id", TB_BOOKING_DETAILS => TB_BOOKING_DETAILS.".booking_id = ".TB_BOOKING_MASTER.".booking_id", TB_TUTOR_CATEGORY => TB_TUTOR_CATEGORY.'.tut_cat_id ='.TB_TUTOR_BOOKING.'.cat_id', TB_TUTOR_SUBCATEGORY => TB_TUTOR_SUBCATEGORY.'.tut_sub_cat_id ='.TB_TUTOR_BOOKING.'.subcat_id');
      $resultTutorList = $this->booking->get_datatables($select, $table, $where_cond, $order, $column_order, $column_search, $join);  
      $data = array();
      $no = $_POST['start'];
      $i = 1;
      foreach ($resultTutorList as $tutorCancelled) {
        $bookingId = $this->encrypt->encode($tutorCancelled->booking_id);  
        $no++;
        $row = array();
        $row[] = $no;
        $row[] = $tutorCancelled->category_name;
        $row[] = $tutorCancelled->subcategory_name;
        $row[] = $tutorCancelled->tut_start_date;
        $row[] = $tutorCancelled->tut_end_date;
        $row[] = $tutorCancelled->estimated_cost;        
        $row[] = '
                  <a data-id="'.$i.'" data-row-id="'.$bookingId.'" class="btn btn-sm btn-primary" onclick="viewBookingScheduleData(this,\'tutor\')" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                      <i class="fa fa-eye"></i>
                  </a>                           
              ';
        
        $data[] = $row;
        $i++;
      }

      $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => $this->booking->count_all($table),
        "recordsFiltered" => $this->booking->count_filtered($select, $table, $where_cond, $order, $column_order, $column_search, $join),
        "data" => $data,
      );
      //output to json format
      echo json_encode($output);
    } else {
      echo(json_encode(array('status'=>'logout')));
      $this->session->sess_destroy();
      redirect('login');
    } 
  }

  function viewBookingInfo()
  {
    if(is_ajax_request()) {
      if(is_user_logged_in()) {
        $postData = $this->input->post();
        if(strcmp(trim($postData['type']),'ride') == 0) {                
          $cond = array(TB_RIDE_BOOKING.".ride_status" => "6", TB_BOOKING_DETAILS.".sub_book_status" => "6", TB_BOOKING_MASTER.".booking_status" => "6", TB_BOOKING_MASTER.".is_deleted" => "0", TB_VEHICLE_CATEGORY.".status"=> '1', TB_BOOKING_MASTER.".booking_id" => $this->encrypt->decode($postData['key']));
          $join = array(TB_BOOKING_MASTER => TB_BOOKING_MASTER.".booking_id = ".TB_RIDE_BOOKING.".booking_id", TB_BOOKING_DETAILS => TB_BOOKING_DETAILS.".booking_id = ".TB_BOOKING_MASTER.".booking_id", TB_SERVICE_USERS => TB_SERVICE_USERS.'.service_user_id ='.TB_BOOKING_MASTER.'.service_user_id',TB_VEHICLE_CATEGORY => TB_VEHICLE_CATEGORY.'.vehicle_category_id='.TB_RIDE_BOOKING.'.vehicle_category_id');
          $resultBookingDetails = $this->Common_model->select_join("DISTINCT(ride_booking_ut.booking_id), user_fullname, user_phone_number, user_gender, user_address, organization_name, pick_up_point, drop_point, ride_start_date, ride_end_date, pick_up_time, estimated_distance, estimated_time, estimated_cost, vehicle_category_name, description, seating_capacity, rate_per_mile", TB_RIDE_BOOKING, $cond, array(), array(), $join ,null);
        } else if(strcmp(trim($postData['type']),'care') == 0){
          $cond = array(TB_CARE_BOOKING.".care_booking_status" => "6", TB_BOOKING_DETAILS.".sub_book_status" => "6", TB_BOOKING_MASTER.".booking_status" => "6", TB_BOOKING_MASTER.".is_deleted" => "0", TB_BOOKING_MASTER.".booking_id" => $this->encrypt->decode($postData['key']));
          $join = array(TB_BOOKING_MASTER => TB_BOOKING_MASTER.".booking_id = ".TB_CARE_BOOKING.".booking_id", TB_BOOKING_DETAILS => TB_BOOKING_DETAILS.".booking_id = ".TB_BOOKING_MASTER.".booking_id", TB_SERVICE_USERS => TB_SERVICE_USERS.'.service_user_id ='.TB_BOOKING_MASTER.'.service_user_id');
          $resultBookingDetails = $this->Common_model->select_join("DISTINCT(care_booking_ut.booking_id), user_fullname, user_phone_number, user_gender, user_address, care_location, care_start_date, care_end_date, care_start_time, care_end_time, estimated_time, estimated_cost", TB_CARE_BOOKING, $cond, array(), array(), $join ,null);
        } else {
          $cond = array(TB_TUTOR_BOOKING.".tution_booking_status" => "6", TB_BOOKING_DETAILS.".sub_book_status" => "6", TB_BOOKING_MASTER.".booking_status" => "6", TB_BOOKING_MASTER.".is_deleted" => "0", TB_BOOKING_MASTER.".booking_id" => $this->encrypt->decode($postData['key']));
          $join = array(TB_BOOKING_MASTER => TB_BOOKING_MASTER.".booking_id = ".TB_TUTOR_BOOKING.".booking_id", TB_BOOKING_DETAILS => TB_BOOKING_DETAILS.".booking_id = ".TB_BOOKING_MASTER.".booking_id", TB_SERVICE_USERS => TB_SERVICE_USERS.'.service_user_id ='.TB_BOOKING_MASTER.'.service_user_id',TB_TUTOR_CATEGORY => TB_TUTOR_CATEGORY.'.tut_cat_id ='.TB_TUTOR_BOOKING.'.cat_id', TB_TUTOR_SUBCATEGORY => TB_TUTOR_SUBCATEGORY.'.tut_sub_cat_id ='.TB_TUTOR_BOOKING.'.subcat_id');
          $resultBookingDetails = $this->Common_model->select_join("DISTINCT(tutor_booking_ut.booking_id), user_fullname, user_phone_number, user_gender, user_address, category_name, subcategory_name, rate_per_hour, tut_start_date, tut_end_date, start_time, end_time, estimated_time, estimated_cost", TB_TUTOR_BOOKING, $cond, array(), array(), $join ,null);
        }
        if(count($resultBookingDetails) > 0) { 
          $pCond = array(TB_PASSENGER_MASTER.'.pass_status'=> '1', TB_PASSENGER_MASTER.'.is_deleted' => '0' ,TB_BOOKING_DETAILS.'.sub_book_status' => '6', TB_BOOKING_DETAILS.'.booking_id' => $this->encrypt->decode($postData['key']));
          $pJoin = array(TB_PASSENGER_MASTER => TB_PASSENGER_MASTER.'.passenger_id='.TB_BOOKING_DETAILS.'.pass_id');
          $resultPassegerDetails = $this->Common_model->select_join("DISTINCT(booking_details_ut.pass_id), pass_dob, pass_gender, pass_fullname",TB_BOOKING_DETAILS, $pCond, array(), array(), $pJoin ,null);
          $kidDetails = "";
          $pCount = 0;
          if(count($resultPassegerDetails) > 0) {
            foreach ($resultPassegerDetails as $key => $value) {            
            $kidDetails .='
              
                <div class="col-md-4">                                
                  <div class="form-group">
                    <label>Name: </label>
                    <lable>'.$value['pass_fullname'].'</lable>
                  </div>                          
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Gender: </label>
                    <lable>'.$value['pass_gender'].'</lable>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Date of birth: </label>
                    <lable>'.$value['pass_dob'].'</lable>
                  </div>
                </div>
             
            ';       
            $pCount++;                   
          }          
        } else {
          $kidDetails = "No passanger exists.";          
        }          
          

        $vehicleData = $bookingRideData = '';
        if(strcmp(trim($postData['type']),'ride') == 0) {
          $vehicleData = 
            '<div class="row">
              <h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Vehicle Type</h4>               
              
              <div class="col-md-6">
                <div class="form-group">
                  <label for="vehicle_category_name">Vehicle Name: </label>
                  <lable>'.$resultBookingDetails[0]['vehicle_category_name'].'</lable>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="description">Vehicle Description: </label>
                  <lable>'.$resultBookingDetails[0]['description'].'</lable>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="rate_per_mile">Rate per mile: </label>
                  <lable>'.$resultBookingDetails[0]['rate_per_mile'].'</lable>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="seating_capacity">Seating Capacity: </label>
                  <lable>'.$resultBookingDetails[0]['seating_capacity'].'</lable>
                </div>
              </div>
            </div>';

          $bookingData = '
            <div class="row">
              <h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Booking Details</h4>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="pick_up_point">Pick Up Point: </label>
                    <lable>'.$resultBookingDetails[0]['pick_up_point'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="drop_point">Drop Point: </label>
                    <lable>'.$resultBookingDetails[0]['drop_point'].'</lable>
                  </div>
                </div>
              </div>
              <div class="row">                                   
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="Start_date">Ride Start Date: </label>
                    <lable>'.$resultBookingDetails[0]['ride_start_date'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="end_date">Ride End Date: </label>
                    <lable>'.$resultBookingDetails[0]['ride_end_date'].'</lable>
                  </div>
                </div>
              </div>
           
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="pick_up_time">Pick Up Time</label>
                    <lable>'.$resultBookingDetails[0]['pick_up_time'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="estimated_distance">Estimate Distance: </label>
                    <lable>'.$resultBookingDetails[0]['estimated_distance'].'</lable>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="estimated_time">Estimate Time: </label>
                    <lable>'.$resultBookingDetails[0]['estimated_time'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="estimated_cost">Estimate Cost: </label>
                    <lable>'.$resultBookingDetails[0]['estimated_cost'].'</lable>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="total_pass">Total passengers: </label>
                    <lable>'.$pCount.'</lable>
                  </div>
                </div>                  
              </div>
              </div>
          ';  
        } else if(strcmp(trim($postData['type']),'care') == 0) {
          $bookingData = '
            <div class="row">
              <h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Booking Details</h4>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="care_location">Care Location: </label>
                    <lable>'.$resultBookingDetails[0]['care_location'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="care_start_date">Care Start Date: </label>
                    <lable>'.$resultBookingDetails[0]['care_start_date'].'</lable>
                  </div>
                </div>
              </div>
              <div class="row">                                   
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="care_end_date">Care End Date: </label>
                    <lable>'.$resultBookingDetails[0]['care_end_date'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="care_start_time">Care Start Time: </label>
                    <lable>'.$resultBookingDetails[0]['care_start_time'].'</lable>
                  </div>
                </div>
              </div>
           
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="care_end_time">Care End Time</label>
                    <lable>'.$resultBookingDetails[0]['care_end_time'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="estimated_time">Estimated Time: </label>
                    <lable>'.$resultBookingDetails[0]['estimated_time'].'</lable>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="estimated_cost">Estimate Cost: </label>
                    <lable>'.$resultBookingDetails[0]['estimated_cost'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="total_pass">Total passengers: </label>
                    <lable>'.$pCount.'</lable>
                  </div>
                </div>                  
              </div>              
              </div>
          ';  
        } else {
          $bookingData = '
            <div class="row">
              <h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Booking Details</h4>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="category_name">Tutor Category Name: </label>
                    <lable>'.$resultBookingDetails[0]['category_name'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="subcategory_name">Tutor Subcategory Name: </label>
                    <lable>'.$resultBookingDetails[0]['subcategory_name'].'</lable>
                  </div>
                </div>
              </div>
              <div class="row">                                   
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="rate_per_hour">Rate Per Hour: </label>
                    <lable>'.$resultBookingDetails[0]['rate_per_hour'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="tut_start_date">Tutor Start Date: </label>
                    <lable>'.$resultBookingDetails[0]['tut_start_date'].'</lable>
                  </div>
                </div>
              </div>
           
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="tut_end_date">Tutor End Date</label>
                    <lable>'.$resultBookingDetails[0]['tut_end_date'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="start_time">Start Time: </label>
                    <lable>'.$resultBookingDetails[0]['start_time'].'</lable>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="end_time">End Time: </label>
                    <lable>'.$resultBookingDetails[0]['end_time'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="estimated_time">Estimate Time: </label>
                    <lable>'.$resultBookingDetails[0]['estimated_time'].'</lable>
                  </div>
                </div>                  
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="estimated_cost">Estimate Cost: </label>
                    <lable>'.$resultBookingDetails[0]['estimated_cost'].'</lable>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="total_pass">Total passengers: </label>
                    <lable>'.$pCount.'</lable>
                  </div>
                </div>                  
              </div>               
            </div>
          ';  
        }

        $data='
            <div class="box">
              <form role="form"  method="post" id="scheduleBookingInfo" name="scheduleBookingInfo">
                <div class="box-body">
                  <div class="row">
                    <h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Booking Cancelled By</h4>               
                    <div class="col-md-6">                                
                      <div class="form-group">
                        <label for="fname">Parent Name: </label>
                        <lable>'.$resultBookingDetails[0]['user_fullname'].'</lable>
                      </div>                              
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="mobile">Mobile: </label>
                        <lable>'.$resultBookingDetails[0]['user_phone_number'].'</lable>
                      </div>
                    </div>
                  </div>
                  <div class="row">                                   
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="user_gender">Gender: </label>
                        <lable>'.$resultBookingDetails[0]['user_gender'].'</lable>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="end_date">Address: </label>
                        <lable>'.$resultBookingDetails[0]['user_address'].'</lable>
                      </div>
                    </div>
                  </div>
                </div>
                '.$bookingData.'
                '.$vehicleData.'
                <div class="row"><h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Kid\'s Details</h4>               
                '.$kidDetails.'                  
                </div> 
              </div><!-- /.box-body -->   
            </form>                
          </div>'; 
          echo json_encode(array("status" => 'success',"data" => $data, "type" => $postData['type']));             
          } else { 
            echo(json_encode(array("status" => FALSE,'msg' => 'No data found.'))); 
          }      
        } else {
        echo(json_encode(array('status'=>'logout')));
        $this->session->sess_destroy();
        redirect('login');
      }
    }
  }
}
?>