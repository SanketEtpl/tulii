<?php
/*
* @author : Kiran.
* description: manage the booking ride map data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Driver_location_map extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Driver current location map', 
            'isActive'  => 'active'
        );
        $this->load->model("Driver_location_map_model","driver_loc");
        $this->load->model("common_model");        
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->view('includes/header',$this->data);
            $this->load->view('map_management/driver_current_location_map');
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of driver location list data
    {
        if(is_user_logged_in()){        

            $driverCurLocData = $this->driver_loc->get_datatables();   
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $lat = $lng = "";

            foreach ($driverCurLocData as $value) {
                $dlID = $value->loc_id;                
                
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($value->user_name);
                $row[] = $value->user_email;  
                $row[] = $value->user_phone_number;                                      
                
                $row[] ='   <a data-id="'.$i.'" class="btn btn-sm btn-info" data-row-id="'.$dlID.'" onclick="viewDriverCurLocMap(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Map">
                                <i class="fa fa-map-marker"></i>
                            </a>                
                                                     
                        ';
                    
                $data[] = $row;             
                $i++;
            }
     
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->driver_loc->count_all(),
                "recordsFiltered" => $this->driver_loc->count_filtered(),
                "data" => $data
            );

            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function viewDriverLocMap() // view data of driver current location  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                  
                if( !empty($postData['key']) ) {
                    $resultDriverCurrentLoc = $this->Common_model->select("loc_lat, loc_long", TB_DRIVER_LOCATION, array("loc_id" => $postData['key']));     
                    
                    if($resultDriverCurrentLoc){                                                
                        echo json_encode(array("status" => "success", "action" => "view", "latLng" => $resultDriverCurrentLoc[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }                  
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please provide driver location id.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 
}