<!--*

Author : Rajendra pawar 
Page :  enquiryList.php
Description : Enquiry List  use for enquiry managment module

*-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/enquiry_management.js" charset="utf-8"></script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-search"></i> Enquiry Management/Enquiry List
        <small>View</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                
                <div class="box-body table-responsive no-padding">
                <table id="enquiryList-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                   <thead>
                       <tr>
                       <th>Sr. no</th>
                       <th>Service Type</th>
                       <th>Name</th>
                       <th>Address</th>
                       <th>Mobile</th>
                       <th>Email</th>
                       <th>Action</th>
                    </tr>
                 </thead>
                 <tfoot> 
                 <tr>
                      <th>Sr. no</th>
                       <th>Service Type</th>
                       <th>Name</th>
                       <th>Address</th>
                       <th>Mobile</th>
                       <th>Email</th>
                       <th>Action</th>
                    </tr>
                    </tfoot>
                </table>       
                </div><!-- /.box-body -->
                
              </div><!-- /.box -->
              </div>
        </div>
    </section>
</div>
 <script type="text/javascript">
  jQuery(document).ready(function(){
loadEnquiryList(); 
}); 
 </script>   
    




