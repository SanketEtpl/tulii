<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="root" >
  <link rel="shortcut icon" href="#" type="image/png">

  <title>404 Page</title>

  <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body>
 
<div class="container ">

	<div id="page-wrapper" style="margin-top:10px;">
		<div class="row">
		<div class="col-lg-4 top_pad_02"><img src="<?php echo base_url();?>assets/img/logos/logo.png" /></div>	
        <div class="col-lg-12"> 
			<h2>404 page not found</h2>
			<p>Sorry, but we are not able to locate this page. Either there was an error inputting the URL, the linking is incorrect or this page just does not exist.</p>
				<p>Here is what you can do now:</p>
				<ul>
					<li><a style="color:blue;" href="javascript: void(0);" onClick="javascript: history.go(-1);">Go Back</a> to the previous page.</li>
					<li>Click your browser's Reload / Refresh button.</li>
					<li>Go to the <a style="color: blue;" href="<?php echo base_url();?>index/">home</a> page.</li>
				</ul>
			<p style="clear:both;">If you feel the web address is correct, please contact the webmaster for additional help.</p>
		</div>
		</div>	
	</div>

</div> 
<!-- Placed js at the end of the document so the pages load faster -->
<!-- JavaScript -->
<script src="<?php echo base_url();?>js/jquery-1.10.2.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.js"></script>

<!-- Page Specific Plugins -->
<script src="<?php echo base_url();?>js/tablesorter/jquery.tablesorter.js"></script>
<script src="<?php echo base_url();?>js/tablesorter/tables.js"></script>

</body>
</html>
