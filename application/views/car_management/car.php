<?php //$this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> Car Category management
      <small>Update</small><small>Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addCategory" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add new"><i class="fa fa-plus"></i> Add new</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Id. no</th>
                    <th>Category name</th>
                    <th>Rate/mile</th>
                    <!-- <th>Capacity</th> -->
                    <th>Date</th>       
                    <th>Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                    <th>Id. no</th>
                    <th>Category name</th>
                    <th>Rate/mile</th>
                    <!-- <th>Capacity</th> -->
                    <th>Date</th>       
                    <th>Action</th>
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    <div id="categoryModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="CategoryForm" name="CategoryForm" novalidate>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 class="modal-title"><b>Add Car Category</b> </h3>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_type">Category<span class="required_star">*</span></label>
                   <input type="text" placeholder="Enter car category" required="true"  id="car_category" name="car_category" class="form-control name">
                </div>
              </div>                           
         
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group dollar-symbol-add">
                  <label class="control-label" for="lbl_subtype">Category rate<span class="required_star">*</span></label>
                  <span class="doller-assign">$</span>
                  <input type="text" placeholder="Enter car category rate" required="true"  id="car_category_rate" maxlength="4" name="car_category_rate" class="form-control ">
                </div>
              </div>
              <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_subtype">Capacity<span class="required_star">*</span></label>
                  <input type="text" placeholder="Enter capacity" required="true"  id="car_capacity" maxlength="2" name="car_capacity" class="form-control">
                </div>
              </div> -->                           
          </div>                         
          </div>
          <div class="modal-footer">
            <input type="hidden" name="user_key" id="user_key" value="">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" id="btnCategoryList">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
//var table; 
$(document).ready(function() { 
   $("#LoadingDiv").css({"display":"block"}); 
    //datatables
     catList();
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });
  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });


});
function catList(){
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"car_management/Car/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
               
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
                $("input[type=search]").keydown(function(event) {
                  k = event.which;
                  if (k === 32 && !this.value.length)
                      event.preventDefault();
                });
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
            $("#table").css({"opacity":"1"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,4], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    }); 
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  // location.reload();
  catList();
}

</script>
<script src="<?php echo base_url(); ?>assets/js/car_category/car_category.js"></script>
