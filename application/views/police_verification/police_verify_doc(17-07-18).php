<?php //$this->load->view('includes/header'); ?>

<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/imageviewer.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/imageviewer.js" charset="utf-8"></script>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-file"></i> Document verification management
      <small>Add/Edit/View/Download</small>
    </h1>
  </section>
  <section class="content">
    <?php if($this->session->userdata('role') != ROLE_ADMIN ) { ?>
    <!-- <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addPopUp" onClick="addPopUp()" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Upload documents"><i class="fa fa-plus"></i> Upload documents</a>
        </div>
      </div>
    </div> -->
    <?php } ?>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Id. no</th>
                    <th>Name</th>
                    <th>Contact no</th>
                    <th>Documents</th>
                    <th class="addClsStatus">Status</th>                    
                    <th class="addClsStatus">Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Name</th>
                  <th>Contact no</th>
                  <th>Documents</th>
                  <th class="addClsStatus">Status</th>  
                  <th class="addClsStatus">Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    <div id="policeVerifyModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="policeVerifyForm" name="policeVerifyForm" enctype="multipart/form-data" novalidate>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 class="modal-title"><b>Add documents</b> </h3>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
          <div class="modal-body">
             <div class="row">           
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_pk">Upload documents<span class="required_star">*</span></label>                  
                 <div>
                    <button type="button" class="addfiles" onclick="document.getElementById('uploadDocuments').click();">Choose file</button>
                    <input type="file" id="uploadDocuments" name="uploadDocuments[]" multiple style="display: none;" accept=".jpg, .jpeg, .png, .gif">                        
                    <span id="showFilename">No file chosen</span>
                  </div>                                    
                </div>
              </div>                         
                     
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_vn">Verification number<span class="required_star">*</span></label>
                  <input type="text" placeholder="Verification number" required="true" maxlength="20" id="verify_no" name="verify_no" class="form-control phone">
                </div>
              </div>
            </div>
           <!--  <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <div id="dvPreview">
                  </div>
                </div>
              </div> 
            </div> -->                  
          </div>
          <div class="modal-footer">
            <input type="hidden" name="user_key" id="user_key" value="">
            <input type="hidden" name="add_user_name" id="add_user_name" value="">
            <input type="hidden" name="copyOfImgPath" id="copyOfImgPath" value="">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" id="btnUploadDocuments">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div id="editpoliceVerifyModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="editpoliceVerifyForm" name="editpoliceVerifyForm" enctype="multipart/form-data">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 class="modal-title"><b>Edit documents</b> </h3>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_dl">Driving license<span class="required_star">*</span></label>
                  <select class="form-control" id="drivingLicense" name="drivingLicense">
                    <option value="" disable="" selected="" hidden=""> Select type</option>
                    <option value="0">Temporary</option>
                    <option value="1">Permanent</option>
                  </select>
                </div>
              </div> 
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="license_no">License no<span class="required_star">*</span></label>
                  <input type="text" placeholder="License no" required="true" maxlength="20" id="license_no" name="license_no" class="form-control licenseNo">
                </div>
              </div>                                                                                                 
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_vf">Valid from<span class="required_star">*</span></label>
                  <input type="text" placeholder="Valid from" id="valid_from" required="true" name="valid_from" readOnly class="form-control validFrom" autocomplete="off">
                </div>
              </div>              
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_vu">Valid until<span class="required_star">*</span></label> 
                  <input type="text" placeholder="Valid until" id="valid_until" required="true" name="valid_until" class="form-control validUntil" readOnly autocomplete="off">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_cm">Car model<span class="required_star">*</span></label>
                  <input type="text" placeholder="Car model" id="car_model" required="true" name="car_model" class="form-control title_valid" maxlength="20" autocomplete="off">
                </div>
              </div>              
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_crn">Car reg. no<span class="required_star">*</span></label> 
                  <input type="text" placeholder="Car reg. no" id="car_reg_no" required="true" name="car_reg_no" class="form-control charnumber" maxlength="20" autocomplete="off">
                </div>
              </div>
            </div>

            <div class="row">             
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_cnp">Car number plate<span class="required_star">*</span></label> 
                  <input type="text" placeholder="Car number plate" id="car_no_plate" required="true" name="car_no_plate" maxlength="12" class="form-control charnumber" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_te">Total experience<span class="required_star">*</span></label>
                  <input type="text" placeholder="Total experience" id="total_experience" required="true" name="total_experience" class="form-control charnumber" maxlength="20" autocomplete="off">
                </div>
              </div> 
            </div>


            <div class="row">           
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_pk">Upload car images<span class="required_star">*</span></label>                  
                  <div>
                    <button type="button" class="editCarImg" onclick="document.getElementById('uploadCarImage').click();">Choose file</button>
                    <input type="file" id="uploadCarImage" name="uploadCarImage[]" multiple style="display: none;" accept=".jpg, .jpeg, .png, .gif">                        
                    <span id="showFilenameCarImg">No file chosen</span>
                  </div>                                    
                </div>
              </div>      
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_pk">Scanned copy of license<span class="required_star">*</span></label>                  
                 <div>
                    <button type="button" class="editLicPic" onclick="document.getElementById('uploadScannedCpofLic').click();">Choose file</button>
                    <input type="file" id="uploadScannedCpofLic" name="uploadScannedCpofLic[]" multiple style="display: none;" accept=".jpg, .jpeg, .png, .gif">                        
                    <span id="showFilenameScanCpofLic">No file chosen</span>
                  </div>                                    
                </div>
              </div>
            </div>  
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_csc">Car seating capacity<span class="required_star">*</span></label>
                  <input type="text" placeholder="Car seating capacity" id="car_seating_capacity" required="true" name="car_seating_capacity" class="form-control age" maxlength="2" autocomplete="off">
                </div>
              </div> 
            </div>              
          </div>
          <div class="modal-footer">
            <input type="hidden" name="user_name" id="user_name" value="">
            <input type="hidden" name="user_id" id="user_id" value=""> 
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" id="editbtnUploadDocuments">Save</button>
          </div>
        </div>
            </div>
      </form>
  </div>
  <div class="modal fade" id="downloadModal" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close close-page-refresh" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="exampleModalLabel"><b>Download documnets</b></h3>
            </div>
            <div class="modal-body">
                 <div class="row">
                    <div class="col-md-12 rid_tim">
                        <label>All documents :</label>
                        <div class="rid_info popupFixedWindow" id="documentsDown"></div>
                    </div>
            </div>
            <div class="modal-footer">
              <input type="hidden" name="dl_user_key" id="dl_user_key" value="">
              <input type="hidden" name="dl_user_name" id="dl_user_name" value="">
              <button type="button" class="btn btn-secondary close-page-refresh" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
  </div>
</div>

  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
$(function () {
    $("#uploadDocuments").change(function () {
        if (typeof (FileReader) != "undefined") {
           // var dvPreview = $("#dvPreview");
            $("#btnUploadDocuments").attr('disabled',false);
           // dvPreview.html("");
            var regex = /^([a-zA-Z0-9()\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        /*var img = $("<img />");
                        img.attr("style", "height:100px;width: 100px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);*/
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                  $("#btnUploadDocuments").attr('disabled',true);
                  $(".uploadDocuments").remove();  
                  $("#uploadDocuments").parent().append("<div class='uploadDocuments' style='color:red;'>"+file[0].name + " is not a valid image file.</div>");
                  return false;
                }
            });
        } else {
          $(".uploadDocuments").remove();  
          $("#uploadDocuments").parent().append("<div class='uploadDocuments' style='color:red;'>This browser does not support HTML5 FileReader.</div>");
          return false;            
        }
    });

    $("#uploadCarImage").change(function () {
        if (typeof (FileReader) != "undefined") {
            $("#editbtnUploadDocuments").attr('disabled',false);
            var regex = /^([a-zA-Z0-9()\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                  $("#editbtnUploadDocuments").attr('disabled',true);
                  $(".uploadCarImage").remove();  
                  $("#uploadCarImage").parent().append("<div class='uploadCarImage' style='color:red;'>"+file[0].name + " is not a valid image file.</div>");
                  return false;
                }
            });
        } else {
          $(".uploadCarImage").remove();  
          $("#uploadCarImage").parent().append("<div class='uploadCarImage' style='color:red;'>This browser does not support HTML5 FileReader.</div>");
          return false;            
        }
    });

    $("#uploadScannedCpofLic").change(function () {
        if (typeof (FileReader) != "undefined") {
            $("#editbtnUploadDocuments").attr('disabled',false);
            var regex = /^([a-zA-Z0-9()\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                  $("#editbtnUploadDocuments").attr('disabled',true);
                  $(".uploadScannedCpofLic").remove();  
                  $("#uploadScannedCpofLic").parent().append("<div class='uploadScannedCpofLic' style='color:red;'>"+file[0].name + " is not a valid image file.</div>");
                  return false;
                }
            });
        } else {
          $(".uploadScannedCpofLic").remove();  
          $("#uploadScannedCpofLic").parent().append("<div class='uploadScannedCpofLic' style='color:red;'>This browser does not support HTML5 FileReader.</div>");
          return false;            
        }
    });
});

// var table; 
$(document).ready(function() { 
    //datatables
$("#LoadingDiv").css({"display":"block"}); 
 verificationList();
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });
  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

});

function verificationList()
{
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
      "processing": false, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "order": [], //Initial no order.

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": baseURL+"police_verification/Police_verify_doc/ajax_list",
          beforeSend: function() {
            var search = $("input[type=search]").val();
            if(search=="")            
              $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
           },
          complete: function(){
          $("#LoadingDiv").css({"display":"none"}); 
          $("#table").css({"opacity":"1"});
        },
          "type": "POST"
      },
      
      //Set column definition initialisation properties.
      "columnDefs": [
      { 
          "targets": [ 0,3,4,5 ], //first column / numbering column
          "orderable": false, //set not orderable
      },
      ],
  }); 
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  verificationList();
  //location.reload();
}
</script>
<script src="<?php echo base_url(); ?>assets/js/police_verification/police_verify.js"></script>
