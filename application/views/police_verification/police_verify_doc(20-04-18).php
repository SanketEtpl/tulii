<?php $this->load->view('includes/header'); ?>

<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/imageviewer.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/imageviewer.js" charset="utf-8"></script>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-file"></i> Document verification
      <small>Add/View/Download/Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Upload documents"><i class="fa fa-plus"></i> Upload documents</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%">
              <thead>
                  <tr>
                    <th>Id. no</th>
                    <th>Name</th>
                    <th>Contact no</th>
                    <th>Documents</th>
                    <th>Status</th>                    
                    <th>Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Name</th>
                  <th>Contact no</th>
                  <th>Documents</th>
                  <th>Status</th>  
                  <th>Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    <div id="policeVerifyModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="policeVerifyForm" name="policeVerifyForm" enctype="multipart/form-data" novalidate>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 class="modal-title"><b>Add documents</b> </h3>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
          <div class="modal-body">
             <div class="row">           
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_pk">Upload documents *</label>                  
                 <div>
                    <button type="button" class="addfiles" onclick="document.getElementById('uploadDocuments').click();">Choose file</button>
                    <input type="file" id="uploadDocuments" name="uploadDocuments[]" multiple style="display: none;">                        
                    <span id="showFilename">No file chosen</span>
                  </div>                                    
                </div>
              </div>              
            
            </div>  
            <div class="row">           
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="lbl_vn">Verification number *</label>
                  <input type="text" placeholder="Verification number" required="true" maxlength="20" id="verify_no" name="verify_no" class="form-control phone">
                </div>
              </div>
            </div>
           <!--  <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <div id="dvPreview">
                  </div>
                </div>
              </div> 
            </div> -->                  
          </div>
          <div class="modal-footer">
            <input type="hidden" name="user_key" id="user_key" value="">
            <input type="hidden" name="copyOfImgPath" id="copyOfImgPath" value="">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" id="btnUploadDocuments">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="modal fade" id="downloadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Download documnets</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class="row">
                    <div class="col-md-12 rid_tim">
                        <label>All documents :</label>
                        <div class="rid_info" id="documentsDown"></div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
  </div>
</div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Delete documnets</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                   <div class="row">
                      <div class="col-md-12 rid_tim">
                          <label>All documents :</label>
                          <div class="rid_info" id="deleteDocuments"></div>
                      </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
    </div>
  </div>
  </section>
</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
$(function () {
    $("#uploadDocuments").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#dvPreview");
            $("#btnUploadDocuments").attr('disabled',false);
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9()\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        /*var img = $("<img />");
                        img.attr("style", "height:100px;width: 100px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);*/
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                  $("#btnUploadDocuments").attr('disabled',true);
                  $(".uploadDocuments").remove();  
                  $("#uploadDocuments").parent().append("<div class='uploadDocuments' style='color:red;'>"+file[0].name + " is not a valid image file.</div>");
                  return false;
                }
            });
        } else {
          $(".uploadDocuments").remove();  
          $("#uploadDocuments").parent().append("<div class='uploadDocuments' style='color:red;'>This browser does not support HTML5 FileReader.</div>");
          return false;            
        }
    });
});

var table; 
$(document).ready(function() { 

 
  $('.addfiles').on('click', function() {     
    $('#slider_img').click();return false;
  });
 
  $('input[type="file"]').change(function(e){
    var names = [];
    for (var i = 0; i < $(this).get(0).files.length; ++i) {
        names.push('<br />'+ $(this).get(0).files[i].name);
    }
    $('#showFilename').html(names);    
  });
    //datatables
    table = $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"police_verification/Police_verify_doc/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
              $("#LoadingDiv").css({"display":"block"}); 
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,5 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    });  
  $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText"><i class="fa fa-times-circle"></i></button></div>');  
  $("#clearText").on("click",function(){
    $("input[type=search]").val("");
    location.reload();
    //$("#table").dataTable().fnDraw();
  }); 
});
</script>
<script src="<?php echo base_url(); ?>assets/js/police_verification/police_verify.js"></script>
