<!--*
Author : Kiran n
Page :  sameDestination.php
Description : show Multiple ride with same destination driver /care details
*-->
<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/tab.css" />
<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/imageviewer.css" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/childcare/sameDestination.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/imageviewer.js" charset="utf-8"></script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i>Care driver management/Carpooling/Multiple ride with same destination
        <small>View/Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">                
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">               
                <div class="box-body table-responsive no-padding">                
                <table id="care-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                      <thead>
                        <tr>
                           <th>Id no</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Mobile</th>
                           <th>Gender</th>
                           <th style="min-width:120px;">Status</th>
                           <th>Action</th>       
                        </tr>
                      </thead>
                      <tfoot> 
                        <tr>
                           <th>Id no</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Mobile</th>
                           <th>Gender</th>
                           <th style="min-width:120px;">Status</th>
                           <th>Action</th>
                        </tr>
                      </tfoot>
                </table>       
                </div><!-- /.box-body -->
                
              </div><!-- /.box -->
              </div>

        </div>
    </section>
</div>


<script type="text/javascript">
  $(function(){
    $('a[title]').tooltip();
  }); 
  $(document).ready(function(){
    loadSameDestinationList(); 
    $("input[type=search]").on("keyup",function() {
      if($("#clear").length == 0) {
        if($(this).val() != ""){
          $("#care-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
        } 
      }
      if($(this).val() == "")  
      $("#clear").remove();      
    });

    $("input[type=search]").keydown(function(event) {
      k = event.which;
      if(k === 32 && !this.value.length)
        event.preventDefault();
    });
  });
</script>   



