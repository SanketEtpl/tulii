<!--*

Author : Rajendra pawar 
Page :  careDriverList.php
Description : care driver list use for care driver managment functionality

*-->
<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/careDriverManagement.css" />
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Care Driver Management/Care Driver
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                 <button type="button" onclick="$('#addCareDriverModel').modal('show');" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a></button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Care Driver List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>careDriver/listOperation" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" onchange=" $('#searchList').submit();" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button  class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                      <th>Status</th>
                      <th class="text-center">Actions</th>
                    </tr>
                    <?php
                    $status_id=1;
                    $rowActive_id="active".$status_id;
                    $rowInActive_id="inActive".$status_id;
                    if(!empty($userRecords))
                    {
                        foreach($userRecords as $record)
                        {
                    ?>
                    <tr>
                      <td><?php echo $record->user_id ?></td>
                      <td><?php echo $record->user_name ?></td>
                      <td><?php echo $record->user_email ?></td>
                      <td><?php echo $record->user_phone_number ?></td>
                      <td><?php 
                      if($record->user_status == 1)
                      {  $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="careDriverStatus('.$record->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');" ';
                         $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="careDriverStatus('.$record->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');"';  } 
                    else{ $active_btn_class='class="btn btn-sm btn-active-disable" onclick="careDriverStatus('.$record->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');"';
                          $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="careDriverStatus('.$record->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');" ';}
                      echo '<button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button>&nbsp;';
                      echo '<button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button>&nbsp;';

                      ?></td>
                      <td class="text-center">
                        <a class="btn btn-sm btn-info editCareDriver" href="#" data-userid="<?php echo $record->user_id; ?>"><i class="fa fa-pencil"></i></a>                          
                          <a class="btn btn-sm btn-danger deleteCareDriver" href="#" data-userid="<?php echo $record->user_id; ?>"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php
                     $status_id++;
                    $rowActive_id="active".$status_id;
                    $rowInActive_id="inActive".$status_id;
                        }
                    }
                    ?>
                  </table>                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
              </div>
        </div>
    </section>
</div>
<!-- Edit care driver popup start -->
<div class="modal fade" id="editCareDriverModel" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-users"></i> Care Driver Management/Care Driver
        <small>Edit</small>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></h5>
      </div>
      <div class="modal-body">
      <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>careDriver/editCareDriver" method="post" id="editcareDriverFrm" name="editcareDriverFrm">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control required" id="fname" placeholder="Full Name" name="fname"  maxlength="128">
                                        <input type="hidden"  name="userId" id="userId" />    
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="email" onblur="checkEmilExist('validate');" onchange="checkEmilExist('validate');" class="form-control required" id="email" placeholder="Enter email" name="email"  maxlength="128">
                                      <label for="email_new" class="error" style="display:none;" id="email_error_id">Email already taken</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" name="cpassword" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control required" id="mobile" placeholder="Mobile Number" name="mobile"  maxlength="10">
                                    </div>
                                </div>
                               </div>
                        </div><!-- /.box-body -->                   
                </div>

      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" onclick="checkEmilExist('submit');" class="btn btn-primary">Save changes</button>
         </form>
      </div>
    </div>
  </div>
</div>
<!-- Edit care driver popup end -->
<!-- Add care driver popup start -->
<div class="modal fade" id="addCareDriverModel" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLbl"><i class="fa fa-users"></i> Care Driver Management/Care Driver
        <small>Add</small>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></h5>
      </div>
      <div class="modal-body">
      <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>careDriver/addCareDriver" method="post" id="addcareDriverfrm" name="addcareDriverfrm">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname_new">Full Name</label>
                                        <input type="text" class="form-control required" id="fname_new" placeholder="Full Name" name="fname_new"  maxlength="128">                                       
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email_new">Email address</label>
                                        <input type="email" class="form-control required" id="email_new" placeholder="Enter email" name="email_new"  maxlength="128">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password_new">Password</label>
                                        <input type="password" class="form-control required" id="password_new" placeholder="Password" name="password_new" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword_new">Confirm Password</label>
                                        <input type="password" class="form-control required" id="cpassword_new" placeholder="Confirm Password" name="cpassword_new" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile_new">Mobile Number</label>
                                        <input type="text" class="form-control required" id="mobile_new" placeholder="Mobile Number" name="mobile_new"  maxlength="10">
                                    </div>
                                </div>
                                  
                            </div>
                        </div><!-- /.box-body -->                   
                </div>

      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
         </form>
      </div>
    </div>
  </div>
</div>
<!-- Add care driver popup end -->

 <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/careDriverManagement.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "careDriver/listOperation/" + value);
            jQuery("#searchList").submit();
        });
    });
    
</script>       
    




