<!--*
Author : Kiran n 
Page :  bookingSchedule.php
Description : Booking schedule List  use for track booking details in admin
*-->



<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    <i class="fa fa-check-square"></i> Booking Management/Booking Schedule(Ride, Care & Tutor)
      <small>View</small> 
    </h1>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">                
      </div>
    </div>
    <ul class="nav nav-tabs nav-justified">
      <li class="active"><a data-toggle="tab" href="#ride">Ride Schedule</a></li>
      <li><a data-toggle="tab" onclick="loadBookingScheduleCare()" href="#care">Care Schedule</a></li>
      <li><a data-toggle="tab" onclick="loadBookingScheduleTutor()" href="#tutor">Tutor Schedule</a></li>      
    </ul>
    <div class="tab-content">
      <div id="ride" class="tab-pane fade in active">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">                
              <div class="box-body table-responsive no-padding">
                <table id="bookingScheduleRideList"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%" style="opacity:0">
                  <thead>
                    <tr>
                      <th style="max-width:30px">Id.no</th>
                      <th>Pick-up Point</th>
                      <th>Drop-up Point</th>
                      <th style="max-width:70px">Ride Start Date</th>
                      <th style="max-width:70px">Ride End Date</th>
                      <th style="max-width:40px">Distance</th>
                      <th style="max-width:30px">Cost</th>
                      <th style="max-width:25px">Action</th>
                    </tr>
                  </thead>
                  <tfoot> 
                    <tr>
                      <th style="max-width:30px">Id.no</th>
                      <th>Pick-up Point</th>
                      <th>Drop-up Point</th>
                      <th style="max-width:70px">Ride Start Date</th>
                      <th style="max-width:70px">Ride End Date</th>
                      <th style="max-width:40px">Distance</th>
                      <th style="max-width:30px">Cost</th>
                      <th style="max-width:25px">Action</th>
                    </tr>
                  </tfoot>
                </table>       
              </div><!-- /.box-body -->          
            </div><!-- /.box -->
          </div>
        </div>
      </div>
      <div id="care" class="tab-pane fade">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">                
              <div class="box-body table-responsive no-padding">
                <table id="bookingScheduleCareList"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%"
                >
                  <thead>
                    <tr>
                      <th>Id.no</th>
                      <th>Care Location</th>
                      <th>Care Start Date</th>
                      <th>Care End Date</th>                      
                      <th>Care Start Time</th>
                      <th>Care End Time</th>
                      <th>Cost</th>                      
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot> 
                    <tr>
                      <th>Id.no</th>
                      <th>Care Location</th>
                      <th>Care Start Date</th>
                      <th>Care End Date</th>                      
                      <th>Care Start Time</th>
                      <th>Care End Time</th>
                      <th>Cost</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>       
              </div><!-- /.box-body -->          
            </div><!-- /.box -->
          </div>
        </div>
      </div>
      <div id="tutor" class="tab-pane fade">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">                
              <div class="box-body table-responsive no-padding">
                <table id="bookingScheduleTutorList"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%"
                >
                  <thead>
                    <tr>
                      <th>Id.no</th>
                      <th>Tutor Category</th>
                      <th>Tutor Sub-category</th>
                      <th>Tutor Start Date</th>                      
                      <th>Tutor End Time</th>
                      <th>Estimate Cost</th>                      
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot> 
                    <tr>
                      <th>Id.no</th>
                      <th>Tutor Category</th>
                      <th>Tutor Sub-category</th>
                      <th>Tutor Start Date</th>                      
                      <th>Tutor End Time</th>
                      <th>Estimate Cost</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>       
              </div><!-- /.box-body -->          
            </div><!-- /.box -->
          </div>
        </div>
      </div>    
    </div>
  </section>

</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/booking/bookingSchedule.js" charset="utf-8"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#LoadingDiv').show();
    loadBookingScheduleRideList();    
  }); 
</script>   
    





