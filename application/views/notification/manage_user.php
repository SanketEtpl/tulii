<?php //$this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-bell"></i> Notification management/Manage user      
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <!-- <a class="btn btn-primary" id="addPopUp" href="javascript:void(0)"><i class="fa fa-plus"></i> Select all</a> -->
          <a class="btn btn-primary" id="btnSendNotification" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Send notification"><i class="fa fa-plus"></i> Send notification</a>
        </div>        
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" id="dynamic_field" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th width="76px"><div class="custombox"><input type="checkbox" name="selectAll" class="selectAll" id="selectAll"><span class="checkspan"></span></div>All</th>
                    <th>Email</th>
                    <th>User name</th>
                    <th>Mobile no</th>
                    <th>Role</th>                                        
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th></th>
                  <th>Email</th>
                  <th>User name</th>
                  <th>Mobile no</th>
                  <th>Role</th> 
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
   <div id="manageNotifModal" class="modal fade" data-backdrop="static" style="display: none;" aria-hidden="false">
      <form action="" id="manageNotifForm" name="manageNotifForm" novalidate>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h3 class="modal-title"><b>Send notification</b></h3>
            </div>
            <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label" for="lbl_name">Message<span class="required_star">*</span></label>
                    <textarea name="message" id="message" maxlength="255" autocomplete="off" class="form-control"></textarea>
                    <input type="hidden" id="user_key" name="user_key">                    
                  </div>
                </div>                             
              </div>    
            </div>
            <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-info" type="button" id="btnSave">Send</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
var table; 
//var ischecked=false;
$(document).ready(function() { 
$("#LoadingDiv").css({"display":"block"}); 
  //datatables
  manageuserList();
    $("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

});

function manageuserList()
{
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": baseURL+"notification_management/Manage_user/ajax_list",
         /*data: function( d ) {
          d.ischecked= ischecked;       
        },*/
       beforeSend: function() {
          var search = $("input[type=search]").val();
          if(search == "")
          
            $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
         },
        complete: function(){
          
          $("#LoadingDiv").css({"display":"none"}); 
          $("#selectAll").prop("checked",false);
        /*$('#selectAll').change(function(){
          if(this.checked == true)
            ischecked = true;
          else
            ischecked = false;              
        });*/
        $("#LoadingDiv").css({"display":"none"}); 
        $("#table").css({"opacity":"1"}); 
      },
        "type": "POST"
    },
    
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [0], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  });
  
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  // location.reload();
  manageuserList();
}
</script>
<script src="<?php echo base_url(); ?>assets/js/notification/manageUser.js"></script>
