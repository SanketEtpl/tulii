<?php 
$segmentMenu = $this->uri->segment(1);
$segmentMenu1 =$this->uri->segment(2);
?>
<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="icon" type="image/jpg" href="<?php echo base_url();?>assets/images/favicon.png" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui.css"/>
    <!-- <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" /> -->
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatable/jquery.dataTables.min.css"/> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css"/> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/media.css"/>

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY;?>&sensor=false&libraries=places"></script>
    <script type="text/javascript">
     $(function() {
     //   $(".dateOfBirth").datepicker();
        $( ".dateOfBirth" ).datepicker({ 
          dateFormat: 'yy-mm-dd', 
          changeMonth:true,
          changeYear:true,
          minDate:"-17y",
          maxDate:"-5y",                                    
        });         
        $( ".validFrom" ).datepicker({      
          dateFormat: 'yy-mm-dd',      
          changeMonth:true,
          changeYear:true,
          maxDate:"-0d",
          //minDate:"-5y"                          
        });        
        $( ".validUntil" ).datepicker({ 
          dateFormat: 'yy-mm-dd',           
          changeMonth:true,
          changeYear:true,
          //maxDate:"+20y",                          
          minDate:"+0d"          
        });

       
    });
        var baseURL = "<?php echo base_url(); ?>";
    </script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     <link href="<?php echo base_url();?>assets/css/toastr.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url(); ?>assets/js/toastr.min.js"></script>


  </head>
  <body class="skin-blue sidebar-mini">
    <div id="LoadingDiv" class="loading" style="display:none">
      <img src="<?php echo base_url(); ?>assets/images/loader.gif">        
      <div class="loading-data" style="text-align:center;text-color:black;margin-top:10px"><b>Please wait while loading data</b></div>
    </div>
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?php echo base_url(); ?>assets/images/logomn.png" alt="Tulii"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="<?php echo base_url(); ?>assets/images/logolg.png" alt="Tulii"></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php echo $this->session->userdata('name'); ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header" style="height: 200px;">
                    <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="img-circle" alt="User Image" />
                    <p>
                      <?php echo $this->session->userdata('name'); ?>
                      <small><?php echo $this->session->userdata('roleText'); ?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="javascript:void(0)" id="loadChangePass" class="btn btn-default btn-flat"><i class="fa fa-key"></i> Change Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu dashboard">
            <li class="header">MAIN NAVIGATION</li>
            <li class="<?php if(in_array($segmentMenu, array("dashboard",""))){echo " active";} ?>">
              <a href="<?php echo base_url(); ?>dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
             </ul>
           <?php

           /*
           *
           * Parent & admin role
           *
           */
           if($this->session->userdata('role') == ROLE_PARENTS || $this->session->userdata('role') == ROLE_ADMIN)
            {           
            ?>  
            <!-- User management menu for Parent & Admin role start -->
           <ul class="sidebar-menu user_magagement"> <!-- Ram K -->
            <li class="<?php if(in_array($segmentMenu, array("userList",""))){echo " active";} ?>">
              <a href="<?php echo base_url(); ?>userList">  <i class="fa fa-users" aria-hidden="true"></i>
                <span>User management</span>
              </a>
            </li>
          </ul>
            <!-- User management menu for Parent & Admin role end  -->
          <?php  }  



           /*
           *
           * Service provider & admin role
           *
           */


            if($this->session->userdata('role') == ROLE_SERVICE_PROVIDER || $this->session->userdata('role') == ROLE_ADMIN)
            {
             ?>    
             <ul class="sidebar-menu police_verify_magagement"> <!-- Kiran -->
              <li class="<?php if(in_array($segmentMenu, array("police-verify",""))){echo " active";} ?>">
                <a href="<?php echo base_url(); ?>police-verify">  <i class="fa fa-file" aria-hidden="true"></i>
                  <span>Document verification management</span>
                </a>
              </li>
            </ul>
            <!-- service provider management menu for Admin & service provider role start -->
          <!-- <ul class="sidebar-menu  home_content service_magagement"> 
            <li class="treeview">
              <a href="#">  <i class="fa fa-users" aria-hidden="true"></i>
                <span>Service provider Management</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
               <ul class="treeview-menu"> 

               <ul class="sidebar-menu rides_mng">        
              <li class="treeview">
              <a href="#">
                <i class="fa fa-circle" aria-hidden="true"></i><span>Rides</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview <?php if(in_array($segmentMenu1, array(1,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/1">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Single Rides</span></a></li>

                <li class="treeview <?php if(in_array($segmentMenu1, array(2,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/2">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Multiple Rides With</span><br><span style="margin-left:10%;">Same Destination</span></a></li>

                   <li class="treeview <?php if(in_array($segmentMenu1, array(3,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/3">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Multiple Rides With</span><br> <span style="margin-left:10%;">Different Destination</span></a></li>

                   <li class="treeview <?php if(in_array($segmentMenu1, array(4,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/4">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Child Car Pooling</span></a></li>

               </ul>
            </li>
            </ul>

              <ul class="sidebar-menu schoolCare_mng">        
              <li class="treeview">
              <a href="#">
                <i class="fa fa-circle" aria-hidden="true"></i><span>After School Care</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview <?php if(in_array($segmentMenu1, array(5,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/5">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Child Care(Baby Sitting)</span></a></li>

                <li class="treeview <?php if(in_array($segmentMenu1, array(6,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/6">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Care Driver(Ride & Care)</span></a></li>

                <li class="treeview <?php if(in_array($segmentMenu1, array(7,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/7">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>My Child’s CareDriver (Ride & Care)</span></a></li>

                 <li class="treeview <?php if(in_array($segmentMenu1, array(8,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/8">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>My Children’s CareDriver (Ride & Care for Siblings)</span></a></li>

               </ul>
            </li>
            </ul>

              <ul class="sidebar-menu tutor_mng">        
              <li class="treeview">
              <a href="#">
                <i class="fa fa-circle" aria-hidden="true"></i><span>Premier Tutoring</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview <?php if(in_array($segmentMenu1, array(9,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/9">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>My Child Tutor</span></a></li>
               </ul>
            </li>
          </ul>
              </ul>

             </ul> 
            </li>
          </ul> -->

           <!-- service provider management menu for Admin & service provider role  end -->

          
            <?php
            } 



           /*
           *
           *  Admin , service provider & Parent role
           *
           */

             if($this->session->userdata('role') == ROLE_ADMIN || $this->session->userdata('role') == ROLE_SERVICE_PROVIDER || $this->session->userdata('role') == ROLE_PARENTS)
            {  ?>

            <!-- Booking management menu for Admin , service provider role start -->
             <!-- <ul class="sidebar-menu home_content booking_mng">        
            <li class="treeview">
              <a href="#">
                <i class="fa fa-check-square" aria-hidden="true"></i><span>Booking management</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview <?php if(in_array($segmentMenu, array("rides",""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>bookingList/rides">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Rides</span></a></li>

                  <li class="treeview <?php if(in_array($segmentMenu, array("schoolCare",""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>bookingList/schoolCare">
                  <i class="fa fa-circle-o" aria-hidden="true"></i><span>After school care</span></a></li>


               <li class="treeview <?php if(in_array($segmentMenu, array("tutor",""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>bookingList/tutor">
                  <i class="fa fa-circle-o" aria-hidden="true"></i><span>Premier Tutoring</span></a></li>
              </ul>
            </li>
          </ul>
          </ul> -->
          <!-- Booking management menu for Admin , service provider role end -->
           <?php }





           /*
           *
           *  admin role
           *
           */

          
            if($this->session->userdata('role') == ROLE_ADMIN)
            {
            ?>
            <ul class="sidebar-menu  home_content service_magagement"> 
            <li class="treeview">
              <a href="#">  <i class="fa fa-users" aria-hidden="true"></i>
                <span>Service provider management</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
               <ul class="treeview-menu"> 

               <ul class="sidebar-menu rides_mng">        
              <li class="treeview">
              <a href="#">
                <i class="fa fa-circle" aria-hidden="true"></i><span>Rides</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview <?php if(in_array($segmentMenu1, array(1,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/1">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Single rides</span></a></li>

                <li class="treeview <?php if(in_array($segmentMenu1, array(2,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/2">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Multiple rides with</span><br><span style="margin-left:10%;">Same destination</span></a></li>

                   <li class="treeview <?php if(in_array($segmentMenu1, array(3,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/3">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Multiple rides with</span><br> <span style="margin-left:10%;">different destination</span></a></li>

                   <li class="treeview <?php if(in_array($segmentMenu1, array(4,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/4">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Child car pooling</span></a></li>

               </ul>
            </li>
            </ul>

              <ul class="sidebar-menu schoolCare_mng">        
              <li class="treeview">
              <a href="#">
                <i class="fa fa-circle" aria-hidden="true"></i><span>After school care</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview <?php if(in_array($segmentMenu1, array(5,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/5">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Single Child Care</span></a></li>

                <li class="treeview <?php if(in_array($segmentMenu1, array(6,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/6">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Multiple Siblings Care</span></a></li>

                <li class="treeview <?php if(in_array($segmentMenu1, array(7,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/7">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Single Child Ride & Care</span></a></li>

                 <li class="treeview <?php if(in_array($segmentMenu1, array(8,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/8">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Multiple Siblings Ride & Care</span></a></li>

               </ul>
            </li>
            </ul>

              <ul class="sidebar-menu tutor_mng">        
              <li class="treeview">
              <a href="#">
                <i class="fa fa-circle" aria-hidden="true"></i><span>Premier tutoring</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview <?php if(in_array($segmentMenu1, array(9,""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>service-provider/service/9">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>My child tutor</span></a></li>
               </ul>
            </li>
          </ul>
              </ul>

             </ul> 
            </li>
          </ul>
          <ul class="sidebar-menu home_content booking_mng">        
            <li class="treeview">
              <a href="#">
                <i class="fa fa-check-square" aria-hidden="true"></i><span>Booking management</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview <?php if(in_array($segmentMenu, array("rides",""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>bookingList/rides">
                <i class="fa fa-circle-o" aria-hidden="true"></i><span>Rides</span></a></li>

                  <li class="treeview <?php if(in_array($segmentMenu, array("schoolCare",""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>bookingList/schoolCare">
                  <i class="fa fa-circle-o" aria-hidden="true"></i><span>After school care</span></a></li>


               <li class="treeview <?php if(in_array($segmentMenu, array("tutor",""))){echo " active";} ?>" ><a href="<?php echo base_url(); ?>bookingList/tutor">
                  <i class="fa fa-circle-o" aria-hidden="true"></i><span>Premier tutoring</span></a></li>
              </ul>
            </li>
          </ul>
          </ul>


          





            <!-- banner management menu for Admin role start -->
          <ul class="sidebar-menu home_content slider_management">        
            <li class="treeview">
              <a href="#">
                <i class="fa fa-image" aria-hidden="true"></i><span>Slider management</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview <?php if(in_array($segmentMenu, array("slider-list",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>slider-list">
                <i class="fa fa-upload" aria-hidden="true"></i><span>Upload slider</span></a></li>
             </ul>
            </li>
          </ul>
          <!-- banner management menu for Admin role end -->
                    
           <!-- General management menu for Admin role start -->
             <ul class="sidebar-menu home_content general_management">        
            <li class="treeview">
              <a href="#">
                <i class="fa fa-users" aria-hidden="true"></i><span>General management</span>
                <span class="pull-right-container">
                  <i class="fa fa-plus pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu1, array("aboutUs/aboutUsDetail",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>aboutUs/aboutUsDetail">
                <i class="fa fa-info-circle" aria-hidden="true"></i><span>About us</span></a></li>

                <li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu1, array("contactUs/contactDetails",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>contactUs/contactDetails">
                <i class="fa fa-phone"></i><span>Contact us</span></a></li>
               <li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu1, array("terms/termsDetail",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>terms/termsDetail">
                <i class="fa fa-book"></i><span>Terms & Conditions</span></a></li>

                <li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu1, array("rating/ratingDetails",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>rating/ratingDetails">
                <i class="fa fa-star"></i><span>Ratings</span></a></li> 
               </ul>
            </li>
          </ul>
           <!-- General management menu for Admin role end -->


            <!-- Care driver management menu for Admin role start -->
             <!-- <ul class="sidebar-menu home_content car_management">        
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list" aria-hidden="true"></i><span>Care driver management</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-down pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview <?php //if(in_array($segmentMenu.'/'.$segmentMenu1, array("careDriver/listOperation",""))){echo " active";} ?>"><a href="<?php //echo base_url(); ?>careDriver/listOperation">
                <i class="fa fa-users" aria-hidden="true"></i><span>Care Driver</span></a></li>
              </ul>
            </li>
          </ul> -->
          </ul>
           <ul class="sidebar-menu parent_management">        
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-tasks" aria-hidden="true"></i><span>Parent management</span>
                  <span class="pull-right-container">
                    <i class="fa fa-plus pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">                             
                  <li class="treeview <?php if(in_array($segmentMenu, array("parent-profile-data",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>parent-profile-data"><i class="fa fa-info-circle"></i><span>Parent info</span></a></li>
                 </ul>
              </li>
            </ul>
           <!--Care driver management menu for Admin role end --> 

           <!-- Tutor management menu for Admin role start -->
             <!-- <ul class="sidebar-menu home_content tutor_mng">        
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list" aria-hidden="true"></i><span>Tutor management</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-down pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview"><a href="<?php //echo base_url(); ?>tutorList/listOperation">
                <i class="fa fa-users" aria-hidden="true"></i><span>Tutor list</span></a></li>
              </ul>
            </li>
          </ul> -->
          </ul>
           <!-- Tutor management menu for Admin role end -->
            <!--  enquiry management menu for Admin role start -->
          <!-- <ul class="sidebar-menu home_content enquiry_mng">        
            <li class="treeview">
              <a href="#">
                <i class="fa fa-list" aria-hidden="true"></i><span>Enquiry management</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-down pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">           
                <li class="treeview"><a href="<?php echo base_url(); ?>enquiryList/listOperation">
                <i class="fa fa-search" aria-hidden="true"></i><span>Enquiry List</span></a></li>
              </ul>
            </li>
          </ul> -->
          </ul>
           <!-- enquiry management menu for Admin role end -->
             
           <!-- <ul class="sidebar-menu childcare">        
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-list" aria-hidden="true"></i><span>Child care provider management</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-down pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">           
                  <li class="treeview <?php //if(in_array($segmentMenu, array("childcare",""))){echo " active";} ?>"><a href="<?php //echo base_url(); ?>childcare"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Child care provide Details</span></a></li>
                 </ul>
              </li>
            </ul> -->
          <!-- <ul class="sidebar-menu service_provider">        
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-list" aria-hidden="true"></i><span>Service management</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-down pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu rides">  
                  <li class="treeview">
                    <a href="#">
                      <i class="fa fa-list" aria-hidden="true"></i><span>Rides</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-down pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">           
                      <li class="treeview <?php //if(in_array($segmentMenu, array("service-care-driver",""))){echo " active";} ?>"><a href="<?php //echo base_url(); ?>service-care-driver"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Care driver</span></a></li>
                      <li class="treeview <?php// if(in_array($segmentMenu, array("service-multisibling",""))){echo " active";} ?>"><a href="<?php //echo base_url(); ?>service-multisibling"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Mulltisibling</span></a></li> 
                      <li class="treeview <?php //if(in_array($segmentMenu, array("service-kids-youth",""))){echo " active";} ?>"><a href="<?php //echo base_url(); ?>service-kids-youth"><i class="fa fa-sitemap" aria-hidden="true"></i><span>For  kids and youth care</span></a></li> 
                      <li class="treeview <?php //if(in_array($segmentMenu, array("service-car-pool",""))){echo " active";} ?>"><a href="<?php //echo base_url(); ?>service-car-pool"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Car pool</span></a></li> 
                     </ul>
                  </li>          
                    <li class="treeview <?php //if(in_array($segmentMenu, array("service-after-school-care",""))){echo " active";} ?>"><a href="<?php //echo base_url(); ?>service-after-school-care"><i class="fa fa-sitemap" aria-hidden="true"></i><span>After-school care</span></a></li>
                     <li class="treeview <?php //if(in_array($segmentMenu, array("service-tutoring",""))){echo " active";} ?>"><a href="<?php //echo base_url(); ?>service-tutoring"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Tutoring</span></a></li> 
                   </ul>
                </li>               
            </ul> -->
               <!-- price management menu for Admin role start -->
            <ul class="sidebar-menu home_content price_management">        
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-usd" aria-hidden="true"></i><span>Price management</span>
                  <span class="pull-right-container">
                    <i class="fa fa-plus pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">           
                  <li class="treeview <?php if(in_array($segmentMenu, array("price-list",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>price-list">
                  <i class="fa fa-usd" aria-hidden="true"></i><span>Price master</span></a></li>
                </ul>
              </li>
            </ul>
               <!-- Notification management menu for Admin role start -->
            <ul class="sidebar-menu home_content notification_management">        
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-bell" aria-hidden="true"></i><span>Notification management</span>
                  <span class="pull-right-container">
                    <i class="fa fa-plus pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">           
                  <li class="treeview <?php if(in_array($segmentMenu, array("manage-user",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>manage-user">
                  <i class="fa fa-user" aria-hidden="true"></i><span>Manage user</span></a></li>
                </ul>
                <ul class="treeview-menu">           
                  <li class="treeview <?php if(in_array($segmentMenu, array("notification-list",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>notification-list">
                  <i class="fa fa-list" aria-hidden="true"></i><span>Notification list</span></a></li>
                </ul>
              </li>
            </ul>
            <ul class="sidebar-menu resolution_center">        
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-search" aria-hidden="true"></i><span>Resolution center</span>
                  <span class="pull-right-container">
                    <i class="fa fa-plus pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">           
                  <li class="treeview <?php if(in_array($segmentMenu, array("resolution-center",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>resolution-center"><i class="fa fa-question" aria-hidden="true"></i><span>Issues</span></a></li>
                  <!-- <li class="treeview <?php //if(in_array($segmentMenu, array("parent-profile",""))){echo " active";} ?>"><a href="<?php //echo base_url(); ?>parent-profile-data"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Parent info</span></a></li> -->
                 </ul>
              </li>
            </ul>
            <!-- faq management menu for Admin role start -->
            <ul class="sidebar-menu home_content faq_management">        
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-list" aria-hidden="true"></i><span>FAQ management</span>
                  <span class="pull-right-container">
                    <i class="fa fa-plus pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">           
                  <li class="treeview <?php if(in_array($segmentMenu, array("faq",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>faq">
                  <i class="fa fa-info-circle" aria-hidden="true"></i><span>FAQ</span></a></li>
               </ul>
              </li>
            </ul>
          </ul>         
          <!--faq management menu for Admin role end -->

          <!-- Subject management menu for Admin role start -->
            <ul class="sidebar-menu subject_management">        
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-list" aria-hidden="true"></i><span>Subject management</span>
                  <span class="pull-right-container">
                    <i class="fa fa-plus pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">           
                  <li class="treeview <?php if(in_array($segmentMenu, array("subject-master",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>subject-master"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Subject manage</span></a></li>
                  <!-- <li class="treeview <?php //if(in_array($segmentMenu, array("parent-profile",""))){echo " active";} ?>"><a href="<?php //echo base_url(); ?>parent-profile-data"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Parent info</span></a></li> -->
                 </ul>
              </li>
            </ul>
            <!-- Subject management menu for Admin role end -->
            <!-- video streaming menu for Admin role start -->
            <ul class="sidebar-menu video_streaming">        
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-video-camera" aria-hidden="true"></i><span>Video streaming</span>
                  <span class="pull-right-container">
                    <i class="fa fa-plus pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">           
                  <li class="treeview <?php if(in_array($segmentMenu, array("video-list/video",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>video-list/video"><i class="fa fa-play-circle" aria-hidden="true"></i><span>Video list</span></a>
                  </li>
                 </ul>
              </li>
            </ul>
             <!-- video streaming menu for Admin role start -->
           <?php
            }
            ?>
          </section>
        <!-- /.sidebar -->
        </aside>
        <div id="changePwdModal" class="modal fade" style="display: none;" data-backdrop="static" data-keyboard="false" aria-hidden="false">
        <form action="" id="changePwdForm" name="changePwdForm" novalidate>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h4 class="modal-title">
                Change Password
                <small>Set new password for your account</small>
              </h4>
            </div>
            <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
            <div class="modal-body">
             <div class="alert alert-success alert-dismissable" style="display:none" id="success">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>        
            </div>
            <div class="alert alert-danger alert-dismissable" style="display:none" id="error">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="inputPassword1">Old Password</label>
                    <input type="password" class="form-control password" id="inputOldPassword" onBlur="checkOldPwd()" placeholder="Old password" name="oldPassword" maxlength="10" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="inputPassword1">New Password</label>
                    <input type="password" class="form-control password" id="inputPassword1" placeholder="New password" name="newPassword" maxlength="10" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="inputPassword2">Confirm New Password</label>
                    <input type="password" class="form-control password" id="inputPassword2" placeholder="Confirm new password" name="cNewPassword" maxlength="10" required>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-primary" type="button">Close</button>
              <button class="btn btn-default" type="button" id="btnChagePwdSave">Save</button>
            </div>
          </div>
          </div>
        </form>
      </div>
      <script type="text/javascript">
  $(document).ready(function(){
    var pathname = window.location.pathname; // Returns path only
    var url      = window.location.href; 
   if(url == baseURL+'parent-profile' || url == baseURL+'parent-profile-data')
    {
      $('.parent_management li:first').addClass('active');
      $(".parent_management .fa-plus").addClass("fa-minus");
    }
     if(url == baseURL+'userList')
    {
      $('.user_magagement li:first').addClass('active');
    }
    if(url == baseURL+'dashboard')
    {
      $('.dashboard li:first').addClass('active');
    }
    // if(url == baseURL+'careDriver/listOperation')
    // {
    //   $('.car_management li:first').addClass('active');
    // }
    if(url == baseURL+'aboutUs/aboutUsDetail' || url == baseURL+'contactUs/contactDetails' || url == baseURL+'terms/termsDetail' || url == baseURL+'rating/ratingDetails')
    {
      $('.general_management li:first').addClass('active');
      $(".general_management .fa-plus").addClass("fa-minus");
    }
    //   if(url == baseURL+'tutorList/listOperation')
    // {
    //   $('.tutor_mng li:first').addClass('active');
    // } 
    if(url == baseURL+'service-provider/service/1' || url == baseURL+'service-provider/service/2' || url == baseURL+'service-provider/service/3' || url == baseURL+'service-provider/service/4')
    {
      $('.service_magagement li:first').addClass('active');
      $('.rides_mng li:first').addClass('active');
      $(".service_magagement .fa-plus").addClass("fa-minus");
      $(".schoolCare_mng .fa-minus").removeClass("fa-minus");
      $(".tutor_mng .fa-minus").removeClass("fa-minus");
    }
   if( url == baseURL+'service-provider/service/5' || url == baseURL+'service-provider/service/6' || url == baseURL+'service-provider/service/7' || url == baseURL+'service-provider/service/8')
    {
      $('.service_magagement li:first').addClass('active');
      $('.schoolCare_mng li:first').addClass('active');
      $(".service_magagement .fa-plus").addClass("fa-minus");
      $(".rides_mng .fa-minus").removeClass("fa-minus");
      $(".tutor_mng .fa-minus").removeClass("fa-minus");
    }
    if(url == baseURL+'service-provider/service/9')
    {
      $('.service_magagement li:first').addClass('active');
      $('.tutor_mng li:first').addClass('active');
      $(".service_magagement .fa-plus").addClass("fa-minus");
      $(".rides_mng .fa-minus").removeClass("fa-minus");
      $(".schoolCare_mng .fa-minus").removeClass("fa-minus");
    }
    if(url == baseURL+'enquiryList/listOperation')
    {
      $('.enquiry_mng li:first').addClass('active');
    }
     if(url == baseURL+'bookingList/rides' || url == baseURL+'bookingList/schoolCare' || url == baseURL+'bookingList/tutor')
    {
      $('.booking_mng li:first').addClass('active');
      $(".booking_mng .fa-plus").addClass("fa-minus");
    }
     if(url == baseURL+'childcare')
    {
      $('.childcare li:first').addClass('active');
    }
    if(url == baseURL+'service-care-driver' || url == baseURL+'service-multisibling' || url == baseURL+'service-kids-youth' || url == baseURL+'service-car-pool')
    {
      $('.service_provider li:first').addClass('active');
      $('.rides li:first').addClass('active');
    }
    if(url == baseURL+'service-after-school-care' || url == baseURL+'service-tutoring')
    {
      $('.service_provider li:first').addClass('active');
    }
    if(url == baseURL+'price-list')
    {
      $('.price_management li:first').addClass('active');
      $(".price_management .fa-plus").addClass("fa-minus");
    }  
    if(url == baseURL+'manage-user' || url == baseURL+'notification-list')
    {
      $('.notification_management li:first').addClass('active');
      $(".notification_management .fa-plus").addClass("fa-minus");
    }  
    if(url == baseURL+"resolution-center")
    {
      $('.resolution_center li:first').addClass('active');
      $(".resolution_center .fa-plus").addClass("fa-minus");
    }
     if(url == baseURL+"slider-list")
    {
      $('.slider_management li:first').addClass('active');
      $(".slider_management .fa-plus").addClass("fa-minus");
    }
    if(url == baseURL+'faq')
    {
      $('.faq_management li:first').addClass('active');
      $(".faq_management .fa-plus").addClass("fa-minus");
    } 
    if(url == baseURL+"subject-master")
    {
      $('.subject_management li:first').addClass('active');
      $(".subject_management .fa-plus").addClass("fa-minus");
    }
    if(url == baseURL+"video-list/video")
    {
      $('.video_streaming li:first').addClass('active');
      $(".video_streaming .fa-plus").addClass("fa-minus");
    }
    if(url == baseURL+"police-verify")
    {
      $('.police_verify_magagement li:first').addClass('active');
    }
  });
 
    
</script>
