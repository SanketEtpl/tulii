<!--  popup start -->
<div class="modal fade commanPopup" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="ModalLabel">          
        </h4>
      </div>
      <div class="modal-body inner_body">
      </div>
          <div class="modal-footer inner_footer">
      
         </div>
    </div>
  </div>
</div>


<div class="modal fade photoGallery" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding_left:1px !important;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalHeader">          
        </h4>
      </div>
      <div class="modal-body inner_body" id="modalBody">
      </div>
          <div class="modal-footer"  id="modalFooter">
      
         </div>
    </div>
  </div>
</div>
<!-- popup end -->
<!--spinner start -->
<!-- <div id="LoadingDiv">
  <table class="table table-striped">
  <tr>
  <img src="<?php echo base_url(); ?>assets/images/preloader.gif" id="ajaxSpinnerImage"  title="Wait.." style="text-align:center;width:50px;margin-top:300px;margin-left:650px;"> 
  <div class="loading-data" style="text-align:center;color:white;padding-left:5%;"><b>Please wait while loading data</b></div>
  <td align='center' class="lottery_results hide table-responsive">
  </td>
  </tr>
  </table>
 </div> -->
 <!--script>
 $(document).ready(function () {
 var showSppiner=true;
$(document).ajaxStart(function(){
 if(showSppiner==true)
 {
 $("#LoadingDiv").show();
 $(".loading-data").html('<b>Please wait while loading data</b>');
 }
$(window.document).find("#LoadingDiv").css({ 'opacity' : 1.0 });
}).ajaxStop(function(){
 $("#LoadingDiv").hide();
 $(".loading-data").html(''); 
});
$('#lType').bind("cut copy paste", function (e) {
e.preventDefault();
}); 
}); 
</script-->


<!--<style>
#LoadingDiv {
 display: none;
overflow: hidden;
width: 100%;
height: 100%;
position: fixed;
z-index: 9999;
left: 0;
top: 0px;
background-color: rgba(0,0,0,0.5);
}
</style>-->

<!--spinner end -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Tulii</b> Admin</div>
        <strong>Copyright &copy; <?php echo date("Y"); ?> <a href="<?php echo base_url(); ?>">Tulii</a>.</strong> All rights reserved.
    </footer>
    
    <!-- jQuery UI 1.11.2 -->
    <!-- <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script> -->
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 3.3.2 JS -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/jquery-1.12.4.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.min.js"></script>
    
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>
    
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/validation.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/form_validation.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/common.js" type="text/javascript"></script>
    <script type="text/javascript">
        var windowURL = window.location.href;
        pageURL = windowURL.substring(0, windowURL.lastIndexOf('/'));
        var x= $('a[href="'+pageURL+'"]');
            x.addClass('active');
            x.parent().addClass('active');
        var y= $('a[href="'+windowURL+'"]');
            y.addClass('active');
            y.parent().addClass('active');

        // add for datepicker select year & month issue.
        var enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        try{
            $confModal.on('hidden', function() {
                $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
            });
            $confModal.modal({ backdrop : false });
        }
        catch (error) {
            if(error.name != 'ReferenceError')
                throw error;
        }
    </script>
  </body>
</html>