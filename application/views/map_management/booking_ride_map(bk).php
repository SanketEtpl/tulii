<?php //$this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-map-marker"></i> Map management/Booking ride map management
      <small>Map</small>
    </h1>
  </section>
  <section class="content">    
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Id. no</th>
                    <th>Driver name</th>
                    <th>Booking Date</th>
                    <th>Status</th>
                    <th style="width: 40px !important">Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                    <th>Driver name</th>
                    <th>Booking Date</th>
                    <th>Status</th>
                    <th style="width: 40px !important">Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    <div class="modal fade" id="viewMapModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
          <h3 class="modal-title" id="exampleModalLabel"><b>View driver current ride</b></h3>
        </div>
        <div class="modal-body"> 
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
              <head>
              <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
              <meta charset="utf-8">
              <title>Simple Polylines</title>
              <style>
                /* Always set the map height explicitly to define the size of the div
                 * element that contains the map. */
                #map {
                  height: 300px;
                }
                /* Optional: Makes the sample page fill the window. */
                html, body {
                  height: 300px;
                  margin: 0;
                  padding: 0;
                }
              </style>
            </head>                     
            <body>
            <div id="mapData"></div>
            <input type="hidden" name="latlngArray" id="latlngArray">
            <div id="map"></div>
            <script>
              function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 14,
                  center: new google.maps.LatLng(18.559662, 73.789351),
                  //mapTypeId: 'terrain'
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                });
                var latLngData = document.getElementById("latlngArray").value;//by id
                //var obj = JSON.parse(latLngData);
                //console.log(latLngData);
                //alert(latLngData);
                /*console.log(word);
                console.log("javascript");*/
               // var flightPlanCoordinates = latLngData;
                //alert(flightPlanCoordinates);
                //console.log(flightPlanCoordinates);
                var flightPlanCoordinates = [
                  {lat: 18.559662, lng: 73.789351},
                  {lat: 18.559994, lng: 73.788757},
                  {lat: 18.560995, lng: 73.787636},
                  {lat: 18.561367, lng: 73.786987},

                  {lat: 18.561423, lng: 73.786198},
                  {lat: 18.561570, lng: 73.785421},
                  {lat: 18.561656, lng: 73.784648},

                  {lat: 18.563570, lng: 73.782627},
                  {lat: 18.563778, lng: 73.782252},
                  {lat: 18.563972, lng: 73.781468},

                ];
                //console.log(flightPlanCoordinates);
                //alert(flightPlanCoordinates);
                //return false;
                var flightPath = new google.maps.Polyline({
                  path: flightPlanCoordinates,
                  geodesic: true,
                  strokeColor: '#FF0000',
                  strokeOpacity: 1.0,
                  strokeWeight: 2
                });
               
                /*var contentStringSource = '<div id="content">'+
            '<div id="siteNotice">'+
            '<div id="bodyContent">'+
            '<p><b>Source location</b></p>'+
            '</div>'+
            '</div>';*/

                /*var infowindow = new google.maps.InfoWindow({
                  content: contentStringSource
                });*/


               /** Start point marker **/
                var marker = new google.maps.Marker({
                  position: {lat: 18.559662, lng: 73.789351},
                  map: map,
                  icon: 'http://162.144.204.188/~etpl2013/Demo/tulii/assets/images/green-dot.png',
                  //title:"Source"                  
                });

                  /** driver ride point marker **/
              /*  var marker = new google.maps.Marker({
                  position: {lat: 18.561656, lng: 73.784648},
                  map: map,
                  icon: 'http://162.144.204.188/~etpl2013/Demo/tulii/assets/images/car.png'
                });
*/                

            /*var contentStringDestination = '<div id="content">'+
            '<div id="siteNotice">'+
            '<div id="bodyContent">'+
            '<p><b>Destination location</b></p>'+
            '</div>'+
            '</div>';

                var infowindow1 = new google.maps.InfoWindow({
                  content: contentStringDestination
                });*/
                /** End point marker **/
                var markerDt = new google.maps.Marker({
                  position: {lat: 18.563972, lng: 73.781468},
                  map: map,
                  icon: 'http://162.144.204.188/~etpl2013/Demo/tulii/assets/images/blue-dot.png',
                  //title:"Destination"
                });
                /*marker.addListener('click', function() {
                  infowindow.open(map, marker);
                });
                markerDt.addListener('click', function() {
                  infowindow1.open(map, markerDt);
                });*/
                
                flightPath.setMap(map);

              }             
            </script>
            <!-- <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBD5NeyT_tKxswCM6YHXRXZCDbb7NDIuko&callback=initMap">
            </script> -->
          </body>
          </div>
        </div>
      </div>

        <div class="modal-footer">
          <!-- <input type="hidden" id="user_key" name="user_key"> -->
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  </div>
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
//var table; 
$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"}); 
    //datatables
    bookingRideMapList();
});

</script>
<script src="<?php echo base_url(); ?>assets/js/map/rideMap.js"></script>

