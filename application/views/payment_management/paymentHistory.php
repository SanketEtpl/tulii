<style type="text/css">
.faqDesc {
  width:15px; 
}
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-credit-card-alt"></i> Payment management/Payment history      
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                <tr>
                  <th>Id. no</th>
                  <th>Driver name</th>
                  <th>Source location</th>
                  <th>Destination location</th>
                  <th>Date</th>
                  <th>Price</th>                  
                  <th>Rating</th>
                  <th>Status</th>
                  <th style="width:120px;">Action</th>                    
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Driver name</th>
                  <th>Source location</th>
                  <th>Destination location</th>
                  <th>Date</th>
                  <th>Price</th>                  
                  <th>Rating</th>                 
                  <th>Status</th>
                  <th style="width:120px;">Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
  </div>
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"}); 
  //datatables
  paymentHistoryList();
  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
});

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();  
  paymentHistoryList();
}
</script>
<script src="<?php echo base_url(); ?>assets/js/payment/paymentHistory.js"></script>
