<?php $this->load->view("frontend/header"); ?>
<div class="content">
	<div>
		<div>
			<img src="<?php echo base_url();?>assets/images/calling.jpg" alt="Image" />
		</div>
		<div>
			<div id="sidebar">
				<h3>Contact Centre</h3>
				<ul>
					<li id="vision">
						<!-- <span>Our Vision</span> -->
						<p>Address: Unit #4 – 10309 117 Street NW Edmonton, Alberta T5K 1X9 Canada</p>
						<p>Contact no: 780 – 446 – 0670</p>
						<p>Email id: info@tulii.ca</p>
					</li>					
				</ul>
			</div>
			<div id="contact">				
				<center>
				<form id="contactForm" name="contactForm">
					<h2>Contact us</h2>
					<div id="respMessage"></div>
					<div class="form-group">
						<label for="lfname">Your name</label>
						<input type="text" class="name" id="fname" name="firstname" placeholder="Your name">
					</div>
					<div class="form-group">
						<label for="lemail">Your email</label>
						<input type="text" class="email-valid" id="email" name="email" placeholder="Your email">
					</div>
					<div class="form-group">
						<label for="lmobile">Mobile no</label>
						<input type="text" class="contact_number" id="mobileno" name="mobileno" placeholder="Your mobile no">					
					</div>
					<div class="form-group">
						<label for="lsubject">Subject</label>
						<input type="text" class="title_valid" id="subject" name="subject" placeholder="Subject">					
					</div>
					<div class="form-group">
						<label for="lmessage">Message</label>
						<textarea id="message" class="description_valid" name="message" placeholder="Message"></textarea>
					</div>
					<button class="btn btn-info" type="button" id="btnContact">Send a message</button>
					
				</form>
			</center>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("frontend/footer"); ?>	
<script src="<?php echo base_url(); ?>assets/js/frontend/home.js"></script>
