<?php $this->load->view("frontend/header"); ?>
<div class="owl-carousel owl-theme">
	<?php 
		if(!empty($banner)) { 
			foreach ($banner as $key => $bannerImg) {				
	?>
	<div class="item"><img src="<?php echo base_url().$bannerImg['banner_img'];?>" alt="<?php echo $bannerImg['title']; ?>" /></div>
	<?php } } else { ?>
	<div class="item"><img src="<?php echo base_url();?>uploads/banner/114282018-03-16-01-23-48.jpg" alt="Image" /></div>
	<?php } ?>    
</div>
<div id="content">
	<div>
		<div>
			<h1>Why Was Tulii Started?</h1>
			<p>Tulii was created by three insightful people who all have a certain perspective and unique understanding of the pressures that parents face trying to get their kids to and from their various activities, events, appointments, etc. This is coupled with the sense of fear that naturally comes when a parent puts their kid in someone else's car and entrusts them to a stranger. The solution is Tulii’s safe, secure, convenient, reliable, and well-thought out uses, borne out of today’s modern family’s necessity to plan ahead, and fueled by the need for transportation and childcare services for kids in our community.</p>
			<h2>Why are we different?</h2>
			<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequeat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
			<p>We are the only safe ride-sharing and afterschool care network that sends consistent CareDrivers, ChildCare Providers and Tutors for your children and youth when and wherever you need them. In summary Tulii is here for you when your family needs that extra hand and we promise you the following services:</p>
			<ul>
				<li>FAMILIAR CAREDRIVERS					
					<p>We understand familiarity and continuity is important for your children and family. Using our super cutting-edge Tulii App, we match you to one of 5 personalized trusted CareDrivers assigned to your family anytime you schedule a ride, care or tutoring service with us.</p>
				</li>
				<li>CHILDCARE PROVIDERS					
					<p>On those days when you need more than just a ride for your kid, Tulii can provide childcare or can stay back with your kid at home or any out-door activities.</p>
				</li>
				<li>DEDICATED FAMILY LIAISON
					<p>To ensure your peace of mind, you will have a single point of contact (dedicated family liaison) that understands your family needs and can be reached at the touch of a button.</p>
				</li>
			</ul>
			<h2>How it works</h2>
			<p>As busy working parents ourselves, we have crafted a worry-free process that will give you back your time and your peace of mind.</p>
			<ul>
				<li>SCHEDULE YOUR PREFERRED SERVICE					
					<p>Schedule a one-time service or a recurring service for weeks and months in advance.</p>
				</li>
				<li>KNOW YOUR SERVICE PROVIDER					
					<p>Receive your Service Provider (CareDriver, ChildCare Provider or Tutor) picture and profile at least 12 hours before the service of your choice.</p>
				</li>
				<li>LET TULII TAKE CARE
					<p>Get notified when your service provider picks up and drops off your kid(s), arrives to support your kid(s) or successfully completes the assigned task.</p>
				</li>
			</ul>
		</div>
	</div>
</div>
<?php $this->load->view("frontend/footer"); ?>