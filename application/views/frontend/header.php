<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Home - Special School</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style-front.css" />
	<link rel="icon" type="image/jpg" href="<?php echo base_url();?>assets/images/favicon.png" />
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" href="css/ie6.css" />
	<![endif]-->
</head>
<body>
	<div id="header">
		<div>
			<?php $active = $this->uri->segment(1); ?>
			<a href="<?php echo base_url();?>home"><img src="<?php echo base_url();?>assets/images/logo.png" width="200" alt="Logo" /></a>
			<ul>
				<li class="<?php if($active == 'home') { echo 'current';} ?>" ><a href="<?php echo base_url();?>home">Home</a></li>
				<li class="<?php if($active == 'about') { echo 'current';} ?>"><a href="<?php echo base_url();?>about">About us</a></li>
				<li class="<?php if($active == 'services') { echo 'current';} ?>"><a href="<?php echo base_url();?>services">Our services</a></li>
				<li class="<?php if($active == 'safety') { echo 'current';} ?>"><a href="<?php echo base_url();?>safety">Safety</a></li>
				<li class="<?php if($active == 'faqs') { echo 'current';} ?>"><a href="<?php echo base_url();?>faqs">FAQ</a></li>
				<li class="<?php if($active == 'career') { echo 'current';} ?>"><a href="<?php echo base_url();?>career">Career</a></li>
				<li class="<?php if($active == 'pricing') { echo 'current';} ?>"><a href="<?php echo base_url();?>pricing">Pricing</a></li>
				<li class="<?php if($active == 'videos') { echo 'current';} ?>"><a href="<?php echo base_url();?>videos">Videos</a></li>
				<li class="<?php if($active == 'contact') { echo 'current';} ?>"><a href="<?php echo base_url();?>contact">Contact us</a></li>
				<li class="<?php if($active == 'download') { echo 'current';} ?>"><a href="<?php echo base_url();?>download">Download</a></li>								
			</ul>
		</div>
	</div>
	<script src="http://localhost/tulii/assets/js/jQuery-2.1.4.min.js"></script>
	<script type="text/javascript">
 		var baseURL = "<?php echo base_url(); ?>";
 	</script>