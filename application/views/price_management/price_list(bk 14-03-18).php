<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> Price list
      <small>Add, Edit, Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addPopUp" href="javascript:void(0)"><i class="fa fa-plus"></i> Add New</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%">
              <thead>
                  <tr>
                    <th>Id no</th>
                    <th>Ride type</th>
                    <th>Price per km</th>
                    <th>Price per hour</th>
                    <th>Price</th>
                    <th>Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id no</th>
                  <th>Ride type</th>
                  <th>Price per km</th>
                  <th>Price per hour</th>
                  <th>Price</th>
                  <th>Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    <div id="priceListModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="priceListForm" name="priceListForm" novalidate>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 class="modal-title"><b>Price list </b></h3>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="lbl_type">Ride Type *</label>
                  <select class="form-control" id="rideType" name="rideType">
                  <?php                     
                    if(!empty($ride_types)){                         
                      foreach ($ride_types as $key => $value) {                                                                      
                  ?>                                        
                    <option value="<?php echo $value['pm_id']; ?>"><?php echo $value['pm_name']; ?></option>                    
                    <?php } } else { ?>
                    <option value=""></option>
                    <?php } ?>
                  </select>
                </div>
              </div>                           
          </div>    
          <div class="row">           
            <div class="col-md-6" id="pricePerHour" style="display:block;">
              <div class="form-group">
                <label class="control-label" for="lbl_pk">Price of per hour *</label>
                <input type="text" placeholder="Price of per hour" required="true" maxlength="3" id="price_per_hour" name="price_per_hour" class="form-control price">
              </div>
            </div> 
            <div class="col-md-6" id="pricePerKm" style="display: none;">
              <div class="form-group">
                <label class="control-label" for="lbl_pk">Price of per km *</label>
                <input type="text" placeholder="Price of per km" required="true" maxlength="3" id="price_per_km" name="price_per_km" class="form-control price">
              </div>
            </div>
          </div>                       
          </div>
          <div class="modal-footer">
            <input type="hidden" name="user_key" id="user_key" value="">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" id="btnPriceList">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  </section>
</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
var table; 
$(document).ready(function() { 
    //datatables
    table = $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"price_management/Price_list/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
              $("#LoadingDiv").css({"display":"block"}); 
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,5 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    });  
});
</script>
<script src="<?php echo base_url(); ?>assets/js/price_management/price_list.js"></script>
