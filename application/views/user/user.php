<?php //$this->load->view('includes/header');?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> User Management
      <?php 
      if($this->session->userdata('role') == ROLE_ADMIN)
      {
       echo "<small>Add/Edit/Delete</small>"; 
      } else if($this->session->userdata('role') == ROLE_PARENTS)
      {
       echo "<small>View</small>"; 
      }

      ?>
      
    </h1>
  </section>
  <section class="content">   
    <?php 
      if($this->session->userdata('role') == ROLE_ADMIN)
      {
      ?>
      <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <!-- <a class="btn btn-primary" id="addPopUpSerPro" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add new service provider"><i class="fa fa-plus"></i> Add new service provider</a>
          <a class="btn btn-primary" id="addPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add new parent"><i class="fa fa-plus"></i> Add new parent</a> -->
        </div>
      </div>
      </div>
      <?php } ?>    
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Id no</th>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Mobile no</th>
                    <th>Types</th>
                    <?php 
                    if($this->session->userdata('role') == ROLE_ADMIN)
                    {
                     echo "<th style='width: 120px;'>Status</th><th>Action</th>";
                     } else {
                       echo "<th>Category</th><th>Sub category</th>";
                     }
                    ?>                                       
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id no</th>
                  <th>Full name</th>
                  <th>Email</th>
                  <th>Mobile no</th>
                  <th>Types</th>
                 <?php 
                    if($this->session->userdata('role') == ROLE_ADMIN)
                    {
                    echo "<th style='width: 120px;'>Status</th><th>Action</th>";
                     } else {
                       echo "<th>Category</th><th>Sub category</th>";
                     }
                    ?> 
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>

    <div id="userModal" class="modal fade" data-backdrop="static" style="display: none;" aria-hidden="false">
      <form action="" id="userForm" name="userForm" novalidate>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h3 class="modal-title"><b>Add user</b></h3>
            </div>
            <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="control-label" for="lbl_name">Full name<!-- <span class="required_star">*</span> --></label>
                    <input type="hidden" id="user_key1" name="user_key1">
                    <!-- <input type="text" placeholder="Full name" id="fname" required="true" name="fname" class="form-control name" maxlength="50" autocomplete="off"> -->
                    <div id="fname"></div>
                  </div>
                </div>              
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="control-label" for="lbl_email">Email<!-- <span class="required_star">*</span> --></label> 
                    <!-- <input type="text" placeholder="Email" id="userEmail" onblur="userCheckEmail()" maxlength="50" required="true" name="email" class="form-control email" autocomplete="off"> -->
                    <div id="userEmail"></div>
                  </div>
                </div>
            </div>    
            <div class="row" id="hidePwd" style="display:block">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_pwd">Password<!-- <span class="required_star">*</span> --></label> 
                  <input type="password" placeholder="Password" id="userPassword" maxlength="20" required="true" name="userPassword" class="form-control password" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_cpwd">Confirm password<!-- <span class="required_star">*</span> --></label> 
                  <input type="password" placeholder="Confirm password" id="userCpassword" maxlength="20" required="true" name="userCpassword" class="form-control password" autocomplete="off">
                </div>
              </div>               
            </div>
   
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="phone">Mobile no<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" placeholder="Mobile no" required="true" maxlength="10" id="contactNumber" name="contactNumber" class="form-control contact_number"> -->
                  <div id="contactNumber"></div>
                </div>
              </div>  
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_spgender">Gender<!-- <span class="required_star">*</span> --></label><br>
                  <!-- <input type="radio" name="spGender" class="parentGender" id="spGenderMale" value="male" checked=""> Male
                  <input type="radio" name="spGender" class="parentGender" id="spGenderFemale" value="female"> Female     -->
                <div id="spGender"></div>
                </div>
              </div> 
            </div>   
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="dob">Date of birth<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" name="parentDOB" placeholder="Date of birth" class="form-control dob" id="parentDOB" readOnly/> -->                  
                  <div id="parentDOB"></div>
                </div>
              </div>  
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_page">Age<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" name="parentAge" placeholder="Age" class="form-control age" id="parentAge" readOnly/> -->
                  <div id="parentAge"></div>
                </div>
              </div> 
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_role">Degree<!-- <span class="required_star">*</span> --></label>
                  <!-- <select class="form-control" id="userDegree" name="userDegree">
                  <option value="" disable="" selected="" hidden=""> Select degree</option>
                  <?php 
                    //$degreeData = $this->Common_model->select('*',TB_DEGREE); 
                    if(!empty($degree)){                         
                      foreach ($degree as $key => $value) {                                                                    
                  ?>
                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>                    
                    <?php  } } else { ?>
                    <option value=""></option>
                    <?php } ?>
                  </select> -->
                  <div id="userDegree"></div>
                </div>
              </div> 
              <div class="col-md-6 col-sm-6 col-xs-12" id="specialid">
                <div class="form-group">
                  <label class="control-label" for="lbl_spec">Specialization<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" name="specialization" placeholder="specialization" class="form-control" id="specialization"> -->
                  <div id="specialization"></div>
                  <span id="special"></span>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12" id="addressid" style="display: none">
                <div class="form-group">
                  <label class="control-label" for="lbl_addre">Tutor Address<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" name="specialization" placeholder="specialization" class="form-control" id="specialization"> -->
                  <div id="tutor_address"></div>
                   <span id="taddress"></span>
                </div>
              </div>
            </div>  
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12" id="edufromid">
                <div class="form-group">
                  <label class="control-label" for="lbl_from">Education from<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" placeholder="Education from" id="valid_from" required="true" name="valid_from" readOnly class="form-control eduFrom" autocomplete="off"> -->                  
                  <div id="valid_from"></div>
                </div>
              </div> 
              <div class="col-md-6 col-sm-6 col-xs-12" id="edutoid">
                <div class="form-group">
                  <label class="control-label" for="lbl_to">Education to<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" placeholder="Education to" id="valid_to" required="true" name="valid_to" readOnly class="form-control eduTo" autocomplete="off"> -->                  
                  <div id="valid_to"></div>
                </div>
              </div>
            </div>  
            <div class="row">    
              <div class="col-md-6 col-sm-6 col-xs-12" id="certificateid">
                <div class="form-group">
                  <label class="control-label" for="lbl_cn" id="certiid">Certificate name<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" placeholder="Certificate name" required="true" maxlength="100" id="certificateName" name="certificateName" maxlength="100" class="form-control name"> -->
                  <div id="certificateName"></div>
                </div>
              </div>        
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_uc">Upload certificate<!-- <span class="required_star">*</span> --></label>                  
                 <!-- <div>
                    <button type="button" class="addfiles" onclick="document.getElementById('uploadDocuments').click();">Choose file</button>
                    <input type="file" id="uploadDocuments" name="uploadDocuments[]" multiple="" style="display: none;" accept=".jpg, .jpeg, .png, .gif, .pdf">                        
                    <span id="showFilename">No file chosen</span>
                  </div> -->   
                  <div id="uploadDoc">
                    <img src="" id="uploadDocuments">
                  </div>                                 
                </div>
              </div>
            </div>
         
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_school" id="schoolid">School/College<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" placeholder="School/College" required="true" maxlength="100" id="schoolCollege" name="schoolCollege" maxlength="100" class="form-control name"> -->
                  <div id="schoolCollege"></div>
                </div>
              </div> 
<!--               <div class="col-md-6">
                <div class="form-group" id="spAddress">
                  <label class="control-label" for="lbl_spaddress">Address *</label>
                  <input type="text" placeholder="Address" id="spAddress" name="pAddress" class="form-control address" maxlength="100" required="true">
                </div>
              </div> -->
         <!--    </div>  
            <div class="row"> -->
              <div class="col-md-6 col-sm-6 col-xs-12" style="min-height: 94px;">
                <div class="form-group">
                  <label class="control-label" for="lbl_role">Role<!-- <span class="required_star">*</span> --></label>
                  <input type="hidden" id="service_provider_role_id" value="<?php echo ROLE_SERVICE_PROVIDER; ?>">
                  <!-- <select class="form-control kidgender" id="userRole" onChange="addCategoryList()" name="role">
                  <option value="" disable="" selected="" hidden="">Select role</option>
                  <?php 
                    if(!empty($roleData)){                                               
                      foreach ($roleData as $key => $value) {                                                                      
                  ?>                   
                    <option value="<?php echo $value['roleId']; ?>"><?php echo $value['role']; ?></option>                    
                    <?php  } } else { ?>
                    <option value=""></option>
                    <?php } ?>
                  </select> -->
                  <div id="userRole"></div>
                </div>
              </div>  
               <div class="" id="categoryDiv"></div>
            </div>      
           
            </div>
           <!--  <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-info" type="button" id="btnUserSave">Save</button>
            </div> -->
          </div>
        </form>
      </div>
    </div>
     <div id="parentProfileModal" class="modal fade" data-backdrop="static" style="display: none;" aria-hidden="false">
      <form action="" enctype="multipart/form-data" id="parentProfileForm" name="parentProfileForm" novalidate>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h3 class="modal-title"><b>Add parent</b></h3>
            </div>
            <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group" id="parentName">
                    <label class="control-label" for="lbl_name">Full name<!-- <span class="required_star">*</span> --></label>
                    <input type="hidden" id="user_key" name="user_key">
                    <input type="hidden" id="parent_id" name="parent_id">
                    <input type="hidden" id="count_id" name="count_id" value="1">
                    <!-- <input type="text" placeholder="Full name" id="name" required="true" name="name" class="form-control name" maxlength="50" autocomplete="off"> -->
                    <div id="name"></div>
                  </div>
                </div>              
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="control-label" for="lbl_gender">Gender<!-- <span class="required_star">*</span> --></label><br>
                   <!--  <input type="radio" name="gender" class="parentGender" id="genderMale" value="male" checked> Male
                    <input type="radio" name="gender" class="parentGender" id="genderFemale" value="female"> Female  -->                 
                    <div id="gender"></div>
                  </div>
                </div>
            </div>    
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_email">Email<!-- <span class="required_star">*</span> --></label> 
                  <!-- <input type="text" placeholder="Email" id="email" onblur="checkEmail()" maxlength="50" required="true" name="email" class="form-control email" autocomplete="off"> -->
                  <div id="email"></div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group" id="parentContact">
                  <label class="control-label" for="lbl_phone">Mobile no<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" placeholder="Mobile no" required="true" maxlength="10" id="phone" name="phone" class="form-control contact_number"> -->
                  <div id="phone"></div>
                </div>
              </div> 
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_noc">No. of childs<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" placeholder="No. of childs" required="true" maxlength="2" id="noOfKis" name="noOfKis" value="1" disabled="true" class="form-control age"> -->
                  <div id="noOfKis"></div>
                </div>
              </div> 
               <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group" id="perentAddress">
                  <label class="control-label" for="lbl_address">Address<!-- <span class="required_star">*</span> --></label>
                  <!-- <input type="text" placeholder="Address" id="address" name="address" class="form-control address" maxlength="100" required="true"> -->
                  <div id="address"></div>
                </div>
              </div>                                                                                                              
            </div> 
            <div class="">
              <h4 class=""><b>Add child</b></h4>              
            </div>     

            <div class="table-responsive">             
              <table class="table table-bordered" id="dynamic_field"> 
                <thead>
                <tr>
                  <th>Name</th> 
                  <th>Gender</th>
                  <th>Date of birth</th>
                  <th>Age</th>     
                  <!-- <th><button type="button" name="add" id="add" onClick="addrow()" class="btn btn-success">Add More</button></th>  -->                       
                </tr>
                </thead>
                <tbody>
                
                </tbody>
              </table>                 
            </div> 
            <div class="map" id="map" style="width: 100%; height: 300px;display:none"></div>
            </div>
            <div class="modal-footer">
              <input type="hidden" name="lat" id="lat">
              <input type="hidden" name="lng" id="lng">
              <!-- <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-info" type="button" id="btnParentProfileSave">Save</button> -->
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 

$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"});
    //datatables
    userList();
$("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

 $( ".eduFrom" ).datepicker({      
          dateFormat: 'yy-mm-dd',      
          changeMonth:true,
          changeYear:true,
          maxDate:"0d",
          onSelect: function(selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $(".eduTo").datepicker("option","minDate", dt);
          }
          //minDate:"-5y"                          
        });        
        $( ".eduTo" ).datepicker({ 
          dateFormat: 'yy-mm-dd',           
          changeMonth:true,
          changeYear:true,
          //maxDate:"+20y",                          
          maxDate:"0d" ,
          onSelect: function(selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
           // $(".eduFrom").datepicker("option","maxDate", dt);
           $('.eduTo').datepicker("option","minDate", null);
        }         
        });


});

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  // location.reload();
  userList();
}

function userList(){
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();

      $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [],
 
        // Load data for the table's content from an Ajax source
          "ajax": {
            "url": baseURL+"user_management/UserController/ajax_list",
            beforeSend: function() {
               // $("#LoadingDiv").css({"display":"block"});
              var search = $("input[type=search]").val();
              if(search=="")
               $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"});     
            $("#table").css({"opacity":"1"});       
          },
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ <?php 
                    if($this->session->userdata('role') == ROLE_ADMIN)
                    {echo '0,5,6'; } else {echo '0';} ?> ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    }); 
}
</script>
<script>
  function initialize() {
     var latlng = new google.maps.LatLng(28.5355161,77.39102649999995);
      var map = new google.maps.Map(document.getElementById('map'), {
        center: latlng,
        zoom: 13
      });
      var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
     });
      var input = document.getElementById('address');
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      var geocoder = new google.maps.Geocoder();
      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.bindTo('bounds', map);
      var infowindow = new google.maps.InfoWindow();   
      autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
              window.alert("Autocomplete's returned place contains no geometry");
              return;
          }
    
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
          } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17);
          }
         
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);          
      
          bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
          infowindow.setContent(place.formatted_address);
          infowindow.open(map, marker);
         
      });
      // this function will work on marker move event into map 
      google.maps.event.addListener(marker, 'dragend', function() {
          geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {        
                bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
                infowindow.setContent(results[0].formatted_address);
                infowindow.open(map, marker);
            }
          }
          });
      });
  }
  function bindDataToForm(address,lat,lng){     
     document.getElementById('lat').value = lat;
     document.getElementById('lng').value = lng;
  }
  google.maps.event.addDomListener(window, 'load', initialize);
  </script>
<script src="<?php echo base_url(); ?>assets/js/parentJS/parentProfileNew.js"></script>
<script src="<?php echo base_url(); ?>assets/js/user/user.js"></script>
