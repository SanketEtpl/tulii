<?php //$this->load->view('includes/header'); ?>
<style type="text/css">
.faqDesc {
  width:15px; 
}
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-list"></i> FAQ management/FAQ
      <small>Add/View/Edit/Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add new"><i class="fa fa-plus"></i> Add new</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                <tr>
                  <th>Id. no</th>
                  <th>Title</th>
                  <th>Sub title</th>
                  <th class="tdwidth">Description</th>
                  <th>Category</th>
                  <th>Sub category</th>
                  <th>Action</th>                    
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Title</th>
                  <th>Sub title</th>
                  <th class="tdwidth">Description</th>
                  <th>Category</th>
                  <th>Sub category</th>                  
                  <th>Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    <div id="FAQModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="faqForm" name="faqForm" novalidate>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 class="modal-title"><b>Add FAQ</b> </h3>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label class="control-label" for="lbl_type">Category *</label>
                  <select class="form-control" id="categories" name="categories">
                    <option value="" disable="" selected="" hidden="">-- Select category --</option>
                  <?php                     
                    if(!empty($categories)){                         
                      foreach ($categories as $key => $value) {                                                                      
                  ?>                                        
                    <option value="<?php echo $value['category_id']; ?>"><?php echo $value['category']; ?></option>                    
                    <?php } } else { ?>
                    <option value=""></option>
                    <?php } ?>
                  </select>
                </div>
              </div>                           
          </div>   
          <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label class="control-label" for="lbl_subtype">Sub category *</label>
                  <select class="form-control" name="subCategory" id="subCategory">
                      <option value="">-- Select sub category --</option>
                  </select>
                </div>
              </div>                           
          </div>   

          <div class="row">           
            <div class="col-md-8">
              <div class="form-group">
                <label class="control-label" for="lbl_title">Title *</label>
                <input type="text" placeholder="Title" required="true" maxlength="255" id="title" name="title" class="form-control title_valid">
              </div>
            </div> 
          </div>

          <div class="row">           
            <div class="col-md-8">
              <div class="form-group">
                <label class="control-label" for="lbl_sub_title">Sub title *</label>
                <input type="text" placeholder="Sub title" required="true" maxlength="255" id="sub_title" name="sub_title" class="form-control sub_title_valid">
              </div>
            </div> 
          </div>

          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label class="control-label" for="lbl_desc">Description *</label>
                <textarea placeholder="Description" required="true" id="description" name="description" class="form-control description_valid"></textarea>
               </div>
            </div>
          </div>                       
          </div>
          <div class="modal-footer">
            <input type="hidden" name="user_key" id="user_key" value="">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" id="btnFAQ">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <div class="modal fade" id="viewFaqModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel"><b>View FAQ</b></h3>
                    </div>
                <div class="modal-body">
                     <div class="row">
                        <div class="col-md-6 rid_tim">
                            <label class="col-md-6 test-alignment">Category :</label>
                            <div class="rid_info col-md-6" id="viewCategory"></div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-6 test-alignment">Sub categories:</label>
                            <div class="rid_info col-md-6" id="viewSubCategories"></div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-md-6 rid_tim">
                            <label class="col-md-6 test-alignment">Title :</label>
                            <div class="rid_info col-md-6" id="viewTitle"></div>
                        </div>    
                        <div class="col-md-6 rid_tim">
                            <label class="col-md-6 test-alignment">Sub title :</label>
                            <div class="rid_info col-md-6" id="viewSubTitle"></div>
                        </div>                    
                    </div>
                    <div class="row">
                        <div class="col-md-12 rid_tim">
                            <label class="col-md-12 test-alignment">Description :</label>
                            <div class="rid_info col-md-12 word-break" id="viewDescription"></div>
                        </div>                        
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="viewFaqModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel"><b>View FAQ Description</b></h3>
                    </div>
                <div class="modal-body">
                     
                    <div class="row">
                        <div class="col-md-12 rid_tim">
                            <label class="col-md-12">Description :</label>
                            <div class="rid_info col-md-12 word-break" id="viewDescription1"></div>
                        </div>                        
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
$(document).ready(function() { 
   $("#LoadingDiv").css({"display":"block"}); 
  //datatables
 faqList();
    $("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
});
function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  // location.reload();
   faqList();
}
</script>
<script src="<?php echo base_url(); ?>assets/js/FAQ/faq.js"></script>
