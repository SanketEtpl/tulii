<!--*
Author : Rajendra pawar 
Page :  video.php
Description : show video streaming list.
*-->

<script src="http://jwpsrv.com/library/6QVxzPkvEeSkJwp+lcGdIw.js"></script>
<script src="jwplayer/jwplayer.js" ></script>
<script>jwplayer.key="7SYdCOHxpEaICfiAz4rXDkkgf+fcssszRYDb2Q==";</script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/video_streaming/video.js" charset="utf-8"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-video-camera"></i>Video Streaming/Video List
        <small>View/Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">                
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">                
                <div class="box-body table-responsive no-padding">
                <table id="videoList-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%" style="opacity:0">
                      <thead>
                        <tr>
                           <th>Id no</th>
                           <th>Parent Name</th>
                           <th>Duration</th>
                           <th>Start Date</th>
                           <th>End Date</th>
                           <th>Pick Up Location</th>
                           <th>Pick Off Location</th>
                           <th style="width:100px !important;">Action</th>       
                        </tr>
                      </thead>
                      <tfoot> 
                        <tr>
                           <th>Id no</th>
                           <th>Parent Name</th>
                           <th>Duration</th>
                           <th>Start Date</th>
                           <th>End Date</th>
                           <th>Pick Up Location</th>
                           <th>Pick Off Location</th>
                           <th style="width:100px !important;">Action</th>    
                        </tr>
                      </tfoot>
                </table>       
                </div><!-- /.box-body -->
                
              </div><!-- /.box -->
              </div>

        </div>
    </section>
</div>
<script type="text/javascript">
  jQuery(document).ready(function(){
  loadVideoList(); 
  $("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#videoList-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
});

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  loadVideoList();
  // location.reload();
}
</script>   



