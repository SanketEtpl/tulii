<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$ci = new CI_Controller();
$ci =& get_instance();
$ci->load->helper('url');

?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	border: 1px solid #D0D0D0;
	box-shadow: 0 0 8px #D0D0D0; float:left ; position: relative;

}
.goback {
    float: left;
    width: auto;
    position: absolute;
    bottom: 20%;
    right: 20%;
}
.goback a {
    padding: 8px 15px;
    background: #10d8ea;
    text-decoration: none;
    margin: 0 auto 10px 0;
    display: inline; color: #fff;
}
p {
	margin: 12px 15px 12px 15px;
}
</style>
</head>
<body>
	<div id="container">

		<?php //echo $heading; ?>
		<?php //echo $message; ?>
		<div class="row">
            <div class="col-xs-12 text-center">
                <img style="margin:0 auto;max-width:100%" src="<?php echo base_url(); ?>assets/images/page-not-found.jpg" alt="Access Denied Image" />
            </div>
            <div class="col-xs-12 text-center goback" style="text-align:center">
                <a href="<?php echo base_url(); ?>">Go Back to Home</a>
            </div>
        </div>
    

	</div>
</body>
</html>