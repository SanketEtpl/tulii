<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> Service after school care
      <small>view, Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%">
              <thead>
                  <tr>
                    <th>Id no</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Age</th>
                    <th>Experience</th>               
                    <th>Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id no</th>
                  <th>Name</th>
                  <th>Gender</th>
                  <th>Age</th>
                  <th>Experience</th>               
                  <th>Action</th>  
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
   <div class="modal fade" id="carpoolModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel"><b>After school care details</b></h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6 rid_tim">
                <label class="col-md-6">Care driver profile :</label>
                <div class="rid_info col-md-6" id="sph_cd_profile_picture"><img name="profile" class="thumbnail"></div>
              </div>                                     
            </div>    
            <div class="row">
              <div class="col-md-6 rid_tim">
                <label class="col-md-6">Care driver name :</label>
                <div class="rid_info col-md-6" id="care_driver_name"></div>
              </div>
              <div class="col-md-6">
                <label class="col-md-3">Age:</label>
                <div class="rid_info col-md-9" id="care_driver_age"></div>
              </div>
            </div>   
            <div class="row">
              <div class="col-md-6 rid_tim">
                <label class="col-md-4">Gender :</label>
                <div class="rid_info col-md-8" id="user_gender"></div>
              </div> 
              <div class="col-md-6 rid_tim">
                <label class="col-md-6">Total experience :</label>
                <div class="rid_info col-md-3" id="care_driver_exp"></div>
              </div>             
            </div>
            <div class="row">
              <div class="col-md-6 rid_tim">
                <label class="col-md-8">Total number of days :</label>
                <div class="rid_info col-md-4" id="care_driver_totalnodays"></div>
              </div>
              <div class="col-md-6 rid_tim">
                <label class="col-md-3">Date :</label>
                <div class="rid_info col-md-6" id="care_driver_date"></div>
              </div>
            </div>    
            <div class="row">
              <div class="col-md-6 rid_tim">
                <label class="col-md-6">Start data :</label>
                <div class="rid_info col-md-6" id="care_driver_start_date"></div>
              </div>
              <div class="col-md-6 rid_tim">
                <label class="col-md-6">End date :</label>
                <div class="rid_info col-md-6" id="care_driver_end_date"></div>
              </div>
            </div> 
            <div class="row">
              <div class="col-md-6 rid_tim">
                <label class="col-md-4">Rating :</label>
                <div class="rid_info col-md-8" id="care_driver_rating"></div>
              </div>
              <div class="col-md-6 rid_tim">
                <label class="col-md-6">Raise an issue :</label>
                <div class="rid_info col-md-6" id="care_driver_raise_an_issues"></div>
              </div>
            </div>  
            <div class="row">
              <div class="col-md-6 rid_tim">
                <label class="col-md-6">Type :</label>
                <div class="rid_info col-md-6" id="care_driver_type"></div>
              </div>             
            </div>                              
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div> 
  </section>
</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
var table; 
$(document).ready(function() { 
  //datatables
  afterschoolcarelist();
  $("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
 
});
function afterschoolcarelist(){
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": baseURL+"service_management/Service_after_school_care/ajax_list",
       beforeSend: function() {
          var search = $("input[type=search]").val();
          if(search=="")
          // $("#LoadingDiv").css({"display":"block"}); 
            $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
         },
        complete: function(){
        $("#LoadingDiv").css({"display":"none"}); 
      },
        "type": "POST"
    },
    
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [0,5], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  });
}
function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  // location.reload();
  afterschoolcarelist();
}
</script>
<script src="<?php echo base_url(); ?>assets/js/service_management/after_school_care.js"></script>
