<?php // $this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-list"></i> Tutor management      
    </h1>
  </section>
  <section class="content">

    <div class="row">
      <div class="col-xs-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%">
              <thead>
                  <tr>
                    <th>Id. no</th>
                    <th>Teacher name</th> 
                    <th>Subject name</th>                                       
                    <!-- <th>Action</th> -->                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Teacher name</th> 
                  <th>Subject name</th>
                  <!-- <th>Action</th>     -->                
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
        
  </section>
<script src="<?php echo base_url(); ?>assets/js/tutor.js"></script>
