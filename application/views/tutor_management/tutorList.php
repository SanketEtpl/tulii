<!--*

Author : Rajendra pawar 
Page :  tutorList.php
Description : tutor list use for tutor managment module

*-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/tutorManagement.js" charset="utf-8"></script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Tutor Management/Tutor List
        <small>View</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                
                <div class="box-body table-responsive no-padding">
                <table id="tutorList-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                   <thead>
                       <tr>
                       <th>Id. no</th>
                       <th>Parent Name</th>
                       <th>Child's Name</th>
                       <th>Tutor's Name</th>
                       <th>Tutor's Number</th>
                       <th>Action</th>
                    </tr>
                 </thead>
                 <tfoot> 
                 <tr>
                      <th>Id. no</th>
                       <th>Parent Name</th>
                       <th>Child's Name</th>
                       <th>Tutor's Name</th>
                       <th>Tutor's Number</th>
                       <th>Action</th>
                    </tr>
                    </tfoot>
                </table>       
                </div><!-- /.box-body -->
                
              </div><!-- /.box -->
              </div>
        </div>
    </section>
</div>
 <script type="text/javascript">
  jQuery(document).ready(function(){
loadTutorList(); 
}); 
 </script>   
    




