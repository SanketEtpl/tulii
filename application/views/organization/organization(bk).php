<?php //$this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> Organization management
      <small>View</small>
    </h1>
  </section>
  <section class="content">    
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Id. no</th>
                    <th>Organization name</th>
                    <th>Contact no</th>
                    <th>Email</th>
                    <th>Address</th>       
                    <th style="width: 40px !important">Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                    <th>Organization name</th>
                    <th>Contact no</th>
                    <th>Email</th>
                    <th>Address</th>       
                    <th style="width: 40px !important">Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    <div class="modal fade" id="viewEmpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLabel"><b>View employees</b></h3>
        </div>
        <div class="modal-body">
          <table class="table dataTable" id="employeePanel">
                    <thead>
                        <tr role="row">
                            <th>Employee Name</th>
                            <th>Age</th>
                            <th>Address</th>
                            <th>Contact no</th>
                            <th>Email</th>                            
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>                 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  </div>
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
//var table; 
$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"}); 
    //datatables
     organizationList();
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

});

</script>
<script src="<?php echo base_url(); ?>assets/js/organization/organization.js"></script>
