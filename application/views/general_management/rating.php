<?php //$this->load->view('includes/header'); ?>
<style type="text/css">
.faqDesc {
  width:15px; 
}
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-star"></i> CMS/Rating
      <small>Add/Edit/Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add new"><i class="fa fa-plus"></i> Add New</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Id.no</th>
                  <th>Role type</th>
                  <th>Rating title</th>                                                      
                  <th>Action</th>                    
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id.no</th>
                  <th>Role type</th>
                  <th>Rating title</th>                                    
                  <th>Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>

    <div id="ratingModal" class="modal fade" style="display: none;" aria-hidden="false">
      <form action="" id="ratingForm" name="ratingForm" novalidate>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h3 class="modal-title"><b>Add Rating</b> </h3>
            </div>
            <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
            <div class="modal-body">
              <div class="row">  
                <div class="col-md-6 col-sm-6 col-xs-12" >
                  <div class="form-group">
                    <label class="control-label" for="lbl_role">Role<span class="required_star">*</span></label>
                    <select class="form-control kidgender" id="userRole" name="userRole">
                    <option value="" disable="" selected="" hidden="">-- Select role --</option>
                    <?php 
                      if(!empty($roleData)){                                             
                        foreach ($roleData as $key => $value) {                                            
                          
                    ?>                   
                      <option value="<?php echo $value['role_id']; ?>"><?php echo $value['role_name']; ?></option>                    
                      <?php } } else { ?>
                      <option value=""></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>  
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="control-label" for="lbl_title">Title<span class="required_star">*</span></label>
                    <input type="text" placeholder="Title" required="true" maxlength="255" id="title" name="title" class="form-control title_valid">
                  </div>
                </div>      
              </div>  
            </div>
            <div class="modal-footer">
              <input type="hidden" name="user_key" id="user_key" value="">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-info" type="button" id="btnRating">Save</button>
            </div>
          </div>
        </div>
      </form>
    </div>  
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"}); 
  //datatables
  ratingList();
});
</script>
<script src="<?php echo base_url(); ?>assets/js/general_management/rating.js"></script>
