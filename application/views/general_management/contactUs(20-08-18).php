<!--

Author : Rajendra pawar 
Page :  ContactUs.php
Description : Contact Us  use for contact us view and deleted functionality

-->
<script src="<?php echo base_url(); ?>assets/js/generalManagement.js" type="text/javascript"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> General Management/Contact Us
        <small>View/Delete</small>
      </h1>
    </section>
    <section class="content">
    <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                </div>
            </div>
        </div>
        <div class="row">            
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body table-responsive no-padding">
                <table id="contact-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%" style="opacity:0">
                   <thead>
                     <tr>
                      <th>Id no</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile no</th>
                      <!-- <th>Message</th> -->
                      <th >Actions</th>
                    </tr>
                 </thead>
                 <tfoot> 
                 <tr>
                    <th>Id no</th>
                    <th>Name</th>
                     <th>Email</th>
                      <th>Mobile no</th>
                      <!-- <th>Message</th> -->
                      <th >Actions</th>
                    </tr>
                    </tfoot>
                </table>
                
                   
                  
                </div><!-- /.box-body -->
               
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
  jQuery(document).ready(function(){    
    loadContactList();
    $("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#contact-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
  }); 
function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  loadContactList();
  // location.reload();
}
</script>

