<!--
Author : Kiran n  
Page :  ContactUs.php
Description : Contact Us use for contact us view and deleted functionality
-->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> CMS/Contact Us
      <small>View/Delete</small>
    </h1>
  </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12 text-right">
          <div class="form-group">
          </div>
        </div>
      </div>
      <div class="row">            
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body table-responsive no-padding">
              <table id="contact-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                <thead>
                  <tr>
                    <th>Id no</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile no</th>
                    <th >Actions</th>
                  </tr>
                </thead>
                <tfoot> 
                  <tr>
                    <th>Id no</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile no</th>                    
                    <th >Actions</th>
                  </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->           
          </div><!-- /.box -->
        </div>
      </div>
    </section>
    <div class="modal fade" id="contactUsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" id="exampleModalLabel"><b>Contact US Message</b></h3>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 rid_tim">
                <label>Message :</label>
                <div class="rid_info" id="contactMessage"></div>
              </div>                     
            </div>                                       
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div> 
</div>
<script src="<?php echo base_url(); ?>assets/js/general_management/contactUs.js" type="text/javascript"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){    
    loadContactList();    
  }); 
</script>

