<?php
class Common_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function insert($table,$data)
    {
        $this->db->insert($table,$data);
        //echo $this->db->last_query();die;
        return $this->db->insert_id();
    }
    
    function update($table,$where=array(),$data)
    {
        $this->db->update($table,$data,$where);
        //echo $this->db->last_query();die;
        return $this->db->affected_rows();
    }
    
    function delete($table,$where=array())
    {
        $this->db->delete($table,$where);
        //echo $this->db->last_query();//die;
        return $this->db->affected_rows();
    }
    
    function select($sel,$table,$cond = array())
	{
		$this->db->select($sel, FALSE);
		$this->db->from($table);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
	}

    function select_where_in($sel,$table,$cond = array(),$whereIn = array(),$status='')
     {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        if($status){
          $this->db->where_in($status,$whereIn);
        }
        else{
         $this->db->where_in('id',$whereIn);
        }
        $query = $this->db->get();        
        return $query->result_array();
    }
	
	function selectQuery($sel,$table,$cond = array(),$orderBy=array())
	{
		$this->db->select($sel, FALSE);
		$this->db->from($table);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
    
    function update_batch($table,$where,$data)
    {
		$this->db->_protect_identifiers=true;
        $this->db->update_batch($table,$data,$where);
       // echo $this->db->last_query();
        return $this->db->affected_rows();
    }
    
     function record_count($sel,$table,$cond = array())
    {
        $this->db->select($sel)->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }        
        $query = $this->db->get();
        return $query->num_rows();
    } 

    function fetch_no_of_records($sel,$table,$cond = array(),$limit,$start)
    {
        $this->db->limit($limit,$start);
        $this->db->select($sel)->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $query = $this->db->get();
        if($query->num_rows()>0){
            foreach ($query->result_array() as $row) {
                $data[] = $row; 
            }
            return $data;
        }
           return false; 
    }

    function fetch_no_of_records_order_by($sel,$table,$cond = array(),$limit,$start,$colomn,$order_by)
    {
        $this->db->limit($limit,$start);
        $this->db->select($sel)->from($table);
        $this->db->order_by($colomn,$order_by);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $query = $this->db->get();
        if($query->num_rows()>0){
            foreach ($query->result_array() as $row) {
                $data[] = $row; 
            }
            return $data;
        }
           return false; 
    }

    public function getRowsFrontPerPage($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(), $page = 0,$join=array(),$group_by="")
    {
        if($this->session->userdata('perpage'))
        {
        $per_page=$this->session->userdata('perpage');
        }else {
        $per_page = "";
        }
        $this->db->protect_identifiers=true;
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
           if($v !=""){
                $this->db->where($k,$v);
            }
            else{
                $this->db->where($k);
            }
        }
        
        
                    
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
        $this->db->where("(".$q.")");
        }
        foreach($join as $key => $val)
        {
            $this->db->join($key, $val,"LEFT");
        }
         //echo "<pre>";print_r($orderBy);die;
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
        if($group_by != ""){
            $this->db->group_by($group_by);
        }
        // $this->db->limit($per_page,($page*$per_page));
        $query = $this->db->get();
        //print_r($query );die;
        // echo $this->db->last_query();die;
        return $query->result_array();
    }


    public function getRowsPerPage($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(), $page = 0,$join=array())
    {
        $per_page=$this->session->userdata('perpage');
        $this->db->protect_identifiers=true;
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
            $this->db->where("(".$q.")");
        }
        foreach($join as $key => $val)
        {
            $this->db->join($key, $val,"LEFT");
        }
       
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
         
        //$this->db->limit($per_page,($page*$per_page));
        $query = $this->db->get();
        //print_r($query );die;
        //echo $this->db->last_query();die;
        return $query->result_array();
    }
    // Added by Rajendra 
    public function select_join($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(),$join=array(),$group_by="")
    {
        if($this->session->userdata('perpage'))
        {
        $per_page=$this->session->userdata('perpage');
        }else {
        $per_page = "";
        }
        $this->db->protect_identifiers=true;
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
           if($v !=""){
                $this->db->where($k,$v);
            }
            else{
                $this->db->where($k);
            }
        }
                    
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
        $this->db->where("(".$q.")");
        }
        foreach($join as $key => $val)
        {
            $this->db->join($key, $val,"LEFT");
        }
         //echo "<pre>";print_r($orderBy);die;
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
        if($group_by != ""){
            $this->db->group_by($group_by);
        }
        // $this->db->limit($per_page,($page*$per_page));
        $query = $this->db->get();
        //print_r($query );die;
        // echo $this->db->last_query();die;
        return $query->result_array();
    }

        // Added by Kiran
function selectJoin($sel,$table,$cond = array(),$orderBy=array(),$join=array(),$joinType=array(),$group_by="")
{

    $this->db->select($sel, FALSE);
    $this->db->from($table);
    foreach ($cond AS $k => $v)
    {
        $this->db->where($k,$v);
    }
    foreach($orderBy as $key => $val)
    {
        $this->db->order_by($key, $val);
    }
    foreach($join as $key => $val)
    {
        if(!empty($joinType) && $joinType[$key]!=""){
            $this->db->join($key, $val,$joinType[$key]);
        }else{
            $this->db->join($key, $val);    
        }
    }
    if($group_by != ""){
        $this->db->group_by($group_by);
    }
    $query = $this->db->get();
    return $query->result_array();
}

    function insert_batch($table,$data)
    {   
        try {
            $this->db->insert_batch($table,$data);
            return true;
        }
        catch(Exception $e) {
          return false;
        }
    }

    function select_where_in_with_no_quote($sel,$table,$col,$cond)
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        $this->db->where($col." IN (".$cond.")",NULL, false);
        $query = $this->db->get();
      //  echo $this->db->last_query();die;
        return $query->result_array();
    }

    function insert_ignore($table,$key,$data)
    {
        $final_data='';
        foreach ($data as $keyd => $valued) {
            $new_data = implode(",",$data[$keyd]);
            $final_data.= "(".$new_data."),";
        }
        $insert_data = rtrim($final_data,",");
        $result = $this->db->query("INSERT IGNORE INTO ".$table."(".$key.") VALUES ".$insert_data);
        $row_cnt = $this->db->affected_rows();
        return $row_cnt;
    }


    public function getData($sel,$table1,$cond = array(),$key,$data)
    {
        $where1 = '';

        if(count($cond1) > 0)
        {
            foreach ($cond as $key => $value) {
                $where.= $key.' = '.$value;
            }
        }
        /*if(count($cond1) > 0)
        {
            foreach ($cond1 as $key => $value) {
                $where1.= $key.' = '.$value;
            }
        }*/

        $query = $this->db->query("select $sel from $table where id not in ($data");

        echo $this->db->last_query();

        exit();
        //$query = $this->db->get();
        return $query->result_array();
    }

    function select_where_in1($sel,$table,$cond = array(),$whereIn = array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $this->db->where_in('roleId',$whereIn);
        $query = $this->db->get();        
        return $query->result_array();
    }  

// priya 
public function getPriceWithCat($service_provider_id)
{

    $query = $this->db->query("select c.category_name, s.subcategory_name, p.cat_id, p.sub_cat_id, s.rate_per_hour from ".TB_TUTOR_CATEGORY." as c JOIN ".TB_TUTOR_SUBCATEGORY." as s ON c.tut_cat_id = s.tut_cat_id JOIN ".TB_PROVIDER_PRICE." as p ON s.tut_sub_cat_id = p.sub_cat_id WHERE p.service_provider_id = ".$service_provider_id." AND p.status = 1 AND c.status = 1 AND s.status = 1  ");
    return $query->result_array();
}


function select_in_cond($sel,$table,$col,$cond,$whereCond = array())
{

    $this->db->select($sel, FALSE);
    $this->db->from($table);
    $this->db->where($col." IN (".$cond.")",NULL, false);
    foreach ($whereCond AS $k => $v)
    {
        $this->db->where($k,$v);
    }
    $query = $this->db->get();
    return $query->result_array();
} 


public function checkAttriAndInsert($attriArr,$user_type = '')
{
    $total = 0;
    foreach($attriArr as $values) {

        $query = $this->db->query("select * from ".TB_ATTRIBUTE_VALUES." WHERE service_provider_id = ".$values['service_provider_id']." AND attribute_id = ".$values['attribute_id']." ");
        $result = $query->result();
           
            if(count($result) == '' || count($result) == 0) {

                if($values['attribute_type'] == 0 ) {  // 0 = text , 1 = file

                    $this->db->insert(TB_ATTRIBUTE_VALUES, $values);
                    
                } else {

                   if(isset($values['attribute_value']) != '' && $values['attribute_value'] != '') {
                        if($user_type == 4){
                            if (!file_exists('./uploads/tutor/'.$values['service_provider_id'].'/')) {
                                mkdir('./uploads/tutor/'.$values['service_provider_id'].'/');
                            }
            
                            $config = array(
                                'upload_path' => './uploads/tutor/'.$values['service_provider_id'].'/',
                                'allowed_types' => 'pdf|png|jpeg|jpg',
                                'max_width' => '6000',
                                'max_height' => '5000'
                            );
                        }
                        else if($user_type == 3){
                            if (!file_exists('./uploads/driver/'.$values['service_provider_id'].'/')) {
                                mkdir('./uploads/driver/'.$values['service_provider_id'].'/');
                            }
            
                            $config = array(
                                'upload_path' => './uploads/driver/'.$values['service_provider_id'].'/',
                                'allowed_types' => 'pdf|png|jpeg|jpg',
                                'max_width' => '6000',
                                'max_height' => '5000'
                            );
                        }
                        else if($user_type == 7){
                            if (!file_exists('./uploads/care/'.$values['service_provider_id'].'/')) {
                                mkdir('./uploads/care/'.$values['service_provider_id'].'/');
                            }
            
                            $config = array(
                                'upload_path' => './uploads/care/'.$values['service_provider_id'].'/',
                                'allowed_types' => 'pdf|png|jpeg|jpg',
                                'max_width' => '6000',
                                'max_height' => '5000'
                            );
                        }
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config); 
                           $check_upload = $this->upload->do_upload($values['attribute_value']);

                        if($check_upload) {

                            $this->db->insert(TB_ATTRIBUTE_VALUES, $values);
                        }   
                    } 
                }

               
            } else {

                if($values['type'] == 0 ) {  // 0 = text , 1 = file

                $update = $this->db->query("UPDATE ".TB_ATTRIBUTE_VALUES." SET attribute_value = '".$values['attribute_value']."', updated_at = '".$values['updated_at']."' WHERE service_provider_id = ".$values['service_provider_id']." AND attribute_id = ".$values['attribute_id']." ");
                // echo $this->db->last_query();

                } else {
                    
                    if(isset($values['attribute_value']) != '' && $values['attribute_value'] != '') {
                        if($user_type == 4){
                            if (!file_exists('./uploads/tutor/'.$values['service_provider_id'].'/')) {
                                mkdir('./uploads/tutor/'.$values['service_provider_id'].'/');
                            }
                            $config = array(
                                'upload_path' => './uploads/tutor/'.$values['service_provider_id'].'/',
                                'allowed_types' => 'pdf|png|jpeg|jpg',
                                'max_width' => '6000',
                                'max_height' => '5000'
                            );
                        }
                            else if($user_type == 3){
                            if (!file_exists('./uploads/driver/'.$values['service_provider_id'].'/')) {
                                mkdir('./uploads/driver/'.$values['service_provider_id'].'/');
                            }
            
                            $config = array(
                                'upload_path' => './uploads/driver/'.$values['service_provider_id'].'/',
                                'allowed_types' => 'pdf|png|jpeg|jpg',
                                'max_width' => '6000',
                                'max_height' => '5000'
                            );
                        }
                        else if($user_type == 7){
                            if (!file_exists('./uploads/care/'.$values['service_provider_id'].'/')) {
                                mkdir('./uploads/care/'.$values['service_provider_id'].'/');
                            }
            
                            $config = array(
                                'upload_path' => './uploads/care/'.$values['service_provider_id'].'/',
                                'allowed_types' => 'pdf|png|jpeg|jpg',
                                'max_width' => '6000',
                                'max_height' => '5000'
                            );
                        }
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config); 
                           $check_upload = $this->upload->do_upload($values['attribute_value']);
                       
                        if($check_upload) {

                            $update = $this->db->query("UPDATE ".TB_ATTRIBUTE_VALUES." SET attribute_value = '".$values['attribute_value']."', updated_at = '".$values['updated_at']."' WHERE service_provider_id = ".$values['service_provider_id']." AND attribute_id = ".$values['attribute_id']." ");
                        }   
                    } 
                }

            }

        $total = $total +1;
    }
    return $total;
}

// priya 
public function checkCatAndInsert($catArr)
{
    $total = 0;
    foreach($catArr as $values) {

        $query = $this->db->query("select * from ".TB_PROVIDER_PRICE." WHERE service_provider_id = ".$values['service_provider_id']." AND cat_id = ".$values['cat_id']." AND sub_cat_id = ".$values['sub_cat_id']." ");
        $result = $query->result();
           
            if(count($result) == '' || count($result) == 0) {

                $this->db->insert(''.TB_PROVIDER_PRICE.'', $values);
            } 
        $total = $total +1;
    }
    return $total;
}

// Ashwini code
function select_simple($sel,$table,$where)
{
    $this->db->select($sel, FALSE);
    $this->db->from($table);
    $this->db->where($where);
    $query = $this->db->get();
    return $query->result_array();
}

// Ashwini code
public function getMaster($sel,$table,$where = FALSE,$orderBy=array(),$join=array(),$joinType=array())
{
    $this->db->select($sel, FALSE);
    $this->db->from($table);
    if ($where) {
        $this->db->where($where, NULL, FALSE);
    }
    foreach($orderBy as $key => $val)
    {
        $this->db->order_by($key, $val);
    }
    foreach($join as $key => $val)
    {
        if(!empty($joinType) && $joinType[$key]!=""){
            $this->db->join($key, $val,$joinType[$key]);
        }else{
            $this->db->join($key, $val);    
        }
    }
    $query = $this->db->get();
    return $query->result_array();
}

//Priya
public function deleteMultiPass($whereArray, $groupId) {
    $this->db->query("UPDATE ".TB_GROUP_MEMBER." SET status = '0' , updated_at = NOW() WHERE passenger_id IN (".$whereArray.") AND group_id = ".$groupId." ");
    return $this->db->affected_rows();
}

//Priya
 function select_in($sel,$table,$id,$whereIn = array())
 {
    $this->db->select($sel, FALSE);
    $this->db->from($table);
    $this->db->where_in($id,$whereIn);
    $this->db->where('is_deleted', '0');
    $query = $this->db->get();        
    return $query->result_array();
}

public function get_message($sel,$table,$id = array(),$whereIn = array(),$orderBy = array(),$cond = array())
{
    $this->db->select($sel);
    $this->db->from($table);
    $c=array_combine($whereIn,$id);

    foreach ($cond AS $k => $v)
    {
        $this->db->where($k,$v);
    }

    foreach ($c AS $k => $v)
    {
        //$this->db->where($k,$v);
        $this->db->where_in($k,$v);
    }

    //exit();
    
    foreach($orderBy as $key => $val)
    {
        $this->db->order_by($key, $val);
    }
    //$this->db->order_by('created_date','ASC');
    $query = $this->db->get();
    $result = $query->result_array();



    return $result;

}


public function select_or($sel,$table,$cond= array(),$or_where=array()){
    $this->db->select($sel);
    $this->db->from($table);
    foreach ($cond AS $k => $v)
    {
         $this->db->where($k,$v);
    }
    foreach ($or_where AS $k => $v)
    {
         $this->db->or_where($k,$v);
    }
    $query = $this->db->get();
    return $query->result_array();
}

public function registerUser($data)
{
    $query = $this->db->query("CALL registerBuyer('".$data['user_type_id']."','".$data['user_fullname']."','".$data['organization_name']."','".$data['user_email']."','".$data['user_password']."','".$data['user_phone_number']."','".$data['registration_no']."','".$data['user_device_id']."','".$data['user_device_type']."','".$data['notification_status']."','".$data['new_record_status']."','".$data['user_status']."','".$data['is_block']."','".$data['is_deleted']."','".$data['created_at']."',@user_id)");

    $query = $this->db->query("select @user_id as id");
    return $query->result_array();
       
}

public function registerParent($data)
{
    $query = $this->db->query("CALL registerSupplier('".$data['sp_user_type']."','".$data['sp_fullname']."','".$data['sp_email']."','".$data['sp_password']."','".$data['sp_phone_number']."','".$data['sp_device_id']."','".$data['sp_device_type']."','".$data['notification_status']."','".$data['new_record_status']."','".$data['sp_status']."','".$data['is_block']."','".$data['is_deleted']."','".$data['created_at']."',@user_id)");

    $query = $this->db->query("select @user_id as id");
    return $query->result_array();
}

public function loginSupplier($email,$status,$password,$deviceType,$deviceId,$date)
{
    $query = $this->db->query("CALL loginSupplier('".$email."','".$status."','".$password."','".$deviceType."','".$deviceId."','".$date."',1)");
    $result = $query->result_array(); 

    //$query->free_result();
     //$db->next_result();

    return $query->result_array();
}

//Priya
public function deleteMultiPassByMember($whereArray, $cond) {

    $this->db->query("UPDATE ".TB_GROUP_MEMBER." SET is_deleted = '1' WHERE group_id IN (".$whereArray.") AND passenger_id IN (".$cond.") ");
    return $this->db->affected_rows();
}

//Priya
 function accept_reject_in($id,$whereIn = array(), $table,$data = array(),$where = array())
 {
    $this->db->set('book_request_status',false);
    $this->db->where_in($id,$whereIn);
    $this->db->where($where);
    $this->db->update(TB_BOOKING_REQUEST,$data);
    return $this->db->affected_rows();

    // $this->db->save_queries = TRUE;
    // $str = $this->db->last_query();
    // echo $str; 
 }

 //Priya
function selectGroupBy($sel,$table,$cond = array(),$group_by=array())
{
  $this->db->select($sel, FALSE);
  $this->db->from($table);
  foreach ($cond AS $k => $v)
  {
     $this->db->where($k,$v);
 }
 foreach($group_by as $key)
 {
     $this->db->group_by($key);
 }
 $query = $this->db->get();
 return $query->result_array();
}

//Priya 
// select date between two column
function select_between($sel,$table,$cond = array(),$cond1 = array())
{
      $this->db->select($sel, FALSE);
      $this->db->from($table);
      foreach ($cond AS $k => $v)
      {
         $this->db->where(' '.$k.'>=',$v);
      }
      foreach ($cond1 AS $k1 => $v1)
      {
        $this->db->where(' '.$k1.'<=',$v1);
      }
     $query = $this->db->get();
     return $query->result_array();
}


//Priya
 function cancelDetailBook($whereIn, $booking_id)
 {
    $this->db->query("UPDATE ".TB_BOOKING_DETAILS." SET is_deleted = '1', sub_book_status = '4' WHERE book_det_id IN (".$whereIn.") AND booking_id = ".$booking_id." ");
    return $this->db->affected_rows();
 }

//Priya
 function select_InData($sel,$table,$id,$whereIn = array())
 {
    $this->db->select($sel, FALSE);
    $this->db->from($table);
    $this->db->where_in($id,$whereIn);
    $query = $this->db->get();        
    return $query->result_array();
}

// Priya 
// to check id in array and remove that array record
function findkeyFromArray($id, $result) {
    foreach($result as $elementKey => $element) {
        if($element['service_provider_id'] == $id){
            unset($result[$elementKey]);
        } 
    }
    return $result;
}



}
?>
