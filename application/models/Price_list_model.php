<?php

/*
* @author : kiran N.
* description: manage the price of rides model
*/

defined('BASEPATH') OR exit('No direct script access allowed');
 
class Price_list_model extends CI_Model { 
    var $table = TB_RIDE_PRICE;
    var $column_order = array(null,'category','name', 'rp_cost'); //set column field database for datatable orderable
    var $column_search = array('category','name','rp_cost'); //set column field database for datatable searchable 
    var $order = array('rp_id' => 'desc'); // default order 
    //var $where_cond = array("tbl_ride_price.c_id=" => "tbl_categories.category_id","tbl_ride_price.sc_id="=>"tbl_subcategories.sub_category_id");
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {         
        $this->db->from($this->table); 
        $this->db->join(TB_CATEGORIES, 'tbl_ride_price.c_id  = tbl_categories.category_id','LEFT');
        $this->db->join(TB_SUBCATEGORIES, 'tbl_ride_price.sc_id  = tbl_subcategories.sub_category_id','LEFT');
        $i = 0;     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        if(isset($this->where_cond)) // here condition on list 
        {
            $where_cond = $this->where_cond;
            foreach ($where_cond as $key => $value) {
                $this->db->where($key,$value);
            }            
        }       
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();        
        return $query->result();
    }
 
    function count_filtered() // filter data of records
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all() // count all record
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
}