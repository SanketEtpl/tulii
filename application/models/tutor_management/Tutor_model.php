<?php 

/*
Author : Rajendra pawar 
Page :  Tutor_model.php
Description : Tutor model use for Tutor  managment module

*/

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Tutor_model extends CI_Model
{
    var $table = TB_USERS;
    var $TB_KIDS =  TB_KIDS;
    var $TB_tutor = TB_TUTORS;
    var $column_order = array(null, 'tutor_id','user_name','kid_name','kid_gender','kid_age','tutor_name','tutor_number','Start_date','end_date','total_day','total_price'); //set column field database for datatable orderable
    var $column_search = array('user_name','kid_name','kid_gender','kid_age','tutor_name','tutor_number','Start_date','end_date','total_day','total_price'); //set column field database for datatable searchable 
    var $order = array('tutor_id' => 'desc'); // default order 
    var $where_cond = array("tbl_kids.kid_status ="=>1,"tbl_tutors.isDeleted"=>0,"tbl_users.isDeleted"=>0);

   
    private function _get_datatables_query()
    {         
        $this->db->from($this->table); 
        $this->db->join(TB_KIDS, 'tbl_users.user_id = tbl_kids.user_id');
        $this->db->join(TB_TUTORS, 'tbl_kids.kid_id = tbl_tutors.kid_id');
        $i = 0;     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        if(isset($this->where_cond)) // here condition on list 
        {
            $where_cond = $this->where_cond;
            foreach ($where_cond as $key => $value) {
                $this->db->where($key,$value);
            }            
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
     function count_filtered() // filter data of records
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all() // count all record
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 function select_query($sel,$table,$cond = array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        $this->db->join(TB_KIDS, 'tbl_users.user_id = tbl_kids.user_id');
        $this->db->join(TB_TUTORS, 'tbl_kids.kid_id = tbl_tutors.kid_id');
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();//die;
        return $query->result_array();
    }
}
?>