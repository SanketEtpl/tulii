<?php

/*
* @author : kiran N.
* description: manage the tutors model
*/

defined('BASEPATH') OR exit('No direct script access allowed');
 
class Tutor_list_model extends CI_Model { 
    var $table = TB_TUTOR_MASTER;
    var $column_order = array(null,'user_name','category_name','subcategory_id'); //set column field database for datatable orderable
    var $column_search = array('user_name','category_name','subcategory_id'); //set column field database for datatable searchable 
    var $order = array('tbl_tutor_master.id' => 'desc'); // default order 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {         
        $this->db->select("tbl_tutor_master.id,user_name,category_name,subcategory_id,tbl_tutor_master.status");
        $this->db->from($this->table); 
        $this->db->join(TB_TUTOR_CATEGORY, 'tbl_tutor_master.category_id  = tbl_tutor_category.id',"LEFT");
        $this->db->join(TB_TUTOR_SUBCATEGORY, 'tbl_tutor_master.subcategory_id  = tbl_tutor_subcategory.id',"LEFT");
        $this->db->join(TB_USERS, 'tbl_tutor_master.user_id  = tbl_users.user_id',"LEFT");      
        
        $i = 0;     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }            
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();         
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();        
        return $query->result();
    }
 
    function count_filtered() // filter data of records
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all() // count all record
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function select_where_in($sel,$table,$cond)
    {        
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        $this->db->where("id IN (".$cond.")",NULL, false);
        $query = $this->db->get();
        return $query->result_array();
    }
 
}