<?php 

/*

Author : Rajendra pawar 
Page :  about_model.php
Description : About model use for about us functionality

*/


if(!defined('BASEPATH')) exit('No direct script access allowed');

class About_model extends CI_Model
{
    
    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     */


      function aboutUsData()
    {
        $this->db->select('about.about_id, about.about_message');
        $this->db->from('tbl_about as about');
       
        $this->db->where('about.isDeleted', 0);
        $this->db->limit(1);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    function updateAboutModel($aboutId, $aboutInfo)
    { 
        $this->db->where('about_id', $aboutId);
        $this->db->update('tbl_about', $aboutInfo);
        return $this->db->affected_rows();
    }
    
}

?>