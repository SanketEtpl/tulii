<?php

/*
* @author : kiran N.
* description: manage the faq model
*/

defined('BASEPATH') OR exit('No direct script access allowed');
 
class Faq_model extends CI_Model { 
    var $table = TB_FAQ;
    var $column_order = array(null,'faq_title','faq_sub_title','faq_description','category', 'name'); //set column field database for datatable orderable
    var $column_search = array('faq_title','faq_sub_title','faq_description','category','name'); //set column field database for datatable searchable 
    var $order = array('id' => 'desc'); // default order 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {         
        $this->db->select("id,faq_title,name,role,category,faq_sub_title,faq_description");
        $this->db->from($this->table); 
        $this->db->join(TB_ROLES,'tbl_faq.role_id=tbl_roles.roleId','Left');
        $this->db->join(TB_CATEGORIES, 'tbl_faq.category_id  = tbl_categories.category_id','Left');
        $this->db->join(TB_SUBCATEGORIES, 'tbl_faq.subcategory_id  = tbl_subcategories.sub_category_id','Left');
        $i = 0;     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }   
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();        
        return $query->result();
    }
 
    function count_filtered() // filter data of records
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all() // count all record
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
}