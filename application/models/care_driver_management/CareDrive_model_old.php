<?php 

/*
Author : Rajendra pawar 
Page :  careDrive_model.php
Description : Care Drive model use for care driver managment functionality

*/

if(!defined('BASEPATH')) exit('No direct script access allowed');

class careDrive_model extends CI_Model
{
function careDriverListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.user_id, BaseTbl.user_email, BaseTbl.user_name, BaseTbl.user_phone_number, BaseTbl.user_status');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.user_email  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_name  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_phone_number  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('Role.role =', 'Care Driver');
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function careDriverListing($searchText = '', $page, $segment)
    {
         $this->db->select('BaseTbl.user_id, BaseTbl.user_email, BaseTbl.user_name, BaseTbl.user_phone_number, BaseTbl.user_status');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.user_email  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_name  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_phone_number  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('Role.role =', 'Care Driver');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }    
   function select_exist($table,$user_id,$user_email)
    {
        $this->db->select('*', FALSE);
        $this->db->from($table);
        $this->db->where('user_email',$user_email);
        if($user_id != 0) { $this->db->where('user_id !=', $user_id); }
        
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        return $query->result_array();
    }
}
?>