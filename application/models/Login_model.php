<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    
    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     */
    function loginMe($email, $password)
    {
        // echo "<pre>";print_r($email);die;
        $this->db->select('BaseTbl.service_user_id, BaseTbl.user_password, BaseTbl.user_fullname, BaseTbl.user_type_id, Roles.role_name');
        $this->db->from('service_user_master_ut as BaseTbl');
        $this->db->join('role_ut as Roles','Roles.role_id = BaseTbl.user_type_id');
        $this->db->where('BaseTbl.user_email', $email);
        $this->db->where('BaseTbl.is_deleted', "0");
        $query = $this->db->get();
        $user = $query->result();
        if(!empty($user)){
            if(verifyHashedPassword($password, $user[0]->user_password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }


    /*function login_old($email, $password)
    {
        // echo "<pre>";print_r($email);die;
        $this->db->select('BaseTbl.user_id, BaseTbl.user_password, BaseTbl.user_name, BaseTbl.user_email, BaseTbl.roleId, BaseTbl.user_phone_number, BaseTbl.user_status');
        $this->db->from('tbl_users as BaseTbl');        
        $this->db->where('BaseTbl.user_email', $email);
        $this->db->where('BaseTbl.isDeleted', '0');
        $query = $this->db->get();
        $user = $query->result_array();
        if(!empty($user)){
            if(verifyHashedPassword($password, $user[0]['user_password'])){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }*/

    function login($email, $password,$user_type)
    {
        if($user_type == '2' || $user_type == '5' || $user_type == '6'){
            $this->db->select('service_user_id AS id, user_type_id, user_password, user_fullname, user_email, user_phone_number, user_status');
            $this->db->from(TB_SERVICE_USERS);        
            $this->db->where('user_email', $email);
        }
        else{
            $this->db->select('service_provider_id AS id,sp_fullname,sp_password,sp_email,sp_user_type,sp_status,sp_phone_number');
            $this->db->from(TB_SERVICE_PROVIDER); 
            $this->db->where('sp_email', $email);
        }
        $this->db->where('is_deleted', '0');
        $query = $this->db->get();
        $userData = $query->result_array();
        if($userData[0]['user_password']){
            $userpassword = $userData[0]['user_password'];
        }
        else{
            $userpassword = $userData[0]['sp_password'];
        }

        if(!empty($userData)){
            if(verifyHashedPassword($password, $userpassword)){
                return $userData;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    /**
     * This function used to check email exists or not
     * @param {string} $email : This is users email id
     * @return {boolean} $result : TRUE/FALSE
     */
    function checkEmailExist($email)
    {
        $this->db->select('user_id');
        $this->db->where('user_email', $email);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('tbl_users');
        //$result = $query->result_array();
       // echo $this->db->last_query();
        //print_r($query);
        //exit;
        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }


    /**
     * This function used to insert reset password data
     * @param {array} $data : This is reset password data
     * @return {boolean} $result : TRUE/FALSE
     */
    function resetPasswordUser($data)
    {
        $result = $this->db->insert('tbl_reset_password', $data);
       
        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This function is used to get customer information by email-id for forget password email
     * @param string $email : Email id of customer
     * @return object $result : Information of customer
     */
    function getCustomerInfoByEmail($email)
    {
        $this->db->select('user_id, user_email, user_name');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('user_email', $email);
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * This function used to check correct activation deatails for forget password.
     * @param string $email : Email id of user
     * @param string $activation_id : This is activation string
     */
    function checkActivationDetails($email, $activation_id)
    {
        $this->db->select('id');
        $this->db->from('tbl_reset_password');
        $this->db->where('email', $email);
        $this->db->where('activation_id', $activation_id);
        $query = $this->db->get();
        return $query->num_rows;
    }

    // This function used to create new password by reset link
    function createPasswordUser($email, $password)
    {
        $this->db->where('user_email', $email);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', array('password'=>getHashedPassword($password)));
        $this->db->delete('tbl_reset_password', array('email'=>$email));
    }

    /*
    Check is valid user Ram k
    */
     function validUser($table,$fields,$where=array())
    {
        $this->db->select($fields, FALSE);
        $this->db->from($table);
        foreach($where as $key => $val)
        {
            $this->db->where($key, $val);
        }
        
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        return $query->result_array();
    }
}

?>