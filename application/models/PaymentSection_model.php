<?php

/*
 * @author : kiran N.
 * description: manage the payment model
 */

defined('BASEPATH') OR exit('No direct script access allowed');
 
class PaymentSection_model extends CI_Model { 
    var $table = TB_PAYMENT;
    var $column_order = array(null, 'user_name', 'booking_pick_up_location', 'booking_drop_off_location', 'booking_start_date', 'booking_price', 'status', 'user_rating'); //set column field database for datatable orderable
    var $column_search = array('user_name', 'booking_pick_up_location', 'booking_drop_off_location', 'booking_start_date', 'booking_price', 'status', 'user_rating'); //set column field database for datatable searchable 
    var $order = array('tbl_payment.id' => 'desc'); // default order 
    var $where_cond = array("tbl_bookings.booking_status" => COMPLETED,"tbl_service_requests.service_request_status" => ACCEPT ,"tbl_users.isDeleted" => '0', "tbl_payment.status" => "pending");
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {         
        $this->db->select("id as payment_id, service_provider_user_id, user_name, booking_pick_up_location, booking_drop_off_location, booking_start_date, booking_price, status, user_rating");
        $this->db->from($this->table);         
        $this->db->join(TB_BOOKINGS, 'tbl_payment.booking_id = tbl_bookings.booking_id', 'Left');
        $this->db->join(TB_USERS, 'tbl_payment.user_id = tbl_users.user_id', 'Left');
        $this->db->join(TB_SERVICE_REQUESTS, 'tbl_bookings.booking_id  = tbl_service_requests.service_booking_id AND tbl_users.user_id  = tbl_service_requests.service_provider_user_id', 'Left'); 
        
        $i = 0;     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }   

        if(isset($this->where_cond)) // here condition on list 
        {
            $where_cond = $this->where_cond;
            foreach ($where_cond as $key => $value) {
                $this->db->where($key,$value);
            }            
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();        
        return $query->result();
    }
 
    function count_filtered() // filter data of records
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all() // count all record
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
}