<?php

/*
 * @author : Kiran.
 * description: manage the organization model
 */

defined('BASEPATH') OR exit('No direct script access allowed');
 
class Pagination_models extends CI_Model { 
    var $table = TB_STUDENT;
    var $column_order = array(null, 'stud_username', 'parent_email','parent_phone','stud_address'); //set column field database for datatable orderable
    var $column_search = array('stud_username', 'parent_email','parent_phone','stud_address'); //set column field database for datatable searchable 
    var $order = array('stud_id' => 'desc'); // default order 
    
    var $where_cond = array();
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query($orgId)
    {
        $this->db->select("stud_id,stud_username, parent_email, parent_phone, stud_address");
        $this->db->from($this->table);       
        $this->db->Where("user_id",$orgId);
        $i = 0;     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }   

     /*   if(isset($this->where_cond)) // here condition on list 
        {
            $where_cond = $this->where_cond;
            foreach ($where_cond as $key => $value) {
                $this->db->where($key,$value);
            }            
        }
*/    }
 
    function get_datatables($orgId)
    {
        $this->_get_datatables_query($orgId);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();        
        return $query->result();
    }
 
    function count_filtered($orgId) // filter data of records
    {
        $this->_get_datatables_query($orgId);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($orgId) // count all record
    {
        $this->db->from($this->table);
        $this->db->Where("user_id",$orgId);
        return $this->db->count_all_results();
    } 
}