<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Input extends CI_Input {

    public function raw_post() {
        return file_get_contents('php://input');
    }

    public function post($index = NULL, $xss_clean = FALSE) {
        $content_type = $this->get_request_header('Content-type');

        if (stripos($content_type, 'application/json') !== FALSE
            && ($postdata = $this->raw_post())
            && in_array($postdata[0], array('{', '['))) {

            $decoded_postdata = json_decode($postdata, true);
            if ((json_last_error() == JSON_ERROR_NONE))
                $_POST = $decoded_postdata;
        }

        return parent::post($index, $xss_clean);
    }
}