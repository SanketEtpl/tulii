<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Common class for common functions
 */
class Common {

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->library('encrypt');
        $this->CI->load->model('common_model');
    }

    /**
     * upload_image
     * @param type $FILES
     * @return type
     */
    function upload_image($FILES, $uploaddir = './uploads/profileimages/') {
        $name = $FILES['name'];
        $size = $FILES['size'];
        $tmp = $FILES['tmp_name'];
        $ext = $this->getExtension($name);
        //$compress_image_name = "";
        if (strlen($name) > 0) {
            $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");
            if (in_array($ext, $valid_formats)) {
                $file_name = underscore($name);
                $path = $name;
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $user_img = rand() . '_profile.' . $ext;
                $uploadfile = $uploaddir . $user_img;
                $maxsize = (1024 * 1024) * 2;
                if ($size < ($maxsize)) {
                    //Upload profile image
                    if (move_uploaded_file($tmp, $uploadfile)) {
                        return array("success" => 1, "msg" => "File uploaded successfully.", 'filename' => $user_img);
                    } else {
                        return array("success" => 0, "msg" => "Some error has been occurred, please try again!!!");
                    }
                } else {
                    return array("success" => 0, "msg" => "You can not upload more that 2 Mb");
                }
            } else {
                return array("success" => 0, "msg" => "Invalid file, please upload image file.");
            }
        } else {
            return array("success" => 0, "msg" => "Please select image file.");
        }
    }

    function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }

        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return strtolower($ext);
    }

    /**
     * validate_category
     */
    function validate_category($post) {
        $response['status'] = false;
        //Check category has subcategory 
        $category = $this->CI->common_model->select("category_id",TB_CATEGORIES,array('category_id'=>$post['category'],'status' => 1));
        if (!$category) {
            return $this->CI->response(array('status' => false, 'message' => 'Invalid Category!!!'), 300);
        } else {
            $response['status'] = true;
            $response['category_id'] =  ($category[0]['category_id']) ? $category[0]['category_id'] :0;
        }
        $cond = array();
        $jointype=array(TB_CATEGORIES=>"INNER");
        $join = array(TB_CATEGORIES=>"tbl_categories.category_id = tbl_subcategories.category_id");
        $subcats = $this->CI->common_model->selectJoin("sub_category_id,name",TB_SUBCATEGORIES,$cond,array(),$join,$jointype);
        $sub_category_ids = [];
        foreach ($subcats as $subcat){
            $sub_category_id['sub_category_id']  = $subcat['sub_category_id'];
            $sub_category_id['name']  = $subcat['name'];
            $sub_category_ids[] = $sub_category_id;
        }
        if ($post['sub_category'] && empty($sub_category_ids)) {
            return $this->CI->response(array('status' => false, 'message' => 'Subcategory not exist for this category!!!'), 300);
        }
        if (!$post['sub_category'] && !empty($sub_category_ids)) {
            return $this->CI->response(array('status' => false, 'message' => 'Category needs subcategory!!!'), 300);
        }
        if ($post['sub_category'] && !empty($sub_category_ids)) {
            $subcategory = $this->CI->common_model->select("sub_category_id",TB_SUBCATEGORIES,array("sub_category_id"=>trim($post['sub_category']),'status' => 1));
            if (!$subcategory) {
                return $this->CI->response(array('status' => false, 'message' => 'Invalid subcategory for entered category!!!'), 300);
            } else {
                $response['status'] = true;
                $response['sub_category_id'] = $subcategory[0]['sub_category_id'];
            }
        }
        return $response;
    }

}
