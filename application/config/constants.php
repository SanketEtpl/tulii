<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
/**** USER DEFINED CONSTANTS **********/

//define('ROLE_ADMIN',                            '1');
//define('ROLE_MANAGER',                         	'2');
//define('ROLE_SERVICE_PROVIDER',                 '3');

define('ROLE_ADMIN',                            1);
define('ROLE_PARENTS',                         	2);
define('ROLE_SERVICE_PROVIDER',                 3);
define('ROLE_CARE_DRIVER', 						3);
define('ROLE_TUTOR', 							4);
define('SCHOOL', 								5);
define('ORGANIZATION', 							6);
define('RIDE',									1);
define('CARE',									2);
define('RIDE_AND_CARE',							3);
define('CARPOOLING',							4);
define('SINGLE_RIDES', 							1);


define('MULTIPLE_RIDES_WITH_SAME_DESTINATION',2);
define('MULTIPLE_RIDES_WITH_DIFFERENT_DESTINATION',3);

define('SEGMENT',								2);
define('SEGMENT1',								3); //Rajendra pawar
/************************** EMAIL CONSTANTS *****************************/

define('EMAIL_FROM',                            'admin@exceptionaire.co');		// e.g. email@example.com
define('EMAIL_BCC',                            	'ram.k@exceptionaire.co');		// e.g. email@example.com
define('FROM_NAME',                             'Tulii Admin');	// Your system name
define('EMAIL_PASS',                            'Your email password');	// Your email password
define('PROTOCOL',                             	'smtp');				// mail, sendmail, smtp
define('SMTP_HOST',                             'Your smtp host');		// your smtp host e.g. smtp.gmail.com
define('SMTP_PORT',                             '25');					// your smtp port e.g. 25, 587
define('SMTP_USER',                             'Your smtp user');		// your smtp user
define('SMTP_PASS',                             'Your smtp password');	// your smtp password
define('MAIL_PATH',                             '/usr/sbin/sendmail');

/* ------- Role name start---------- */
//define("USER_TYPE_PARENT","Parents"); //Added by Rajendra
//define("USER_TYPE_SERVICE_PROVIDER","Service provider");
/* ------- Role name  end---------- */

/* ------- database tables ---------- */
define("TB_USERS","tbl_users"); //Added by Ram K => For user data
define("TB_KIDS","tbl_kids");
define("TB_SERVICE_REQUESTS","tbl_service_requests");
define("TB_BOOKINGS","tbl_bookings");
//define("TB_ROLES", "tbl_roles");
define("TB_SUBJECTS","tbl_subjects");
define("TB_TEACHERS","tbl_teachers");
define("TB_SUBJECT_MASTER","tbl_subject_master");
define("TB_TUTOR_SUBJECTS","tbl_tutor_subjects");
define('TB_GROUP', 'tbl_group');
//define('TB_PAYMENT','tbl_payment');
define("TB_CONTACT","contact_ut");

// define("TB_role","tbl_roles");//Added by Rajendra
// define("TB_Contact","tbl_contact");
// define("TB_tutor","tbl_tutors");
// define("TB_enquiry","tbl_enquiry");
// define("TB_categorie","tbl_categories");
// define("TB_subcategorie","tbl_subcategories");

define("TB_RATING","tbl_rating");
//define("TB_CONTACT","tbl_contact");
define("TB_TUTORS","tbl_tutor");
define("TB_ENQUIRY","tbl_enquiry");
define("TB_UNAVAILABILITY","tbl_unavailability");

define("SERVICE_SINGLE_RIDES","Single Rides");
define("SERVICE_MULTIPLE_RIDE_WITH_SAME_DESTINATION","Multiple Rides With Same Destination");
define("SERVICE_SINGLE_RIDES_WITH_DIFFERENT_DESTINATION","Multiple Rides With Different Destination");
define("SERVICE_CHILD_CAR_POOLING","Child Car Pooling");
define("SERVICE_CHILD_CARE","Single Child Care");
define("SERVICE_CARE_DRIVER","Multiple Siblings Care");
define("SERVICE_MY_CHILDS_CARE_DRIVER","Single Child Ride & Care");
define("SERVICE_MY_CHILDRENS_CARE_DRIVER","Multiple Siblings Ride & Care");
define("SERVICE_MY_CHILD_TUTOR","My Child Tutor");


define("TB_SERVICE_PROVIDER_HISTORY","tbl_service_provider_history"); // Added by Kiran 
define("TB_RIDE_PRICE","tbl_ride_price");
define("TB_PRICE_MASTER","tbl_price_master");

define("TB_CATEGORIES","tbl_categories");
define("TB_SUBCATEGORIES","tbl_subcategories");
define("TB_RESOLUTION_CENTER","tbl_resolution_center");
//define("TB_BANNER", "tbl_banner");
define("TB_KIDS_BOOKING", "tbl_kids_booking");
//define("TB_FAQ","tbl_faq");
define("TB_VIDEO_STREAM","video_streaming_ut");
define("TB_STUDENT","tbl_students");
// define("TB_CAR_CATEGORY","tbl_car_categoty");

//define('USER_PROFILE_IMG', 'uploads/');
define('USER_PROFILE_IMG', 'uploads/user_profile/');
define('CERTIFICATE_IMG_PATH', 'uploads/certificate/');
define('LICENSE_IMG_PATH', 'uploads/license/');
define('CAR_PIC_IMG_PATH', 'uploads/car_pic/');
define('CAR_NUMBER_PLATE_IMG_PATH', 'uploads/car_number_plate_pic/');
DEFINE('VIDEO_STREAM','video/');
/***************booking_service_type start***********************/
$booking_service_type[1] = 'place_a_request';
$booking_service_type[2] = 'my_preference';
$booking_service_type[3] = 'my_matches';
define("SERVICE_TYPES", json_encode($booking_service_type));
define("PLACE_REQUEST","place_a_request");
define("MY_PREFERENCE","my_preference");
define("MY_MATCHES","my_matches");
/***************booking_service_type end***********************/
/***************user_type start***********************/
$user_types[1] = 'parent';
$user_types[2] = 'service_provider';
define("USER_TYPE_PARENT", 2);
define("USER_TYPE_SERVICE_PROVIDER", 3);
define("USER_TYPES", json_encode($user_types));
/***************user_type end***********************/
// Added Kiran N.
define("DAILY","daily");
define("WEEKLY","weekly");
define("MONTHLY","monthly");

/***************service_request_status start***********************/
define("CANCELLED", 'cancelled');
define("AVAILABLE",'available');
define("NOT_AVAILABLE", 'not_available');
define("START", 'start');
define("END", 'end');
/***************service_request_status end***********************/

/***************booking_status start***********************/
define("PENDING",'pending');
define("IN_PROCESS", 'in_process');
define("PAID","paid");
define("ONHOLD","onhold");
define("ACCEPTED", 'accepted');
define("ACCEPT","accept");
define("COMPLETED", 'completed');
define("TB_DRIVER_LOCATION","tbl_driver_location");
define("TB_MY_PREFERENCE","tbl_my_preference");

// define("ONSIGNALKEY","eba166b2-02ab-4911-b815-29a27022faee");
define("ONSIGNALKEYPARENT","e79a3495-64f8-432e-af8c-14a08e825136");
define("ONSIGNALKEYSP","d201e1c3-c6f5-4afb-917e-aab707163ab4");
define("GOOGLE_API_KEY","AIzaSyBD5NeyT_tKxswCM6YHXRXZCDbb7NDIuko");

define("TB_DEMO","tbl_demo");
define("TB_DEGREE","tbl_degree");
define("TB_SPECILIZATION","tbl_specilization");
define("TB_VEHICLES", "tbl_vehicles");
define("TB_PARENT_LOCATION","tbl_parent_location");
define('SMS_API_KEY', 'f676c0e6-e63b-11e7-a328-0200cd936042');
define("AWS_VIDEO_URL", "http://rrewmsexceptionaire.s3.amazonaws.com/");
define("awsSecretKey", "GyDD9yvgXKls/JfAfvibveJ5yUPB4Uwk2LnL2UmP");
define("awsAccessKey", "AKIAIWS53ORT2EPXTKVQ");
define("BUCKET", "rrewmsexceptionaire");

define('STRIPE_SECRET_KEY', 'sk_test_pCPNKY3zT06WVxwqH5VBvETE');
// define("TB_TUTOR_SUBCATEGORY", "tbl_tutor_subcategory");
define("TB_TUTOR_MASTER", "tbl_tutor_master");
// define("TB_TUTOR_CATEGORY", "tbl_tutor_category");

//---- Ashwini changes here ---
//define('TB_GROUP_SHIFT', 'tbl_group_shift');
//define('TB_TEMPORARY_GROUP', 'tbl_temporary_group');
define('TB_TUTOR_STANDARD', 'tbl_tutor_standard');
define('TB_CAR_CATEGORY', 'tbl_car_category');
//define('TB_BOOKING', 'tbl_booking');
//define('TB_RIDE_BOOKING', 'tbl_ride_booking');
//define('TB_CARE_BOOKING', 'tbl_care_booking');
define('TB_TUTION_BOOKING', 'tbl_tution_booking');
define('TB_SUB_BOOKING_DATES', 'tbl_sub_booking_dates');
//define('TB_RIDE_BOOKING_NEW', 'tbl_ride_booking_new');
//define('TB_CHAT', 'tbl_chat');
//define('TB_CARE_BOOKING_NEW', 'tbl_care_booking_new');
define('TB_BOOKING_NEW', 'tbl_booking_new');
define("TB_DRIVER","tbl_driver");
define("TB_BOOKING_LAT_LNG", "tbl_booking_lat_lng");
define("TB_USER_FEEDBACK","tbl_user_feedback");
define("TB_TUTOR","tbl_tutor");
//define("TB_SERVICE_MASTER","tbl_service_master");
//define("TB_PASSENGER", "tbl_passenger");
define("TB_SERVICE_USERS","service_user_master_ut"); //user table ie. parent,school,organization
define("TB_SERVICE_PROVIDER","service_provider_ut"); //SP table ie. driver, tutor
define("TB_ATTRIBUTE_MAPPING","service_attribute_mapping_ut");
define("TB_ATTRIBUTE_MASTER","service_provider_attribute_master_ut");
define("TB_ATTRIBUTE_VALUES","service_provider_profile_ut");
define("TB_PROVIDER_PRICE","service_price_structure_ut");
define("TB_TUTOR_CATEGORY","tutor_category_ut");
define("TB_TUTOR_SUBCATEGORY","tutor_subcategory_ut");
define("TB_SERVICE_PROVIDER_VEHICLE", "service_provider_vehicle_ut");
define('TB_CHAT', 'chat_ut');

define("TB_GROUP_SHIFT","group_shift_ut");
define("TB_FAQ", "faq_ut");
define("TB_SERVICE_MASTER", "service_master_ut");
define("TEMPORARY",'1');

define("TB_GROUP_MASTER","group_master_ut");
define("TB_GROUP_MEMBER","group_member_ut");
define("TB_TEMPORARY_GROUP","temporary_group_ut");
define("TB_PASSENGER", "passenger_master_ut");
define("TB_BOOKING", "booking_master_ut");
define("TB_TUTOR_BOOKING", "tutor_booking_ut");
define("TB_BOOKING_DETAILS", "booking_details_ut");
define("TB_BOOKING_REQUEST", "booking_request_ut");
define("TB_SP_UNAVAILABILITY", "service_provider_unavailability_ut");
define("TB_ROLES", "role_ut"); //all user type
define("TB_NOTIFICATION","notification_ut");
define("TB_ABOUT","about_ut");
define("TB_TERMS","terms_ut");
define("TB_RIDE", "ride_booking_ut");
define("TB_VEHICLE_CAT", "vehicle_category_ut");
define("TB_RATE_TYPE", "rating_type_ut");
define("TB_RATE_MASTER", "rating_ut");
define("TB_RATE_DETAILS", "rating_details_ut");
define("TB_TICKET","ticket_ut");
define("TB_CARE_BOOKING", "care_booking_ut");
define("TB_RIDE_BOOKING", "ride_booking_ut");
define("TB_PAYMENT","booking_payment_ut");
define('TB_LIVE_LOCATION', 'live_location_ut');
define('TB_SP_RATE', 'service_provider_rate_ut');
define('TB_DRIVER_LATLONG', 'driver_latlong_ut');
define('CHAPERONE', 7);
//define("TB_SERVICE_PROVIDER_PROFILE", "service_provider_profile_ut");
define("SERVICE_PROVIDER_DRIVER", "uploads/driver");
define("SERVICE_PROVIDER_TUTOR", "uploads/tutor");
define("TB_BANNER", "banner_ut");
define("BUYER_KEY", "AIzaSyDbZFt1f49j2xfRWGxlpJCgVGQFLeOH7gQ");
//define("PROVIDER_KEY", "AIzaSyDHY6kKeSGWgXWuWR_4ouOODasNZZaPFzA");
define("PROVIDER_KEY", "AIzaSyDbZFt1f49j2xfRWGxlpJCgVGQFLeOH7gQ");
define('STANDARD', serialize(array(
	"1"=>"1st",
	"2"=>"2nd",
	"3"=>"3rd",
	"4"=>"4th",
	"5"=>"5th",
	"6"=>"6th",
	"7"=>"7th",
	"8"=>"8th",
	"9"=>"9th",
	"10"=>"10th"
	)));
