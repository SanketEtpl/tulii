<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// $route['default_controller'] = 'welcome';
// $route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['default_controller'] = "login";
//$route['404_override'] = 'error';
$route['404_override'] = 'my404/index';


/*********** USER DEFINED ROUTES *******************/

$route['loginMe'] = 'login/loginMe';
$route['dashboard'] = 'user';
$route['logout'] = 'user/logout';

$route['service-user'] = 'user_management/ServiceUserController'; 
$route['service-provider'] = "user_management/ServiceProviderController";
$route['about-us'] = "general_management/AboutUs_controller";
$route['terms-and-condition'] = "general_management/Terms_controller";
$route['contact-us'] = "general_management/ContactUs_controller";
$route['rating'] = 'general_management/Rating_controller';
$route['booking-schedule'] = "booking_management/Booking_schedule";
$route['booking-ongoing'] = "booking_management/Booking_ongoing";
$route['booking-complete'] = "booking_management/Booking_complete";
$route['booking-complaints'] = "booking_management/Booking_complaints";
$route['booking-cancelled'] = "booking_management/Booking_cancelled";
$route['group-listing'] = "group_management/Group";
$route['banner'] = "banner_management/Banner";
$route['tutor-category'] = "tutor_management/Tutor_category";
$route['tutor-sub-category'] = "tutor_management/Tutor_subcategory";
$route['faq'] = "faq_management/Faq_controller";
$route['tickets'] = "ticket_management/Ticket";
$route['notification-list'] = "notification_management/Notification_list";
$route['manage-user'] = "notification_management/Manage_user";
/*$route['userListing'] = 'user/userListing';
$route['userListing/(:num)'] = "user/userListing/$1";
$route['addNew'] = "user/addNew";

$route['addNewUser'] = "user/addNewUser";
$route['editOld'] = "user/editOld";
$route['editOld/(:num)'] = "user/editOld/$1";
$route['editUser'] = "user/editUser";
$route['deleteUser'] = "user/deleteUser";*/
$route['loadChangePass'] = "user/loadChangePass";
$route['changePassword'] = "user/changePassword";
$route['pageNotFound'] = "user/pageNotFound";
$route['checkEmailExists'] = "user/checkEmailExists";

$route['forgotPassword'] = "login/forgotPassword";
$route['resetPasswordUser'] = "login/resetPasswordUser";
$route['resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
//$route['resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
$route['resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
$route['createPasswordUser'] = "login/createPasswordUser";
// Added by Rajendra pawar for Genenaral management module start 
/*$route['contactUs'] = 'general_management/ContactUs_controller';
$route['contactUs/(:any)'] = "general_management/ContactUs_controller/$1";
$route['contactUs/(:any)/(:any)'] = "general_management/ContactUs_controller/$1/$2";
$route['aboutUs'] = 'general_management/AboutUs_controller';
$route['aboutUs/(:any)'] = "general_management/AboutUs_controller/$1";
$route['terms'] = 'general_management/Terms_controller';
$route['terms/(:any)'] = "general_management/Terms_controller/$1";*/
/*$route['rating'] = 'general_management/Rating_controller';
$route['rating/(:any)'] = "general_management/Rating_controller/$1";
$route['rating/(:any)/(:any)'] = "general_management/Rating_controller/$1/$2";*/
// Added by Rajendra pawar for Genenaral management module end 

// Added by Rajendra pawar for care driver management module start 
/*$route['careDriver'] = 'care_driver_management/CareDriver_controller';
$route['careDriver/(:any)'] = "care_driver_management/CareDriver_controller/$1";
$route['careDriver/(:any)/(:any)'] = "care_driver_management/CareDriver_controller/$1/$2";*/
// Added by Rajendra pawar for care driver management module end 
/*
* @author: added by kiran n.
*/
$route['userList'] = 'user_management/UserController'; 
$route['parent-profile-data'] = "parent_management/ParentdetailsController";
$route['checkemailAvailability'] = "parent_management/ParentdetailsController/check_email_availability";
$route['serviceProvider'] = "service_provider/ServiceproviderController";
$route['childcare'] = "childcare_provider/Childcare";
$route['service-care-driver'] = "service_management/Service_care_driver";
$route['service-multisibling'] = "service_management/Service_multisibling";
$route['service-kids-youth'] = "service_management/Service_kids_and_youth";
$route['service-car-pool'] = "service_management/Service_car_pool";
$route['service-after-school-care'] = "service_management/Service_after_school_care";
$route['service-tutoring'] = "service_management/Service_tutoring";
$route['price-list'] = "price_management/Price_list";
/*$route['notification-list'] = "notification_management/Notification_list";
$route['manage-user'] = "notification_management/Manage_user";*/
$route['resolution-center'] = "resolution_center/Resolution";
$route['slider-list'] = "slider_management/Slider";
//$route['faq'] = 'FAQ/Faq_controller';
$route['subject-master'] = "subject_management/Subject";
$route['video-list'] = "video_streaming/Video_list";
$route['video-list/(:any)'] = "video_streaming/Video_list/$1";
/*
* @author: added by kiran n.
*/

// Added by Rajendra pawar for Tutor management start 
/*$route['tutorList'] = 'tutor_management/TutorList_controller';
$route['tutorList/(:any)'] = "tutor_management/TutorList_controller/$1";
$route['tutorList/(:any)/(:any)'] = "tutor_management/TutorList_controller/$1/$2";*/
// Added by Rajendra pawar for Tutor management end 

// Added by Rajendra pawar for enquiry management start 
/*$route['enquiryList'] = 'enquiry_management/EnquiryList_controller';
$route['enquiryList/(:any)'] = "enquiry_management/EnquiryList_controller/$1";
$route['enquiryList/(:any)/(:any)'] = "enquiry_management/EnquiryList_controller/$1/$2";*/
// Added by Rajendra pawar for enquiry management end 

// Added by Rajendra pawar for booking management start 
/*$route['bookingList'] = 'booking_management/BookingList_controller';
$route['bookingList/(:any)'] = "booking_management/BookingList_controller/$1";
$route['bookingList/(:any)/(:any)'] = "booking_management/BookingList_controller/$1/$2";*/
// Added by Rajendra pawar for booking management end 

// Added by Rajendra pawar for service provider management start 
/*$route['service-provider'] = 'service_provider/Service_controller';
$route['service-provider/(:any)'] = "service_provider/Service_controller/$1";
$route['service-provider/(:any)/(:any)'] = "service_provider/Service_controller/$1/$2";*/
// Added by Rajendra pawar for service provider management end 

//Added by kiran nyalpelli frontend routes
$route['police-verify'] = "police_verification/Police_verify_doc";

/*$route['home'] = "frontend/Home_controller";
$route['about'] = "frontend/Home_controller/about";
$route['services'] = "frontend/Home_controller/services";
$route['safety'] = "frontend/Home_controller/safety";
$route['contact'] = "frontend/Home_controller/contact";
$route['career'] = "frontend/Home_controller/career";
$route['pricing'] = "frontend/Home_controller/pricing";
$route['faqs'] = "frontend/Home_controller/faq";
$route['videos'] = "frontend/Home_controller/videos";
$route['download'] = "frontend/Home_controller/download";*/

/*=============================== Kiran n (06-06-18) ==============================*/
$route['care-driver'] = "care_driver_management/Care";
$route['ride-and-care'] = "care_driver_management/Ride_and_care";
$route['single-ride'] = "care_driver_management/Single_ride";
$route['tutor'] = "tutor_management/Tutor";
$route['care-booking'] = "booking_management/Care_booking";
$route['ride-and-care-booking'] = "booking_management/Ridecare_booking";
$route['single-ride-booking'] = "booking_management/Singleride_booking";
$route['payment-onhold'] = "payment_management/Payment_onhold";
$route['payment-section'] = "payment_management/Payment_section";
$route['payment-history'] = "payment_management/Payment_history";
$route['group'] = "group_management/Group";
$route['car'] = "car_management/Car";
$route['tutor-list'] = "tutor_list/Tutor_list";
$route['school'] = "school_management/School_controller";
$route['organization'] = "organization_management/Organization_controller";
$route['current-booking-ride-map'] = "map_management/Booking_ride_map";
$route['driver-current-location'] = "map_management/Driver_location_map";
$route['feedback'] = "feedback_management/Feedback";