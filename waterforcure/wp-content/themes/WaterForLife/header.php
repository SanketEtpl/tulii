<!DOCTYPE html>
<html lang="en">
<head>
<title>Home</title>
<meta charset="utf-8">    
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/img/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<meta name = "format-detection" content = "telephone=no" />
<!--CSS-->
<link rel="stylesheet" href="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/css/bootstrap.css" >
<link rel="stylesheet" href="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/style.css">
<link rel="stylesheet" href="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/css/camera.css">
<link rel="stylesheet" href="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/fonts/font-awesome.css">
<!--JS-->
<script src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/js/jquery.js"></script>
<script src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/js/jquery-migrate-1.2.1.min.js"></script>
<script src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/js/superfish.js"></script>
<script src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/js/jquery.easing.1.3.js"></script>
<script src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/js/jquery.mobilemenu.js"></script>
<script src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/js/jquery.ui.totop.js"></script>
<script src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/js/jquery.equalheights.js"></script>
<script src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/js/camera.js"></script>
<script>
    $(document).ready(function(){
        jQuery('.camera_wrap').camera();
    });
</script>
<!--[if (gt IE 9)|!(IE)]><!-->
      <script src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/js/jquery.mobile.customized.min.js"></script>
<!--<![endif]-->
<!--[if lt IE 9]>
    <div style='text-align:center'><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div>  
  <![endif]-->
  <!--[if lt IE 9]><script src="../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<!--header-->
<header> 
    <div class="container" id="top"> 

<div class="header-top">
  <ul class="contact-details">
    <li class="emails-pointer"><a href="tel:0000000000"><i class="fa fa-phone"></i> : 0000000000 </a></li>
    <li class="emails-pointer"><a href="#"><i class="fa fa-envelope-o"></i> : test@gmail.com</a></li>
    <li><a href="javascript:void(0);">Mon - Sun : 9AM - 6PM EST </a></li>
    <li class="set-appointment"><a href="#" data-toggle="modal" data-target="#call-back">Set an Appointment</a></li>
  </ul>
</div>


        <h1 class="navbar-brand navbar-brand_"><a href="index.html"><img src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/img/12.png" alt=""><span>company name</span></a></h1>
        <nav class="navbar navbar-default navbar-static-top tm_navbar clearfix" role="navigation">
            <ul class="nav sf-menu clearfix">
               
                <li class="sub-menu"><a href="#">BLOG</a></li>
					<li><a href="<?php echo site_url(); ?>/about">ABOUT US</a></li>
                                        <li><a href="#">CONTACT</a></li>
        				<li><a href="#">REFERRAL BONUS</a></li>
                    <ul class="submenu">
        				
					
                          <?php /*<ul class="submenu">
                                <li><a href="#">Lorem ipsum</a></li>
                                <li><a href="#">Dolor sit amet</a></li>
                                <li><a href="#">Conse ctetur</a></li>
                                <li><a href="#">Dipisicing</a></li>
                                <li><a href="#">Eeliteiusmod</a></li>
                          </ul>  */?>
                        </li>
        				
                        
        			</ul>
                </li>
                </ul>
        </nav>
    </div>

    <div class="quick-connect-form">
        <a class="connect">Quick Connect</a>
        <div class="connect-form">
        <?php
            echo do_shortcode( ' [contact-form-7 id="14" title="contact"] ' );
        ?>
        </div>    
    </div>
</header>


<div class="modal fade" id="call-back" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Schedule a call back</h4>
        </div>
        <div class="modal-body call-back">      
        <?php
            echo do_shortcode( ' [contact-form-7 id="7" title="Appointment"] ' );
         ?>
        </div>
      </div>
      
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function(){
      $('.connect').click(function(){
          $('.connect-form').toggleClass('display');
          $('.connect').toggleClass('dist');
      });
    });    
  </script>
